﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ProjectGemini.Graphics_Utilities
{
    public class SpriteAnimation : SpriteManager
    {
        private float timeElapsed;

        public SpriteAnimation(Texture2D Texture, int frames, int animations)
            : base(Texture, frames, animations)
        {
        }

        public SpriteAnimation(Texture2D Texture)
            : base(Texture)
        {
        }

        public void Update(GameTime gameTime)
        {
            timeElapsed += (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (timeElapsed > Animations[Animation].TimeToUpdate)
            {
                timeElapsed -= Animations[Animation].TimeToUpdate;

                if (FrameIndex < Animations[Animation].Frames - 1)
                    FrameIndex++;
                else if (Animations[Animation].IsLooping)
                    FrameIndex = 0;
                else
                    AnimationFinished = true;
            }
        }
    }
}
