﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ProjectGemini.Graphics_Utilities
{
    public abstract class SpriteManager
    {
        protected Texture2D Texture;
        public Vector2 Position = Vector2.Zero;
        protected Dictionary<string, AnimationClass> Animations =
            new Dictionary<string, AnimationClass>();
        protected int FrameIndex = 0;
        protected Vector2 Origin;
        public bool Active;
        public bool AnimationFinished;

        private int height;
        private int width;
        public int Height { get { return height; } }
        public int Width { get { return width; } }

        private string animation;
        public string Animation
        {
            get { return animation; }
            set
            {
                animation = value;
                FrameIndex = 0;
                AnimationFinished = false;
            }
        }
        public string AnimationWithRetainedFrameIndex
        {
            get { return animation; }
            set
            {
                animation = value;
                AnimationFinished = false;
            }
        }

        public SpriteManager(Texture2D Texture, int frames, int animations)
        {
            this.Texture = Texture;
            width = Texture.Width / frames;
            height = Texture.Height / animations;
            Origin = new Vector2(width / 2, height / 2);

            // Sets to active by default.
            Active = true;
            AnimationFinished = false;
        }

        public SpriteManager(Texture2D Texture)
        {
            this.Texture = Texture;
            width = Texture.Width;
            height = Texture.Height;
            Origin = new Vector2(width / 2, height / 2);

            AnimationClass ani = new AnimationClass();
            this.AddAnimation("static", 1, 1, ani.Copy());
            this.Animation = "static";
            Active = true;
            AnimationFinished = false;
        }

        public void AddAnimation(string name, int row,
            int frames, AnimationClass animation)
        {
            Rectangle[] recs = new Rectangle[frames];
            for (int i = 0; i < frames; i++)
            {
                recs[i] = new Rectangle(i * width,
                    (row - 1) * height, width, height);
            }
            animation.Frames = frames;
            animation.Rectangles = recs;
            Animations.Add(name, animation);
        }

        public void AddAnimation(string name, int row, int offset, int frames, AnimationClass animation)
        {
            Rectangle[] recs = new Rectangle[frames];
            for (int i = 0; i < frames; i++)
            {
                recs[i] = new Rectangle((i + offset) * width, (row - 1) * height, width, height);
            }
            animation.Frames = frames;
            animation.Rectangles = recs;
            Animations.Add(name, animation);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (Active)
            {
                spriteBatch.Draw(Texture, Position,
                    Animations[Animation].Rectangles[FrameIndex],
                    Animations[Animation].Color,
                    Animations[Animation].Rotation, Origin,
                    Animations[Animation].Scale,
                    Animations[Animation].SpriteEffect, 0f);
            }
        }

        public void Draw(SpriteBatch spriteBatch, Color color)
        {
            if (Active)
            {
                spriteBatch.Draw(Texture, Position,
                    Animations[Animation].Rectangles[FrameIndex],
                    color,
                    Animations[Animation].Rotation, Origin,
                    Animations[Animation].Scale,
                    Animations[Animation].SpriteEffect, 0f);
            }
        }

        public void Draw(SpriteBatch spriteBatch, Color color, float scale)
        {
            if (Active)
            {
                spriteBatch.Draw(Texture, Position,
                    Animations[Animation].Rectangles[FrameIndex],
                    color,
                    Animations[Animation].Rotation, Origin,
                    scale,
                    Animations[Animation].SpriteEffect, 0f);
            }
        }
    }
}
