﻿namespace BaseEditor
{
    partial class EnemyEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enemyListBox = new System.Windows.Forms.ListBox();
            this.addButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.animationsTextBox = new System.Windows.Forms.TextBox();
            this.aiComboBox = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.passivesListBox = new System.Windows.Forms.CheckedListBox();
            this.label11 = new System.Windows.Forms.Label();
            this.dropListBox = new System.Windows.Forms.CheckedListBox();
            this.label10 = new System.Windows.Forms.Label();
            this.abilitiesListBox = new System.Windows.Forms.CheckedListBox();
            this.resistanceBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.damageBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.speedBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.expBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.defenseBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.HPBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.levelBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.spriteBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.nameBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(664, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.loadToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // enemyListBox
            // 
            this.enemyListBox.FormattingEnabled = true;
            this.enemyListBox.Location = new System.Drawing.Point(6, 22);
            this.enemyListBox.Name = "enemyListBox";
            this.enemyListBox.Size = new System.Drawing.Size(208, 290);
            this.enemyListBox.TabIndex = 1;
            this.enemyListBox.Visible = false;
            this.enemyListBox.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(37, 362);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 29);
            this.addButton.TabIndex = 2;
            this.addButton.Text = "Add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Visible = false;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(127, 362);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(75, 29);
            this.deleteButton.TabIndex = 3;
            this.deleteButton.Text = "Delete";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Visible = false;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.animationsTextBox);
            this.groupBox1.Controls.Add(this.aiComboBox);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.passivesListBox);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.dropListBox);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.abilitiesListBox);
            this.groupBox1.Controls.Add(this.resistanceBox);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.damageBox);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.speedBox);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.expBox);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.defenseBox);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.HPBox);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.levelBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.spriteBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.nameBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(238, 28);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(414, 372);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Enemy Properties";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(258, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(84, 13);
            this.label14.TabIndex = 29;
            this.label14.Text = "Animation Setup";
            // 
            // animationsTextBox
            // 
            this.animationsTextBox.Location = new System.Drawing.Point(261, 35);
            this.animationsTextBox.Multiline = true;
            this.animationsTextBox.Name = "animationsTextBox";
            this.animationsTextBox.Size = new System.Drawing.Size(134, 328);
            this.animationsTextBox.TabIndex = 28;
            this.animationsTextBox.Visible = false;
            this.animationsTextBox.TextChanged += new System.EventHandler(this.animationsTextBox_TextChanged);
            // 
            // aiComboBox
            // 
            this.aiComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.aiComboBox.FormattingEnabled = true;
            this.aiComboBox.Items.AddRange(new object[] {
            "Dumb",
            "Less Dumb",
            "Godly"});
            this.aiComboBox.Location = new System.Drawing.Point(124, 294);
            this.aiComboBox.Name = "aiComboBox";
            this.aiComboBox.Size = new System.Drawing.Size(121, 21);
            this.aiComboBox.TabIndex = 27;
            this.aiComboBox.Visible = false;
            this.aiComboBox.SelectedIndexChanged += new System.EventHandler(this.aiComboBox_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(122, 278);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(57, 13);
            this.label13.TabIndex = 26;
            this.label13.Text = "AI Routine";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(122, 195);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(49, 13);
            this.label12.TabIndex = 25;
            this.label12.Text = "Passives";
            // 
            // passivesListBox
            // 
            this.passivesListBox.CheckOnClick = true;
            this.passivesListBox.FormattingEnabled = true;
            this.passivesListBox.Items.AddRange(new object[] {
            "Put passives here"});
            this.passivesListBox.Location = new System.Drawing.Point(125, 211);
            this.passivesListBox.Name = "passivesListBox";
            this.passivesListBox.Size = new System.Drawing.Size(120, 64);
            this.passivesListBox.TabIndex = 24;
            this.passivesListBox.Visible = false;
            this.passivesListBox.SelectedIndexChanged += new System.EventHandler(this.passivesListBox_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(122, 111);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 13);
            this.label11.TabIndex = 23;
            this.label11.Text = "Drop List";
            // 
            // dropListBox
            // 
            this.dropListBox.CheckOnClick = true;
            this.dropListBox.FormattingEnabled = true;
            this.dropListBox.Items.AddRange(new object[] {
            "PUT ITEMS HERE"});
            this.dropListBox.Location = new System.Drawing.Point(125, 127);
            this.dropListBox.Name = "dropListBox";
            this.dropListBox.Size = new System.Drawing.Size(120, 64);
            this.dropListBox.TabIndex = 22;
            this.dropListBox.Visible = false;
            this.dropListBox.SelectedIndexChanged += new System.EventHandler(this.dropListBox_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(122, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "Abilities";
            // 
            // abilitiesListBox
            // 
            this.abilitiesListBox.CheckOnClick = true;
            this.abilitiesListBox.FormattingEnabled = true;
            this.abilitiesListBox.Items.AddRange(new object[] {
            "PUT ABILITIES HERE",
            "1",
            "2",
            "3",
            "43",
            "4",
            "5",
            "5",
            "6"});
            this.abilitiesListBox.Location = new System.Drawing.Point(125, 38);
            this.abilitiesListBox.Name = "abilitiesListBox";
            this.abilitiesListBox.Size = new System.Drawing.Size(120, 64);
            this.abilitiesListBox.TabIndex = 20;
            this.abilitiesListBox.Visible = false;
            this.abilitiesListBox.SelectedIndexChanged += new System.EventHandler(this.abilitiesListBox_SelectedIndexChanged);
            // 
            // resistanceBox
            // 
            this.resistanceBox.Location = new System.Drawing.Point(9, 268);
            this.resistanceBox.Name = "resistanceBox";
            this.resistanceBox.Size = new System.Drawing.Size(100, 20);
            this.resistanceBox.TabIndex = 19;
            this.resistanceBox.Visible = false;
            this.resistanceBox.TextChanged += new System.EventHandler(this.resistanceBox_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 251);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Resistance";
            // 
            // damageBox
            // 
            this.damageBox.Location = new System.Drawing.Point(9, 346);
            this.damageBox.Name = "damageBox";
            this.damageBox.Size = new System.Drawing.Size(100, 20);
            this.damageBox.TabIndex = 17;
            this.damageBox.Visible = false;
            this.damageBox.TextChanged += new System.EventHandler(this.damageBox_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 329);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Damage";
            // 
            // speedBox
            // 
            this.speedBox.Location = new System.Drawing.Point(9, 306);
            this.speedBox.Name = "speedBox";
            this.speedBox.Size = new System.Drawing.Size(100, 20);
            this.speedBox.TabIndex = 13;
            this.speedBox.Visible = false;
            this.speedBox.TextChanged += new System.EventHandler(this.speedBox_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 289);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Speed";
            // 
            // expBox
            // 
            this.expBox.Location = new System.Drawing.Point(9, 150);
            this.expBox.Name = "expBox";
            this.expBox.Size = new System.Drawing.Size(100, 20);
            this.expBox.TabIndex = 11;
            this.expBox.Visible = false;
            this.expBox.TextChanged += new System.EventHandler(this.expBox_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 134);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Experience Points";
            // 
            // defenseBox
            // 
            this.defenseBox.Location = new System.Drawing.Point(9, 228);
            this.defenseBox.Name = "defenseBox";
            this.defenseBox.Size = new System.Drawing.Size(100, 20);
            this.defenseBox.TabIndex = 9;
            this.defenseBox.Visible = false;
            this.defenseBox.TextChanged += new System.EventHandler(this.defenseBox_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 211);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Defense";
            // 
            // HPBox
            // 
            this.HPBox.Location = new System.Drawing.Point(9, 188);
            this.HPBox.Name = "HPBox";
            this.HPBox.Size = new System.Drawing.Size(100, 20);
            this.HPBox.TabIndex = 7;
            this.HPBox.Visible = false;
            this.HPBox.TextChanged += new System.EventHandler(this.HPBox_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 172);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(22, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "HP";
            // 
            // levelBox
            // 
            this.levelBox.Location = new System.Drawing.Point(9, 111);
            this.levelBox.Name = "levelBox";
            this.levelBox.Size = new System.Drawing.Size(100, 20);
            this.levelBox.TabIndex = 5;
            this.levelBox.Visible = false;
            this.levelBox.TextChanged += new System.EventHandler(this.levelBox_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Level";
            // 
            // spriteBox
            // 
            this.spriteBox.Location = new System.Drawing.Point(9, 72);
            this.spriteBox.Name = "spriteBox";
            this.spriteBox.Size = new System.Drawing.Size(100, 20);
            this.spriteBox.TabIndex = 3;
            this.spriteBox.Visible = false;
            this.spriteBox.TextChanged += new System.EventHandler(this.spriteBox_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Sprite Filename";
            // 
            // nameBox
            // 
            this.nameBox.Location = new System.Drawing.Point(9, 36);
            this.nameBox.Name = "nameBox";
            this.nameBox.Size = new System.Drawing.Size(100, 20);
            this.nameBox.TabIndex = 1;
            this.nameBox.Visible = false;
            this.nameBox.TextChanged += new System.EventHandler(this.nameBox_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.enemyListBox);
            this.groupBox2.Location = new System.Drawing.Point(12, 28);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(220, 324);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Enemy List";
            // 
            // EnemyEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(664, 404);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "EnemyEditor";
            this.Text = "Enemy XML Editor";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ListBox enemyListBox;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox spriteBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox nameBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox HPBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox levelBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckedListBox abilitiesListBox;
        private System.Windows.Forms.TextBox resistanceBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox damageBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox speedBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox expBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox defenseBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckedListBox passivesListBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckedListBox dropListBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox animationsTextBox;
        private System.Windows.Forms.ComboBox aiComboBox;
        private System.Windows.Forms.Label label13;

    }
}

