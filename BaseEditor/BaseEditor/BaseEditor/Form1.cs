﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace BaseEditor
{
    public partial class EnemyEditor : Form
    {
        public partial class item
        {
            public XElement Enemy { get; set; }
            public int id { get; set; }
            public string Enemy_Name { get; set; }
            public string Sprite_Filename { get; set; }
            public int Level { get; set; }
            public int Exp { get; set; }
            public int HP { get; set; }
            public int Defense { get; set; }
            public int Resistance { get; set; }
            public int Speed { get; set; }
            public int Damage { get; set; }
            public string AI { get; set; }
            public string Animation { get; set; }
            public HashSet<int> Abilities { get; set; }
            public HashSet<int> DropList { get; set; }
            public HashSet<int> Passives { get; set; }
        }

        XDocument doc = new XDocument();
        List<int> unusedIDs = new List<int>();
        BindingList<item> lbList = new BindingList<item>();
        string pathForFileInUse;

        public EnemyEditor()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Debug.WriteLine(((item)enemyListBox.SelectedItem).Abilities);
            foreach (int i in abilitiesListBox.CheckedIndices)
            {
                abilitiesListBox.SetItemCheckState(i, CheckState.Unchecked);
            }
            foreach (int i in dropListBox.CheckedIndices)
            {
                dropListBox.SetItemCheckState(i, CheckState.Unchecked);
            }
            foreach (int i in passivesListBox.CheckedIndices)
            {
                passivesListBox.SetItemCheckState(i, CheckState.Unchecked);
            }

            nameBox.Text = ((item)enemyListBox.SelectedItem).Enemy_Name;
            spriteBox.Text = ((item)enemyListBox.SelectedItem).Sprite_Filename;
            levelBox.Text = ((item)enemyListBox.SelectedItem).Level.ToString();
            expBox.Text = ((item)enemyListBox.SelectedItem).Exp.ToString();
            HPBox.Text = ((item)enemyListBox.SelectedItem).HP.ToString();
            defenseBox.Text = ((item)enemyListBox.SelectedItem).Defense.ToString();
            resistanceBox.Text = ((item)enemyListBox.SelectedItem).Resistance.ToString();
            speedBox.Text = ((item)enemyListBox.SelectedItem).Speed.ToString();
            damageBox.Text = ((item)enemyListBox.SelectedItem).Damage.ToString();
            aiComboBox.Text = ((item)enemyListBox.SelectedItem).AI;
            animationsTextBox.Text = ((item)enemyListBox.SelectedItem).Animation;
            foreach (int ability in ((item)enemyListBox.SelectedItem).Abilities)
            {
                //Debug.WriteLine(ability);
                abilitiesListBox.SetItemCheckState(ability, CheckState.Checked);
            }
            foreach (int drop in ((item)enemyListBox.SelectedItem).DropList)
            {
                //Debug.WriteLine(ability);
                dropListBox.SetItemCheckState(drop, CheckState.Checked);
            }
            foreach (int passive in ((item)enemyListBox.SelectedItem).Passives)
            {
                //Debug.WriteLine(ability);
                passivesListBox.SetItemCheckState(passive, CheckState.Checked);
            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            int nextID;
            if (unusedIDs.Count > 0)
            {
                nextID = unusedIDs[0];
                unusedIDs.RemoveAt(0);
            }
            else
            {
                nextID = enemyListBox.Items.Count;
            }

            doc.Root.Add(
         new XElement("Enemy",
             new XAttribute("id", nextID),
             new XAttribute("Enemy_Name", "New Enemy " + nextID),
             new XAttribute("Sprite_Filename", "default_enemy.png"),
             new XAttribute("Level", "1"),
             new XAttribute("Exp", "0"),
             new XAttribute("HP", "1"),
             new XAttribute("Defense", "1"),
             new XAttribute("Resistance", "1"),
             new XAttribute("Speed", "1"),
             new XAttribute("Damage", "1"),
             new XAttribute("AI", "Dumb"),
             new XAttribute("Animation", "Needs animations defined."),
             new XElement("Abilities"),
             new XElement("Drop_List"),
             new XElement("Passives")));

            XElement newEnemy = ((XElement)doc.Root.LastNode);

            lbList.Add(new item
            {
                Enemy = newEnemy,
                id = Convert.ToInt32(newEnemy.Attribute("id").Value),
                Enemy_Name = newEnemy.Attribute("Enemy_Name").Value,
                Sprite_Filename = newEnemy.Attribute("Sprite_Filename").Value,
                Level = Convert.ToInt32(newEnemy.Attribute("Level").Value),
                Exp = Convert.ToInt32(newEnemy.Attribute("Exp").Value),
                HP = Convert.ToInt32(newEnemy.Attribute("HP").Value),
                Defense = Convert.ToInt32(newEnemy.Attribute("Defense").Value),
                Resistance = Convert.ToInt32(newEnemy.Attribute("Resistance").Value),
                Speed = Convert.ToInt32(newEnemy.Attribute("Speed").Value),
                Damage = Convert.ToInt32(newEnemy.Attribute("Damage").Value),
                AI = newEnemy.Attribute("AI").Value,
                Animation = newEnemy.Attribute("Animation").Value,
                Abilities = new HashSet<int>(),
                DropList = new HashSet<int>(),
                Passives = new HashSet<int>()
            });
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            //Debug.WriteLine(doc);
            unusedIDs.Add(((item)enemyListBox.SelectedItem).id);
            unusedIDs.Sort();

            string enemyID = ((item)enemyListBox.SelectedItem).id.ToString();
            XElement enemySelected = doc.Root.Elements().Single(element => element.Attribute("id").Value.Equals(enemyID));
            //Debug.WriteLine(enemySelected);
            //Debug.WriteLine(((item)enemyListBox.SelectedItem).Enemy);
            lbList.RemoveAt(enemyListBox.SelectedIndex);
            enemySelected.Remove();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Set up the save file dialog.
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "XML Files (*.xml)|*.xml";
            openFileDialog.FilterIndex = 1;
            openFileDialog.RestoreDirectory = true;

            // Opens the save file dialog and then saves the xml document with
            // the desired name.
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                pathForFileInUse = openFileDialog.FileName;
                doc = XDocument.Load(openFileDialog.FileName);
            }

            foreach (XElement element in doc.Descendants("Enemy"))
            {
                HashSet<int> abilities = new HashSet<int>();
                HashSet<int> dropList = new HashSet<int>();
                HashSet<int> passives = new HashSet<int>();
                foreach (XElement ability in element.Descendants("Abilities").Descendants("Ability"))
                {
                    abilities.Add(Convert.ToInt32(ability.Attribute("id").Value));
                }
                foreach (XElement item in element.Descendants("Drop_List").Descendants("Item"))
                {
                    dropList.Add(Convert.ToInt32(item.Attribute("id").Value));
                }
                foreach (XElement passive in element.Descendants("Passives").Descendants("Passive"))
                {
                    passives.Add(Convert.ToInt32(passive.Attribute("id").Value));
                }

                lbList.Add(new item
                {
                    Enemy = element,
                    id = Convert.ToInt32(element.Attribute("id").Value),
                    Enemy_Name = element.Attribute("Enemy_Name").Value,
                    Sprite_Filename = element.Attribute("Sprite_Filename").Value,
                    Level = Convert.ToInt32(element.Attribute("Level").Value),
                    Exp = Convert.ToInt32(element.Attribute("Exp").Value),
                    HP = Convert.ToInt32(element.Attribute("HP").Value),
                    Defense = Convert.ToInt32(element.Attribute("Defense").Value),
                    Resistance = Convert.ToInt32(element.Attribute("Resistance").Value),
                    Speed = Convert.ToInt32(element.Attribute("Speed").Value),
                    Damage = Convert.ToInt32(element.Attribute("Damage").Value),
                    AI = element.Attribute("AI").Value,
                    Animation = element.Attribute("Animation").Value,
                    Abilities = abilities,
                    DropList = dropList,
                    Passives = passives
                });
            }

            enemyListBox.DataSource = lbList;
            enemyListBox.DisplayMember = "Enemy_Name";

            enemyListBox.Visible = true;

            nameBox.Visible = true;
            spriteBox.Visible = true;
            levelBox.Visible = true;
            expBox.Visible = true;
            HPBox.Visible = true;
            defenseBox.Visible = true;
            resistanceBox.Visible = true;
            speedBox.Visible = true;
            damageBox.Visible = true;
            abilitiesListBox.Visible = true;
            dropListBox.Visible = true;
            passivesListBox.Visible = true;
            aiComboBox.Visible = true;
            animationsTextBox.Visible = true;
            addButton.Visible = true;
            deleteButton.Visible = true;
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            doc = new XDocument(
         new XDeclaration("1.0", "utf-8", "yes"),
         new XComment("Project Gemini Enemy Data"),
         new XElement("Enemies",
             new XElement("Enemy",
                new XAttribute("id", "0"),
                new XAttribute("Enemy_Name", "New Enemy 0"),
                new XAttribute("Sprite_Filename", "default_enemy.png"),
                new XAttribute("Level", "1"),
                new XAttribute("Exp", "0"),
                new XAttribute("HP", "1"),
                new XAttribute("Defense", "1"),
                new XAttribute("Resistance", "1"),
                new XAttribute("Speed", "1"),
                new XAttribute("Damage", "1"),
                new XAttribute("AI", "Dumb"),
                new XAttribute("Animation", "Needs animations defined."),
                new XElement("Abilities"),
                new XElement("Drop_List"),
                new XElement("Passives"))));

            // Set up the save file dialog.
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "XML Files (*.xml)|*.xml";
            saveFileDialog.FilterIndex = 1;
            saveFileDialog.RestoreDirectory = true;

            // Opens the save file dialog and then saves the xml document with
            // the desired name.
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                pathForFileInUse = saveFileDialog.FileName;
                doc.Save(saveFileDialog.FileName);
            }

            foreach (XElement element in doc.Descendants("Enemy"))
            {
                lbList.Add(new item
                {
                    Enemy = element,
                    id = Convert.ToInt32(element.Attribute("id").Value),
                    Enemy_Name = element.Attribute("Enemy_Name").Value,
                    Sprite_Filename = element.Attribute("Sprite_Filename").Value,
                    Level = Convert.ToInt32(element.Attribute("Level").Value),
                    Exp = Convert.ToInt32(element.Attribute("Exp").Value),
                    HP = Convert.ToInt32(element.Attribute("HP").Value),
                    Defense = Convert.ToInt32(element.Attribute("Defense").Value),
                    Resistance = Convert.ToInt32(element.Attribute("Resistance").Value),
                    Speed = Convert.ToInt32(element.Attribute("Speed").Value),
                    Damage = Convert.ToInt32(element.Attribute("Damage").Value),
                    AI = element.Attribute("AI").Value,
                    Animation = element.Attribute("Animation").Value,
                    Abilities = new HashSet<int>(),
                    DropList = new HashSet<int>(),
                    Passives = new HashSet<int>()
                });
            }

            enemyListBox.DataSource = lbList;
            enemyListBox.DisplayMember = "Enemy_Name";

            enemyListBox.Visible = true;

            nameBox.Visible = true;
            spriteBox.Visible = true;
            levelBox.Visible = true;
            expBox.Visible = true;
            HPBox.Visible = true;
            defenseBox.Visible = true;
            resistanceBox.Visible = true;
            speedBox.Visible = true;
            damageBox.Visible = true;
            abilitiesListBox.Visible = true;
            dropListBox.Visible = true;
            passivesListBox.Visible = true;
            aiComboBox.Visible = true;
            animationsTextBox.Visible = true;
            addButton.Visible = true;
            deleteButton.Visible = true;

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (pathForFileInUse != null)
                doc.Save(pathForFileInUse);
        }

        private void nameBox_TextChanged(object sender, EventArgs e)
        {
            if (nameBox.Modified)
            {
                lbList[enemyListBox.SelectedIndex].Enemy_Name = nameBox.Text;
                lbList[enemyListBox.SelectedIndex].Enemy.Attribute("Enemy_Name").Value = nameBox.Text;
                lbList.ResetBindings();
            }
        }

        private void spriteBox_TextChanged(object sender, EventArgs e)
        {
            if (spriteBox.Modified)
            {
                lbList[enemyListBox.SelectedIndex].Sprite_Filename = spriteBox.Text;
                lbList[enemyListBox.SelectedIndex].Enemy.Attribute("Sprite_Filename").Value = spriteBox.Text;
                lbList.ResetBindings();
            }
        }

        private void levelBox_TextChanged(object sender, EventArgs e)
        {
            if (levelBox.Modified)
            {
                lbList[enemyListBox.SelectedIndex].Level = Convert.ToInt32(levelBox.Text);
                lbList[enemyListBox.SelectedIndex].Enemy.Attribute("Level").Value = levelBox.Text;
                lbList.ResetBindings();
            }
        }

        private void expBox_TextChanged(object sender, EventArgs e)
        {
            if (expBox.Modified)
            {
                lbList[enemyListBox.SelectedIndex].Exp = Convert.ToInt32(expBox.Text);
                lbList[enemyListBox.SelectedIndex].Enemy.Attribute("Exp").Value = expBox.Text;
                lbList.ResetBindings();
            }
        }

        private void HPBox_TextChanged(object sender, EventArgs e)
        {
            if (HPBox.Modified)
            {
                lbList[enemyListBox.SelectedIndex].HP = Convert.ToInt32(HPBox.Text);
                lbList[enemyListBox.SelectedIndex].Enemy.Attribute("HP").Value = HPBox.Text;
                lbList.ResetBindings();
            }
        }

        private void defenseBox_TextChanged(object sender, EventArgs e)
        {
            if (defenseBox.Modified)
            {
                lbList[enemyListBox.SelectedIndex].Defense = Convert.ToInt32(defenseBox.Text);
                lbList[enemyListBox.SelectedIndex].Enemy.Attribute("Defense").Value = defenseBox.Text;
                lbList.ResetBindings();
            }
        }

        private void resistanceBox_TextChanged(object sender, EventArgs e)
        {
            if (resistanceBox.Modified)
            {
                lbList[enemyListBox.SelectedIndex].Resistance = Convert.ToInt32(resistanceBox.Text);
                lbList[enemyListBox.SelectedIndex].Enemy.Attribute("Resistance").Value = resistanceBox.Text;
                lbList.ResetBindings();
            }
        }

        private void speedBox_TextChanged(object sender, EventArgs e)
        {
            if (speedBox.Modified)
            {
                lbList[enemyListBox.SelectedIndex].Speed = Convert.ToInt32(speedBox.Text);
                lbList[enemyListBox.SelectedIndex].Enemy.Attribute("Speed").Value = speedBox.Text;
                lbList.ResetBindings();
            }
        }

        private void damageBox_TextChanged(object sender, EventArgs e)
        {
            if (damageBox.Modified)
            {
                lbList[enemyListBox.SelectedIndex].Damage = Convert.ToInt32(damageBox.Text);
                lbList[enemyListBox.SelectedIndex].Enemy.Attribute("Damage").Value = damageBox.Text;
                lbList.ResetBindings();
            }
        }

        private void abilitiesListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (abilitiesListBox.GetItemCheckState(abilitiesListBox.SelectedIndex) == CheckState.Unchecked)
            {
                lbList[enemyListBox.SelectedIndex].Abilities.Remove(abilitiesListBox.SelectedIndex);
                XElement enemy = doc.Root.Elements().Single(element => element.Attribute("id").Value.Equals(((item)enemyListBox.SelectedItem).id.ToString()));
                enemy.Element("Abilities").Elements().Single(element => element.Attribute("id").Value.Equals(abilitiesListBox.SelectedIndex.ToString())).Remove();
            }
            else
            {
                HashSet<int> checkedAbilities = new HashSet<int>();
                foreach (int ability in abilitiesListBox.CheckedIndices)
                {
                    checkedAbilities.Add(ability);
                }
                lbList[enemyListBox.SelectedIndex].Abilities = checkedAbilities;

                doc.Root.Elements().Single(element => element.Attribute("id").Value.Equals(((item)enemyListBox.SelectedItem).id.ToString())).
                    Element("Abilities").Add(
                    new XElement("Ability",
                        new XAttribute("id", abilitiesListBox.SelectedIndex),
                        new XAttribute("Name", abilitiesListBox.SelectedItem)));
            }

            lbList.ResetBindings();
        }

        private void dropListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dropListBox.GetItemCheckState(dropListBox.SelectedIndex) == CheckState.Unchecked)
            {
                lbList[enemyListBox.SelectedIndex].DropList.Remove(dropListBox.SelectedIndex);
                XElement enemy = doc.Root.Elements().Single(element => element.Attribute("id").Value.Equals(((item)enemyListBox.SelectedItem).id.ToString()));
                enemy.Element("Drop_List").Elements().Single(element => element.Attribute("id").Value.Equals(dropListBox.SelectedIndex.ToString())).Remove();
            }
            else
            {
                HashSet<int> checkedDrops = new HashSet<int>();
                foreach (int item in dropListBox.CheckedIndices)
                {
                    checkedDrops.Add(item);
                }
                lbList[enemyListBox.SelectedIndex].DropList = checkedDrops;

                doc.Root.Elements().Single(element => element.Attribute("id").Value.Equals(((item)enemyListBox.SelectedItem).id.ToString())).
                    Element("Drop_List").Add(
                    new XElement("Item",
                        new XAttribute("id", dropListBox.SelectedIndex),
                        new XAttribute("Name", dropListBox.SelectedItem)));
            }

            lbList.ResetBindings();
        }

        private void passivesListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (passivesListBox.GetItemCheckState(passivesListBox.SelectedIndex) == CheckState.Unchecked)
            {
                lbList[enemyListBox.SelectedIndex].Passives.Remove(passivesListBox.SelectedIndex);
                XElement enemy = doc.Root.Elements().Single(element => element.Attribute("id").Value.Equals(((item)enemyListBox.SelectedItem).id.ToString()));
                enemy.Element("Passives").Elements().Single(element => element.Attribute("id").Value.Equals(passivesListBox.SelectedIndex.ToString())).Remove();
            }
            else
            {
                HashSet<int> checkedPassives = new HashSet<int>();
                foreach (int passive in passivesListBox.CheckedIndices)
                {
                    checkedPassives.Add(passive);
                }
                lbList[enemyListBox.SelectedIndex].Passives = checkedPassives;

                doc.Root.Elements().Single(element => element.Attribute("id").Value.Equals(((item)enemyListBox.SelectedItem).id.ToString())).
                    Element("Passives").Add(
                    new XElement("Passive",
                        new XAttribute("id", passivesListBox.SelectedIndex),
                        new XAttribute("Name", passivesListBox.SelectedItem)));
            }

            lbList.ResetBindings();
        }

        private void aiComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbList[enemyListBox.SelectedIndex].AI = aiComboBox.Text;
            lbList[enemyListBox.SelectedIndex].Enemy.Attribute("AI").Value = aiComboBox.Text;
            lbList.ResetBindings();
        }

        private void animationsTextBox_TextChanged(object sender, EventArgs e)
        {
            lbList[enemyListBox.SelectedIndex].AI = animationsTextBox.Text;
            lbList[enemyListBox.SelectedIndex].Enemy.Attribute("Animation").Value = animationsTextBox.Text;
            lbList.ResetBindings();
        }
    }
}
