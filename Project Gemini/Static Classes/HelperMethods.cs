﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using Microsoft.Xna.Framework;
using ProjectGemini.EnumTypes;

namespace ProjectGemini.Static_Classes
{
    public static class HelperMethods
    {
        // This constant string is used as a "salt" value for the PasswordDeriveBytes function calls.
        // This size of the IV (in bytes) must = (keysize / 8).  Default keysize is 256, so the IV must be
        // 32 bytes long.  Using a 16 character string here gives us 32 bytes when converted to a byte array.
        private const string initVector = "tu89geji340t89u2";

        // This constant is used to determine the keysize of the encryption algorithm.
        private const int keysize = 256;

        // The key for encrypting and decrypting
        private const string passPhrase = "Chr0n0Tr1gg3R";

        public static string Encrypt(string plainText)
        {
            byte[] initVectorBytes = Encoding.UTF8.GetBytes(initVector);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null);
            byte[] keyBytes = password.GetBytes(keysize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            return Convert.ToBase64String(cipherTextBytes);
        }

        public static string Decrypt(string cipherText)
        {
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] cipherTextBytes = Convert.FromBase64String(cipherText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null);
            byte[] keyBytes = password.GetBytes(keysize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream(cipherTextBytes);
            CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];
            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
        }

        public static bool TestRange(float numberToCheck, float bottom, float top)
        {
            return (numberToCheck > bottom && numberToCheck < top);
        }

        public static String TextWrap(String sText)
        {
            sText = sText + " ";
            string textBuilder = "";
            //this will avoid dropping lines in the middle of a word  
            int spaceEnd = 0;
            bool exiting = false;
            //This will break apart our text to a certain number of characters wide  
            // this will create a generic word wrapping for our string  
            do
            {
                do
                {
                    if (sText.Substring(0, 45 + spaceEnd).EndsWith(" ") || sText.Length < 45 + spaceEnd)
                    {
                        if (sText.Length < 45 + spaceEnd)
                        {
                            textBuilder += sText;
                        }
                        else
                        {
                            textBuilder += sText.Substring(0, 45 + spaceEnd) + "\n";
                            sText = sText.Remove(0, 45 + spaceEnd);
                            exiting = true;
                        }
                    }
                    else
                        spaceEnd++;
                }
                while (exiting == false);
                exiting = false;
                spaceEnd = 0;
            }
            while (sText.Length >= 45);
            textBuilder += sText;
            return textBuilder;
        }

        public static Color GetColor(AbilityElementType element)
        {
            Color color = Color.White;

            switch (element)
            {
                case AbilityElementType.Earth:
                    color = Color.Brown;
                    break;
                case AbilityElementType.Fire:
                    color = Color.Orange;
                    break;
                case AbilityElementType.Frost:
                    color = Color.Blue;
                    break;
                case AbilityElementType.Light:
                    color = Color.Goldenrod;
                    break;
                case AbilityElementType.Nature:
                    color = Color.LawnGreen;
                    break;
                case AbilityElementType.Physical:
                    color = Color.DarkRed;
                    break;
                case AbilityElementType.Shadow:
                    color = Color.Black;
                    break;
                case AbilityElementType.Lightning:
                    color = Color.LightBlue;
                    break;
            }

            return color;
        }
    }
}
