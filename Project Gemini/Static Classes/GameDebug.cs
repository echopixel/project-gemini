﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FarseerPhysics.DebugViews;

namespace ProjectGemini.Static_Classes
{
    public static class GameDebug
    {
        public static bool ShowCollision { get; set; }

        static public DebugViewXNA DebugView;
    }
}
