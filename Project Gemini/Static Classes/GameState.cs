﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using ProjectGemini.Entities;
using FuncWorks.XNA.XTiled;
using ProjectGemini.Game_World;
using ProjectGemini.Managers;
using FarseerPhysics.Dynamics;
using FarseerPhysics.DebugViews;
using ProjectGemini.Utilities;
using EasyStorage;
using System.IO;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ProjectGemini.Graphics_Utilities;
using ProjectGemini.EnumTypes;
using ProjectGemini.Audio;
using ProjectGemini.Message_Boxes;
using ProjectGemini.Controllers;

namespace ProjectGemini.Static_Classes
{
    public static class GameState
    {
        static public IAsyncSaveDevice SaveDevice;
        static public List<string> GameSaves = new List<string>();
        static public string LastPlayedSave;
        static public string FileName_Options = "projectgemini.cfg";
        static public string ContainerName = "Project Gemini";

        static public Game          Game;
        static public GameTime      GameTime;
        static public ScreenManager ScreenManager;
        static public AudioManager  AudioManager;
        static public EventManager  WorldEventManager;

        static public Camera        Camera;
        static public Rectangle     VisibleArea;

        static public Hero      HeroOne;
        static public Hero      HeroTwo;
        static public Hero      HeroThree;

        static public bool      PlayersInCombat;

        static public int       ScreenWidth;
        static public int       ScreenHeight;

        static public World     PhysicsWorld;
        static public Map       CurrentMap;
        static public Zone      CurrentZone;
        static public int       CollisionLayer;
        static public int       EventLayer;
        static public int       InteractEventLayer;
        static public int       NPCLayer;
        static public int       EnemyLayer;
        static public int       StartLayer;

        static public Inventory Inventory;
        static public Int64 Currency = 0;

        static public Dictionary<int, String> Events;
        static public Dictionary<BaseEntity, String> InteractableEvents;
        static public HashSet<Tuple<string, int>> OpenedChests;
        static public Dictionary<string, Vector2> Starts;

        static public float         TimeScale;
        static public Random        RandomNumGenerator = new Random();
        static public List<Enemy>   Enemies = new List<Enemy>();
        static public List<Ability> Abilities = new List<Ability>();
        static public List<Item>    Items = new List<Item>();
        static public List<Weapon>  Weapons = new List<Weapon>();
        static public List<Armor>   Armors = new List<Armor>();

        static public void ChangeMap(ChangeMapEvent source)
        {
            ScreenManager.AddScreen(new TransitionScreen(source.Position, source.Map), null);
            source.End();
        }

        static public void ChangeMap(string position, MapTypes map)
        {
            ScreenManager.AddScreen(new TransitionScreen(position, map), null);
        }

        static public void DisplayMessageBox(MessageBoxEvent source)
        {
            MessageBoxScreen messageBox = new MessageBoxScreen(source.Message,
                            HelperMethods.TestRange(GameState.HeroOne.Position.Y,
                                                GameState.CurrentMap.Bounds.Height - ResolutionManager.VirtualHeight / 4,
                                                GameState.CurrentMap.Bounds.Height));
            ScreenManager.AddScreen(messageBox, PlayerIndex.One); // Story message boxes should only be controlled by player one.
            source.End();
        }

        static public void RefreshSaves()
        {
            GameSaves.Clear();
            string[] saves = SaveDevice.GetFiles(ContainerName);

            foreach (string save in saves)
            {
                if (save.Contains(".sav"))
                    GameSaves.Add(save);
            }
        }

        static public void SaveOptions()
        {
            SaveDevice.Save(
                ContainerName,
                FileName_Options,
                stream =>
                {
                    using (StreamWriter writer = new StreamWriter(stream))
                    {
                        writer.WriteLine(LastPlayedSave);
                    }
                });
        }

        static public void LoadOptions()
        {
            if (SaveDevice.FileExists(ContainerName, FileName_Options))
            {
                SaveDevice.Load(
                    ContainerName,
                    FileName_Options,
                    stream =>
                    {
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            LastPlayedSave = reader.ReadLine();
                        }
                    });
            }
        }

        static public void SaveGame(string saveIndex)
        {
            // Set Last Played Save
            LastPlayedSave = saveIndex;
            SaveOptions();

            // save a file asynchronously. this will trigger IsBusy to return true
            // for the duration of the save process.
            SaveDevice.Save(
                ContainerName,
                saveIndex,
                stream =>
                {
                    using (StreamWriter writer = new StreamWriter(stream))
                    {
                        // Save Time Information
                        writer.WriteLine(DateTime.Now);
                        // Money!
                        writer.WriteLine(GameState.Currency.ToString());
                        // Area Info
                        writer.WriteLine(CurrentMap.Properties["fileName"].Value);

                        writer.WriteLine("Event Flags");
                        writer.Write(FlagManager.FlagSaveString());

                        writer.WriteLine("Opened Chests");
                        foreach (Tuple<string, int> chestID in OpenedChests)
                        {
                            writer.WriteLine(chestID.Item1 + ":" + chestID.Item2);
                        }
                                             
                        // Hero One
                        writer.WriteLine("Hero One Stats");
                        writer.WriteLine(HeroOne.Level);
                        writer.WriteLine(HeroOne.EXP);
                        writer.WriteLine(HeroOne.SkillPoints);
                        writer.WriteLine(HeroOne.CurrentHP);
                        writer.WriteLine(HeroOne.MaxHP);
                        writer.WriteLine(HeroOne.CurrentMP);
                        writer.WriteLine(HeroOne.MaxMP);
                        writer.WriteLine(HeroOne.ResourceType);
                        writer.WriteLine(HeroOne.CurrentPersonal);
                        writer.WriteLine(HeroOne.MaxPersonal);
                        writer.WriteLine(HeroOne.BaseHP);
                        writer.WriteLine(HeroOne.BaseMP);
                        writer.WriteLine(HeroOne.BaseStr);
                        writer.WriteLine(HeroOne.BaseInt);
                        writer.WriteLine(HeroOne.BaseDex);
                        writer.WriteLine(HeroOne.BaseAla);
                        writer.WriteLine(HeroOne.BaseSta);
                        writer.WriteLine(HeroOne.BaseWis);
                        writer.WriteLine(HeroOne.Damage);
                        writer.WriteLine(HeroOne.Magic);
                        writer.WriteLine(HeroOne.Heal);
                        writer.WriteLine(HeroOne.Defense);
                        writer.WriteLine(HeroOne.Resistance);
                        writer.WriteLine(HeroOne.HPRegen);
                        writer.WriteLine(HeroOne.MPRegen);
                        writer.WriteLine(HeroOne.PersonalRegen);
                        writer.WriteLine(HeroOne.HPMod);
                        writer.WriteLine(HeroOne.MPMod);
                        writer.WriteLine(HeroOne.StrMod);
                        writer.WriteLine(HeroOne.IntMod);
                        writer.WriteLine(HeroOne.DexMod);
                        writer.WriteLine(HeroOne.AlaMod);
                        writer.WriteLine(HeroOne.WisMod);
                        writer.WriteLine(HeroOne.ModDam);
                        writer.WriteLine(HeroOne.ModMag);
                        writer.WriteLine(HeroOne.ModHea);
                        writer.WriteLine(HeroOne.ModDef);
                        writer.WriteLine(HeroOne.ModRes);
                        writer.WriteLine(HeroOne.HPRegenMod);
                        writer.WriteLine(HeroOne.MPRegenMod);
                        writer.WriteLine(HeroOne.Weapon.itemId);
                        writer.WriteLine(HeroOne.ArmorChest.itemId);
                        writer.WriteLine(HeroOne.ArmorHead.itemId);
                        writer.WriteLine(HeroOne.ArmorLegs.itemId);
                        writer.WriteLine(HeroOne.Body.Position.X);
                        writer.WriteLine(HeroOne.Body.Position.Y);
                        
                        // Skills
                        writer.WriteLine((HeroOne.Skill1 != null) ? HeroOne.Skill1.abilityId : -1);
                        writer.WriteLine((HeroOne.Skill2 != null) ? HeroOne.Skill2.abilityId : -1);
                        writer.WriteLine((HeroOne.Skill3 != null) ? HeroOne.Skill3.abilityId : -1);
                        writer.WriteLine((HeroOne.Skill4 != null) ? HeroOne.Skill4.abilityId : -1);
                        writer.WriteLine((HeroOne.Skill5 != null) ? HeroOne.Skill5.abilityId : -1);
                        writer.WriteLine((HeroOne.Skill6 != null) ? HeroOne.Skill6.abilityId : -1);
                        writer.WriteLine((HeroOne.Skill7 != null) ? HeroOne.Skill7.abilityId : -1);
                        writer.WriteLine((HeroOne.Skill8 != null) ? HeroOne.Skill8.abilityId : -1);
                        writer.WriteLine((HeroOne.Skill9 != null) ? HeroOne.Skill9.abilityId : -1);
                        writer.WriteLine("Known Skills");
                        foreach (Ability ability in HeroOne.Skills)
                        {
                            writer.WriteLine(ability.abilityId);
                        }

                        // Inventory
                        writer.WriteLine(Inventory.SaveOutput());
                    }
                });
            
        }

        static public void LoadGame(ContentManager content, string saveIndex)
        {
            //Console.WriteLine("Loading save: " + saveIndex);
            if (SaveDevice.FileExists(ContainerName, saveIndex))
            {
                SaveDevice.Load(
                    ContainerName,
                    saveIndex,
                    stream =>
                    {
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            string line;
                            string[] splitString;

                            // Save Time Info
                            reader.ReadLine();

                            // Money!
                            GameState.Currency = Int64.Parse(reader.ReadLine());

                            // Area Info
                            Events.Clear();
                            InteractableEvents.Clear();
                            OpenedChests.Clear();
                            PhysicsWorld.Clear();

                            CurrentMap = content.Load<Map>("TileMaps/" + reader.ReadLine());

                            line = reader.ReadLine();
                            do
                            {
                                if (line != "Event Flags")
                                {
                                    splitString = line.Split('|');
                                    FlagManager.SetFlag(splitString[0], bool.Parse(splitString[1]));
                                }
                            } while ((line = reader.ReadLine()) != "Opened Chests");
                            do
                            {
                                if (line != "Opened Chests")
                                {
                                    splitString = line.Split(':');
                                    OpenedChests.Add(Tuple.Create(splitString[0], int.Parse(splitString[1])));
                                }
                            } while ((line = reader.ReadLine()) != "Hero One Stats");

                            CurrentZone.Initialize(content);

                            // Hero One
                            if (HeroOne != null)
                            {
                                HeroOne.IsDead = false;
                                HeroOne.CreateHeroBody();
                                HeroOne.SpriteAnimation = new SpriteAnimation(content.Load<Texture2D>("Sprites/duskarmovementsheet"), 4, 8);
                                HeroOne.AttackAnimation = new SpriteAnimation(content.Load<Texture2D>("Sprites/duskarattacksheet"), 2, 4);
                                HeroOne.CastingAnimation = new SpriteAnimation(content.Load<Texture2D>("Sprites/duskarcastsheet"), 12, 4);
                                HeroOne.SetUpAnimations();
                            }
                            else
                            {
                                GameState.HeroOne = new Hero(
                                content.Load<Texture2D>("Sprites/duskarmovementsheet"), 4, 8,
                                content.Load<Texture2D>("Sprites/duskarattacksheet"), 2, 4, 
                                content.Load<Texture2D>("Sprites/duskarcastsheet"), 12, 4,
                                GameState.CurrentMap);
                            }
                            HeroOne.Buffs.Clear();
                            HeroOne.Passives.Clear();

                            HeroOne.Level = int.Parse(reader.ReadLine());
                            HeroOne.EXP = int.Parse(reader.ReadLine());
                            HeroOne.SkillPoints = int.Parse(reader.ReadLine());
                            HeroOne.CurrentHP = int.Parse(reader.ReadLine());
                            HeroOne.MaxHP = int.Parse(reader.ReadLine());
                            HeroOne.CurrentMP = int.Parse(reader.ReadLine());
                            HeroOne.MaxMP = int.Parse(reader.ReadLine());
                            HeroOne.ResourceType = (PersonalResourceType)Enum.Parse(typeof(PersonalResourceType), reader.ReadLine());
                            HeroOne.SetupViaPersonal(HeroOne.ResourceType);
                            HeroOne.CurrentPersonal = int.Parse(reader.ReadLine());
                            HeroOne.MaxPersonal = int.Parse(reader.ReadLine());
                            HeroOne.BaseHP = int.Parse(reader.ReadLine());
                            HeroOne.BaseMP = int.Parse(reader.ReadLine());
                            HeroOne.BaseStr = int.Parse(reader.ReadLine());
                            HeroOne.BaseInt = int.Parse(reader.ReadLine());
                            HeroOne.BaseDex = int.Parse(reader.ReadLine());
                            HeroOne.BaseAla = int.Parse(reader.ReadLine());
                            HeroOne.BaseSta = int.Parse(reader.ReadLine());
                            HeroOne.BaseWis = int.Parse(reader.ReadLine());
                            HeroOne.Damage = int.Parse(reader.ReadLine());
                            HeroOne.Magic = int.Parse(reader.ReadLine());
                            HeroOne.Heal = int.Parse(reader.ReadLine());
                            HeroOne.Defense = int.Parse(reader.ReadLine());
                            HeroOne.Resistance = int.Parse(reader.ReadLine());
                            HeroOne.HPRegen = float.Parse(reader.ReadLine());
                            HeroOne.MPRegen = float.Parse(reader.ReadLine());
                            HeroOne.PersonalRegen = float.Parse(reader.ReadLine());

                            // Mods
                            HeroOne.HPMod = float.Parse(reader.ReadLine());
                            HeroOne.MPMod = float.Parse(reader.ReadLine());
                            HeroOne.StrMod = float.Parse(reader.ReadLine());
                            HeroOne.IntMod = float.Parse(reader.ReadLine());
                            HeroOne.DexMod = float.Parse(reader.ReadLine());
                            HeroOne.AlaMod = float.Parse(reader.ReadLine());
                            HeroOne.WisMod = float.Parse(reader.ReadLine());
                            HeroOne.ModDam = float.Parse(reader.ReadLine());
                            HeroOne.ModMag = float.Parse(reader.ReadLine());
                            HeroOne.ModHea = float.Parse(reader.ReadLine());
                            HeroOne.ModDef = float.Parse(reader.ReadLine());
                            HeroOne.ModRes = float.Parse(reader.ReadLine());
                            HeroOne.HPRegenMod = float.Parse(reader.ReadLine());
                            HeroOne.MPRegenMod = float.Parse(reader.ReadLine());

                            HeroOne.Equip(Weapons[int.Parse(reader.ReadLine())]);
                            HeroOne.Equip(Armors[int.Parse(reader.ReadLine())]);
                            HeroOne.Equip(Armors[int.Parse(reader.ReadLine())]);
                            HeroOne.Equip(Armors[int.Parse(reader.ReadLine())]);
                            HeroOne.Body.Position = new Vector2(float.Parse(reader.ReadLine()),
                                float.Parse(reader.ReadLine()));

                            HeroOne.Skill1 = ((line = reader.ReadLine()) != "-1") ? Abilities[int.Parse(line)] : null;
                            HeroOne.Skill2 = ((line = reader.ReadLine()) != "-1") ? Abilities[int.Parse(line)] : null;
                            HeroOne.Skill3 = ((line = reader.ReadLine()) != "-1") ? Abilities[int.Parse(line)] : null;
                            HeroOne.Skill4 = ((line = reader.ReadLine()) != "-1") ? Abilities[int.Parse(line)] : null;
                            HeroOne.Skill5 = ((line = reader.ReadLine()) != "-1") ? Abilities[int.Parse(line)] : null;
                            HeroOne.Skill6 = ((line = reader.ReadLine()) != "-1") ? Abilities[int.Parse(line)] : null;
                            HeroOne.Skill7 = ((line = reader.ReadLine()) != "-1") ? Abilities[int.Parse(line)] : null;
                            HeroOne.Skill8 = ((line = reader.ReadLine()) != "-1") ? Abilities[int.Parse(line)] : null;
                            HeroOne.Skill9 = ((line = reader.ReadLine()) != "-1") ? Abilities[int.Parse(line)] : null; 
                            line = reader.ReadLine();
                            do
                            {
                                if (line != "Known Skills")
                                {
                                    HeroOne.Skills.Add(Abilities[int.Parse(line)]);
                                }
                            } while ((line = reader.ReadLine()) != "Items");

                            GameState.HeroOne.UpdateStats();
                            
                            if (Camera == null)
                                Camera = new Camera();
                            Camera.FollowHeroPosition(ref HeroOne, ref CurrentMap);
                            Camera.CurrentMap = CurrentMap;

                            //Items
                            //Keys
                            //Armors
                            //5:2
                            //8:2
                            //10:2
                            //3:2
                            //4:2
                            //7:2
                            //6:2
                            //9:2
                            //Weapons
                            //1:lvl0:2
                            //2:lvl0:2
                            do
                            {
                                if (line != "Items")
                                {
                                    splitString = line.Split(':');
                                    Inventory.AddRemoveItem(Items[int.Parse(splitString[0])], int.Parse(splitString[1]));
                                }
                            } while ((line = reader.ReadLine()) != "Keys");
                            do
                            {
                                if (line != "Keys")
                                {
                                    splitString = line.Split(':');
                                    Inventory.AddRemoveItem(Items[int.Parse(splitString[0])], int.Parse(splitString[1]));
                                }
                            } while ((line = reader.ReadLine()) != "Armors");
                            do
                            {
                                if (line != "Armors")
                                {
                                    splitString = line.Split(':');
                                    Inventory.AddRemoveItem(Armors[int.Parse(splitString[0])], int.Parse(splitString[1]));
                                }
                            } while ((line = reader.ReadLine()) != "Weapons");
                            do
                            {
                                if (line != "Weapons")
                                {
                                    splitString = line.Split(':');
                                    Inventory.AddRemoveItem(Weapons[int.Parse(splitString[0])], int.Parse(splitString[2]));
                                }
                            } while ((line = reader.ReadLine()) != "");

                            HeroOne.UpdateStats();
                        }
                    });
            }
        }

        static public void SaveDevicePrompt()
        {
            IAsyncSaveDevice saveDevice;
            // we can set our supported languages explicitly or we can allow the
            // game to support all the languages. the first language given will
            // be the default if the current language is not one of the supported
            // languages. this only affects the text found in message boxes shown
            // by EasyStorage and does not have any affect on the rest of the game.
            EasyStorageSettings.SetSupportedLanguages(Language.French, Language.Spanish);

            // on Windows Phone we use a save device that uses IsolatedStorage
            // on Windows and Xbox 360, we use a save device that gets a
            //shared StorageDevice to handle our file IO.
#if WINDOWS_PHONE
            saveDevice = new IsolatedStorageSaveDevice();
            Global.SaveDevice = saveDevice;
 
            // we use the tap gesture for input on the phone
            TouchPanel.EnabledGestures = GestureType.Tap;
#else
            // create and add our SaveDevice
            SharedSaveDevice sharedSaveDevice = new SharedSaveDevice();
            ScreenManager.Game.Components.Add(sharedSaveDevice);

            // make sure we hold on to the device
            saveDevice = sharedSaveDevice;

            // hook two event handlers to force the user to choose a new device if they cancel the
            // device selector or if they disconnect the storage device after selecting it
            sharedSaveDevice.DeviceSelectorCanceled +=
                (s, e) => e.Response = SaveDeviceEventResponse.Force;
            sharedSaveDevice.DeviceDisconnected +=
                (s, e) => e.Response = SaveDeviceEventResponse.Force;

            // prompt for a device on the first Update we can
            sharedSaveDevice.PromptForDevice();

            sharedSaveDevice.DeviceSelected += (s, e) =>
            {
                //Save our save device to the global counterpart, so we can access it
                //anywhere we want to save/load
                SaveDevice = (SaveDevice)s;

                //Once they select a storage device, we can load the main menu.
                //You'll notice I hard coded PlayerIndex.One here. You'll need to
                //change that if you plan on releasing your game. I linked to an
                //example on how to do that but here's the link if you need it.
                //<a href="http://blog.nickgravelyn.com/2009/03/basic-handling-of-multiple-controllers/">http://blog.nickgravelyn.com/2009/03/basic-handling-of-multiple-controllers/</a>
                //We need to perform a check to see if we're on the Press Start Screen.
                //If a storage device is selected NOT from this page, we don't want to
                //create a new Main Menu screen! (Thanks @FreelanceGames for the mention)

                ScreenManager.AddScreen(new MainMenuScreen(), null);
            };
#endif

#if XBOX
			// add the GamerServicesComponent
			Components.Add(new Microsoft.Xna.Framework.GamerServices.GamerServicesComponent(this));
#endif
        }
        
    }
}
