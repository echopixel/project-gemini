﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FarseerPhysics.Dynamics.Contacts;
using FarseerPhysics.Collision;
using Microsoft.Xna.Framework;
using FarseerPhysics.Dynamics;

namespace ProjectGemini.Static_Classes
{
    public static class GamePhysics
    {
        public static bool BeginContact(Contact contact)
        {
            return true;
        }
        public static void EndContact(Contact contact) 
        { 
        }
        public static void PreSolve(Contact contact, ref Manifold oldManifold) 
        {
        }
        public static void PostSolve(Contact contact, ContactConstraint impulse) 
        {
            if (contact.FixtureA.UserData != null && contact.FixtureB.UserData != null)
            {
                impulse.BodyB.LinearVelocity = Vector2.Zero;
                impulse.BodyA.LinearVelocity = impulse.BodyA.LinearVelocity * -1;
            }
        }
    }
}
