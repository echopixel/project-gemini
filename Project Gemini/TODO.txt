﻿1. Add Object and Object Layer functionality to MapProcessor.cs
2. Get some collisions working
3. NPC draw order with player depends on Y values.

// Better solution.  Using the Y pos of the player and npc, draw them in a different order after comparing their Y pos.
Higher y pos should be behind the lower y pos.  Need to find which position of the characters to compare so it looks right.

4. Try to get an XML file to load in the sprite animation for a player.  Creating a sprite animation from hand is too verbose
to have inside a method by itself.

5. Load in controls from a text .ini file.  Allow for customization.