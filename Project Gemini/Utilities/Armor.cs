﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectGemini.EnumTypes;

namespace ProjectGemini.Utilities
{
    public class Armor : BaseEquipment
    {
        public Armor(
            int id,
            string name,
            string description,
            string icon,
            EquipmentSlotType slot,
            int hpmod,
            int mpmod,
            int damagemod,
            int magicmod,
            int healmod,
            int defencemod,
            int resistancemod,
            int strmod,
            int intmod,
            int dexmod,
            int alamod,
            int stamod,
            int wismod,
            //placeholder, needs to be buff/debuff class.
            List<BaseBuff> passives)
        {
            
            itemId = id;
            itemName = name;
            itemDescription = description;
            itemIcon = icon;
            equipSlot = slot;
            equipHpmod = hpmod;
            equipMpmod = mpmod;
            equipDamageMod = damagemod;
            equipMagicMod = magicmod;
            equipHealMod = healmod;
            equipDefenseMod = defencemod;
            equipResistanceMod = resistancemod;
            equipStrmod = strmod;
            equipIntmod = intmod;
            equipDexmod = dexmod;
            equipAlamod = alamod;
            equipStamod = stamod;
            equipWismod = wismod;
            equipPassives = passives;

            itemCurrentStack = 0;
            itemStackSize = 99;
        }
    }
}