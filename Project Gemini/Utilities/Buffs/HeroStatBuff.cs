﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectGemini.Entities;
using ProjectGemini.EnumTypes;
using ProjectGemini.Static_Classes;
using Microsoft.Xna.Framework;

namespace ProjectGemini.Utilities.Buffs
{
    class HeroStatBuff : BaseBuff
    {
        string Stat;

        public HeroStatBuff(string unique, double duration, double magnitude, TargetableEntity host, TargetableEntity owner, string stat, BuffType type, AbilityElementType element)
            :base(unique, duration, magnitude, host, owner, type, element)
        {
            Stat = stat;
        }

        public HeroStatBuff(HeroStatBuff buff, TargetableEntity host, TargetableEntity owner)
            : base(buff, host, owner)
        {
            Stat = buff.Stat;
        }

        public override bool StartBuff(GameTime gameTime)
        {
            //Console.WriteLine("Start");

            if (!base.StartBuff(gameTime))
            {
                return false;
            }

            if (Stat == "STR")
            {
                ((Hero)Host).StrMod += Magnitude;
            }
            else if (Stat == "INT")
            {
                ((Hero)Host).IntMod += Magnitude;
            }
            else if (Stat == "DEX")
            {
                ((Hero)Host).DexMod += Magnitude;
            }
            else if (Stat == "ALA")
            {
                ((Hero)Host).AlaMod += Magnitude;
            }
            else if (Stat == "STA")
            {
                ((Hero)Host).StaMod += Magnitude;
            }
            else if (Stat == "WIS")
            {
                ((Hero)Host).WisMod += Magnitude;
            }
            else if (Stat == "HPRegen")
            {
                ((Hero)Host).HPRegenMod += Magnitude;
            }
            else if (Stat == "MPRegen")
            {
                ((Hero)Host).MPRegenMod += Magnitude;
            }
            else if (Stat == "DamageCritDamage")
            {
                ((Hero)Host).DamageCritDamage += (int)Magnitude;
            }
            else if (Stat == "MagicCritDamage")
            {
                 ((Hero)Host).MagicCritDamage += (int)Magnitude;
            }

            Host.UpdateStats();

            return true;
        }

        public override void StopBuff(GameTime gameTime)
        {
            //Console.WriteLine("Stop");

            if (Stat == "STR")
            {
                ((Hero)Host).StrMod -= Magnitude;
            }
            else if (Stat == "INT")
            {
                ((Hero)Host).IntMod -= Magnitude;
            }
            else if (Stat == "DEX")
            {
                ((Hero)Host).DexMod -= Magnitude;
            }
            else if (Stat == "ALA")
            {
                ((Hero)Host).AlaMod -= Magnitude;
            }
            else if (Stat == "STA")
            {
                ((Hero)Host).StaMod -= Magnitude;
            }
            else if (Stat == "WIS")
            {
                ((Hero)Host).WisMod -= Magnitude;
            }
            else if (Stat == "HPRegen")
            {
                ((Hero)Host).HPRegenMod -= Magnitude;
            }
            else if (Stat == "MPRegen")
            {
                ((Hero)Host).MPRegenMod -= Magnitude;
            }
            else if (Stat == "DamageCritDamage")
            {
                ((Hero)Host).DamageCritDamage -= (int)Magnitude;
            }
            else if (Stat == "MagicCritDamage")
            {
                ((Hero)Host).MagicCritDamage -= (int)Magnitude;
            }

            Host.UpdateStats();
        }
    }
}
