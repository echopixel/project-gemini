﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectGemini.EnumTypes;
using ProjectGemini.Entities;
using Microsoft.Xna.Framework;
using ProjectGemini.Static_Classes;
using ParticleEngine2D;
using Microsoft.Xna.Framework.Graphics;

namespace ProjectGemini.Utilities
{
    public class BaseBuff
    {
        public bool Started;
        public float Duration;
        public float TotalDuration;
        public float Magnitude;
        public TargetableEntity Host;
        public TargetableEntity Owner;
        public string Unique;
        public BuffType Type;
        public AbilityElementType Element;
        public Color Color;

        public BuffParticleEngine ParticleEngine;

        public BaseBuff(string unique, double duration, double magnitude, TargetableEntity host, TargetableEntity owner, BuffType type, AbilityElementType element)
        {
            Started = false;
            Duration = (float)duration * 1000f;
            //Console.WriteLine(duration);
            if (duration != -1)
            {
                TotalDuration = (float)duration * 1000f;
            }
            else
            {
                
                TotalDuration = 1000;
            }
            //Console.WriteLine(TotalDuration);
            Magnitude = (float)magnitude;
            Host = host;
            Owner = owner;
            Unique = unique;
            Type = type;
            Element = element;

            Color = HelperMethods.GetColor(element);

            if (host != null)
            {
                ParticleEngine = new BuffParticleEngine(GameState.CurrentZone.Textures, new Vector2(400, 240), Color, Host);
            }
        }

        public BaseBuff(BaseBuff buff, TargetableEntity host, TargetableEntity owner)
        {
            Started = false;
            Duration = buff.Duration;
            TotalDuration = buff.TotalDuration;
            Magnitude = buff.Magnitude;
            Host = host;
            Owner = owner;
            Unique = buff.Unique;
            Type = buff.Type;
            Element = buff.Element;

            Color = buff.Color;

            if (host != null)
            {
                //Console.WriteLine("Test");
                ParticleEngine = new BuffParticleEngine(GameState.CurrentZone.Textures, new Vector2(400, 240), Color, Host);
            }
        }

        public virtual void DrawBuff(SpriteBatch spriteBatch)
        {
            ParticleEngine.Draw(spriteBatch);
        }

        public virtual bool StartBuff(GameTime gameTime)
        {
            if (Started)
            {
                return false;
            }
            //Console.WriteLine("Check");
            Started = true;

            return true;
        }

        public virtual bool RunBuff(GameTime gameTime)
        {
            ParticleEngine.EmitterLocation = new Vector2(Host.Position.X, Host.Position.Y);
            ParticleEngine.Update();
            
            float time = (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            if (Duration != -1000)
            {
                Duration -= time;

                //Console.WriteLine(Duration);

                if (Duration < 0)
                {
                    return false;
                }
            }

            return true;
        }

        public virtual void StopBuff(GameTime gameTime)
        {
        }
    }
}
