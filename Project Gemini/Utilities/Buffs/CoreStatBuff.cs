﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectGemini.Entities;
using ProjectGemini.EnumTypes;
using ProjectGemini.Static_Classes;
using Microsoft.Xna.Framework;

namespace ProjectGemini.Utilities.Buffs
{
    class CoreStatBuff : BaseBuff
    {
        string Stat;

        public CoreStatBuff(string unique, double duration, double magnitude, TargetableEntity host, TargetableEntity owner, string stat, BuffType type, AbilityElementType element)
            :base(unique, duration, magnitude, host, owner, type, element)
        {
            Stat = stat;
        }

        public CoreStatBuff(CoreStatBuff buff, TargetableEntity host, TargetableEntity owner)
            : base(buff, host, owner)
        {
            Stat = buff.Stat;
        }

        public override bool StartBuff(GameTime gameTime)
        {
            if (!base.StartBuff(gameTime))
            {
                return false;
            }

            if (Stat == "Damage")
            {
                Host.ModDam += Magnitude;
            }
            else if (Stat == "Magic")
            {
                Host.ModMag += Magnitude;
            }
            else if (Stat == "Heal")
            {
                Host.ModHea += Magnitude;
            }
            else if (Stat == "Defense")
            {
                Host.ModDef += Magnitude;
            }
            else if (Stat == "Resistance")
            {
                Host.ModRes += Magnitude;
            }

            Host.UpdateStats();

            return true;
        }

        public override void StopBuff(GameTime gameTime)
        {
            if (Stat == "Damage")
            {
                Host.ModDam -= Magnitude;
            }
            else if (Stat == "Magic")
            {
                Host.ModMag -= Magnitude;
            }
            else if (Stat == "Heal")
            {
                Host.ModHea -= Magnitude;
            }
            else if (Stat == "Defense")
            {
                Host.ModDef -= Magnitude;
            }
            else if (Stat == "Resistance")
            {
                Host.ModRes -= Magnitude;
            }

            Host.UpdateStats();
        }
    }
}
