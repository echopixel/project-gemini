﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectGemini.Entities;
using ProjectGemini.EnumTypes;
using ProjectGemini.Static_Classes;
using Microsoft.Xna.Framework;

namespace ProjectGemini.Utilities.Buffs
{
    class MovementSpeedBuff : BaseBuff
    {
        public MovementSpeedBuff(string unique, double duration, double magnitude, TargetableEntity host, TargetableEntity owner, BuffType type, AbilityElementType element)
            :base(unique, duration, magnitude, host, owner, type, element)
        {
        }

        public MovementSpeedBuff(MovementSpeedBuff buff, TargetableEntity host, TargetableEntity owner)
            : base(buff, host, owner)
        {
        }

        public override bool RunBuff(GameTime gameTime)
        {
            Host.MovementSpeed += Host.BaseSpeed * Magnitude;
            

            return base.RunBuff(gameTime);
        }
    }
}
