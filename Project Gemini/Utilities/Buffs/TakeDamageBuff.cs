﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectGemini.Entities;
using ProjectGemini.EnumTypes;
using ProjectGemini.Static_Classes;
using Microsoft.Xna.Framework;

namespace ProjectGemini.Utilities.Buffs
{
    class TakeDamageBuff : BaseBuff
    {
        public float Buildup;

        public TakeDamageBuff(string unique, double duration, double magnitude, TargetableEntity host, TargetableEntity owner, AbilityElementType element, BuffType type)
            :base(unique, duration, magnitude, host, owner, type, element)
        {
            Element = element;

            Color = HelperMethods.GetColor(Element);

            Buildup = 0;
        }

        public TakeDamageBuff(TakeDamageBuff buff, TargetableEntity host, TargetableEntity owner, float mod)
            : base(buff, host, owner)
        {
            Element = buff.Element;

            Magnitude = Magnitude * mod;

            Buildup = 0;
        }

        public override bool RunBuff(GameTime gameTime)
        {
            float time = (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            if (Element == AbilityElementType.Physical)
            {
                Buildup += (Magnitude * Owner.Damage / TotalDuration) * time * (1f - Host.DefenseRating / 100f);
            }
            else
            {
                Buildup += (Magnitude * Owner.Magic / TotalDuration) * time * (1f - Host.ResistanceRating / 100f);
            }

            while (Buildup > 1)
            {
                Host.TakeBuffDamage(1, Element, false);
                Buildup -= 1f;
            }

            return base.RunBuff(gameTime);
        }
    }
}
