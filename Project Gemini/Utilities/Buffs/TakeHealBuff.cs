﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectGemini.Entities;
using ProjectGemini.EnumTypes;
using ProjectGemini.Static_Classes;
using Microsoft.Xna.Framework;

namespace ProjectGemini.Utilities.Buffs
{
    class TakeHealBuff : BaseBuff
    {
        public float Buildup;
        public bool CountHealStat;

        public TakeHealBuff(string unique, double duration, double magnitude, TargetableEntity host, TargetableEntity owner, bool countHealStat, BuffType type, AbilityElementType element)
            :base(unique, duration, magnitude, host, owner, type, element)
        {
            Buildup = 0;
            CountHealStat = countHealStat;
            //Console.WriteLine(countHealStat+ " " + CountHealStat);
        }

        public TakeHealBuff(TakeHealBuff buff, TargetableEntity host, TargetableEntity owner, float mod)
            : base(buff, host, owner)
        {
            Magnitude = Magnitude * mod;
            CountHealStat = buff.CountHealStat;

            Buildup = 0;
        }

        public override bool RunBuff(GameTime gameTime)
        {
            float time = (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            if (CountHealStat)
            {
                //Console.WriteLine("true");
                Buildup += (Magnitude * Owner.Heal / TotalDuration) * time;
            }
            else
            {
                //Console.WriteLine("false");
                Buildup += (Magnitude / TotalDuration) * time;
            }

            //Console.WriteLine( (Magnitude * Owner.Heal / TotalDuration) * time);
            
            while (Buildup > 1)
            {
                Host.TakeBuffHeal(1);
                Buildup -= 1f;
            }

            return base.RunBuff(gameTime);
        }
    }
}
