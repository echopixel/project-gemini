﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectGemini.EnumTypes;

namespace ProjectGemini.Utilities
{
    public class Inventory
    {
        public List<Item> items;
        public List<Item> keys;
        public List<Armor> heads;
        public List<Armor> chests;
        public List<Armor> legs;
        public List<Weapon> weapons;

        public Inventory()
        {
            items = new List<Item>();
            keys  = new List<Item>();
            heads = new List<Armor>();
            chests = new List<Armor>();
            legs = new List<Armor>();
            weapons = new List<Weapon>();
        }

        public void Clear()
        {
            // Make foreach loop to clear current stacks...
            items.Clear();
            keys.Clear();
            heads.Clear();
            chests.Clear();
            legs.Clear();
            weapons.Clear();
        }

        public bool AddRemoveItem(BaseItem item, int amount)
        {
            if (item is Item)
            {
                if (!((Item)item).itemIsKey)
                {
                    return baseItemAdd(((Item)item), items, amount);
                }

                return baseItemAdd(((Item)item), keys, amount);
            }
            if (item is Armor)
            {
                switch (((Armor)item).equipSlot)
                {
                    case EquipmentSlotType.Head:
                        return baseItemAdd(((Armor)item), heads, amount);
                    case EquipmentSlotType.Chest:
                        return baseItemAdd(((Armor)item), chests, amount);
                    case EquipmentSlotType.Legs:
                        return baseItemAdd(((Armor)item), legs, amount);
                }
            }
            if (item is Weapon)
            {
                return baseItemAdd(((Weapon)item), weapons, amount);
            }

            return false;
        }

        private bool baseItemAdd<T>(T item, List<T> list, int count) where T : BaseItem
        {
            if (list.Contains(item))
            {
                int location = list.IndexOf(item);

                if (list[location].itemCurrentStack >= list[location].itemStackSize && count > 0)
                {
                    return false;
                }
                else
                {
                    list[location].itemCurrentStack += count;

                    if (list[location].itemCurrentStack <= 0)
                    {
                        list.Remove(item);
                    }

                    return true;
                }
            }
            else if(count > 0)
            {
                list.Add(item);
                list[list.IndexOf(item)].itemCurrentStack += count;

                return true;
            }

            return false;
        }

        public string SaveOutput()
        {
            string output = "";

            output += "Items\n";
            foreach (Item item in items)
            {
                output += item.itemId + ":" + item.itemCurrentStack + "\n";
            }

            output += "Keys\n";
            foreach (Item item in keys)
            {
                output += item.itemId + ":" + item.itemCurrentStack + "\n";
            }

            output += "Armors\n";
            foreach (Armor item in heads)
            {
                output += item.itemId + ":" + item.itemCurrentStack + "\n";
            }
            foreach (Armor item in chests)
            {
                output += item.itemId + ":" + item.itemCurrentStack + "\n";
            }
            foreach (Armor item in legs)
            {
                output += item.itemId + ":" + item.itemCurrentStack + "\n";
            }

            output += "Weapons\n";
            foreach (Weapon item in weapons)
            {
                output += item.itemId + ":lvl" + item.upgradeLevel + ":" + item.itemCurrentStack + "\n";
            }

            //Console.WriteLine(output);

            return output;
        }
    }
}
