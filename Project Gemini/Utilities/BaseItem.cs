﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectGemini.EnumTypes;
using Microsoft.Xna.Framework.Graphics;

namespace ProjectGemini.Utilities
{
    public abstract class BaseItem
    {
        public int itemId { get; protected set; }
        public string itemName { get; protected set; }
        public string itemDescription { get; protected set; }
        public string itemIcon { get; protected set; }
        public Texture2D itemIconTexture;

        public Dictionary<ItemEffectType, int> itemEffect { get; protected set; }
        public int itemStackSize { get; protected set; }
        public int itemCurrentStack { get; set; }

        public override string ToString()
        {
            return itemId + " " + itemName + " " + itemDescription;
        }
    }
}
