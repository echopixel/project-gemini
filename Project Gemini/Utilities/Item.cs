﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectGemini.EnumTypes;

namespace ProjectGemini.Utilities
{
    public class Item : BaseItem
    {
        public TargetType itemTarget { get; private set; }
        public bool itemIsKey { get; private set; }

        /// <summary>
        /// Creates an item.
        /// </summary>
        /// <param name="id">The unique id number for the item.</param>
        /// <param name="name">The name of the item.</param>
        /// <param name="description">The description of the item.</param>
        /// <param name="icon">The location of the image for the item.</param>
        /// <param name="effect">What the item does when used and the magnitude of the effect.</param>
        /// <param name="target">The valid targets of the item.</param>
        /// <param name="stackSize">The max number of items that can be held at once.</param>
        /// <param name="isKey">If the item is used for the story.</param>
        public Item(
            int id,
            string name,
            string description,
            string icon,
            Dictionary<ItemEffectType, int> effect,
            TargetType target,
            int stackSize,
            string isKey)
        {
            itemId = id;
            itemName = name;
            itemDescription = description;
            itemIcon = icon;
            itemEffect = effect;
            itemTarget = target;
            itemStackSize = stackSize;
            itemCurrentStack = 0;
            if(isKey == "true")
                itemIsKey = true;
            if(isKey == "false")
                itemIsKey = false;
        }
    }
}
