﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectGemini.EnumTypes;
using Microsoft.Xna.Framework.Graphics;

namespace ProjectGemini.Utilities
{
    public class Ability
    {
        public int abilityId { get; private set; }
        public string abilityName { get; private set; }
        public string abilityIcon { get; private set; }
        public Texture2D abilityIconTexture { get; set; }
        public string abilityDescription { get; private set; }
        public double abilityDamageFormula { get; private set; }
        public AbilityElementType abilityElement { get; private set; }
        public AbilityProjectileType abilityProjectileType { get; private set; }
        public float abilityAoERadius { get; private set; }
        public TargetType abilityTargetType { get; private set; }
        //placeholder for ability buff/debuff. needs to use buff/debuff class
        public List<string> abilityPassive { get; private set; }
        public string abilityAnimation { get; private set; }
        public float abilityCastTime { get; private set; }
        public int abilityResourceCost { get; private set; }
        public int abilityPersonalCost { get; private set; }
        public string user;
        public List<BaseBuff> Debuffs;
        public List<BaseBuff> Buffs;
        public double Cooldown;
        public double CooldownTimer;

        /// <summary>
        /// Creates an ability.
        /// </summary>
        /// <param name="id">The unique id number for this ability.</param>
        /// <param name="name">The name for the ability.</param>
        /// <param name="icon">The sprite location of the ability icon.</param>
        /// <param name="description">The description of the ability.</param>
        /// <param name="damageFormula">The way to calculate the damage done by the ability.</param>
        /// <param name="element">The ability's element type.</param>
        /// <param name="projectileType">How the ability targets.</param>
        /// <param name="AoERadius">If the ability has an AoE, it will have a radius.</param>
        /// <param name="targetType">If the ability targets allies or enemies.</param>
        /// <param name="passive">The buffs/debuffs the ability applied to target.</param>
        /// <param name="animation">What the ability looks like when used.</param>
        /// <param name="castTime">What the abilities cast time is.</param>
        /// <param name="resourceCost">The amount of mp the ability uses.</param>
        public Ability(
            int id,
            string name,
            string icon,
            string description,
            double damageFormula,
            AbilityElementType element,
            AbilityProjectileType projectileType,
            float AoERadius,
            TargetType targetType,
            List<string> passive,
            string animation,
            float castTime,
            int resourceCost,
            int personalCost,
            string user,
            List<BaseBuff> debuffs,
            List<BaseBuff> buffs,
            double cooldown)
            
        {
            abilityId = id;
            abilityName = name;
            abilityIcon = icon;
            abilityDescription = description;
            abilityDamageFormula = damageFormula;
            abilityElement = element;
            abilityProjectileType = projectileType;
            abilityAoERadius = AoERadius;
            abilityTargetType = targetType;
            abilityPassive = passive;
            abilityAnimation = animation;
            abilityCastTime = castTime;
            abilityResourceCost = resourceCost;
            abilityPersonalCost = personalCost;
            this.user = user;
            Debuffs = debuffs;
            Buffs = buffs;
            Cooldown = cooldown;
            CooldownTimer = 0;
        }

        public Ability() { }

        /// <summary>
        /// Returns the ability id and ability name
        /// </summary>
        /// <returns>A string that gives readable information about the ability.</returns>
        public override string ToString()
        {
            return abilityId + " " + abilityName;
        }
    }
}
