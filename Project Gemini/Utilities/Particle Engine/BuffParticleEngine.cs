﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ProjectGemini.Utilities;
using ProjectGemini.Entities;

namespace ParticleEngine2D
{
    public class BuffParticleEngine
    {


        private Random random;
        public Vector2 EmitterLocation { get; set; }
        private List<BuffParticle> particles;
        private List<Texture2D> textures;
        private Color Color;
        private TargetableEntity Host;

        public BuffParticleEngine(List<Texture2D> textures, Vector2 location, Color color, TargetableEntity host)
        {
            EmitterLocation = location;
            this.textures = textures;
            this.particles = new List<BuffParticle>();
            random = new Random();
            Color = color;
            Host = host;
        }

        public void Update()
        {
            if (particles.Count < 2)
            {
                particles.Add(GenerateNewParticle());
            }

            for (int particle = 0; particle < particles.Count; particle++)
            {
                particles[particle].Update();
                if (particles[particle].TTL <= 0)
                {
                    particles.RemoveAt(particle);
                    particle--;
                }
            }
        }

        private BuffParticle GenerateNewParticle()
        {
            Texture2D texture = textures[0];
            Vector2 position = new Vector2((float)(random.NextDouble() - .5f) * 1.2f * Host.Width + EmitterLocation.X, (float)(random.NextDouble() - .5f) * 1.2f * Host.Height + EmitterLocation.Y);
            Vector2 velocity = new Vector2(
                                    1f * (float)(random.NextDouble() * 2 - 1),
                                    1f * (float)(random.NextDouble() * 2 - 1));
            float angle = 0;
            float angularVelocity = 0.1f * (float)(random.NextDouble() * 2 - 1);
            float size = (float)random.NextDouble() + 1;
            int ttl = 10 + random.Next(5);

            return new BuffParticle(texture, position, velocity, angle, angularVelocity, Color, size / 12, ttl);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            //spriteBatch.Begin();
            for (int index = 0; index < particles.Count; index++)
            {
                particles[index].Draw(spriteBatch);
            }
            //spriteBatch.End();
        }
    }
}