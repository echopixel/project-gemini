﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ProjectGemini.Utilities;

namespace ParticleEngine2D
{
    public class DamageParticleEngine
    {


        private Random random;
        public Vector2 EmitterLocation { get; set; }
        private List<DamageParticle> particles;
        private List<Texture2D> textures;
        public Color Color;
        public float Live;
        public double Size;

        public DamageParticleEngine(List<Texture2D> textures, Vector2 location, Color color, double size)
        {
            EmitterLocation = location;
            this.textures = textures;
            this.particles = new List<DamageParticle>();
            random = new Random();
            Color = color;
            Live = 0;
            Size = size;
        }

        public void Update(GameTime gameTime)
        {
            if (Live > 0)
            {
                for (int i = 0; i < 10; i++)
                {
                    particles.Add(GenerateNewParticle());
                }

                Live -= (float)gameTime.ElapsedGameTime.TotalMilliseconds;

                if (Live < 0)
                {
                    Live = 0;
                }
            }

            for (int particle = 0; particle < particles.Count; particle++)
            {
                particles[particle].Update();
                if (particles[particle].TTL <= 0)
                {
                    particles.RemoveAt(particle);
                    particle--;
                }
            }
        }

        private DamageParticle GenerateNewParticle()
        {
            Texture2D texture = textures[1];
            Vector2 position = EmitterLocation;
            Vector2 velocity = new Vector2(
                                    (1f * (float)(random.NextDouble() * 2 - 1)) * (float)Size,
                                    (1f * (float)(random.NextDouble() * 2 - 1)) * (float)Size);
            float angle = 0;
            float angularVelocity = 0.1f * (float)(random.NextDouble() * 2 - 1);
            float size = (float)random.NextDouble() + 1;
            int ttl = 10 + random.Next(5);

            return new DamageParticle(texture, position, velocity, angle, angularVelocity, Color, size / 12, ttl);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            //spriteBatch.Begin();
            for (int index = 0; index < particles.Count; index++)
            {
                particles[index].Draw(spriteBatch);
            }
            //spriteBatch.End();
        }
    }
}