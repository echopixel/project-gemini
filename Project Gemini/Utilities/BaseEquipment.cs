﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectGemini.EnumTypes;

namespace ProjectGemini.Utilities
{
    public abstract class BaseEquipment : BaseItem
    {
        public EquipmentSlotType equipSlot { get; protected set; }
        public int equipHpmod { get; protected set; }
        public int equipMpmod { get; protected set; }
        public int equipDamageMod { get; protected set; }
        public int equipMagicMod { get; protected set; }
        public int equipHealMod { get; protected set; }
        public int equipDefenseMod { get; protected set; }
        public int equipResistanceMod { get; protected set; }
        public int equipStrmod { get; protected set; }
        public int equipIntmod { get; protected set; }
        public int equipDexmod { get; protected set; }
        public int equipAlamod { get; protected set; }
        public int equipStamod { get; protected set; }
        public int equipWismod { get; protected set; }
        //placeholder, needs to be buff/debuff class.
        public List<BaseBuff> equipPassives { get; protected set; }
        public List<BaseBuff> onHero { get; protected set; }

        public BaseEquipment()
        {
            onHero = new List<BaseBuff>();
            itemCurrentStack = 0;
            itemStackSize = 99;
        }
    }
}