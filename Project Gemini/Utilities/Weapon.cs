﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectGemini.EnumTypes;

namespace ProjectGemini.Utilities
{
    public class Weapon : BaseEquipment
    {
        public int weaponTier { get; private set; }
        public int upgradeLevel { get; set; }
        //not sure what this should be.
        public List<string> weaponUpgrades { get; private set; }

        public Weapon(
            int id,
            string name,
            string description,
            string icon,
            EquipmentSlotType slot,
            int tier,
            int hpmod,
            int mpmod,
            int damagemod,
            int magicmod,
            int healmod,
            int defencemod,
            int resistancemod,
            int strmod,
            int intmod,
            int dexmod,
            int alamod,
            int stamod,
            int wismod,
            //placeholder, needs to be buff/debuff class.
            List<BaseBuff> passives,
            //not sure what this should be.
            List<string> upgrades)
        {
            itemId = id;
            itemName = name;
            itemDescription = description;
            itemIcon = icon;
            equipSlot = slot;
            weaponTier = tier;
            equipHpmod = hpmod;
            equipMpmod = mpmod;
            equipDamageMod = damagemod;
            equipMagicMod = magicmod;
            equipHealMod = healmod;
            equipDefenseMod = defencemod;
            equipResistanceMod = resistancemod;
            equipStrmod = strmod;
            equipIntmod = intmod;
            equipDexmod = dexmod;
            equipAlamod = alamod;
            equipStamod = stamod;
            equipWismod = wismod;
            equipPassives = passives;
            weaponUpgrades = upgrades;

            upgradeLevel = 0;
            itemStackSize = 99;
        }

        public Weapon(Weapon other)
        {
            itemId = other.itemId;
            itemName = other.itemName;
            itemDescription = other.itemDescription;
            itemIcon = other.itemIcon;
            equipSlot = other.equipSlot;
            weaponTier = other.weaponTier;
            equipHpmod = other.equipHpmod;
            equipMpmod = other.equipMpmod;
            equipDamageMod = other.equipDamageMod;
            equipMagicMod = other.equipMagicMod;
            equipHealMod = other.equipHealMod;
            equipDefenseMod = other.equipDefenseMod;
            equipResistanceMod = other.equipResistanceMod;
            equipStrmod = other.equipStrmod;
            equipIntmod = other.equipIntmod;
            equipDexmod = other.equipDexmod;
            equipAlamod = other.equipAlamod;
            equipStamod = other.equipStamod;
            equipWismod = other.equipWismod;
            equipPassives = other.equipPassives;
            weaponUpgrades = other.weaponUpgrades;

            upgradeLevel = other.upgradeLevel;
            itemStackSize = other.itemStackSize;
        }
    }
}
