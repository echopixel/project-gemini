﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using FuncWorks.XNA.XTiled;
using ProjectGemini.Entities;
using ProjectGemini.Managers;

namespace ProjectGemini.Controllers
{
    public class Camera
    {
        private Vector2 _cameraVector;

        public Hero HeroToFollow { get; set; }
        public Vector2 FollowPosition { get; set; }
        public Map CurrentMap { get; set; }
        public bool IsFollowingHero { get; set; }
        public EventManager EventManager { get; set; }

        /// <summary>
        /// Creates a camera vector for the screen to follow and clamp.
        /// </summary>
        /// <param name="followPosition">The position the camera should follow.</param>
        /// <param name="map">The map that the camera will get its bounds from.</param>
        public Camera()
        {
            this.EventManager = new EventManager();
            _cameraVector = Vector2.Zero;
        }

        public void FollowVector2Position(ref Vector2 followPosition, ref Map map)
        {
            FollowPosition = followPosition;
            CurrentMap = map;
            IsFollowingHero = false;
        }

        public void FollowHeroPosition(ref Hero hero, ref Map map)
        {
            HeroToFollow = hero;
            CurrentMap = map;
            IsFollowingHero = true;
        }

        /// <summary>
        /// Follows a specified vector2, will usually be a character's position but can be set to any vector2 for scenic camera motion.
        /// Also clamps the camera to the map so it cannot go out of its bounds.
        /// </summary>
        public void Update()
        {
            if (IsFollowingHero)
            {
                if (HeroToFollow.IsAttacking)
                {
                    _cameraVector.X = HeroToFollow.AttackPosition.X - (ResolutionManager.VirtualWidth / 2);
                    _cameraVector.Y = HeroToFollow.AttackPosition.Y + 3 - (ResolutionManager.VirtualHeight / 2);
                }
                else
                {
                    _cameraVector.X = HeroToFollow.Position.X - (ResolutionManager.VirtualWidth / 2);
                    _cameraVector.Y = HeroToFollow.Position.Y - (ResolutionManager.VirtualHeight / 2);
                }
                Vector2 cameraMax = new Vector2(
                    CurrentMap.Bounds.Width - ResolutionManager.VirtualWidth,
                    CurrentMap.Bounds.Height - ResolutionManager.VirtualHeight);
                _cameraVector = Vector2.Clamp(_cameraVector, Vector2.Zero, cameraMax);
            }
            else
            {
                _cameraVector.X = FollowPosition.X - (ResolutionManager.VirtualWidth / 2);
                _cameraVector.Y = FollowPosition.Y - (ResolutionManager.VirtualHeight / 2);
                Vector2 cameraMax = new Vector2(
                    CurrentMap.Bounds.Width - ResolutionManager.VirtualWidth,
                    CurrentMap.Bounds.Height - ResolutionManager.VirtualHeight);
                _cameraVector = Vector2.Clamp(_cameraVector, Vector2.Zero, cameraMax);
            }
        }

        public Vector2 getCameraVector { get { return _cameraVector; } }

    }
}
