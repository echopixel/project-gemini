﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ProjectGemini.Controllers
{
    public class PathPoint
    {
        public Vector2 Direction;
        public TimeSpan Duration;
        public PathPoint next;

        public PathPoint(Vector2 Direction, TimeSpan Duration)
        {
            this.Direction = Direction;
            this.Duration = Duration;
            this.next = null;
        }

        public PathPoint Next()
        {
            return next;
        }
    }
}