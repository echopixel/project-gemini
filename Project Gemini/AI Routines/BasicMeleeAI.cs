﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectGemini.Entities;
using Microsoft.Xna.Framework;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using ProjectGemini.EnumTypes;

namespace ProjectGemini.AI_Routines
{
    class BasicMeleeAI : AI
    {
        private Hero _target;

        public BasicMeleeAI()
        {
        }

        public override void Update(Enemy enemy, Hero hero)
        {
            AcquireTarget(enemy, hero);

            if (enemy.IsActive)
            {
                MoveTowardsTarget(enemy);
            }
        }

        private void AcquireTarget(Enemy enemy, Hero hero)
        {
            if (Vector2.Distance(enemy.Position, hero.Position) < 125)
            {
                _target = hero;
                enemy.IsActive = true;
            }
            else
            {
                _target = null;
                enemy.IsActive = false;
                enemy.Body.LinearVelocity = Vector2.Zero;
            }
        }

        private void MoveTowardsTarget(Enemy enemy)
        {
            if (!enemy.HeroWithinRange)
                enemy.Body.LinearVelocity = Vector2.Normalize(_target.Body.Position - enemy.Body.Position) * enemy.MovementSpeed;
        }

        #region Delegate Methods

        public override OnCollisionEventHandler GetMeleeRangeSensorOnCollisionListener()
        {
            return new OnCollisionEventHandler(MeleeAI_OnCollision);
        }

        public override OnSeparationEventHandler GetMeleeRangeSensorOnSeparationListener()
        {
            return new OnSeparationEventHandler(MeleeAI_OnSeparation);
        }

        public override OnCollisionEventHandler GetMeleeAttackOnCollisionListener()
        {
            return new OnCollisionEventHandler(MeleeAttack_OnCollision);
        }

        private bool MeleeAI_OnCollision(Fixture fixtureA, Fixture fixtureB, Contact contact)
        {
            if (fixtureB.UserData != null && fixtureB.UserData is Hero)
            {
                //Console.WriteLine("Hero within melee range");
                ((Enemy)fixtureA.Body.UserData).IsAttacking = true;
                ((Enemy)fixtureA.Body.UserData).HeroWithinRange = true;
                ((Enemy)fixtureA.Body.UserData).LeftDamageFixture.CollidesWith = Category.All;
                ((Enemy)fixtureA.Body.UserData).RightDamageFixture.CollidesWith = Category.All;
                ((Enemy)fixtureA.Body.UserData).TopDamageFixture.CollidesWith = Category.All;
                ((Enemy)fixtureA.Body.UserData).BottomDamageFixture.CollidesWith = Category.All;
                return true;
            }
            return false;
        }

        public void MeleeAI_OnSeparation(Fixture fixtureA, Fixture fixtureB)
        {
            if (fixtureB.UserData != null && fixtureB.UserData is Hero)
            {
                ((Enemy)fixtureA.Body.UserData).HeroWithinRange = false;
                //Console.WriteLine("Hero not in range.");
            }
        }

        private bool MeleeAttack_OnCollision(Fixture fixtureA, Fixture fixtureB, Contact contact)
        {
            if (fixtureB.UserData != null && fixtureB.UserData is Hero)
            {
                //Console.WriteLine("Enemy attack collision happened");
                return ((TargetableEntity)fixtureB.UserData).TakeDamage((int)(((Enemy)fixtureA.Body.UserData).Damage * ((TargetableEntity)fixtureB.UserData).damageVariance()), AbilityElementType.Physical, true, true);
            }
            return false;
        }

        #endregion
    }
}
