﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectGemini.Entities;
using ProjectGemini.EnumTypes;
using FarseerPhysics.Dynamics;

namespace ProjectGemini.AI_Routines
{
    public abstract class AI
    {
        static public AI GetAIInstance(EnemyAIType enemyAI)
        {
            switch (enemyAI)
            {
                case EnemyAIType.Melee:
                    return new BasicMeleeAI();
                case EnemyAIType.Caster:
                    return new BasicCasterAI();
                case EnemyAIType.Boss:
                    return new BasicCasterAI();
                default:
                    {
                        //Console.WriteLine("Invalid enemy AI type given.  No suitable AI instance available.");
                        return null;
                    }
            }
        }

        public abstract void Update(Enemy enemy, Hero hero);

        public virtual OnCollisionEventHandler GetMeleeRangeSensorOnCollisionListener()
        {
            //Console.WriteLine("Base implementation.  Returning null.");
            return null;
        }

        public virtual OnSeparationEventHandler GetMeleeRangeSensorOnSeparationListener()
        {
            //Console.WriteLine("Base implementation.  Returning null.");
            return null;
        }

        public virtual OnCollisionEventHandler GetMeleeAttackOnCollisionListener()
        {
            //Console.WriteLine("Base implementation.  Returning null.");
            return null;
        }

    }
}
