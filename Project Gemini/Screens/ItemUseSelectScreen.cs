#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

using ProjectGemini.Static_Classes;
using ProjectGemini.EnumTypes;
using ProjectGemini.Utilities;
using ProjectGemini.Entities;
using System;
#endregion

namespace ProjectGemini
{
    /// <summary>
    /// The main menu screen is the first thing displayed when the game starts up.
    /// </summary>
    class ItemUseSelectScreen : BaseMenu
    {
        Item item;
        InventoryMenuScreen menu;

        #region Initialization

        /// <summary>
        /// Constructor fills in the menu contents.
        /// </summary>
        public ItemUseSelectScreen(Item item, InventoryMenuScreen menu)
            : base("Use " + item.itemName + " on:", Color.White, 81, 230, 3, 200, 90, 100)
        {
            this.item = item;
            this.menu = menu;
        }

        /// <summary>
        /// Loads the assets used for this menu screen
        /// </summary>
        public override void LoadContent()
        {
            base.LoadContent();
            
            Texture2D image2 = content.Load<Texture2D>("Buttons/MainMenuSelected");
            Texture2D image1 = content.Load<Texture2D>("Buttons/MainMenu");

            BaseButton hero1Entry = new BaseButton("Hero1", image1, image2, Color.Black); //DONT USE SAME IMAGE TWICE
            BaseButton hero2Entry = new BaseButton("Hero2", image1, image2, Color.Black, Color.Gray);
            BaseButton hero3Entry = new BaseButton("Hero3", image1, image2, Color.Black, Color.Gray);

            // Hook up menu event handlers.
            hero1Entry.Selected += UsedEntrySelected;
            //hero2Entry.Selected += UsedEntrySelected;
            //hero3Entry.Selected += UsedEntrySelected;

            MenuEntries.Add(hero1Entry);
            MenuEntries.Add(hero2Entry);
            MenuEntries.Add(hero3Entry);
        }

        /// <summary>
        /// Unloads the assets for this menu screen.
        /// </summary>
        public override void UnloadContent()
        {
            if (content != null)
                content.Unload();
        }

        #endregion

        #region Handle Input

        /// <summary>
        /// Event handler for when the Options menu entry is selected.
        /// </summary>
        void UsedEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            //Console.WriteLine(item.itemEffect + " " + item.itemEffectMagnitude);
            bool itemWasUsed = false;

            foreach (KeyValuePair<ItemEffectType, int> effect in item.itemEffect)
            {
                if (GameState.HeroOne.effectedBy(effect.Key, effect.Value) && itemWasUsed == false)
                {
                    //Console.WriteLine("item used");
                    GameState.Inventory.AddRemoveItem(item, -1);
                    itemWasUsed = true;
                }
            }

            if (GameState.HeroOne.SelectedItem >= GameState.Inventory.items.Count)
            {
                GameState.HeroOne.SelectedItem = 0;
            }
            else if (GameState.HeroOne.SelectedItem < 0)
            {
                GameState.HeroOne.SelectedItem = GameState.Inventory.items.Count - 1;
            }
            if (GameState.HeroOne.SelectedItem == -1)
            {
                GameState.HeroOne.SelectedItem = 0;
            }

            /*
            if (item.itemEffect == EnumTypes.ItemEffectType.HpRestore)
            {
                if (GameState.HeroOne.HealHPPercent(item.itemEffectMagnitude))
                {
                    GameState.Inventory.removeItem(item);
                }
            }
            else if (item.itemEffect == EnumTypes.ItemEffectType.MpRestore)
            {
                if (GameState.HeroOne.HealMPPercent(item.itemEffectMagnitude))
                {
                    GameState.Inventory.removeItem(item);
                }
            }
            else if (item.itemEffect == EnumTypes.ItemEffectType.HpMpRestore)
            {
                bool itemWasUsed = false;
                if (GameState.HeroOne.HealHPPercent(item.itemEffectMagnitude))
                {
                    GameState.Inventory.removeItem(item);
                    itemWasUsed = true;
                }
                if (GameState.HeroOne.HealMPPercent(item.itemEffectMagnitude) && itemWasUsed == false)
                {
                    GameState.Inventory.removeItem(item);
                }
            }
             * */
            
            menu.Reload();
            OnCancel(sender, e);
        }

        #endregion
    }
}
