#region File Description
//-----------------------------------------------------------------------------
// MenuScreen.cs
//
// XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Input;

using ProjectGemini.EnumTypes;
using ProjectGemini.GUI;
#endregion

namespace ProjectGemini
{
    /// <summary>
    /// Base class for screens that contain a menu of options. The user can
    /// move up and down to select an entry, or cancel to back out of the screen.
    /// </summary>
    abstract class InventoryMenu : BaseMenu
    {
        #region Fields

        #endregion

        #region Initialization


        /// <summary>
        /// Constructor for the base class that menus inherit from.
        /// </summary>
        /// <param name="menuTitle">The text to display as a title.</param>
        /// <param name="menuTitleColor">The color to display the text in.</param>
        /// <param name="menuType">The type of menu using the Enum MenuType</param>
        public InventoryMenu(string menuTitle, Color menuTitleColor, int xIndent, int xOffset, int xCount, int yIndent, int yOffset, int yCount)
            : base(menuTitle, menuTitleColor, xIndent, xOffset, xCount, yIndent, yOffset, yCount)
        {
            
        }

        public override void LoadContent()
        {
            base.LoadContent();
        }

        #endregion

        #region Update and Draw

        /// <summary>
        /// Updates the menu.
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);
        }

        /// <summary>
        /// Draws the menu.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            GraphicsDevice graphics = ScreenManager.GraphicsDevice;
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            SpriteFont font = ScreenManager.Font;

            spriteBatch.Begin();

            string description = "";

            if (!menuEntries[selectedEntry].Text.Equals("Inventory Empty"))
            {
                description = ((InventoryButton)menuEntries[selectedEntry]).item.itemDescription + "\n";
                description += "Count: " + ((InventoryButton)menuEntries[selectedEntry]).item.itemCurrentStack;
            }

            spriteBatch.DrawString(font, description, new Vector2(xIndent / 2, graphics.Viewport.Height * 23 / 30), Color.White, 0,
                    new Vector2(0, 0), 1, SpriteEffects.None, 0.0f);

            spriteBatch.End();
        }


        #endregion
    }
}
