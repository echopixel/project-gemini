#region File Description
//-----------------------------------------------------------------------------
// MenuEntry.cs
//
// XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;
using ProjectGemini.Utilities;
using ProjectGemini.Static_Classes;
#endregion

namespace ProjectGemini
{
    /// <summary>
    /// Helper class represents a single entry in a MenuScreen. By default this
    /// just draws the entry text string, but it can be customized to display menu
    /// entries in different ways. This also provides an event that will be raised
    /// when the menu entry is selected.
    /// </summary>
    class SkillButton: BaseButton
    {
        #region Fields

        ScreenManager ScreenManager;
        public Ability skill;
        SkillsMenuScreen menu;

        #endregion

        #region Properties
        
        public Texture2D Icon
        {
            get { return icon; }
            set { icon = value; }
        }

        #endregion

        #region Events

        void ItemSelected(object sender, PlayerIndexEventArgs e)
        {
            if (GameState.HeroOne.SkillPoints > 0 && !GameState.HeroOne.Skills.Contains(skill))
            {
                if (bodyColor.Equals(Color.DarkGreen)) //skill.abilityId == 0 || skill.abilityId == 2 || skill.abilityId == 4 || (skill.abilityId == 1 && GameState.HeroOne.skills.Contains(GameState.Abilities[0])) || (skill.abilityId == 3 && GameState.HeroOne.skills.Contains(GameState.Abilities[2])) || (skill.abilityId == 5 && GameState.HeroOne.skills.Contains(GameState.Abilities[4])) || (skill.abilityId == 7 && GameState.HeroOne.skills.Contains(GameState.Abilities[1])) || (skill.abilityId == 8 && GameState.HeroOne.skills.Contains(GameState.Abilities[3])) || (skill.abilityId == 9 && GameState.HeroOne.skills.Contains(GameState.Abilities[5])))
                {
                    GameState.HeroOne.Skills.Add(skill);
                    GameState.HeroOne.SkillPoints--;

                    if (GameState.HeroOne.Skill1 == null)
                    {
                        GameState.HeroOne.Skill1 = skill;
                    }
                    else if (GameState.HeroOne.Skill2 == null)
                    {
                        GameState.HeroOne.Skill2 = skill;
                    }
                    else if (GameState.HeroOne.Skill3 == null)
                    {
                        GameState.HeroOne.Skill3 = skill;
                    }
                    else if (GameState.HeroOne.Skill4 == null)
                    {
                        GameState.HeroOne.Skill4 = skill;
                    }
                    else if (GameState.HeroOne.Skill5 == null)
                    {
                        GameState.HeroOne.Skill5 = skill;
                    }
                    else if (GameState.HeroOne.Skill6 == null)
                    {
                        GameState.HeroOne.Skill6 = skill;
                    }
                    else if (GameState.HeroOne.Skill7 == null)
                    {
                        GameState.HeroOne.Skill7 = skill;
                    }
                    else if (GameState.HeroOne.Skill8 == null)
                    {
                        GameState.HeroOne.Skill8 = skill;
                    }
                    else if (GameState.HeroOne.Skill9 == null)
                    {
                        GameState.HeroOne.Skill9 = skill;
                    }

                    menu.Reload();
                }
            }
            else if (GameState.HeroOne.Skills.Contains(skill))
            {
                ScreenManager.AddScreen(new KeybindingMenuScreen(skill), e.PlayerIndex);
            }

        }

        #endregion

        #region Initialization

        /// <summary>
        /// Constructs a new menu entry with the specified text.
        /// </summary>
        public SkillButton(string text, Texture2D image, Texture2D highlighted, Color color, ScreenManager ScreenManager, Ability skill, SkillsMenuScreen menu, Texture2D icon, Color bodyColor)
            : base(text, image, highlighted, color, bodyColor)
        {
            this.icon = icon;
            this.skill = skill;
            this.ScreenManager = ScreenManager;
            this.Selected += ItemSelected;
            this.menu = menu;
        }

        #endregion

        #region Update and Draw

        /// <summary>
        /// Updates the menu entry.
        /// </summary>
        override public void Update(BaseMenu screen, bool isSelected, GameTime gameTime)
        {
            base.Update(screen, isSelected, gameTime);
        }

        #endregion
    }
}
