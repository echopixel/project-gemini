#region File Description
//-----------------------------------------------------------------------------
// MenuEntry.cs
//
// XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;
using ProjectGemini.Utilities;
#endregion

namespace ProjectGemini
{
    /// <summary>
    /// Helper class represents a single entry in a MenuScreen. By default this
    /// just draws the entry text string, but it can be customized to display menu
    /// entries in different ways. This also provides an event that will be raised
    /// when the menu entry is selected.
    /// </summary>
    class InventoryButton: BaseButton
    {
        #region Fields

        ScreenManager ScreenManager;
        public Item item;
        InventoryMenuScreen menu;

        #endregion

        #region Properties
        
        public Texture2D Icon
        {
            get { return icon; }
            set { icon = value; }
        }

        #endregion

        #region Events

        void ItemSelected(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.AddScreen(new ItemUseSelectScreen(item, menu), e.PlayerIndex);
        }

        #endregion

        #region Initialization

        /// <summary>
        /// Constructs a new menu entry with the specified text.
        /// </summary>
        public InventoryButton(string text, Texture2D image, Texture2D highlighted, Color color, ScreenManager ScreenManager, Item item, InventoryMenuScreen menu, Texture2D icon)
            : base(text, image, highlighted, color)
        {
            this.icon = icon;
            this.item = item;
            this.ScreenManager = ScreenManager;
            this.Selected += ItemSelected;
            this.menu = menu;
        }

        #endregion

        #region Update and Draw

        /// <summary>
        /// Updates the menu entry.
        /// </summary>
        override public void Update(BaseMenu screen, bool isSelected, GameTime gameTime)
        {
            base.Update(screen, isSelected, gameTime);
        }

        #endregion
    }
}
