#region File Description
//-----------------------------------------------------------------------------
// MenuScreen.cs
//
// XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Input;

using ProjectGemini.EnumTypes;
using ProjectGemini.GUI;
using ProjectGemini.Static_Classes;
#endregion

namespace ProjectGemini
{
    /// <summary>
    /// Base class for screens that contain a menu of options. The user can
    /// move up and down to select an entry, or cancel to back out of the screen.
    /// </summary>
    class SkillMenu : BaseMenu
    {
        #region Fields

        #endregion

        #region Initialization


        /// <summary>
        /// Constructor for the base class that menus inherit from.
        /// </summary>
        /// <param name="menuTitle">The text to display as a title.</param>
        /// <param name="menuTitleColor">The color to display the text in.</param>
        /// <param name="menuType">The type of menu using the Enum MenuType</param>
        public SkillMenu(string menuTitle, Color menuTitleColor, int xIndent, int xOffset, int xCount, int yIndent, int yOffset, int yCount)
            : base(menuTitle, menuTitleColor, xIndent, xOffset, xCount, yIndent, yOffset, yCount)
        {
            
        }

        public override void LoadContent()
        {
            base.LoadContent();
        }

        #endregion

        #region Update and Draw

        /// <summary>
        /// Updates the menu.
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);
        }

        /// <summary>
        /// Draws the menu.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            GraphicsDevice graphics = ScreenManager.GraphicsDevice;
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            SpriteFont font = ScreenManager.Font;

            spriteBatch.Begin();

            #region Description

            string description = ((SkillButton)menuEntries[selectedEntry]).skill.abilityDescription + "\n";

            float mult = 1;
            if (((SkillButton)menuEntries[selectedEntry]).skill.abilityTargetType == TargetType.Enemy)
            {
                if (((SkillButton)menuEntries[selectedEntry]).skill.abilityElement == AbilityElementType.Physical)
                {
                    mult = GameState.HeroOne.Damage;
                }
                else
                {
                    mult = GameState.HeroOne.Magic;
                }
            }
            else
            {
                mult = GameState.HeroOne.Heal;
            }

            description += "Damage: " + ((int)(((SkillButton)menuEntries[selectedEntry]).skill.abilityDamageFormula * mult)) + "   ";


            description += "Element Type: " + ((SkillButton)menuEntries[selectedEntry]).skill.abilityElement +"   ";
            description += "Target: " + ((SkillButton)menuEntries[selectedEntry]).skill.abilityProjectileType + "\n";
            description += "Cast Time: " + ((SkillButton)menuEntries[selectedEntry]).skill.abilityCastTime / 100f + "   ";

            if(((SkillButton)menuEntries[selectedEntry]).skill.abilityResourceCost != 0)
                description += "Mana Cost: " + ((SkillButton)menuEntries[selectedEntry]).skill.abilityResourceCost + "   ";

            if (((SkillButton)menuEntries[selectedEntry]).skill.abilityPersonalCost != 0)
                description += "Light Cost: " + ((SkillButton)menuEntries[selectedEntry]).skill.abilityPersonalCost;

            if (((SkillButton)menuEntries[selectedEntry]).skill.Cooldown > 0)
                description += "Cooldown: " + ((SkillButton)menuEntries[selectedEntry]).skill.Cooldown / 1000f + "s";

            if(!((SkillButton)menuEntries[selectedEntry]).bodyColor.Equals(Color.DarkRed))
                description += "\nSkill Points Available: " + GameState.HeroOne.SkillPoints;
            else
                spriteBatch.DrawString(font, "\n\n\nPrerequisite skill requiered.", new Vector2(xIndent / 2, graphics.Viewport.Height * 23 / 30), Color.Red, 0,
                    new Vector2(0, 0), .8f, SpriteEffects.None, 0.0f);

            spriteBatch.DrawString(font, description, new Vector2(xIndent / 2, graphics.Viewport.Height * 23 / 30), Color.White, 0,
                    new Vector2(0, 0), .8f, SpriteEffects.None, 0.0f);

            //if (((SkillButton)menuEntries[selectedEntry]).bodyColor.Equals(Color.DarkGray))
                

            #endregion

            spriteBatch.End();
        }


        #endregion
    }
}
