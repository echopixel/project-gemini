#region File Description
//-----------------------------------------------------------------------------
// MenuScreen.cs
//
// XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Input;

using ProjectGemini.EnumTypes;
using ProjectGemini.GUI;
using ProjectGemini.Static_Classes;
using ProjectGemini.Utilities;
#endregion

namespace ProjectGemini
{
    /// <summary>
    /// Base class for screens that contain a menu of options. The user can
    /// move up and down to select an entry, or cancel to back out of the screen.
    /// </summary>
    abstract class EquipmentMenu : BaseMenu
    {
        #region Fields

        protected int weapons;
        protected int heads;
        protected int chests;
        protected int legs;

        

        protected EquipmentSlotType current;

        #endregion

        #region Initialization


        /// <summary>
        /// Constructor for the base class that menus inherit from.
        /// </summary>
        /// <param name="menuTitle">The text to display as a title.</param>
        /// <param name="menuTitleColor">The color to display the text in.</param>
        /// <param name="menuType">The type of menu using the Enum MenuType</param>
        public EquipmentMenu(string menuTitle, Color menuTitleColor, int xIndent, int xOffset, int xCount, int yIndent, int yOffset, int yCount)
            : base(menuTitle, menuTitleColor, xIndent, xOffset, xCount, yIndent, yOffset, yCount)
        {
            weapons = 0;
            heads = 0;
            chests = 0;
            legs = 0;

            current = EquipmentSlotType.Weapon;
            section = 4;

            
        }

        public override void LoadContent()
        {
            base.LoadContent();
        }

        #endregion

        public override void HandleInput(InputState input)
        {
            if (selectedEntry < 4)
            {
                // Move up in then menu?
                if (input.IsMenuUp(ControllingPlayer))
                {
                    selectedEntry = selectedEntry - xCount;

                    if (selectedEntry < 0)
                    {
                        selectedEntry = selectedEntry + xCount;

                    }
                    else
                    {
                        current--;
                    }

                    switch ((int)current)
                    {
                        case 0:
                            section = 4;
                            break;
                        case 1:
                            section = 4 + weapons;
                            break;
                        case 2:
                            section = 4 + weapons + heads;
                            break;
                        case 3:
                            section = 4 + weapons + heads + chests;
                            break;
                    }

                }

                // Move down in the menu?
                if (input.IsMenuDown(ControllingPlayer))
                {
                    selectedEntry = selectedEntry + xCount;

                    if (selectedEntry >= 4)
                        selectedEntry = selectedEntry - xCount;
                    else
                    {
                        current++;
                    }

                    switch ((int)current)
                    {
                        case 0:
                            section = 4;
                            break;
                        case 1:
                            section = 4 + weapons;
                            break;
                        case 2:
                            section = 4 + weapons + heads;
                            break;
                        case 3:
                            section = 4 + weapons + heads + chests;
                            break;
                    }
                }

                PlayerIndex playerIndex;

                //Move right in the menu?
                if (input.IsMenuRight(ControllingPlayer) || input.IsMenuSelect(ControllingPlayer, out playerIndex))
                {
                    if ((weapons > 0 && current == EquipmentSlotType.Weapon) || (heads > 0 && current == EquipmentSlotType.Head) || (chests > 0 && current == EquipmentSlotType.Chest) || (legs > 0 && current == EquipmentSlotType.Legs))
                        selectedEntry = section;

                    /*
                    switch ((int)current)
                    {
                        case 0:
                            if(weapons != 0)
                                selectedEntry = 4;
                            break;
                        case 1:
                            if (heads != 0)
                                selectedEntry = 4 + weapons;
                            break;
                        case 2: 
                            if (chests != 0)
                                selectedEntry = 4 + weapons + heads;
                            break;
                        case 3:
                            if (legs != 0)
                                selectedEntry = 4 + weapons + heads + chests;
                            break;
                    }
                     */
                }


                // Accept or cancel the menu? We pass in our ControllingPlayer, which may
                // either be null (to accept input from any player) or a specific index.
                // If we pass a null controlling player, the InputState helper returns to
                // us which player actually provided the input. We pass that through to
                // OnSelectEntry and OnCancel, so they can tell which player triggered them.
                

                //if (input.IsMenuSelect(ControllingPlayer, out playerIndex))
                //{
                //    OnSelectEntry(selectedEntry, playerIndex);
                //}
                else if (input.IsMenuCancel(ControllingPlayer, out playerIndex))
                {
                    OnCancel(playerIndex);
                }
                else if (input.IsMenuDeleteTest(ControllingPlayer, out playerIndex))
                {
                    OnDeleteEntry(selectedEntry, playerIndex);
                }
            }
            else
            {
                int start = 0;
                int end = 0;

                switch ((int)current)
                {
                    case 0:
                        start = 4;
                        end = start + weapons;
                        break;
                    case 1:
                        start = 4 + weapons;
                        end = start + heads;
                        break;
                    case 2:
                        start = 4 + weapons + heads;
                        end = start + chests;
                        break;
                    case 3:
                        start = 4 + weapons + heads + chests;
                        end = start + legs;
                        break;
                }

                // Move up in then menu?
                if (input.IsMenuUp(ControllingPlayer))
                {
                    selectedEntry = selectedEntry - xCount;

                    if (selectedEntry < start)
                    {
                        selectedEntry = selectedEntry + xCount;
                    }
                    else
                    {
                        int temp_section = (selectedEntry - (selectedEntry % xCount)) / xCount;
                        if (temp_section < section)
                        {
                            section--;
                        }
                    }
                }

                // Move down in the menu?
                if (input.IsMenuDown(ControllingPlayer))
                {
                    selectedEntry = selectedEntry + xCount;

                    if (selectedEntry >= end)
                        selectedEntry = selectedEntry - xCount;
                    else
                    {
                        int temp_section = (selectedEntry - (selectedEntry % xCount)) / xCount;
                        if (temp_section >= section + yCount)
                        {
                            section++;
                        }
                    }
                }

                PlayerIndex playerIndex;

                //Move left in the menu?
                if (input.IsMenuLeft(ControllingPlayer) || input.IsMenuCancel(ControllingPlayer, out playerIndex))
                {
                    selectedEntry = (int)current;
                }

                // Accept or cancel the menu? We pass in our ControllingPlayer, which may
                // either be null (to accept input from any player) or a specific index.
                // If we pass a null controlling player, the InputState helper returns to
                // us which player actually provided the input. We pass that through to
                // OnSelectEntry and OnCancel, so they can tell which player triggered them.
                

                if (input.IsMenuSelect(ControllingPlayer, out playerIndex))
                {
                    OnSelectEntry(selectedEntry, playerIndex);
                }
                else if (input.IsMenuDeleteTest(ControllingPlayer, out playerIndex))
                {
                    OnDeleteEntry(selectedEntry, playerIndex);
                }
            }
        }

        protected override void UpdateMenuEntryLocations()
        {
            // Make the menu slide into place during transitions, using a
            // power curve to make things look more interesting (this makes
            // the movement slow down as it nears the end).
            float transitionOffset = (float)Math.Pow(TransitionPosition, 2);

            // start at Y = 175; each X value is generated per entry
            Vector2 position = new Vector2(0f, yIndent); // + (yOffset * section));

            for (int i = 0; i < 4; i++)
            {
                BaseButton menuEntry = menuEntries[i];

                    int pos = i % xCount;

                    bool next = false;

                    if (pos == (xCount - 1) || xCount == 1)
                    {
                        next = true;
                    }

                    //Console.WriteLine(menuEntry.Text + ": " + pos);

                    // each entry is to be centered horizontally
                    position.X = (xOffset * pos + xIndent);


                    //if (ScreenState == ScreenState.TransitionOn)
                    //    position.X -= transitionOffset * 256;
                    //else
                    //    position.X += transitionOffset * 512;

                    // set the entry's position
                    menuEntry.Position = position;

                    if (next)
                    {
                        // move down for the next entry the size of this entry
                        position.Y += yOffset;  // Adding 20 otherwise they seem too close together.
                    }
               
            }

            for (int i = 4; i < menuEntries.Count; i++)
            {
                menuEntries[i].Position = new Vector2(1000, 1000);
            }

            // start at Y = 175; each X value is generated per entry
            position = new Vector2(0f, yIndent); // + (yOffset * section));

            int start = 0;
            int end = 0;

            switch ((int)current)
            {
                case 0:
                    start = 4;
                    end = start + weapons;
                    break;
                case 1:
                    start = 4 + weapons;
                    end = start + heads;
                    break;
                case 2:
                    start = 4 + weapons + heads;
                    end = start + chests;
                    break;
                case 3:
                    start = 4 + weapons + heads + chests;
                    end = start + legs;
                    break;
            }

            // update each menu entry's location in turn
            for (int i = start; i < end; i++)
            {
                int temp_section = (i - (i % xCount)) / xCount;

                if (temp_section >= section && temp_section < section + yCount)
                {
                    if (section > start)
                    {
                        DrawUpArrow = true;
                    }
                    else
                    {
                        DrawUpArrow = false;
                    }

                    if (section < end - 4)
                    {
                        DrawDownArrow = true;
                    }
                    else
                    {
                        DrawDownArrow = false;
                    }

                    BaseButton menuEntry = menuEntries[i];

                    int pos = i % xCount;

                    bool next = false;

                    if (pos == (xCount - 1) || xCount == 1)
                    {
                        next = true;
                    }

                    //Console.WriteLine(menuEntry.Text + ": " + pos);

                    // each entry is to be centered horizontally
                    position.X = (xOffset * pos + xIndent + xOffset);


                    //if (ScreenState == ScreenState.TransitionOn)
                    //    position.X -= transitionOffset * 256;
                    //else
                    //    position.X += transitionOffset * 512;

                    // set the entry's position
                    menuEntry.Position = position;

                    if (next)
                    {
                        // move down for the next entry the size of this entry
                        position.Y += yOffset;  // Adding 20 otherwise they seem too close together.
                    }

                    //Console.WriteLine(position.Y);
                }
                else
                {
                    menuEntries[i].Position = new Vector2(1000, 1000);
                }
            }
        }

        #region Update and Draw

        /// <summary>
        /// Updates the menu.
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);
        }

        /// <summary>
        /// Draws the menu.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            GraphicsDevice graphics = ScreenManager.GraphicsDevice;
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            SpriteFont font = ScreenManager.Font;

            spriteBatch.Begin();

            //Console.WriteLine("Up:   " + DrawUpArrow);
            //Console.WriteLine("Down: " + DrawDownArrow);

            if (DrawUpArrow)
            {
                spriteBatch.Draw(UpArrow, new Vector2(graphics.Viewport.Width / 2 - UpArrow.Width / 2, menuEntries[section].Position.Y + (menuEntries[section].Image.Height / 2) - UpArrow.Height * 3), Color.White);
            }
            if (DrawDownArrow)
            {
                spriteBatch.Draw(DownArrow, new Vector2(graphics.Viewport.Width / 2 - DownArrow.Width / 2, menuEntries[section + 3].Position.Y + (menuEntries[section + 3].Image.Height / 2) + DownArrow.Height * 2), Color.White);//530), Color.White);
            }

            double[] updated = GameState.HeroOne.UpdateStats(((EquipmentButton)menuEntries[selectedEntry]).equipment);

            String text = "";
            Color color;

            #region First Col

            if (GameState.HeroOne.MaxHP < updated[0])
                color = Color.LimeGreen;
            else if (GameState.HeroOne.MaxHP > updated[0])
                color = Color.Red;
            else
                color = Color.White;

            text = "HP:    " + updated[0];
            spriteBatch.DrawString(font, text, new Vector2(graphics.Viewport.Width * 2 / 3, yIndent - 15), color, 0,
                new Vector2(0, 0), .65f, SpriteEffects.None, 0.0f);

            if (GameState.HeroOne.HPRegen < updated[1])
                color = Color.LimeGreen;
            else if (GameState.HeroOne.HPRegen > updated[1])
                color = Color.Red;
            else
                color = Color.White;
            
            text = "\nHPREG: " + updated[1].ToString("f1");
            spriteBatch.DrawString(font, text, new Vector2(graphics.Viewport.Width * 2 / 3, yIndent - 15), color, 0,
                new Vector2(0, 0), .65f, SpriteEffects.None, 0.0f);

            if (GameState.HeroOne.Damage < updated[2])
                color = Color.LimeGreen;
            else if (GameState.HeroOne.Damage > updated[2])
                color = Color.Red;
            else
                color = Color.White;

            text = "\n\n\nPHYSI: " + updated[2];
            spriteBatch.DrawString(font, text, new Vector2(graphics.Viewport.Width * 2 / 3, yIndent - 15), color, 0,
                new Vector2(0, 0), .65f, SpriteEffects.None, 0.0f);

            if (GameState.HeroOne.DamageCritChance < updated[3])
                color = Color.LimeGreen;
            else if (GameState.HeroOne.DamageCritChance > updated[3])
                color = Color.Red;
            else
                color = Color.White;

            text = "\n\n\n\nPHCRI: " + updated[3].ToString("f1") + "%";
            spriteBatch.DrawString(font, text, new Vector2(graphics.Viewport.Width * 2 / 3, yIndent - 15), color, 0,
                new Vector2(0, 0), .65f, SpriteEffects.None, 0.0f);

            if (GameState.HeroOne.Defense < updated[4])
                color = Color.LimeGreen;
            else if (GameState.HeroOne.Defense > updated[4])
                color = Color.Red;
            else
                color = Color.White;

            text = "\n\n\n\n\nDEFEN: " + updated[4];
            spriteBatch.DrawString(font, text, new Vector2(graphics.Viewport.Width * 2 / 3, yIndent - 15), color, 0,
                new Vector2(0, 0), .65f, SpriteEffects.None, 0.0f);

            if (GameState.HeroOne.Heal < updated[5])
                color = Color.LimeGreen;
            else if (GameState.HeroOne.Heal > updated[5])
                color = Color.Red;
            else
                color = Color.White;

            text = "\n\n\n\n\n\nHEAL:  " + updated[5];
            spriteBatch.DrawString(font, text, new Vector2(graphics.Viewport.Width * 2 / 3, yIndent - 15), color, 0,
                new Vector2(0, 0), .65f, SpriteEffects.None, 0.0f);

            if (GameState.HeroOne.TotalStr < updated[6])
                color = Color.LimeGreen;
            else if (GameState.HeroOne.TotalStr > updated[6])
                color = Color.Red;
            else
                color = Color.White;

            text = "\n\n\n\n\n\n\n\nSTR:   " + updated[6];
            spriteBatch.DrawString(font, text, new Vector2(graphics.Viewport.Width * 2 / 3, yIndent - 15), color, 0,
                new Vector2(0, 0), .65f, SpriteEffects.None, 0.0f);

            if (GameState.HeroOne.TotalDex < updated[7])
                color = Color.LimeGreen;
            else if (GameState.HeroOne.TotalDex > updated[7])
                color = Color.Red;
            else
                color = Color.White;

            text = "\n\n\n\n\n\n\n\n\nDEX:   " + updated[7];
            spriteBatch.DrawString(font, text, new Vector2(graphics.Viewport.Width * 2 / 3, yIndent - 15), color, 0,
                new Vector2(0, 0), .65f, SpriteEffects.None, 0.0f);

            if (GameState.HeroOne.TotalSta < updated[8])
                color = Color.LimeGreen;
            else if (GameState.HeroOne.TotalSta > updated[8])
                color = Color.Red;
            else
                color = Color.White;

            text = "\n\n\n\n\n\n\n\n\n\nSTA:   " + updated[8];
            spriteBatch.DrawString(font, text, new Vector2(graphics.Viewport.Width * 2 / 3, yIndent - 15), color, 0,
                new Vector2(0, 0), .65f, SpriteEffects.None, 0.0f);

            #endregion

            #region Second Col

            if (GameState.HeroOne.MaxMP < updated[9])
                color = Color.LimeGreen;
            else if (GameState.HeroOne.MaxMP > updated[9])
                color = Color.Red;
            else
                color = Color.White;

            text = "MP:    " + updated[9];
            spriteBatch.DrawString(font, text, new Vector2(graphics.Viewport.Width * 5 / 6, yIndent - 15), color, 0,
                new Vector2(0, 0), .65f, SpriteEffects.None, 0.0f);

            if (GameState.HeroOne.MPRegen < updated[10])
                color = Color.LimeGreen;
            else if (GameState.HeroOne.MPRegen > updated[10])
                color = Color.Red;
            else
                color = Color.White;

            text = "\nMPREG: " + updated[10].ToString("f1");
            spriteBatch.DrawString(font, text, new Vector2(graphics.Viewport.Width * 5 / 6, yIndent - 15), color, 0,
                new Vector2(0, 0), .65f, SpriteEffects.None, 0.0f);

            if (GameState.HeroOne.Magic < updated[11])
                color = Color.LimeGreen;
            else if (GameState.HeroOne.Magic > updated[11])
                color = Color.Red;
            else
                color = Color.White;

            text = "\n\n\nMAGIC: " + updated[11];
            spriteBatch.DrawString(font, text, new Vector2(graphics.Viewport.Width * 5 / 6, yIndent - 15), color, 0,
                new Vector2(0, 0), .65f, SpriteEffects.None, 0.0f);

            if (GameState.HeroOne.MagicCritChance < updated[12])
                color = Color.LimeGreen;
            else if (GameState.HeroOne.MagicCritChance > updated[12])
                color = Color.Red;
            else
                color = Color.White;

            text = "\n\n\n\nMACRI: " + updated[12].ToString("f1") + "%";
            spriteBatch.DrawString(font, text, new Vector2(graphics.Viewport.Width * 5 / 6, yIndent - 15), color, 0,
                new Vector2(0, 0), .65f, SpriteEffects.None, 0.0f);

            if (GameState.HeroOne.Resistance < updated[13])
                color = Color.LimeGreen;
            else if (GameState.HeroOne.Resistance > updated[13])
                color = Color.Red;
            else
                color = Color.White;

            text = "\n\n\n\n\nRESIS: " + updated[13];
            spriteBatch.DrawString(font, text, new Vector2(graphics.Viewport.Width * 5 / 6, yIndent - 15), color, 0,
                new Vector2(0, 0), .65f, SpriteEffects.None, 0.0f);

            if (GameState.HeroOne.Haste < updated[14])
                color = Color.LimeGreen;
            else if (GameState.HeroOne.Haste > updated[14])
                color = Color.Red;
            else
                color = Color.White;

            text = "\n\n\n\n\n\nHASTE: " + updated[14].ToString("f1") + "%";
            spriteBatch.DrawString(font, text, new Vector2(graphics.Viewport.Width * 5 / 6, yIndent - 15), color, 0,
                new Vector2(0, 0), .65f, SpriteEffects.None, 0.0f);

            if (GameState.HeroOne.TotalInt < updated[15])
                color = Color.LimeGreen;
            else if (GameState.HeroOne.TotalInt > updated[15])
                color = Color.Red;
            else
                color = Color.White;

            text = "\n\n\n\n\n\n\n\nINT:   " + updated[15];
            spriteBatch.DrawString(font, text, new Vector2(graphics.Viewport.Width * 5 / 6, yIndent - 15), color, 0,
                new Vector2(0, 0), .65f, SpriteEffects.None, 0.0f);

            if (GameState.HeroOne.TotalAla < updated[16])
                color = Color.LimeGreen;
            else if (GameState.HeroOne.TotalAla > updated[16])
                color = Color.Red;
            else
                color = Color.White;

            text = "\n\n\n\n\n\n\n\n\nALA:   " + updated[16];
            spriteBatch.DrawString(font, text, new Vector2(graphics.Viewport.Width * 5 / 6, yIndent - 15), color, 0,
                new Vector2(0, 0), .65f, SpriteEffects.None, 0.0f);

            if (GameState.HeroOne.TotalWis < updated[17])
                color = Color.LimeGreen;
            else if (GameState.HeroOne.TotalWis > updated[17])
                color = Color.Red;
            else
                color = Color.White;

            text = "\n\n\n\n\n\n\n\n\n\nWIS:   " + updated[17];
            spriteBatch.DrawString(font, text, new Vector2(graphics.Viewport.Width * 5 / 6, yIndent - 15), color, 0,
                new Vector2(0, 0), .65f, SpriteEffects.None, 0.0f);

            #endregion

            #region Description

            string description = ((EquipmentButton)menuEntries[selectedEntry]).equipment.itemDescription + "\n";
            description += "HP: " + ((EquipmentButton)menuEntries[selectedEntry]).equipment.equipHpmod + "   ";
            description += "MP: " + ((EquipmentButton)menuEntries[selectedEntry]).equipment.equipMpmod + "   ";
            description += "Defense: " + ((EquipmentButton)menuEntries[selectedEntry]).equipment.equipDefenseMod + "   ";
            description += "Resistance: " + ((EquipmentButton)menuEntries[selectedEntry]).equipment.equipResistanceMod + "\n";
            description += "Physical: " + ((EquipmentButton)menuEntries[selectedEntry]).equipment.equipDamageMod + "   ";
            description += "Magic: " + ((EquipmentButton)menuEntries[selectedEntry]).equipment.equipMagicMod + "   ";
            description += "Healing: " + ((EquipmentButton)menuEntries[selectedEntry]).equipment.equipHealMod + "\n";
            description += "Str: " + ((EquipmentButton)menuEntries[selectedEntry]).equipment.equipStrmod + "   ";
            description += "Int: " + ((EquipmentButton)menuEntries[selectedEntry]).equipment.equipIntmod + "   ";
            description += "Dex: " + ((EquipmentButton)menuEntries[selectedEntry]).equipment.equipDexmod + "   ";
            description += "Ala: " + ((EquipmentButton)menuEntries[selectedEntry]).equipment.equipAlamod + "   ";
            description += "Sta: " + ((EquipmentButton)menuEntries[selectedEntry]).equipment.equipStamod + "   ";
            description += "Wis: " + ((EquipmentButton)menuEntries[selectedEntry]).equipment.equipWismod + "\n";
            if (selectedEntry > 3)
            {
                description += "Count: " + ((EquipmentButton)menuEntries[selectedEntry]).equipment.itemCurrentStack;
            }

            spriteBatch.DrawString(font, description, new Vector2(xIndent / 2, graphics.Viewport.Height * 23 / 30), Color.White, 0,
                    new Vector2(0, 0), .65f, SpriteEffects.None, 0.0f);

            #endregion

            spriteBatch.End();
        }


        #endregion
    }
}