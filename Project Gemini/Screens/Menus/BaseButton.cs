#region File Description
//-----------------------------------------------------------------------------
// MenuEntry.cs
//
// XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;
#endregion

namespace ProjectGemini
{
    /// <summary>
    /// Helper class represents a single entry in a MenuScreen. By default this
    /// just draws the entry text string, but it can be customized to display menu
    /// entries in different ways. This also provides an event that will be raised
    /// when the menu entry is selected.
    /// </summary>
    class BaseButton
    {
        #region Fields

        /// <summary>
        /// The text rendered for this entry.
        /// </summary>
        string text;

        /// <summary>
        /// The position at which the entry is drawn. This is set by the MenuScreen
        /// each frame in Update.
        /// </summary>
        protected Vector2 position;

        protected Texture2D image;
        protected Texture2D highlighted;

        public Color bodyColor;

        private bool isHighlighted;
        private Color color;

        public Texture2D icon;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the text of this menu entry.
        /// </summary>
        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        public Texture2D Image
        {
            get { return image; }
            set { image = value; }
        }

        /// <summary>
        /// Gets or sets the position at which to draw this menu entry.
        /// </summary>
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        public void ToggleHighlighted()
        {
            if (isHighlighted)
                isHighlighted = false;
            else
                isHighlighted = true;
        }


        #endregion

        #region Events


        /// <summary>
        /// Event raised when the menu entry is selected.
        /// </summary>
        public event EventHandler<PlayerIndexEventArgs> Selected;
        public event EventHandler<PlayerIndexEventArgs> Deleted;


        /// <summary>
        /// Method for raising the Selected event.
        /// </summary>
        protected internal virtual void OnSelectEntry(PlayerIndex playerIndex)
        {
            if (Selected != null)
                Selected(this, new PlayerIndexEventArgs(playerIndex));
        }

        /// <summary>
        /// Method for raising the Deleted event.
        /// </summary>
        protected internal virtual void OnDeleteEntry(PlayerIndex playerIndex)
        {
            if (Deleted != null)
                Deleted(this, new PlayerIndexEventArgs(playerIndex));
        }

        #endregion

        #region Initialization

        /// <summary>
        /// Constructs a new menu entry with the specified text.
        /// </summary>
        public BaseButton(string text, Texture2D image, Texture2D highlighted, Color color)
        {
            this.text = text;
            this.image = image;
            this.highlighted = highlighted;
            this.color = color;
            isHighlighted = false;
            bodyColor = Color.White;
        }

        public BaseButton(string text, Texture2D image, Texture2D highlighted, Color color, Color bodyColor)
        {
            this.text = text;
            this.image = image;
            this.highlighted = highlighted;
            this.color = color;
            isHighlighted = false;
            this.bodyColor = bodyColor;
        }

        #endregion

        #region Update and Draw

        /// <summary>
        /// Updates the menu entry.
        /// </summary>
        public virtual void Update(BaseMenu screen, bool isSelected, GameTime gameTime)
        {
            // When the menu selection changes, entries gradually fade between
            // their selected and deselected appearance, rather than instantly
            // popping to the new state.
            float fadeSpeed = (float)gameTime.ElapsedGameTime.TotalSeconds * 4;
            if(isSelected && !isHighlighted)
                ToggleHighlighted();
        }

        /// <summary>
        /// Draws the menu entry. This can be overridden to customize the appearance.
        /// </summary>
        public virtual void Draw(BaseMenu screen, bool isSelected, GameTime gameTime)
        {
            // Draw text, centered on the middle of each line.
            ScreenManager screenManager = screen.ScreenManager;
            SpriteBatch spriteBatch = screenManager.SpriteBatch;
            SpriteFont font = screenManager.Font;

            Texture2D texture;
            if (!isHighlighted)
                texture = image;
            else
            {
                texture = highlighted;
                ToggleHighlighted();
            }

            float xOffset = 0;

            if (icon != null)
            {
                xOffset = 20 + (icon.Width);
            //Console.WriteLine(icon.Width + " " + xOffset);
            }

            

            float height2 = font.MeasureString(text).Y;
            float width2 = font.MeasureString(text).X;
            float scale = 1f;

            while (height2 * scale > (texture.Height * .95) || width2 * scale > ((texture.Width - xOffset) * .95))
            {
                scale = scale * .95f;
            }

            float height = ((font.MeasureString(text).Y * scale) / 2) - ((font.MeasureString(text).Y * (1 - scale)) / 2) - (texture.Height / 2);
            float width = ((font.MeasureString(text).X * scale) / 2)  - ((texture.Width) / 2);

            spriteBatch.Draw(texture, position, bodyColor);

            Vector2 textorigin = new Vector2(0, 0);

            if (icon != null)
            {
                textorigin = new Vector2(-xOffset * (1 + (1 - scale)), height);
            }
            else
            {
                textorigin = new Vector2(width, height);
            }

            spriteBatch.DrawString(font, text, position, color, 0, textorigin, scale, SpriteEffects.None, 0.0f);

            if (icon != null)
            {
                float iconheight = position.Y - (icon.Height / 2) + (image.Height / 2);
                float iconwidth = position.X + 11;
                Vector2 iconorigin = new Vector2(iconwidth, iconheight);

                spriteBatch.Draw(icon, iconorigin, Color.White);
            }
        }

        #endregion
    }
}
