#region File Description
//-----------------------------------------------------------------------------
// MainMenuScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

using ProjectGemini.Static_Classes;
using ProjectGemini.EnumTypes;
using EasyStorage;
using System;
using ProjectGemini.Entities;
using ProjectGemini.Controllers;
using ProjectGemini.Utilities;
#endregion

namespace ProjectGemini
{
    /// <summary>
    /// The main menu screen is the first thing displayed when the game starts up.
    /// </summary>
    class KeybindingMenuScreen : BaseMenu
    {

        Ability skill;

        #region Initialization

        /// <summary>
        /// Constructor fills in the menu contents.
        /// </summary>
        public KeybindingMenuScreen(Ability skill)
            : base("Set " + skill.abilityName + " to what button?", Color.White, 81, 230, 3, 200, 90, 100)
        {
            this.skill = skill;

        }

        /// <summary>
        /// Loads the assets used for this menu screen
        /// </summary>
        public override void LoadContent()
        {
            base.LoadContent();

            Bar.xText = "Remove";

            Reload();
        }

        /// <summary>
        /// Unloads the assets for this menu screen.
        /// </summary>
        public override void UnloadContent()
        {
            if (content != null)
                content.Unload();
        }

        #endregion

        #region Handle Input

        /**
         * For Continue game
         * Normally would load the most recently saved game data.
         * */
        void buttonOneSelected(object sender, PlayerIndexEventArgs e)
        {
            GameState.HeroOne.Skill1 = skill;
            Reload();
        }

        void buttonTwoSelected(object sender, PlayerIndexEventArgs e)
        {
            GameState.HeroOne.Skill2 = skill;
            Reload();
        }

        void buttonThreeSelected(object sender, PlayerIndexEventArgs e)
        {
            GameState.HeroOne.Skill3 = skill;
            Reload();
        }

        void buttonFourSelected(object sender, PlayerIndexEventArgs e)
        {
            GameState.HeroOne.Skill4 = skill;
            Reload();
        }

        void buttonFiveSelected(object sender, PlayerIndexEventArgs e)
        {
            GameState.HeroOne.Skill5 = skill;
            Reload();
        }

        void buttonSixSelected(object sender, PlayerIndexEventArgs e)
        {
            GameState.HeroOne.Skill6 = skill;
            Reload();
        }

        void buttonSevenSelected(object sender, PlayerIndexEventArgs e)
        {
            GameState.HeroOne.Skill7 = skill;
            Reload();
        }

        void buttonEightSelected(object sender, PlayerIndexEventArgs e)
        {
            GameState.HeroOne.Skill8 = skill;
            Reload();
        }

        void buttonNineSelected(object sender, PlayerIndexEventArgs e)
        {
            GameState.HeroOne.Skill9 = skill;
            Reload();
        }

        protected override void OnDeleteEntry(int entryIndex, PlayerIndex playerIndex)
        {
            if (selectedEntry == 0)
                GameState.HeroOne.Skill1 = null;
            if (selectedEntry == 1)
                GameState.HeroOne.Skill2 = null;
            if (selectedEntry == 2)
                GameState.HeroOne.Skill3 = null;
            if (selectedEntry == 3)
                GameState.HeroOne.Skill4 = null;
            if (selectedEntry == 4)
                GameState.HeroOne.Skill5 = null;
            if (selectedEntry == 5)
                GameState.HeroOne.Skill6 = null;
            if (selectedEntry == 6)
                GameState.HeroOne.Skill7 = null;
            if (selectedEntry == 7)
                GameState.HeroOne.Skill8 = null;
            if (selectedEntry == 8)
                GameState.HeroOne.Skill9 = null;

            Reload();
        }

        void Reload()
        {
            MenuEntries.Clear();

            Texture2D image2 = content.Load<Texture2D>("Buttons/MainMenuSelected");
            Texture2D image1 = content.Load<Texture2D>("Buttons/MainMenu");

            BaseButton button11Entry = new BaseButton("Skill Set 1: X/1", image1, image2, Color.Black);
            BaseButton button21Entry = new BaseButton("Skill Set 1: Y/2", image1, image2, Color.Black);
            BaseButton button31Entry = new BaseButton("Skill Set 1: B/3", image1, image2, Color.Black);

            BaseButton button12Entry = new BaseButton("Skill Set 2: X/1", image1, image2, Color.Black);
            BaseButton button22Entry = new BaseButton("Skill Set 2: Y/2", image1, image2, Color.Black);
            BaseButton button32Entry = new BaseButton("Skill Set 2: B/3", image1, image2, Color.Black);

            BaseButton button13Entry = new BaseButton("Skill Set 3: X/1", image1, image2, Color.Black);
            BaseButton button23Entry = new BaseButton("Skill Set 3: Y/2", image1, image2, Color.Black);
            BaseButton button33Entry = new BaseButton("Skill Set 3: B/3", image1, image2, Color.Black);

            if (GameState.HeroOne.Skill1 != null)
            {
                button11Entry.icon = GameState.HeroOne.Skill1.abilityIconTexture;
            }

            if (GameState.HeroOne.Skill2 != null)
            {
                button21Entry.icon = GameState.HeroOne.Skill2.abilityIconTexture;
            }

            if (GameState.HeroOne.Skill3 != null)
            {
                button31Entry.icon = GameState.HeroOne.Skill3.abilityIconTexture;
            }

            if (GameState.HeroOne.Skill4 != null)
            {
                button12Entry.icon = GameState.HeroOne.Skill4.abilityIconTexture;
            }

            if (GameState.HeroOne.Skill5 != null)
            {
                button22Entry.icon = GameState.HeroOne.Skill5.abilityIconTexture;
            }

            if (GameState.HeroOne.Skill6 != null)
            {
                button32Entry.icon = GameState.HeroOne.Skill6.abilityIconTexture;
            }

            if (GameState.HeroOne.Skill7 != null)
            {
                button13Entry.icon = GameState.HeroOne.Skill7.abilityIconTexture;
            }

            if (GameState.HeroOne.Skill8 != null)
            {
                button23Entry.icon = GameState.HeroOne.Skill8.abilityIconTexture;
            }

            if (GameState.HeroOne.Skill9 != null)
            {
                button33Entry.icon = GameState.HeroOne.Skill9.abilityIconTexture;
            }


            // Hook up menu event handlers.
            button11Entry.Selected += buttonOneSelected;
            button21Entry.Selected += buttonTwoSelected;
            button31Entry.Selected += buttonThreeSelected;

            button12Entry.Selected += buttonFourSelected;
            button22Entry.Selected += buttonFiveSelected;
            button32Entry.Selected += buttonSixSelected;

            button13Entry.Selected += buttonSevenSelected;
            button23Entry.Selected += buttonEightSelected;
            button33Entry.Selected += buttonNineSelected;

            MenuEntries.Add(button11Entry);
            MenuEntries.Add(button21Entry);
            MenuEntries.Add(button31Entry);

            MenuEntries.Add(button12Entry);
            MenuEntries.Add(button22Entry);
            MenuEntries.Add(button32Entry);

            MenuEntries.Add(button13Entry);
            MenuEntries.Add(button23Entry);
            MenuEntries.Add(button33Entry);
        }


        #endregion

    }
}
