#region File Description
//-----------------------------------------------------------------------------
// GameplayScreen.cs
//
// The screen where all the gameplay happens, dood!
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ProjectGemini.Controllers;
using ProjectGemini.Graphics_Utilities;
using ProjectGemini.Entities;
using FuncWorks.XNA.XTiled;
using ProjectGemini.Static_Classes;
using ProjectGemini.Game_World;
using FarseerPhysics.Dynamics;
using ProjectGemini.GUI;
using ProjectGemini.EnumTypes;
using ProjectGemini.Utilities;
using System.Collections.Generic;
using ProjectGemini.Message_Boxes;
using ProjectGemini.Managers;
#endregion

namespace ProjectGemini
{
    /// <summary>
    /// This screen implements the actual game logic. It is just a
    /// placeholder to get the idea across: you'll probably want to
    /// put some more interesting gameplay in here!
    /// </summary>
    class GameplayScreen : GameScreen
    {
        #region Fields

        Game game;
        static ContentManager content;

        bool isLoadingSave;

        // Assets
        SpriteFont gameFont;
        private SpriteFont _font;
        Texture2D pixel;
        public Texture2D targetIcon;
        PersonalResourceType HeroPersonal;

        float pauseAlpha;

        #endregion

        #region Initialization


        
        //For starting a fresh hero
        public GameplayScreen(Game game, bool isLoadingSave, PersonalResourceType heroPersonal)
        {
            this.isLoadingSave = isLoadingSave;
            GameState.PhysicsWorld.Clear();
            GameState.Events = new Dictionary<int,string>();
            GameState.InteractableEvents = new Dictionary<BaseEntity, string>();
            GameState.OpenedChests = new HashSet<Tuple<string, int>>();
            GameState.Starts = new Dictionary<string, Vector2>();
            
            this.game = game;

            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);

            GameState.CurrentZone = new Zone();

            HeroPersonal = heroPersonal;

            //game.Components.Add(new FrameRateComponent(game));    // How to add components to the game in this screen.
        }

        //For loading a hero
        public GameplayScreen(Game game, bool isLoadingSave)
        {
            this.isLoadingSave = isLoadingSave;
            GameState.PhysicsWorld.Clear();
            GameState.Events = new Dictionary<int, string>();
            GameState.InteractableEvents = new Dictionary<BaseEntity, string>();
            GameState.OpenedChests = new HashSet<Tuple<string, int>>();
            GameState.Starts = new Dictionary<string, Vector2>();

            this.game = game;

            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);

            GameState.CurrentZone = new Zone();

            //HeroPersonal = heroPersonal;

            //game.Components.Add(new FrameRateComponent(game));    // How to add components to the game in this screen.
        }

        /// <summary>
        /// Load graphics content for the game.
        /// </summary>
        public override void LoadContent()
        {
            if (content == null)
                content = new ContentManager(ScreenManager.Game.Services, "Content");

            GameState.AudioManager.LoadContent("Content/Audio");
            GameState.AudioManager.LoadSound("Slash", "Sound Effects/slash2");

            TimedMessageBoxManager.LoadContent(content);

            _font = content.Load<SpriteFont>("font");
            gameFont = content.Load<SpriteFont>("Fonts/gamefont");
            targetIcon = content.Load<Texture2D>("Icons/target_arrow");
            foreach (Ability ability in GameState.Abilities)
            {
                ability.abilityIconTexture = content.Load<Texture2D>("Icons/" + ability.abilityIcon);
            }
            foreach (Item item in GameState.Items)
            {
                item.itemIconTexture = content.Load<Texture2D>("Icons/" + item.itemIcon);
            }

            Map.InitObjectDrawing(ScreenManager.GraphicsDevice);

            pixel = new Texture2D(ScreenManager.GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
            pixel.SetData(new[] { Color.White }); // so that we can draw whatever color we want on top of it

            if (!isLoadingSave)
            {
                GameState.CurrentMap = content.Load<Map>("TileMaps/testarenaone");
                GameState.CurrentZone.Initialize(content);

                GameState.HeroOne = new Hero(
                    content.Load<Texture2D>("Sprites/duskarmovementsheet"), 4, 8,
                    content.Load<Texture2D>("Sprites/duskarattacksheet"), 2, 4,
                    content.Load<Texture2D>("Sprites/duskarcastsheet"), 12, 4,
                    GameState.CurrentMap, HeroPersonal);

                Vector2 placement = new Vector2(0, 0);

                foreach (KeyValuePair<string, Vector2> start in GameState.Starts)
                {
                    if (start.Key == "HeroOneStart")
                    {
                        placement = start.Value;
                    }
                }
                GameState.HeroOne.SetPosition(placement);
            }

            GameState.Camera = new Camera();
            GameState.Camera.FollowHeroPosition(ref GameState.HeroOne, ref GameState.CurrentMap);

            GameDebug.DebugView = new FarseerPhysics.DebugViews.DebugViewXNA(GameState.PhysicsWorld);
            GameDebug.DebugView.AppendFlags(FarseerPhysics.DebugViewFlags.DebugPanel);
            GameDebug.DebugView.DefaultShapeColor = Color.White;
            GameDebug.DebugView.SleepingShapeColor = Color.LightGray;
            GameDebug.DebugView.LoadContent(ScreenManager.GraphicsDevice, content);
            GameDebug.DebugView.SetSpriteBatch(ScreenManager.SpriteBatch);

            // A real game would probably have more content than this sample, so
            // it would take longer to load. We simulate that by delaying for a
            // while, giving you a chance to admire the beautiful loading screen.
            //Thread.Sleep(5000);

            // once the load has finished, we use ResetElapsedTime to tell the game's
            // timing mechanism that we have just finished a very long frame, and that
            // it should not try to catch up.
            ScreenManager.Game.ResetElapsedTime();
        }

        static public void ChangeMap(string position, MapTypes map)
        {
            GameState.Events.Clear();
            GameState.InteractableEvents.Clear();
            
            if (map == MapTypes.DemoMap)
            {
                GameState.CurrentMap = content.Load<Map>("TileMaps/demomap");
            }
            else if (map == MapTypes.TestHouse)
            {
                GameState.CurrentMap = content.Load<Map>("TileMaps/testhouse");
            }
            else if (map == MapTypes.TestArenaOne)
            {
                GameState.CurrentMap = content.Load<Map>("TileMaps/testarenaone");
            }
            else if (map == MapTypes.TestArenaTwo)
            {
                GameState.CurrentMap = content.Load<Map>("TileMaps/testarenatwo");
            }
            else if (map == MapTypes.DemoArena)
            {
                GameState.CurrentMap = content.Load<Map>("TileMaps/demoarena");
            }
            GameState.Starts.Clear();

            GameState.Camera.CurrentMap = GameState.CurrentMap;
            GameState.CurrentZone.Initialize(content);

            Vector2 placement = new Vector2(0, 0);

            foreach (KeyValuePair<string, Vector2> start in GameState.Starts)
            {
                if (start.Key == position)
                {
                    placement = start.Value;
                }
            }
            GameState.HeroOne.SetPosition(placement);
        }
        

        /// <summary>
        /// Unload graphics content used by the game.
        /// </summary>
        public override void UnloadContent()
        {
            content.Unload();
        }


        #endregion

        #region Update and Draw


        /// <summary>
        /// Updates the state of the game. This method checks the GameScreen.IsActive
        /// property, so the game will stop updating when the pause menu is active,
        /// or if you tab away to a different application.
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, false);

            // Gradually fade in or out depending on whether we are covered by the pause screen.
            if (coveredByOtherScreen)
                pauseAlpha = Math.Min(pauseAlpha + 1f / 32, 1);
            else
                pauseAlpha = Math.Max(pauseAlpha - 1f / 32, 0);

            if (IsActive)
            {
                if (GameState.WorldEventManager.IsRunning)
                {
                    GameState.WorldEventManager.RunEvents();
                }
                GameState.PhysicsWorld.Step((float)gameTime.ElapsedGameTime.TotalMilliseconds * 0.001f);
                if (GameState.HeroOne.IsDead)
                {
                    //Console.WriteLine("Game Over!");
                    LoadingScreen.Load(ScreenManager, false, null, new BackgroundScreen(),
                                                           new MainMenuScreen());
                }

                // Checks to see if players are in combat.
                if (GameState.CurrentZone.Enemies.Contains(GameState.CurrentZone.Enemies.Find(x => x.IsActive)))
                    GameState.PlayersInCombat = true;
                else
                    GameState.PlayersInCombat = false;

                GameState.HeroOne.Update(gameTime);

                GameState.CurrentZone.Update(gameTime);
                GameState.HeroOne.HeroHUD.Update(gameTime);

                TimedMessageBoxManager.Update(gameTime);
            }
        }


        /// <summary>
        /// Lets the game respond to player input. Unlike the Update method,
        /// this will only be called when the gameplay screen is active.
        /// </summary>
        public override void HandleInput(InputState input)
        {
            if (input == null)
                throw new ArgumentNullException("input");

            GameState.HeroOne.HandleInput(input, ScreenManager, ControllingPlayer, GameState.Camera);
        }

        /// <summary>
        /// Draws the gameplay screen.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;

            spriteBatch.Begin(SpriteSortMode.Immediate, 
                              BlendState.AlphaBlend, 
                              SamplerState.PointClamp, 
                              null, 
                              null, 
                              null,
                              Matrix.CreateTranslation(-(int)GameState.Camera.getCameraVector.X, -(int)GameState.Camera.getCameraVector.Y, 0) * ResolutionManager.getTransformationMatrix());

            // Main Drawing
            GameState.CurrentZone.Draw(spriteBatch);
            GameState.HeroOne.HeroHUD.Draw(spriteBatch);
            // Main Drawing End

            spriteBatch.End();

            if (GameDebug.ShowCollision)
            {
                // calculate the projection and view adjustments for the debug view
                Matrix projection = Matrix.CreateOrthographicOffCenter(0f,
                    ConvertUnits.ToSimUnits(ResolutionManager.VirtualWidth),
                    ConvertUnits.ToSimUnits(ResolutionManager.VirtualHeight), 0f, 0f, 1f);
                Matrix view = Matrix.CreateTranslation(new Vector3(
                    ConvertUnits.ToSimUnits(GameState.Camera.getCameraVector * -1) - 
                    ConvertUnits.ToSimUnits(new Vector2(
                        ScreenManager.GraphicsDevice.Viewport.Width / 2f, 
                        ScreenManager.GraphicsDevice.Viewport.Height / 2f)),
                        0f))
                        * Matrix.CreateTranslation(new Vector3(
                            ConvertUnits.ToSimUnits(new Vector2(
                                ScreenManager.GraphicsDevice.Viewport.Width / 2f,
                                ScreenManager.GraphicsDevice.Viewport.Height / 2f)),
                                0f));
                // draw the debug view
                GameDebug.DebugView.RenderDebugData(ref projection, ref view);
            }

            // If the game is transitioning on or off, fade it out to black.
            if (TransitionPosition > 0 || pauseAlpha > 0)
            {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, pauseAlpha / 2);

                ScreenManager.FadeBackBufferToBlack(alpha);
            }
        }

        #endregion
    }
}
