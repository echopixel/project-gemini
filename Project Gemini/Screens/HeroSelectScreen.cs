#region File Description
//-----------------------------------------------------------------------------
// PauseMenuScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using ProjectGemini.Static_Classes;
using ProjectGemini.EnumTypes;
using ProjectGemini.GUI;
using System;
using ProjectGemini.Utilities;
#endregion

namespace ProjectGemini
{
    class HeroSelectScreen : BaseMenu
    {
        #region Initialization

        /// <summary>
        /// Constructor.
        /// </summary>
        public HeroSelectScreen()
            : base("Select Hero", Color.Black, 311, 0, 1, 300, 90, 4)
        { 
        }

        public override void LoadContent()
        {
            base.LoadContent();

            Texture2D image2 = content.Load<Texture2D>("Buttons/MainMenuSelected");
            Texture2D image1 = content.Load<Texture2D>("Buttons/MainMenu");

            // Create our menu entries.
            BaseButton heroOneEntry = new BaseButton("Shimmer", image1, image2, Color.Black);
            //returnMenuEntry.Position = new Vector2(0, 220);
            BaseButton heroTwoEntry = new BaseButton("Duskar", image1, image2, Color.Black);
            
            // Hook up menu event handlers.
            heroTwoEntry.Selected += heroTwoSelected;
            heroOneEntry.Selected += heroOneSelected;
            

            // Add entries to the menu.
            MenuEntries.Add(heroTwoEntry);
            MenuEntries.Add(heroOneEntry);
            

            
        }

        #endregion

        #region Handle Input

        /// <summary>
        /// Event handler for when the Quit Game menu entry is selected.
        /// </summary>
        void heroOneSelected(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, true, e.PlayerIndex,
                               new GameplayScreen(ScreenManager.Game, false, PersonalResourceType.Light));
        }

        void heroTwoSelected(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, true, e.PlayerIndex,
                               new GameplayScreen(ScreenManager.Game, false, PersonalResourceType.Shadow));
        }

        #endregion

        public override void Draw(GameTime gameTime)
        {

            GraphicsDevice graphics = ScreenManager.GraphicsDevice;
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            SpriteFont font = ScreenManager.Font;

            spriteBatch.Begin();

            string description = "";

            if (selectedEntry == 0)
            {
                description = "Basic Attack: Melee" + 
                    "\nPersonal Resource: Shadow" +
                    "\nShadow element attacks make non shadow elements stronger." +
                    "\nNon shadow element attacks make shadow elements stronger." +
                    "\nFavors STR and INT.";
            }
            else if (selectedEntry == 1)
            {
                description = "Basic Attack: Bow" +
                    "\nPersonal Resource: Light" +
                    "\nLight element attacks use the light bar instead of MP." +
                    "\nLight regenerates at a steady rate in combat." +
                    "\nFavors DEX and INT.";
            }

            spriteBatch.DrawString(font, description, new Vector2(81 / 2, graphics.Viewport.Height * 23 / 30), Color.White, 0,
                    new Vector2(0, 0), .65f, SpriteEffects.None, 0.0f);

            spriteBatch.End();

            base.Draw(gameTime);

            
        }
    }
}
