#region File Description
//-----------------------------------------------------------------------------
// MenuScreen.cs
//
// XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Input;

using ProjectGemini.EnumTypes;
using ProjectGemini.GUI;
using ProjectGemini.Message_Boxes;
#endregion

namespace ProjectGemini
{
    /// <summary>
    /// Base class for screens that contain a menu of options. The user can
    /// move up and down to select an entry, or cancel to back out of the screen.
    /// </summary>
    abstract class MenuScreen : GameScreen
    {
        #region Fields

        List<MenuEntry> menuEntries = new List<MenuEntry>();
        int selectedEntry = 0;
        string menuTitle;
        MenuType menuType;
        int menuSize;

        protected ContentManager content;

        protected HelperBar Bar;
        
        /// <summary>
        /// The color of the menuTitle text.
        /// </summary>
        Color menuTitleColor;

        /// <summary>
        /// The texture, if any, located underneath the menuTitleSubBackground
        /// </summary>
        private BorderedTextureBox menuTitleSubBackground;

        /// <summary>
        /// The texture, if any, located underneath the menu title text.
        /// </summary>
        private BorderedTextureBox menuTitleBackground;

        /// <summary>
        /// The amount of "padding" in pixels to add between the left and right of the string and the edge of the menuTitleBackground texture.
        /// </summary>
        private int menuTitleBackgroundXPadding;

        /// <summary>
        /// The amount of "padding" in pixels to add between the top and bottom of the string and the edge of the menuTitleBackground texture.
        /// </summary>
        private int menuTitleBackgroundYPadding;

        /// <summary>
        /// The amount of "padding" in pixels to add between the left and right of the string and the edge of the menuTitleSubBackground texture.
        /// </summary>
        private int menuTitleSubBackgroundXPadding;

        /// <summary>
        /// The amount of "padding" in pixels to add between the top and bottom of the string and the edge of the menuTitleSubBackground texture.
        /// </summary>
        private int menuTitleSubBackgroundYPadding;

        /// <summary>
        /// The distance in pixels which the menuTitleSubBackground image will be offset along the X axis from menuTitleBackground.
        /// Positive for right, negative for left.
        /// </summary>
        private int menuTitleSubBackgroundXOffset;

        /// <summary>
        /// The distance in pixels which the menuTitleSubBackground image will be offset along the X axis from menuTitleBackground.
        /// Positive for down, negative for up.
        /// </summary>
        private int menuTitleSubBackgroundYOffset;

        #endregion

        #region Properties


        /// <summary>
        /// Gets the list of menu entries, so derived classes can add
        /// or change the menu contents.
        /// </summary>
        protected IList<MenuEntry> MenuEntries
        {
            get { return menuEntries; }
        }


        #endregion

        #region Initialization

        /// <summary>
        /// Constructor for the base class that menus inherit from.  Uses a default light grey color from the sample.
        /// </summary>
        public MenuScreen(string menuTitle, MenuType menuType)
            : this(menuTitle, new Color(150, 150, 150), menuType)
        {
        }

        /// <summary>
        /// Constructor for the base class that menus inherit from.
        /// </summary>
        /// <param name="menuTitle">The text to display as a title.</param>
        /// <param name="menuTitleColor">The color to display the text in.</param>
        /// <param name="menuType">The type of menu using the Enum MenuType</param>
        public MenuScreen(string menuTitle, Color menuTitleColor, MenuType menuType)
        {
            this.menuTitle = menuTitle;
            this.menuTitleColor = menuTitleColor;
            this.menuType = menuType;
            CalculateMenuSize();

            menuTitleBackground = null;
            menuTitleSubBackground = null;

            TransitionOnTime = TimeSpan.FromSeconds(0.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);
        }

        public override void LoadContent()
        {
            if (content == null)
            {
                content = new ContentManager(ScreenManager.Game.Services, "Content");
            }

            
            Bar = new HelperBar(content);
            Bar.padText = "Move";
            Bar.aText = "Accept";
            Bar.bText = "Cancel";
        }

        /// <summary>
        /// Sets a background image that will be displayed beneath the menu title text.
        /// </summary>
        /// <param name="menuTitleBackgroundImage">The image.</param>
        /// <param name="menuTitleBackgroundBorderWidth">The width in pixels of the border along the width (i.e. horizontal sides) of the menuTitleBackground image. Use 0 for no border.</param>
        /// <param name="menuTitleBackgroundBorderHeight">The width in pixels of the border along the height (i.e. vertical sides) of the menuTitleBackground image. Use 0 for no border.</param>
        /// <param name="menuTitleBackgroundXPadding">The amount of "padding" in pixels to add between the left and right of the string and the edge of the menuTitleBackground texture.</param>
        /// <param name="menuTitleBackgroundYPadding">The amount of "padding" in pixels to add between the top and bottom of the string and the edge of the menuTitleBackground texture.</param>
        public void SetMenuTitleBackground(Texture2D menuTitleBackgroundImage, int menuTitleBackgroundBorderWidth, int menuTitleBackgroundBorderHeight, int menuTitleBackgroundXPadding, int menuTitleBackgroundYPadding)
        {
            this.menuTitleBackground = new BorderedTextureBox(menuTitleBackgroundImage, menuTitleBackgroundBorderWidth, menuTitleBackgroundBorderHeight);
            this.menuTitleBackgroundXPadding = menuTitleBackgroundXPadding;
            this.menuTitleBackgroundYPadding = menuTitleBackgroundYPadding;
        }

        /// <summary>
        /// Sets a sub-background image that will be displayed beneath the background image (if any) that appears behind the menu title text.
        /// </summary>
        /// <param name="menuTitleSubBackgroundImage">The image.</param>
        /// <param name="menuTitleSubBackGroundBorderWidth">The width in pixels of the border along the width (i.e. horizontal sides) of the menuTitleSubBackground image. Use 0 for no border.</param>
        /// <param name="menuTitleSubBackgroundBorderHeight">The width in pixels of the border along the height (i.e. vertical sides) of the menuTitleSubBackground image. Use 0 for no border.</param>
        /// <param name="menuTitleSubBackgroundXPadding">The amount of "padding" in pixels to add between the left and right of the string and the edge of the menuTitleSubBackground texture.</param>
        /// <param name="menuTitleSubBackgroundYPadding">The amount of "padding" in pixels to add between the top and bottom of the string and the edge of the menuTitleSubBackground texture.</param>
        /// <param name="menuTitleSubBackgroundXOffset">The distance in pixels which the menuTitleSubBackground image will be offset along the X axis from menuTitleBackground. Positive for right, negative for left.</param>
        /// <param name="menuTitleSubBackgroundYOffset">The distance in pixels which the menuTitleSubBackground image will be offset along the X axis from menuTitleBackground. Positive for down, negative for up.</param>
        public void SetMenuTitleSubBackground(Texture2D menuTitleSubBackgroundImage, int menuTitleSubBackGroundBorderWidth, int menuTitleSubBackgroundBorderHeight, int menuTitleSubBackgroundXPadding, int menuTitleSubBackgroundYPadding, int menuTitleSubBackgroundXOffset, int menuTitleSubBackgroundYOffset)
        {
            this.menuTitleSubBackground = new BorderedTextureBox(menuTitleSubBackgroundImage, menuTitleSubBackGroundBorderWidth, menuTitleSubBackgroundBorderHeight);
            this.menuTitleSubBackgroundXPadding = menuTitleSubBackgroundXPadding;
            this.menuTitleSubBackgroundYPadding = menuTitleSubBackgroundYPadding;
            this.menuTitleSubBackgroundXOffset = menuTitleSubBackgroundXOffset;
            this.menuTitleSubBackgroundYOffset = menuTitleSubBackgroundYOffset;
        }

        /// <summary>
        /// Calculates the size of the menu depending on the type of menu.
        /// </summary>
        private void CalculateMenuSize()
        {
            if (menuType == MenuType.LeftMiddleRight)
            {
                menuSize = 3;
            }
            else if (menuType == MenuType.LeftMiddle || menuType == MenuType.LeftRight || menuType == MenuType.MiddleRight)
            {
                menuSize = 2;
            }
            else
            {
                menuSize = 1;
            }
        }


        #endregion

        #region Handle Input


        /// <summary>
        /// Responds to user input, changing the selected entry and accepting
        /// or cancelling the menu.
        /// </summary>
        public override void HandleInput(InputState input)
        {
            // Move up in then menu?
            if (input.IsMenuUp(ControllingPlayer))
            {
                selectedEntry = selectedEntry - menuSize;

                if (selectedEntry < 0)
                    selectedEntry = selectedEntry + menuSize;
            }

            // Move down in the menu?
            if (input.IsMenuDown(ControllingPlayer))
            {
                selectedEntry = selectedEntry + menuSize;

                if (selectedEntry >= menuEntries.Count)
                    selectedEntry = selectedEntry - menuSize;
            }

            //Move right in the menu?
            if (input.IsMenuRight(ControllingPlayer))
            {
                selectedEntry++;

                if (selectedEntry % menuSize == 0 || selectedEntry >= menuEntries.Count)
                {
                    selectedEntry--;
                }
            }

            //Move left in the menu?
            if (input.IsMenuLeft(ControllingPlayer))
            {
                selectedEntry--;

                if (selectedEntry % menuSize == menuSize - 1 || selectedEntry < 0)
                {
                    selectedEntry++;
                }
            }

            // Accept or cancel the menu? We pass in our ControllingPlayer, which may
            // either be null (to accept input from any player) or a specific index.
            // If we pass a null controlling player, the InputState helper returns to
            // us which player actually provided the input. We pass that through to
            // OnSelectEntry and OnCancel, so they can tell which player triggered them.
            PlayerIndex playerIndex;

            if (input.IsMenuSelect(ControllingPlayer, out playerIndex))
            {
                OnSelectEntry(selectedEntry, playerIndex);
            }
            else if (input.IsMenuCancel(ControllingPlayer, out playerIndex))
            {
                OnCancel(playerIndex);
            }
            else if (input.IsMenuDeleteTest(ControllingPlayer, out playerIndex))
            {
                OnDeleteEntry(selectedEntry, playerIndex);
            }
        }


        /// <summary>
        /// Handler for when the user has chosen a menu entry.
        /// </summary>
        protected virtual void OnSelectEntry(int entryIndex, PlayerIndex playerIndex)
        {
            menuEntries[entryIndex].OnSelectEntry(playerIndex);
        }

        protected virtual void OnDeleteEntry(int entryIndex, PlayerIndex playerIndex)
        {
            menuEntries[entryIndex].OnDeleteEntry(playerIndex);
        }


        /// <summary>
        /// Handler for when the user has cancelled the menu.
        /// </summary>
        protected virtual void OnCancel(PlayerIndex playerIndex)
        {
            ExitScreen();
        }


        /// <summary>
        /// Helper overload makes it easy to use OnCancel as a MenuEntry event handler.
        /// </summary>
        protected void OnCancel(object sender, PlayerIndexEventArgs e)
        {
            OnCancel(e.PlayerIndex);
        }


        #endregion

        #region Update and Draw


        /// <summary>
        /// Allows the screen the chance to position the menu entries. By default
        /// all menu entries are lined up in a vertical list, centered on the screen.
        /// </summary>
        protected virtual void UpdateMenuEntryLocations()
        {
            // Make the menu slide into place during transitions, using a
            // power curve to make things look more interesting (this makes
            // the movement slow down as it nears the end).
            float transitionOffset = (float)Math.Pow(TransitionPosition, 2);

            // start at Y = 175; each X value is generated per entry
            Vector2 position = new Vector2(0f, menuEntries[0].Position.Y);

            // update each menu entry's location in turn
            for (int i = 0; i < menuEntries.Count; i++)
            {
                MenuEntry menuEntry = menuEntries[i];

                int pos = i % menuSize;

                bool next = false;

                if (pos == (menuSize - 1) || menuSize == 0)
                {
                    next = true;
                }


                if (menuType == MenuType.Middle || menuType == MenuType.MiddleRight)
                {
                    pos++;
                }
                else if (menuType == MenuType.Right)
                {
                    pos = pos + 2;
                }

                // each entry is to be centered horizontally
                position.X = (ScreenManager.GraphicsDevice.Viewport.Width / 2 - menuEntry.GetWidth(this) / 2) - 256 + (256 * pos);

                if (ScreenState == ScreenState.TransitionOn)
                    position.X -= transitionOffset * 256;
                else
                    position.X += transitionOffset * 512;

                // set the entry's position
                menuEntry.Position = position;

                if(next)
                {
                    // move down for the next entry the size of this entry
                    position.Y += menuEntry.GetHeight(this) + 20;  // Adding 20 otherwise they seem too close together.
                }
            }
        }


        /// <summary>
        /// Updates the menu.
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            // Update each nested MenuEntry object.
            for (int i = 0; i < menuEntries.Count; i++)
            {
                bool isSelected = IsActive && (i == selectedEntry);

                menuEntries[i].Update(this, isSelected, gameTime);
            }
        }


        /// <summary>
        /// Draws the menu.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            // make sure our entries are in the right place before we draw them
            UpdateMenuEntryLocations();

            GraphicsDevice graphics = ScreenManager.GraphicsDevice;
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            SpriteFont font = ScreenManager.Font;

            spriteBatch.Begin();

            Bar.Draw(spriteBatch, gameTime, TransitionAlpha);

            // Draw each menu entry in turn.
            for (int i = 0; i < menuEntries.Count; i++)
            {
                MenuEntry menuEntry = menuEntries[i];

                bool isSelected = IsActive && (i == selectedEntry);

                menuEntry.Draw(this, isSelected, gameTime);
            }

            // Make the menu slide into place during transitions, using a
            // power curve to make things look more interesting (this makes
            // the movement slow down as it nears the end).
            float transitionOffset = (float)Math.Pow(TransitionPosition, 2);

            // Draw the menu title centered on the screen
            Vector2 titlePosition = new Vector2(graphics.Viewport.Width / 2, 80);
            Vector2 titleOrigin = font.MeasureString(menuTitle) / 2;
            // Use the menuTitleColor multiplied by the TransitionAlpha for transition effects to look good
            Color titleColor = menuTitleColor * TransitionAlpha;
            float titleScale = 1.25f;

            titlePosition.Y -= transitionOffset * 100;

            // Measure the string for computations
            Vector2 measurement = font.MeasureString(menuTitle);

            // Determine the proper "upper left corner" of the menuTitleBackground image. Notice how we need to take titleScale into account.
            Vector2 menuTitleBackgroundPosition = new Vector2(titlePosition.X - (titleOrigin.X * titleScale) - menuTitleBackgroundXPadding, titlePosition.Y - (titleOrigin.Y * titleScale) - menuTitleBackgroundYPadding);

            // Determine the proper width and height of the menuEntryBackground image, assigning the width to the X field and the height to the Y field.
            Vector2 menuTitleBackgroundDimensions = new Vector2((measurement.X * titleScale) + (menuTitleBackgroundXPadding * 2), (measurement.Y * titleScale) + (menuTitleBackgroundYPadding * 2));

            // Determine the proper "upper left corner" of the menuTitleBackground image. Notice how we need to take titleScale into account.
            Vector2 menuTitleSubBackgroundPosition = new Vector2(titlePosition.X - (titleOrigin.X * titleScale) - menuTitleSubBackgroundXPadding + menuTitleSubBackgroundXOffset, titlePosition.Y - (titleOrigin.Y * titleScale) - menuTitleSubBackgroundYPadding + menuTitleSubBackgroundYOffset);

            // Determine the proper width and height of the menuEntryBackground image, assigning the width to the X field and the height to the Y field.
            Vector2 menuTitleSubBackgroundDimensions = new Vector2((measurement.X * titleScale) + (menuTitleSubBackgroundXPadding * 2), (measurement.Y * titleScale) + (menuTitleSubBackgroundYPadding * 2));

            // If menuTitleSubBackground exists, draw it first. Notice how we create a Rectangle and use the offsets here.
            if (menuTitleSubBackground != null)
            {
                menuTitleSubBackground.Draw(spriteBatch,
                    new Rectangle((int)menuTitleSubBackgroundPosition.X, (int)menuTitleSubBackgroundPosition.Y, 
                        (int)menuTitleSubBackgroundDimensions.X, (int)menuTitleSubBackgroundDimensions.Y),
                        new Color(255, 255, 255) * TransitionAlpha, 0.0f);
            }

            // If menuEntryBackground exists, draw it second.
            if (menuTitleBackground != null)
            {
                menuTitleBackground.Draw(spriteBatch,
                    new Rectangle((int)menuTitleBackgroundPosition.X, (int)menuTitleBackgroundPosition.Y, 
                        (int)menuTitleBackgroundDimensions.X, (int)menuTitleBackgroundDimensions.Y),
                        new Color(255, 255, 255) * TransitionAlpha, 0.0f);

            }

            // Lastly, draw the menu entry text. Note we change the original 0 of the layerDepth to 0.0f. This is just for consistency and shouldn't change anything.
            spriteBatch.DrawString(font, menuTitle, titlePosition, titleColor, 0,
                titleOrigin, titleScale, SpriteEffects.None, 0.0f);

            spriteBatch.End();
        }


        #endregion
    }
}
