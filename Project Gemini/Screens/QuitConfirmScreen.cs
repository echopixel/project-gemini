#region File Description
//-----------------------------------------------------------------------------
// PauseMenuScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using ProjectGemini.Static_Classes;
using ProjectGemini.EnumTypes;
using ProjectGemini.GUI;
using System;
using ProjectGemini.Utilities;
using ProjectGemini.Managers;
#endregion

namespace ProjectGemini
{
    class QuitConfirmScreen : BaseMenu
    {
        #region Initialization

        /// <summary>
        /// Constructor.
        /// </summary>
        public QuitConfirmScreen()
            : base("Quit?", Color.White, 311, 230, 1, 200, 90, 100)
        { 
        }

        public override void LoadContent()
        {
            base.LoadContent();

            Texture2D image2 = content.Load<Texture2D>("Buttons/MainMenuSelected");
            Texture2D image1 = content.Load<Texture2D>("Buttons/MainMenu");

            // Create our menu entries.
            BaseButton returnMenuEntry = new BaseButton("No", image1, image2, Color.Black);
            //returnMenuEntry.Position = new Vector2(0, 220);
            BaseButton quitMenuEntry = new BaseButton("Quit", image1, image2, Color.Black);
            
            // Hook up menu event handlers.
            returnMenuEntry.Selected += OnCancel;
            quitMenuEntry.Selected   += QuitGameMenuEntrySelected;

            // Add entries to the menu.
            MenuEntries.Add(returnMenuEntry);
            MenuEntries.Add(quitMenuEntry);

            
        }

        #endregion

        #region Handle Input

        /// <summary>
        /// Event handler for when the Quit Game menu entry is selected.
        /// </summary>
        void QuitGameMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.TraceScreens(); //TESTING LINE
            LoadingScreen.Load(ScreenManager, false, null, new BackgroundScreen(),
                                                           new MainMenuScreen());
            GameState.RefreshSaves();
            GameState.Currency = 0;
            GameState.Events.Clear();
            GameState.InteractableEvents.Clear();
            GameState.OpenedChests.Clear();
            FlagManager.ClearFlags();
            TimedMessageBoxManager.ClearItemDropMessages();
            GameState.HeroOne.Equip(GameState.Armors[0]);
            GameState.HeroOne.Equip(GameState.Armors[1]);
            GameState.HeroOne.Equip(GameState.Armors[2]);
            GameState.HeroOne.Equip(GameState.Weapons[0]);
            GameState.HeroOne.Skills.Clear();
            
            GameState.Inventory.Clear();
            foreach (Item item in GameState.Items)
            {
                item.itemCurrentStack = 0;
            }
            foreach (Armor armor in GameState.Armors)
            {
                armor.itemCurrentStack = 0;
            }
            foreach (Weapon weapon in GameState.Weapons)
            {
                weapon.itemCurrentStack = 0;
            }
            
        }

        #endregion

    }
}
