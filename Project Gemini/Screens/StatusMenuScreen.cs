﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ProjectGemini.Static_Classes;
using ProjectGemini.Utilities;

namespace ProjectGemini.Screens
{
    class StatusMenuScreen : BaseMenu
    {
        public StatusMenuScreen()
            : base("Status", Color.White, 81, 230, 3, 200, 90, 100)
        {
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            GraphicsDevice graphics = ScreenManager.GraphicsDevice;
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            SpriteFont font = ScreenManager.Font;

            spriteBatch.Begin();

            

            #region First Column

            string text = "Level: " + GameState.HeroOne.Level;
            text += "\nHP: " + GameState.HeroOne.CurrentHP + " / " + GameState.HeroOne.MaxHP;
            text += "\nHPRegen: " + GameState.HeroOne.HPRegen.ToString("f1");

            text += "\n\nPhysical: " + GameState.HeroOne.Damage;
            text += "\nPhy Crit Chance: " + GameState.HeroOne.DamageCritChance.ToString("f1") + "%";
            text += "\nPhy Crit Damage: " + (GameState.HeroOne.DamageCritDamage + 100) + "%";
            text += "\nDefense: " + GameState.HeroOne.Defense;
            text += "\nPhy Reduction: " + GameState.HeroOne.DefenseRating + "%";
            text += "\nHealing: " + GameState.HeroOne.Heal;

            text += "\n\nSTR: " + GameState.HeroOne.TotalStr;
            text += "\nDEX: " + GameState.HeroOne.TotalDex;
            text += "\nSTA: " + GameState.HeroOne.TotalSta;

            spriteBatch.Draw(TextBackground,
                 new Rectangle(
                     (graphics.Viewport.Width * 1 / 12) - 10,
                     yIndent - 20,
                     (int)(font.MeasureString(text).X * .7) + 20, (int)(font.MeasureString(text).Y * .7) + 10),
                     new Rectangle(0, 0, 1, 1), Color.White * .4f);

            spriteBatch.DrawString(font, text, new Vector2(graphics.Viewport.Width * 1 / 12, yIndent - 15), Color.White, 0,
                new Vector2(0, 0), .7f, SpriteEffects.None, 0.0f);

            #endregion

            #region Second Column

            text = "EXP: " + GameState.HeroOne.EXP + " / 1000";
            text += "\nMP: " + GameState.HeroOne.CurrentMP + " / " + GameState.HeroOne.MaxMP;
            text += "\nMPRegen: " + GameState.HeroOne.MPRegen.ToString("f1");

            text += "\n\nMagic: " + GameState.HeroOne.Magic;
            text += "\nMag Crit Chance: " + GameState.HeroOne.MagicCritChance.ToString("f1") + "%";
            text += "\nMag Crit Damage: " + (GameState.HeroOne.MagicCritDamage + 100) + "%";
            text += "\nResistance: " + GameState.HeroOne.Resistance;
            text += "\nMag Reduction: " + GameState.HeroOne.ResistanceRating + "%";
            text += "\nHaste: " + GameState.HeroOne.Haste.ToString("f1") + "%";

            text += "\n\nINT: " + GameState.HeroOne.TotalInt;
            text += "\nALA: " + GameState.HeroOne.TotalAla;
            text += "\nWIS: " + GameState.HeroOne.TotalWis;

            spriteBatch.Draw(TextBackground,
                 new Rectangle(
                     (graphics.Viewport.Width * 5 / 12) - 10,
                     yIndent - 20,
                     (int)(font.MeasureString(text).X * .7) + 20, (int)(font.MeasureString(text).Y * .7) + 10),
                     new Rectangle(0, 0, 1, 1), Color.White * .4f);

            spriteBatch.DrawString(font, text, new Vector2(graphics.Viewport.Width * 5 / 12, yIndent - 15), Color.White, 0,
                new Vector2(0, 0), .7f, SpriteEffects.None, 0.0f);

            #endregion

            #region Third Column

            text = "Equipment:\n";
            text += GameState.HeroOne.Weapon.itemName + "\n";
            text += GameState.HeroOne.ArmorHead.itemName + "\n";
            text += GameState.HeroOne.ArmorChest.itemName + "\n";
            text += GameState.HeroOne.ArmorLegs.itemName + "\n\n";

            text += "Skills:";

            foreach (Ability skill in GameState.HeroOne.Skills)
            {
                text += "\n" + skill.abilityName;
            }

            spriteBatch.Draw(TextBackground,
                 new Rectangle(
                     (graphics.Viewport.Width * 9 / 12) - 10,
                     yIndent - 20,
                     (int)(font.MeasureString(text).X * .7 + 20), (int)(font.MeasureString(text).Y * .7) + 10),
                     new Rectangle(0, 0, 1, 1), Color.White * .4f);

            spriteBatch.DrawString(font, text, new Vector2(graphics.Viewport.Width * 9 / 12, yIndent - 15), Color.White, 0,
                    new Vector2(0, 0), .7f, SpriteEffects.None, 0.0f);

            #endregion

            spriteBatch.End();
        }

    }
}
