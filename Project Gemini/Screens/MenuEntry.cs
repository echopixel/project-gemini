#region File Description
//-----------------------------------------------------------------------------
// MenuEntry.cs
//
// XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;
using ProjectGemini.Message_Boxes;
#endregion

namespace ProjectGemini
{
    /// <summary>
    /// Helper class represents a single entry in a MenuScreen. By default this
    /// just draws the entry text string, but it can be customized to display menu
    /// entries in different ways. This also provides an event that will be raised
    /// when the menu entry is selected.
    /// </summary>
    class MenuEntry
    {
        #region Fields

        /// <summary>
        /// The text rendered for this entry.
        /// </summary>
        string text;

        /// <summary>
        /// The position at which the entry is drawn. This is set by the MenuScreen
        /// each frame in Update.
        /// </summary>
        Vector2 position;

        /// <summary>
        /// The text color for a selected menu item.
        /// </summary>
        private Color textSelectedColor;

        /// <summary>
        /// The text color for an unselected menu item.
        /// </summary>
        private Color textColor;

        /// <summary>
        /// The texture, if any, located underneath the menuEntryBackground.
        /// </summary>
        private BorderedTextureBox menuEntrySubBackground;

        /// <summary>
        /// The texture, if any, located underneath the menu entry text.
        /// </summary>
        private BorderedTextureBox menuEntryBackground;

        /// <summary>
        /// The amount of padding in pixels to add between the top and bottom of the string and the edge of the menuEntryBackground texture.
        /// </summary>
        private int menuEntryBackgroundYPadding;

        /// <summary>
        /// The amount of padding in pixels to add between the left and right of the string and the edge of the menuEntryBackground texture;
        /// </summary>
        private int menuEntryBackgroundXPadding;

        /// <summary>
        /// The amount of "padding" in pixels to add between the left and right of the string and the edge of the menuEntrySubBackground texture.
        /// </summary>
        private int menuEntrySubBackgroundXPadding;

        /// <summary>
        /// The amount of "padding" in pixels to add between the top and bottom of the string and the edge of the menuEntrySubBackground texture.
        /// </summary>
        private int menuEntrySubBackgroundYPadding;

        /// <summary>
        /// The distance in pixels which the menuEntrySubBackground image will be offset along the X axis from menuEntryBackground.
        /// Positive for right, negative for left.
        /// </summary>
        private int menuEntrySubBackgroundXOffset;

        /// <summary>
        /// The distance in pixels which the menuEntrySubBackground image will be offset along the X axis from menuEntryBackground.
        /// Positive for down, negative for up.
        /// </summary>
        private int menuEntrySubBackgroundYOffset;

        #endregion

        #region Properties


        /// <summary>
        /// Gets or sets the text of this menu entry.
        /// </summary>
        public string Text
        {
            get { return text; }
            set { text = value; }
        }


        /// <summary>
        /// Gets or sets the position at which to draw this menu entry.
        /// </summary>
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }


        #endregion

        #region Events


        /// <summary>
        /// Event raised when the menu entry is selected.
        /// </summary>
        public event EventHandler<PlayerIndexEventArgs> Selected;
        public event EventHandler<PlayerIndexEventArgs> Deleted;


        /// <summary>
        /// Method for raising the Selected event.
        /// </summary>
        protected internal virtual void OnSelectEntry(PlayerIndex playerIndex)
        {
            if (Selected != null)
                Selected(this, new PlayerIndexEventArgs(playerIndex));
        }

        /// <summary>
        /// Method for raising the Deleted event.
        /// </summary>
        protected internal virtual void OnDeleteEntry(PlayerIndex playerIndex)
        {
            if (Deleted != null)
                Deleted(this, new PlayerIndexEventArgs(playerIndex));
        }


        #endregion

        #region Initialization


        /// <summary>
        /// Constructs a new menu entry with the specified text and default colors of yellow (selected) and white (unselected).
        /// </summary>
        public MenuEntry(string text)
            : this(text, Color.Yellow, Color.White, null, 0, 0, 0, 0, null, 0, 0, 0, 0, 0, 0)
        {
        }

        /// <summary>
        /// Constructs a new menu entry with the specified text.
        /// </summary>
        /// <param name="text">The text for the MenuEntry item.</param>
        /// <param name="textSelectedColor">The color when the menu entry is the selected menu item (not used on WP7).</param>
        /// <param name="textColor">The text color when the menu entry is not the selected item (used for all items on WP7).</param>
        /// <param name="menuEntryBackground">The image, if any, to be drawn behind the MenuEntry text. Use null if not desired.</param>
        /// <param name="menuEntrySubBackground">The image, if any, to be drawn behind the menuEntryBackground image. Use null if not desired.</param>
        /// <param name="menuEntryBackgroundBorderWidth">The width in pixels of the border along the width (i.e. horizontal sides) of the menuEntryBackground image. Use 0 for no border.</param>
        /// <param name="menuEntryBackgroundBorderHeight">The width in pixels of the border along the height (i.e. vertical sides) of the menuEntryBackground image. Use 0 for no border.</param>
        /// <param name="menuEntryBackgroundXPadding">The amount of "padding" in pixels to add between the left and right of the string and the edge of the menuEntryBackground texture.</param>
        /// <param name="menuEntryBackgroundYPadding">The amount of "padding" in pixels to add between the top and bottom of the string and the edge of the menuEntryBackground texture.</param>
        /// <param name="menuEntrySubBackgroundBorderWidth">The width in pixels of the border along the width (i.e. horizontal sides) of the menuEntrySubBackground image. Use 0 for no border.</param>
        /// <param name="menuEntrySubBackgroundBorderHeight">The width in pixels of the border along the height (i.e. vertical sides) of the menuEntrySubBackground image. Use 0 for no border.</param>
        /// <param name="menuEntrySubBackgroundXPadding">The amount of "padding" in pixels to add between the left and right of the string and the edge of the menuEntrySubBackground texture.</param>
        /// <param name="menuEntrySubBackgroundYPadding">The amount of "padding" in pixels to add between the top and bottom of the string and the edge of the menuEntrySubBackground texture.</param>
        /// <param name="menuEntrySubBackgroundXOffset">The distance in pixels which the menuEntrySubBackground image will be offset along the X axis from menuEntryBackground. Positive for right, negative for left.</param>
        /// <param name="menuEntrySubBackgroundYOffset">The distance in pixels which the menuEntrySubBackground image will be offset along the X axis from menuEntryBackground. Positive for down, negative for up.</param>
        public MenuEntry(string text, Color textSelectedColor, Color textColor, 
            Texture2D menuEntryBackground, int menuEntryBackgroundBorderWidth, int menuEntryBackgroundBorderHeight, 
            int menuEntryBackgroundXPadding, int menuEntryBackgroundYPadding, Texture2D menuEntrySubBackground, 
            int menuEntrySubBackgroundBorderWidth, int menuEntrySubBackgroundBorderHeight, int menuEntrySubBackgroundXPadding, 
            int menuEntrySubBackgroundYPadding, int menuEntrySubBackgroundXOffset, int menuEntrySubBackgroundYOffset)
        {
            this.text = text;

            this.textSelectedColor = textSelectedColor;
            this.textColor = textColor;

            this.menuEntryBackgroundXPadding = menuEntryBackgroundXPadding;
            this.menuEntryBackgroundYPadding = menuEntryBackgroundYPadding;

            this.menuEntrySubBackgroundXPadding = menuEntrySubBackgroundXPadding;
            this.menuEntrySubBackgroundYPadding = menuEntrySubBackgroundYPadding;

            this.menuEntrySubBackgroundXOffset = menuEntrySubBackgroundXOffset;
            this.menuEntrySubBackgroundYOffset = menuEntrySubBackgroundYOffset;

            if (menuEntryBackground != null)
                this.menuEntryBackground = new BorderedTextureBox(menuEntryBackground, menuEntryBackgroundBorderWidth, menuEntryBackgroundBorderHeight);
            else
                this.menuEntryBackground = null;
            if (menuEntrySubBackground != null)
                this.menuEntrySubBackground = new BorderedTextureBox(menuEntrySubBackground, menuEntrySubBackgroundBorderWidth, menuEntrySubBackgroundBorderHeight);
            else
                this.menuEntrySubBackground = null;
        }

        /// <summary>
        /// Constructs a new menu entry with specified text plus adds some default parameters for simple usage.
        /// </summary>
        /// <param name="text"> The text you want to display in the menu entry.</param>
        /// <param name="menuEntryBackground"> The Texture2D you want for menuEntryBackground.</param>
        /// <param name="menuEntrySubBackground"> The Texture2D you want for menuEntrySubBackground.</param>
        /// <param name="maxLength"> The longest MenuEntry name for the menu. Needed to attempt to make width equal for all elements in menu.</param>
        public MenuEntry(string text, Texture2D menuEntryBackground, Texture2D menuEntrySubBackground, int maxLength)
        {
            int xPadding = alignMenuEntry(text, maxLength);
            int yPadding = 0;
            int offSet = 4;
            int borderWidth = 0;
            int borderHeight = 0;
            this.text = text;

            this.textSelectedColor = Color.RoyalBlue;
            this.textColor = Color.Black;

            this.menuEntryBackgroundXPadding = xPadding;
            this.menuEntryBackgroundYPadding = yPadding;

            this.menuEntrySubBackgroundXPadding = xPadding;
            this.menuEntrySubBackgroundYPadding = yPadding;

            this.menuEntrySubBackgroundXOffset = offSet;
            this.menuEntrySubBackgroundYOffset = offSet;

            this.menuEntryBackground = new BorderedTextureBox(menuEntryBackground, borderWidth, borderHeight);

            this.menuEntrySubBackground = new BorderedTextureBox(menuEntrySubBackground, borderWidth, borderHeight);
        }
        /// <summary>
        /// The attempt at keeping all menu elements the same width.
        /// Not 100% accurate because of fonts not being mono and spaces being smaller than characters.
        /// </summary>
        /// <param name="text"> The MenuEntry text value.</param>
        /// <param name="maxLength"> The largest MenuEntry name in the given grouping.</param>
        /// <returns></returns>
        private int alignMenuEntry(string text, int maxLength)
        {
            int charSpace = 8;
            int padding = 15;
            int _text = text.Length;
            if (maxLength > _text)
                return (maxLength - text.Length) * charSpace + padding;
            else
                return padding;
        }


        #endregion

        #region Update and Draw


        /// <summary>
        /// Updates the menu entry.
        /// </summary>
        public virtual void Update(MenuScreen screen, bool isSelected, GameTime gameTime)
        {
            // When the menu selection changes, entries gradually fade between
            // their selected and deselected appearance, rather than instantly
            // popping to the new state.
            float fadeSpeed = (float)gameTime.ElapsedGameTime.TotalSeconds * 4;
        }


        /// <summary>
        /// Draws the menu entry. This can be overridden to customize the appearance.
        /// </summary>
        public virtual void Draw(MenuScreen screen, bool isSelected, GameTime gameTime)
        {
            // Draw the selected entry in yellow, otherwise white.
            Color color = isSelected ? textSelectedColor : textColor;

            // Modify the alpha to fade text out during transitions.
            color *= screen.TransitionAlpha;

            // Draw text, centered on the middle of each line.
            ScreenManager screenManager = screen.ScreenManager;
            SpriteBatch spriteBatch = screenManager.SpriteBatch;
            SpriteFont font = screenManager.Font;

            Vector2 origin = new Vector2(0, font.LineSpacing / 2);

            // Measure the string for computational purposes.
            Vector2 measurement = font.MeasureString(text);

            // Determine the proper upper left corner of the menuEntryBackground image.
            Vector2 menuEntryBackgroundPosition = new Vector2(position.X - menuEntryBackgroundXPadding, position.Y - origin.Y - menuEntryBackgroundYPadding);

            // Determine the proper width and height of the menuEntryBackground image, assigning the width to the X field and the height to the Y field.
            Vector2 menuEntryBackgroundDimensions = new Vector2(measurement.X + (menuEntryBackgroundXPadding * 2), measurement.Y + (menuEntryBackgroundYPadding * 2));

            // Determine the proper "upper left corner" of the menuEntrySubBackground image.
            Vector2 menuEntrySubBackgroundPosition = new Vector2(position.X - menuEntrySubBackgroundXPadding + menuEntrySubBackgroundXOffset, position.Y - origin.Y - menuEntrySubBackgroundYPadding + menuEntrySubBackgroundYOffset);

            // Determine the proper width and height of the menuEntrySubBackground image, assigning the width to the X field and the height to the Y field.
            Vector2 menuEntrySubBackgroundDimensions = new Vector2(measurement.X + (menuEntrySubBackgroundXPadding * 2), measurement.Y + (menuEntrySubBackgroundYPadding * 2));

            // If menuEntrySubBackground exists, draw it first.  Notice how we create a Rectangle and use the offsets here.
            if (menuEntrySubBackground != null)
            {
                menuEntrySubBackground.Draw(spriteBatch,
                    new Rectangle((int)menuEntrySubBackgroundPosition.X, (int)menuEntrySubBackgroundPosition.Y,
                        (int)menuEntrySubBackgroundDimensions.X, (int)menuEntrySubBackgroundDimensions.Y),
                        new Color(255, 255, 255) * screen.TransitionAlpha, 0.0f);
            }

            // If menuEntryBackground exists, draw it second.
            if (menuEntryBackground != null)
            {
                menuEntryBackground.Draw(spriteBatch,
                    new Rectangle((int)menuEntryBackgroundPosition.X, (int)menuEntryBackgroundPosition.Y,
                        (int)menuEntryBackgroundDimensions.X, (int)menuEntryBackgroundDimensions.Y),
                        new Color(190, 190, 187) * screen.TransitionAlpha, 0.0f);
            }

            spriteBatch.DrawString(font, text, position, color, 0, origin, 1.0f, SpriteEffects.None, 0.0f);
        }


        /// <summary>
        /// Queries how much space this menu entry requires.
        /// </summary>
        public virtual int GetHeight(MenuScreen screen)
        {
            return screen.ScreenManager.Font.LineSpacing;
        }


        /// <summary>
        /// Queries how wide the entry is, used for centering on the screen.
        /// </summary>
        public virtual int GetWidth(MenuScreen screen)
        {
            return (int)screen.ScreenManager.Font.MeasureString(Text).X;
        }


        #endregion
    }
}
