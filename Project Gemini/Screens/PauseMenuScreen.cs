#region File Description
//-----------------------------------------------------------------------------
// PauseMenuScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using ProjectGemini.Static_Classes;
using ProjectGemini.EnumTypes;
using ProjectGemini.GUI;
using ProjectGemini.Screens.Menus;
using ProjectGemini.Screens;
#endregion

namespace ProjectGemini
{
    /// <summary>
    /// The pause menu comes up over the top of the game,
    /// giving the player options to resume or quit.
    /// </summary>
    class PauseMenuScreen : BaseMenu
    {
        #region Initialization

        /// <summary>
        /// Constructor.
        /// </summary>
        public PauseMenuScreen()
            : base("Paused", Color.White, 81, 230, 2, 200, 90, 100)
        { 
        }

        public override void LoadContent()
        {
            base.LoadContent();

            Texture2D image2 = content.Load<Texture2D>("Buttons/MainMenuSelected");
            Texture2D image1 = content.Load<Texture2D>("Buttons/MainMenu");

            // Create our menu entries.
            BaseButton resumeGameMenuEntry = new BaseButton("Resume Game", image1, image2, Color.Black);
            BaseButton inventoryMenuEntry = new BaseButton("Inventory", image1, image2, Color.Black);
            BaseButton skillsMenuEntry = new BaseButton("Skills", image1, image2, Color.Black);
            BaseButton statusMenuEntry = new BaseButton("Status", image1, image2, Color.Black);
            BaseButton equipmentMenuEntry = new BaseButton("Equipment", image1, image2, Color.Black);
            BaseButton partyAIMenuEntry = new BaseButton("Party AI", image1, image2, Color.Black, Color.Gray);
            BaseButton questLogMenuEntry = new BaseButton("Quest Log", image1, image2, Color.Black, Color.Gray);
            BaseButton saveGameMenuEntry = new BaseButton("Save Game", image1, image2, Color.Black);
            BaseButton optionMenuEntry = new BaseButton("Options", image1, image2, Color.Black, Color.Gray);
            BaseButton quitGameMenuEntry = new BaseButton("Quit Game", image1, image2, Color.Black);

            // Hook up menu event handlers.
            resumeGameMenuEntry.Selected += OnCancel;
            equipmentMenuEntry.Selected  += EquipmentMenuEntrySelected;
            inventoryMenuEntry.Selected  += InventoryMenuEntrySelected;
            //optionMenuEntry.Selected     += OptionMenuEntrySelected;
            skillsMenuEntry.Selected     += SkillsMenuEntrySelected;
            saveGameMenuEntry.Selected   += SaveGameMenuEntrySelected;
            quitGameMenuEntry.Selected   += QuitGameMenuEntrySelected;
            statusMenuEntry.Selected     += StatusMenuEntrySelected;

            // Add entries to the menu.
            MenuEntries.Add(resumeGameMenuEntry);
            MenuEntries.Add(inventoryMenuEntry);
            MenuEntries.Add(skillsMenuEntry);
            MenuEntries.Add(statusMenuEntry);
            MenuEntries.Add(equipmentMenuEntry);
            MenuEntries.Add(partyAIMenuEntry);
            MenuEntries.Add(questLogMenuEntry);
            MenuEntries.Add(saveGameMenuEntry);
            MenuEntries.Add(optionMenuEntry);
            MenuEntries.Add(quitGameMenuEntry);
        }

        #endregion

        #region Draw

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            SpriteFont font = ScreenManager.Font;

            spriteBatch.Begin();

            Vector2 size = font.MeasureString("DG: " + GameState.Currency.ToString());
            spriteBatch.DrawString(font, 
                "DG: " + GameState.Currency.ToString(), 
                new Vector2(ScreenManager.GraphicsDevice.Viewport.Width * 0.95f - size.X,
                    ScreenManager.GraphicsDevice.Viewport.Height * 0.95f - size.Y), 
                    Color.White);

            spriteBatch.End();
        }

        #endregion

        #region Handle Input

        /// <summary>
        /// Event handler for when the Save Game menu entry is selected.
        /// </summary>
        void SaveGameMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.AddScreen(new SaveLoadScreen("Save", null), e.PlayerIndex);
        }

        void InventoryMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.AddScreen(new InventoryMenuScreen(), e.PlayerIndex);
        }

        void SkillsMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.AddScreen(new SkillsMenuScreen(), e.PlayerIndex);
        }

        void EquipmentMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.AddScreen(new EquipmentMenuScreen(), e.PlayerIndex);
        }

        /// <summary>
        /// Event handler for when the Quit Game menu entry is selected.
        /// </summary>
        void OptionMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.AddScreen(new OptionsMenuScreen(), e.PlayerIndex);
        }

        /// <summary>
        /// Event handler for when the Quit Game menu entry is selected.
        /// </summary>
        void QuitGameMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.AddScreen(new QuitConfirmScreen(), e.PlayerIndex);
        }

        void StatusMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.AddScreen(new StatusMenuScreen(), e.PlayerIndex);
        }

        #endregion

    }
}

