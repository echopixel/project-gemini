﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ProjectGemini.Static_Classes;
using ProjectGemini.EnumTypes;

namespace ProjectGemini.Screens.Menus
{
    class SaveLoadScreen : BaseMenu
    {
        List<BaseButton> fileEntries;
        BaseButton newSaveEntry;
        Texture2D image1;
        Texture2D image2;
        MainMenuScreen Menu;

        #region Initialization

        public SaveLoadScreen(string title, MainMenuScreen menu)
            : base(title, Color.White, 311, 0, 1, 150, 90, 6)
        {
            fileEntries = new List<BaseButton>();

            Menu = menu;
        }

        /// <summary>
        /// Loads the assets used for this menu screen
        /// </summary>
        public override void LoadContent()
        {
            base.LoadContent();

            Reload();
        }

        /// <summary>
        /// Unloads the assets for this menu screen.
        /// </summary>
        public override void UnloadContent()
        {
            if (content != null)
                content.Unload();
        }

        public void Reload()
        {
            MenuEntries.Clear();
            fileEntries.Clear();

            image2 = content.Load<Texture2D>("Buttons/MainMenuSelected");
            image1 = content.Load<Texture2D>("Buttons/MainMenu");

            

            // Back button
            BaseButton backButton = new BaseButton("Back", image1, image2, Color.Black);
            backButton.Selected += OnCancel;
            fileEntries.Add(backButton);

            foreach (string fileName in GameState.GameSaves)
            {
                BaseButton fileEntry = new BaseButton(fileName, image1, image2, Color.Black);
                fileEntry.Selected += OnFileSelection;
                fileEntries.Add(fileEntry);
            }

            if (menuTitle == "Save")
            {
                newSaveEntry = new BaseButton("New Save", image1, image2, Color.Black);
                newSaveEntry.Selected += OnNewSaveSelection;
                fileEntries.Add(newSaveEntry);
            }

            // Add entries to the menu.
            foreach (BaseButton fileEntry in fileEntries)
            {
                MenuEntries.Add(fileEntry);
            }

            while (selectedEntry >= MenuEntries.Count)
            {
                selectedEntry--;
            }
        }

        #endregion

        #region Handle Input

        void OnNewSaveSelection(object sender, PlayerIndexEventArgs e)
        {
            int saveIndex = MenuEntries.Count - 2;
            GameState.SaveGame(DateTime.Now.Month + "-" + DateTime.Now.Day + "-" + DateTime.Now.Year + " " +
                DateTime.Now.Hour + "-" + DateTime.Now.Minute + "-" + DateTime.Now.Second + "-" + DateTime.Now.Millisecond + ".sav");
            MenuEntries[saveIndex + 1].Text = GameState.LastPlayedSave; // After saving, last played save becomes that save
            MenuEntries[saveIndex + 1].Selected -= OnNewSaveSelection;
            MenuEntries[saveIndex + 1].Selected += OnFileSelection;
            newSaveEntry = new BaseButton("New Save", image1, image2, Color.Black);
            newSaveEntry.Selected += OnNewSaveSelection;
            MenuEntries.Add(newSaveEntry);
            GameState.RefreshSaves();
        }

        void OnFileSelection(object sender, PlayerIndexEventArgs e)
        {
            string saveIndex = GameState.GameSaves[MenuEntries.IndexOf(((BaseButton)sender)) - 1];
            //Console.WriteLine("File Selected: " + saveIndex);
            if (menuTitle == "Save")
            {
                ScreenManager.AddScreen(new SaveLoadConfirmScreen(SaveLoadDeleteType.Save, saveIndex), e.PlayerIndex);
            }
            else if (menuTitle == "Load")
            {
                ScreenManager.AddScreen(new SaveLoadConfirmScreen(SaveLoadDeleteType.Load, saveIndex), e.PlayerIndex);
            }
        }

        protected override void OnDeleteEntry(int entryIndex, PlayerIndex playerIndex)
        {
            if (MenuEntries[selectedEntry].Text != "Back" && MenuEntries[selectedEntry].Text != "New Save")
            {
                ScreenManager.AddScreen(new SaveLoadConfirmScreen(SaveLoadDeleteType.Delete, GameState.GameSaves[entryIndex - 1]), playerIndex);
            }
        }

        protected override void OnCancel(PlayerIndex playerIndex)
        {
            //Console.WriteLine("test");
            if (Menu != null)
            {
                //Console.WriteLine("test");
                Menu.Reload();
            }
            this.ExitScreen();
        }

        #endregion

        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            //Console.WriteLine(GameState.GameSaves.Count);

            if (MenuEntries[selectedEntry].Text != "Back" && MenuEntries[selectedEntry].Text != "New Save")
            {
                Bar.xText = "Delete";
            }
            else
            {
                Bar.xText = "";
            }
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            GraphicsDevice graphics = ScreenManager.GraphicsDevice;
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            SpriteFont font = ScreenManager.Font;

            spriteBatch.Begin();

            if (DrawUpArrow)
            {
                spriteBatch.Draw(UpArrow, new Vector2(graphics.Viewport.Width / 2 - UpArrow.Width / 2, menuEntries[section].Position.Y + (menuEntries[section].Image.Height / 2) - UpArrow.Height * 3), Color.White);
            }
            if (DrawDownArrow)
            {
                spriteBatch.Draw(DownArrow, new Vector2(graphics.Viewport.Width / 2 - DownArrow.Width / 2, menuEntries[section + 5].Position.Y + (menuEntries[section + 5].Image.Height / 2) + DownArrow.Height * 2), Color.White);
            }

            spriteBatch.End();
        }
    }
}
