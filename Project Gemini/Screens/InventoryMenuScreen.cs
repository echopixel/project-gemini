#region File Description
//-----------------------------------------------------------------------------
// PauseMenuScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using ProjectGemini.Static_Classes;
using ProjectGemini.EnumTypes;
using ProjectGemini.GUI;
using ProjectGemini.Utilities;
#endregion

namespace ProjectGemini
{
    /// <summary>
    /// The pause menu comes up over the top of the game,
    /// giving the player options to resume or quit.
    /// </summary>
    class InventoryMenuScreen : InventoryMenu
    {
        #region Initialization

        /// <summary>
        /// Constructor.
        /// </summary>
        public InventoryMenuScreen()
            : base("Inventory", Color.White, 81, 230, 3, 200, 90, 100)
        {
        }

        public override void LoadContent()
        {
            base.LoadContent();

            Reload();
        }

        public void Reload()
        {
            MenuEntries.Clear();
            selectedEntry = 0;

            Texture2D image2 = content.Load<Texture2D>("Buttons/MainMenuSelected");
            Texture2D image1 = content.Load<Texture2D>("Buttons/MainMenu");
            Texture2D icon = content.Load<Texture2D>("Icons/TestItemIcon");

            bool check = true;
            foreach (Item item in GameState.Inventory.items)
            {
                if (item.itemCurrentStack > 0)
                {
                    InventoryButton button = new InventoryButton(item.itemName, image1, image2, Color.Black, ScreenManager, item, this, item.itemIconTexture);
                    MenuEntries.Add(button);

                    check = false;
                }
            }
            if (check)
            {
                BaseButton button = new BaseButton("Inventory Empty", image1, image2, Color.Black);
                button.Selected += OnCancel;
                MenuEntries.Add(button);
            }
        }

        #endregion

        #region Handle Input


        #endregion

    }
}

