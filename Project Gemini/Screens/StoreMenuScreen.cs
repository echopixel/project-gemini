#region File Description
//-----------------------------------------------------------------------------
// MainMenuScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

using ProjectGemini.Static_Classes;
using ProjectGemini.EnumTypes;
using EasyStorage;
using System;
using ProjectGemini.Entities;
using ProjectGemini.Controllers;
#endregion

namespace ProjectGemini
{
    /// <summary>
    /// The main menu screen is the first thing displayed when the game starts up.
    /// </summary>
    class StoreMenuScreen : BaseMenu
    {

        #region Initialization

        /// <summary>
        /// Constructor fills in the menu contents.
        /// </summary>
        public StoreMenuScreen()
            : base("Project Gemini", Color.Black, 81, 0, 3, 300, 90, 4)
        {
        }

        /// <summary>
        /// Loads the assets used for this menu screen
        /// </summary>
        public override void LoadContent()
        {
            base.LoadContent();

            Texture2D image2 = content.Load<Texture2D>("Buttons/MainMenuSelected");
            Texture2D image1 = content.Load<Texture2D>("Buttons/MainMenu");

            BaseButton continueEntry = new BaseButton("Continue Game", image1, image2, Color.Black); //DONT USE SAME IMAGE TWICE
            BaseButton loadGameEntry = new BaseButton("Load Game", image1, image2, Color.Black);
            BaseButton playGameMenuEntry = new BaseButton("Play Game", image1, image2, Color.Black);
            BaseButton optionsMenuEntry = new BaseButton("Options", image1, image2, Color.Black);
            BaseButton exitMenuEntry = new BaseButton("Exit", image1, image2, Color.Black);

            // Hook up menu event handlers.
            continueEntry.Selected += ContinueMenuEntrySelected;
            loadGameEntry.Selected += LoadGameMenuEntrySelected;
            playGameMenuEntry.Selected += PlayGameMenuEntrySelected;
            optionsMenuEntry.Selected += OptionsMenuEntrySelected;
            exitMenuEntry.Selected += OnCancel;

            // Add entries to the menu.
            if (GameState.SaveDevice.FileExists(GameState.ContainerName, "test"))
            {
                MenuEntries.Add(continueEntry);
                MenuEntries.Add(loadGameEntry);
            }
            MenuEntries.Add(playGameMenuEntry);
            MenuEntries.Add(optionsMenuEntry);
            MenuEntries.Add(exitMenuEntry);
        }

        /// <summary>
        /// Unloads the assets for this menu screen.
        /// </summary>
        public override void UnloadContent()
        {
            if (content != null)
                content.Unload();
        }

        #endregion

        #region Handle Input

        /**
         * For Continue game
         * Normally would load the most recently saved game data.
         * */
        void ContinueMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, true, e.PlayerIndex,
                               new GameplayScreen(ScreenManager.Game, true));

            if (content == null)
                content = new ContentManager(GameState.ScreenManager.Game.Services, "Content");

            GameState.LoadGame(content, "");
        }

        /**
         * For Load Game
         * Normally would go to Load Game Menu
         * */
        void LoadGameMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            //ScreenManager.AddScreen(new LoadGameScreen(), e.PlayerIndex);
        }

        /// <summary>
        /// Event handler for when the Play Game menu entry is selected.
        /// </summary>
        void PlayGameMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, true, e.PlayerIndex,
                               new GameplayScreen(ScreenManager.Game, false));
        }


        /// <summary>
        /// Event handler for when the Options menu entry is selected.
        /// </summary>
        void OptionsMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.AddScreen(new OptionsMenuScreen(), e.PlayerIndex);
        }


        /// <summary>
        /// When the user cancels the main menu, ask if they want to exit the sample.
        /// </summary>
        protected override void OnCancel(PlayerIndex playerIndex)
        {
            ScreenManager.Game.Exit();

            /*
            const string message = "Are you sure you want to exit this sample?";

            MessageBoxScreen confirmExitMessageBox = new MessageBoxScreen(message);

            confirmExitMessageBox.Accepted += ConfirmExitMessageBoxAccepted;

            ScreenManager.AddScreen(confirmExitMessageBox, playerIndex);
            */
        }

        /*
        /// <summary>
        /// Event handler for when the user selects ok on the "are you sure
        /// you want to exit" message box.
        /// </summary>
        void ConfirmExitMessageBoxAccepted(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.Game.Exit();
        }
        */

        #endregion

    }
}
