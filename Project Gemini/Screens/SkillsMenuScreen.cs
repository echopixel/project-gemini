#region File Description
//-----------------------------------------------------------------------------
// PauseMenuScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using ProjectGemini.Static_Classes;
using ProjectGemini.EnumTypes;
using ProjectGemini.GUI;
using ProjectGemini.Utilities;
#endregion

namespace ProjectGemini
{
    class SkillsMenuScreen : SkillMenu
    {
        #region Initialization

        public SkillsMenuScreen()
            : base("Skills", Color.White, 81, 230, 3, 200, 90, 100)
        {
        }

        public override void LoadContent()
        {
            base.LoadContent();

            Reload();
        }

        public void Reload()
        {
            MenuEntries.Clear();

            Texture2D image2 = content.Load<Texture2D>("Buttons/mainMenuSelected");
            Texture2D image1 = content.Load<Texture2D>("Buttons/mainMenu");

            for (int i = 0; i < GameState.HeroOne.AvailableSkills.Count; i++)
            {
                Color color = Color.DarkRed;

                if (GameState.HeroOne.Skills.Contains(GameState.HeroOne.AvailableSkills[i]))
                {
                    color = Color.White;
                }
                else if(i < 3 || GameState.HeroOne.Skills.Contains(GameState.HeroOne.AvailableSkills[i - 3]))
                {
                    color = Color.DarkGreen;
                }

                SkillButton button = new SkillButton(GameState.HeroOne.AvailableSkills[i].abilityName, image1, image2, Color.White, ScreenManager, GameState.HeroOne.AvailableSkills[i], this, GameState.HeroOne.AvailableSkills[i].abilityIconTexture, color);
                MenuEntries.Add(button);
            }
        }

        #endregion
    }
}

