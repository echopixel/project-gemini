#region File Description
//-----------------------------------------------------------------------------
// PauseMenuScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using ProjectGemini.Static_Classes;
using ProjectGemini.EnumTypes;
using ProjectGemini.GUI;
using ProjectGemini.Utilities;
#endregion

namespace ProjectGemini
{
    /// <summary>
    /// The pause menu comes up over the top of the game,
    /// giving the player options to resume or quit.
    /// </summary>
    class EquipmentMenuScreen : EquipmentMenu
    {
        #region Initialization

        /// <summary>
        /// Constructor.
        /// </summary>
        public EquipmentMenuScreen()
            : base("Equipment", Color.White, 81, 230, 1, 200, 90, 4)
        {
            
        }

        public override void LoadContent()
        {
            base.LoadContent();

            Reload();
        }

        public void Reload()
        {
            MenuEntries.Clear();
            selectedEntry = (int)current;

            Texture2D image1 = content.Load<Texture2D>("Buttons/mainMenu");
            Texture2D image2 = content.Load<Texture2D>("Buttons/mainMenuSelected");
            Texture2D image3 = content.Load<Texture2D>("Buttons/mainMenu");
            Texture2D image4 = content.Load<Texture2D>("Buttons/mainMenuSelected");
            Texture2D icon = content.Load<Texture2D>("Icons/TestItemIcon");

            EquipmentButton heroWeapon = new EquipmentButton(GameState.HeroOne.Weapon.itemName, image3, image4, Color.Black, ScreenManager, GameState.HeroOne.Weapon, this, null);
            EquipmentButton heroHead = new EquipmentButton(GameState.HeroOne.ArmorHead.itemName, image3, image4, Color.Black, ScreenManager, GameState.HeroOne.ArmorHead, this, null);
            EquipmentButton heroChest = new EquipmentButton(GameState.HeroOne.ArmorChest.itemName, image3, image4, Color.Black, ScreenManager, GameState.HeroOne.ArmorChest, this, null);
            EquipmentButton heroLegs = new EquipmentButton(GameState.HeroOne.ArmorLegs.itemName, image3, image4, Color.Black, ScreenManager, GameState.HeroOne.ArmorLegs, this, null);

            MenuEntries.Add(heroWeapon);
            MenuEntries.Add(heroHead);
            MenuEntries.Add(heroChest);
            MenuEntries.Add(heroLegs);

            weapons = 0;
            heads = 0;
            chests = 0;
            legs = 0;

            //bool check = true;
            foreach (Weapon weapon in GameState.Inventory.weapons)
            {
                EquipmentButton button = new EquipmentButton(weapon.itemName, image1, image2, Color.Black, ScreenManager, weapon, this, null);
                MenuEntries.Add(button);
                weapons++;
            }
            foreach (Armor head in GameState.Inventory.heads)
            {
                EquipmentButton button = new EquipmentButton(head.itemName, image1, image2, Color.Black, ScreenManager, head, this, null);
                MenuEntries.Add(button);
                heads++;
            }
            foreach (Armor chest in GameState.Inventory.chests)
            {
                EquipmentButton button = new EquipmentButton(chest.itemName, image1, image2, Color.Black, ScreenManager, chest, this, null);
                MenuEntries.Add(button);
                chests++;
            }
            foreach (Armor leg in GameState.Inventory.legs)
            {
                EquipmentButton button = new EquipmentButton(leg.itemName, image1, image2, Color.Black, ScreenManager, leg, this, null);
                MenuEntries.Add(button);
                legs++;
            }
            /*
            if (check)
            {
                BaseButton button = new BaseButton("Inventory Empty", image1, image2, Color.Black);
                button.Selected += OnCancel;
                MenuEntries.Add(button);
            }
             */
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            if (selectedEntry < 4)
                Bar.xText = "Unequip";
            else
                Bar.xText = "";
        }

        #endregion

        #region Handle Input

        protected override void OnDeleteEntry(int entryIndex, PlayerIndex playerIndex)
        {
            if (selectedEntry == 0)
            {
                GameState.HeroOne.Equip(GameState.Weapons[0]);
                Reload();
                GameState.HeroOne.UpdateStats();
            }
            if (selectedEntry == 1)
            {
                GameState.HeroOne.Equip(GameState.Armors[0]);
                Reload();
                GameState.HeroOne.UpdateStats();
            }
            if (selectedEntry == 2)
            {
                GameState.HeroOne.Equip(GameState.Armors[1]);
                Reload();
                GameState.HeroOne.UpdateStats();
            }
            if (selectedEntry == 3)
            {
                GameState.HeroOne.Equip(GameState.Armors[2]);
                Reload();
                GameState.HeroOne.UpdateStats();
            }
        }

        #endregion

    }
}

