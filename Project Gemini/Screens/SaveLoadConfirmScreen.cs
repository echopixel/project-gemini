﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ProjectGemini.Static_Classes;
using Microsoft.Xna.Framework.Content;
using ProjectGemini.Screens.Menus;
using ProjectGemini.EnumTypes;

namespace ProjectGemini.Screens
{
    class SaveLoadConfirmScreen : BaseMenu
    {
        string saveIndex;
        SaveLoadDeleteType screenType;

        #region Initialization

        /// <summary>
        /// Constructor.
        /// </summary>
        public SaveLoadConfirmScreen(SaveLoadDeleteType screenType, string saveIndex)
            : base("", Color.White, 311, 230, 1, 200, 90, 100)
        {
            this.screenType = screenType;

            switch (screenType)
            {
                case SaveLoadDeleteType.Save:
                    menuTitle = "Are you sure you want to\n   overwrite that file?";
                    break;
                case SaveLoadDeleteType.Load:
                    menuTitle = "Load this save file?";
                    break;
                case SaveLoadDeleteType.Delete:
                    menuTitle = "Are you sure you want to\n   delete that file?";
                    break;
            }

            this.saveIndex = saveIndex;
        }

        public override void LoadContent()
        {
            base.LoadContent();

            Texture2D image2 = content.Load<Texture2D>("Buttons/MainMenuSelected");
            Texture2D image1 = content.Load<Texture2D>("Buttons/MainMenu");

            // Create our menu entries.
            BaseButton returnMenuEntry = new BaseButton("No", image1, image2, Color.Black);
            //returnMenuEntry.Position = new Vector2(0, 220);
            BaseButton confirmMenuEntry = new BaseButton("Yes", image1, image2, Color.Black);
            
            // Hook up menu event handlers.
            returnMenuEntry.Selected += OnCancel;
            confirmMenuEntry.Selected   += ConfirmSelected;

            // Add entries to the menu.
            MenuEntries.Add(returnMenuEntry);
            MenuEntries.Add(confirmMenuEntry);

            
        }

        #endregion

        #region Handle Input

        /// <summary>
        /// Event handler for when the Quit Game menu entry is selected.
        /// </summary>
        void ConfirmSelected(object sender, PlayerIndexEventArgs e)
        {
            switch (screenType)
            {
                case SaveLoadDeleteType.Save:
                    GameState.SaveDevice.Delete(GameState.ContainerName, saveIndex);
                    GameState.SaveGame(DateTime.Now.Month + "-" + DateTime.Now.Day + "-" + DateTime.Now.Year + " " +
                    DateTime.Now.Hour + "-" + DateTime.Now.Minute + "-" + DateTime.Now.Second + "-" + DateTime.Now.Millisecond + ".sav");
                    GameState.RefreshSaves();
                    ((SaveLoadScreen)ScreenManager.screens[ScreenManager.screens.Count - 2]).Reload();
                    break;
                case SaveLoadDeleteType.Load:
                    LoadingScreen.Load(ScreenManager, true, e.PlayerIndex,
                                   new GameplayScreen(ScreenManager.Game, true));

                    if (content == null)
                        content = new ContentManager(GameState.ScreenManager.Game.Services, "Content");

                    GameState.LoadGame(content, saveIndex);
                    break;
                case SaveLoadDeleteType.Delete:
                    GameState.SaveDevice.Delete(GameState.ContainerName, saveIndex);
                    GameState.RefreshSaves();
                    ((SaveLoadScreen)ScreenManager.screens[ScreenManager.screens.Count - 2]).Reload();
                    break;
                
            }

            ExitScreen();
        }

        #endregion

    }
}
