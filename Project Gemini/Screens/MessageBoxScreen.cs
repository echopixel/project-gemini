#region File Description
//-----------------------------------------------------------------------------
// MessageBoxScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ProjectGemini.Message_Boxes;
using ProjectGemini.Static_Classes;
#endregion

namespace ProjectGemini
{
    /// <summary>
    /// A popup message box screen, used to display all manners of text 
    /// (NPC dialog, sign text, chest contents, etc.)
    /// </summary>
    class MessageBoxScreen : GameScreen
    {
        #region Fields

        string message;
        BorderedTextureBox borderedMessageBox;
        bool shouldFadeBackground;
        bool showAtTopOfScreen;
        bool isTreasureChestMessage;

        #endregion

        #region Events

        public event EventHandler<PlayerIndexEventArgs> Accepted;
        public event EventHandler<PlayerIndexEventArgs> Cancelled;
        public event EventHandler<PlayerIndexEventArgs> Deleted;

        #endregion

        #region Initialization


        /// <summary>
        /// Constructor automatically makes a message box with just the required text in it.
        /// </summary>
        public MessageBoxScreen(string message)
            : this(message, false, false, false, false)
        { }

        public MessageBoxScreen(string message, bool showAtTopOfScreen)
            : this(message, showAtTopOfScreen, false, false, false)
        { }

        public MessageBoxScreen(string message, bool showAtTopOfScreen, bool isTreasureChestMessage)
            : this(message, showAtTopOfScreen, isTreasureChestMessage, false, false)
        { }


        /// <summary>
        /// Constructor lets the caller specify whether to include the standard
        /// "A=ok, B=cancel" usage text prompt.
        /// </summary>
        public MessageBoxScreen(string message, bool showAtTopOfScreen, bool isTreasureChestMessage, bool shouldFadeBackground, bool includeUsageText)
        {
            const string usageText = "\nA button, Space, Enter = ok" +
                                     "\nB button, Esc = cancel"; 
            
            if (includeUsageText)
                this.message = message + usageText;
            else
                this.message = message;

            IsPopup = true;
            this.shouldFadeBackground = shouldFadeBackground;
            this.showAtTopOfScreen = showAtTopOfScreen;
            this.isTreasureChestMessage = isTreasureChestMessage;

            TransitionOnTime = TimeSpan.FromSeconds(0.2);
            TransitionOffTime = TimeSpan.FromSeconds(0.2);
        }


        /// <summary>
        /// Loads graphics content for this screen. This uses the shared ContentManager
        /// provided by the Game class, so the content will remain loaded forever.
        /// Whenever a subsequent MessageBoxScreen tries to load this same content,
        /// it will just get back another reference to the already loaded data.
        /// </summary>
        public override void LoadContent()
        {
            ContentManager content = ScreenManager.Game.Content;

            borderedMessageBox = new BorderedTextureBox(content.Load<Texture2D>("border"), 3);
        }


        #endregion

        #region Handle Input


        /// <summary>
        /// Responds to user input, accepting or cancelling the message box.
        /// </summary>
        public override void HandleInput(InputState input)
        {
            PlayerIndex playerIndex;

            // We pass in our ControllingPlayer, which may either be null (to
            // accept input from any player) or a specific index. If we pass a null
            // controlling player, the InputState helper returns to us which player
            // actually provided the input. We pass that through to our Accepted and
            // Cancelled events, so they can tell which player triggered them.
            if (input.IsMenuSelect(ControllingPlayer, out playerIndex))
            {
                // Raise the accepted event, then exit the message box.
                if (Accepted != null)
                    Accepted(this, new PlayerIndexEventArgs(playerIndex));

                ExitScreen();
            }
            else if (input.IsMenuCancel(ControllingPlayer, out playerIndex))
            {
                // Raise the cancelled event, then exit the message box.
                if (Cancelled != null)
                    Cancelled(this, new PlayerIndexEventArgs(playerIndex));

                ExitScreen();
            }
            else if (input.IsMenuDeleteTest(ControllingPlayer, out playerIndex))
            {
                // I dont know what this does maybe probably omg.
                if (Deleted != null)
                    Deleted(this, new PlayerIndexEventArgs(playerIndex));
            }
        }


        #endregion

        #region Draw


        /// <summary>
        /// Draws the message box.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            SpriteFont font = ScreenManager.Font;

            // Darken down any other screens that were drawn beneath the popup.
            if (shouldFadeBackground)
                ScreenManager.FadeBackBufferToBlack(TransitionAlpha * 2 / 3);

            // The background includes a border somewhat larger than the text itself.
            const int hPad = 32;
            const int vPad = 8;

            // Center the message text in the viewport.
            Viewport viewport = ScreenManager.GraphicsDevice.Viewport;
            Vector2 viewportSize = new Vector2(viewport.Width, viewport.Height);
            Vector2 textSize = font.MeasureString(message);
            Vector2 textPosition = new Vector2(hPad, viewportSize.Y - viewportSize.Y / 5 + vPad);

            Rectangle backgroundRectangle = new Rectangle((int)hPad / 2,
                                                          (int)(viewportSize.Y - viewportSize.Y / 5) + vPad / 2,
                                                          (int)((viewportSize.X - hPad) * TransitionAlpha),
                                                          (int)viewportSize.Y / 5 - vPad);
            if (showAtTopOfScreen)
            {
                textPosition = new Vector2(hPad, vPad);
                backgroundRectangle = new Rectangle((int)hPad / 2,
                                                    (int)vPad / 2,
                                                    (int)((viewportSize.X - hPad) * TransitionAlpha),
                                                    (int)viewportSize.Y / 5 - vPad);
            }

            // Fade the popup alpha during transitions.
            Color boxColor = Color.White * TransitionAlpha;
            Color textColor = Color.Black * TransitionAlpha;

            spriteBatch.Begin();

            // Draw the background rectangle.
            borderedMessageBox.Draw(spriteBatch, backgroundRectangle, boxColor, 0);

            // Draw the message box text.
            if (message.Length >= 45 && !isTreasureChestMessage)
            {
                string wrappedMessage = HelperMethods.TextWrap(message);
                spriteBatch.DrawString(font, wrappedMessage, textPosition, textColor);
            }
            else
                spriteBatch.DrawString(font, message, textPosition, textColor);

            spriteBatch.End();
        }


        #endregion
    }
}
