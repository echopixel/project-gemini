﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectGemini.Entities;
using ProjectGemini.Static_Classes;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using ProjectGemini.Controllers;
using FarseerPhysics;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using FarseerPhysics.Common;
using ProjectGemini.Utilities;
using FuncWorks.XNA.XTiled;
using ProjectGemini.GUI;
using ProjectGemini.Managers;
using ParticleEngine2D;


namespace ProjectGemini.Game_World
{
    public class Zone
    {
        ContentManager content;

        public List<NPC> NPCs;
        public List<Enemy> Enemies;
        public List<Enemy> DestroyedEnemies;
        public List<DisplayAbilityText> DamageText;
        public List<DisplayAbilityText> StupidList;
        Texture2D targetIcon;
        Texture2D healthbar;
        SpriteFont gameFont;
        public List<Texture2D> Textures;
        public DamageParticleEngine DamageParticleEngine;

        public Zone()
        {
            NPCs = new List<NPC>();
            Enemies = new List<Enemy>();
            DestroyedEnemies = new List<Enemy>();
            DamageText = new List<DisplayAbilityText>();
            StupidList = new List<DisplayAbilityText>();
            Textures = new List<Texture2D>();
        }

        public void Initialize(ContentManager content)
        {
            this.content = content;
            NPCs.Clear();
            Enemies.Clear();
            targetIcon = content.Load<Texture2D>("Icons/target_arrow");
            gameFont = content.Load<SpriteFont>("Fonts/gamefont");
            healthbar = content.Load<Texture2D>("GUI/HealthBar");

            Textures.Add(content.Load<Texture2D>("Particles/circle"));
            Textures.Add(content.Load<Texture2D>("Particles/star"));
            Textures.Add(content.Load<Texture2D>("Particles/diamond"));

            DamageParticleEngine = new DamageParticleEngine(Textures, new Vector2(400, 200), Color.White, 1);
             

            for (int i = 0; i < GameState.CurrentMap.ObjectLayers.Count; i++)
            {
                switch (GameState.CurrentMap.ObjectLayers[i].Name)
                {
                    case "Collisions":
                        GameState.CollisionLayer = i;
                        break;
                    
                    case "Events":
                        GameState.EventLayer = i;
                        break;

                    case "InteractEvents":
                        GameState.InteractEventLayer = i;
                        break;

                    case "NPCs":
                        GameState.NPCLayer = i;
                        break;

                    case "Enemies":
                        GameState.EnemyLayer = i;
                        break;
                    case "Start":
                        GameState.StartLayer = i;
                        break;

                    default:
                        break;
                }
            }

            var npcs = GameState.CurrentMap.ObjectLayers[GameState.NPCLayer].MapObjects; //.Select(x => x.Name).Distinct()
            foreach (var npc in npcs)
            {
                //Console.WriteLine("Loaded NPC: " + npc.Name); //TESTING LINE

                //TODO: Add some logic for loading NPCs that are animated and follow a path.
                NPC n = new NPC(content.Load<Texture2D>("Sprites/" + npc.Name));
                foreach (Property dialogue in npc.Properties.Values)
                {
                    string[] delimiters = new string[] { "/" };
                    string[] parsedDialogue = dialogue.Value.Split(delimiters, StringSplitOptions.None);
                    string text = parsedDialogue[0];
                    string[] tags = new string[parsedDialogue.Length - 1];
                    for (int i = 1; i < parsedDialogue.Length; i++)
                    {
                        tags[i-1] = parsedDialogue[i];
                    }
                    n.Dialogue.Add(Tuple.Create(text, tags));
                }
                /*
                if (npc.Properties.ContainsKey("npcText"))
                    n.Dialogue = npc.Properties["npcText"].Value;
                else
                    //Console.WriteLine(npc.Name + "does not have npcText!");
                 */
                n.SetPosition(new Vector2(npc.Bounds.X, npc.Bounds.Y));
                NPCs.Add(n);

                GameState.InteractableEvents.Add(n, npc.Name);
            }

            var enemies = GameState.CurrentMap.ObjectLayers[GameState.EnemyLayer].MapObjects;
            foreach (var enemy in enemies)
            {
                // DISCLAIMER: Enemy name in maps (.tmx files) are case sensitive. Make sure they match in enemies.xml
                //Console.WriteLine("Loaded Enemy: " + enemy.Name);
                Enemy e = new Enemy(GameState.Enemies.Find(x => x.enemyName.Contains(enemy.Name)),
                                    content.Load<Texture2D>("Sprites/" + GameState.Enemies.Find(x => x.enemyName.Contains(enemy.Name)).enemySpriteLocation),
                                    healthbar,
                                    gameFont);
                e.SetPosition(new Vector2(enemy.Bounds.X, enemy.Bounds.Y));
                Enemies.Add(e);

            }

            foreach (var start in GameState.CurrentMap.ObjectLayers[GameState.StartLayer].MapObjects)
            {
                GameState.Starts.Add(start.Name, new Vector2(start.Bounds.X, start.Bounds.Y));
                //Console.WriteLine(start.Name);
            }

            var iEvents = GameState.CurrentMap.ObjectLayers[GameState.InteractEventLayer].MapObjects;
            foreach (var iEvent in iEvents)
            {
                //Console.WriteLine("Loaded Interactable Event: " + iEvent.Name); // TESTING LINE
                //Console.WriteLine(iEvent.Properties["signText"].Value);
                switch (iEvent.Name)
                {
                    case "Sign":
                        Sign sign = new Sign(new Vector2(iEvent.Bounds.X + iEvent.Bounds.Width / 2, iEvent.Bounds.Y + iEvent.Bounds.Height / 2),
                                                iEvent.Bounds.Width,
                                                iEvent.Bounds.Height,
                                                iEvent.Properties["signText"].Value);

                        GameState.InteractableEvents.Add(sign, iEvent.Name);
                        break;
                    case "Chest":
                        bool beenOpened = false;
                        Dictionary<BaseItem, int> chestContents = new Dictionary<BaseItem, int>();
                        if (GameState.OpenedChests.Contains(Tuple.Create(GameState.CurrentMap.Properties["fileName"].Value, int.Parse(iEvent.Properties["id"].Value))))
                        {
                            beenOpened = true;
                        }
                        foreach (KeyValuePair<string, FuncWorks.XNA.XTiled.Property> chestProperties in iEvent.Properties)
                        {
                            //Console.WriteLine(chestProperties.Key);
                            //Console.WriteLine(chestProperties.Value);
                            if (GameState.Items.Contains(GameState.Items.Find(x => x.itemName.Contains(chestProperties.Key))))
                            {
                                //Console.WriteLine("contained item");
                                Item item = GameState.Items.Find(x => x.itemName.Contains(chestProperties.Key));
                                chestContents.Add(item, int.Parse(chestProperties.Value.Value));
                            }
                            if (GameState.Weapons.Contains(GameState.Weapons.Find(x => x.itemName.Contains(chestProperties.Key))))
                            {
                                //Console.WriteLine("contained weapon");
                                Weapon weapon = GameState.Weapons.Find(x => x.itemName.Contains(chestProperties.Key));
                                chestContents.Add(weapon, int.Parse(chestProperties.Value.Value));
                            }
                            if (GameState.Armors.Contains(GameState.Armors.Find(x => x.itemName.Contains(chestProperties.Key))))
                            {
                                //Console.WriteLine("contained armor");
                                Armor armor = GameState.Armors.Find(x => x.itemName.Contains(chestProperties.Key));
                                chestContents.Add(armor, int.Parse(chestProperties.Value.Value));
                            }
                        }

                        TreasureChest chest = new TreasureChest(content.Load<Texture2D>("Sprites/chest"),
                        new Vector2(iEvent.Bounds.X + iEvent.Bounds.Width / 2, iEvent.Bounds.Y + iEvent.Bounds.Height / 2),
                        iEvent.Bounds.Width,
                        iEvent.Bounds.Height,
                        int.Parse(iEvent.Properties["id"].Value),
                        chestContents,
                        beenOpened);

                        GameState.InteractableEvents.Add(chest, iEvent.Name);
                        break;
                    default:
                        //Console.WriteLine("Not a valid interactable event!");
                        break;
                }

            }

            foreach (var trigger in GameState.CurrentMap.ObjectLayers[GameState.EventLayer].MapObjects)
            {
                //Console.WriteLine("Loaded Event: " + trigger.Name); //TESTING LINE

                if (trigger.Polygon != null)
                {
                    //Console.WriteLine(trigger.Name + ": 1"); //TESTING LINE

                    Vertices vertices = new Vertices();
                    foreach (Point p in trigger.Polygon.Points)
                    {
                        vertices.Add(new Vector2(ConvertUnits.ToSimUnits(p.X), ConvertUnits.ToSimUnits(p.Y)));
                        if (vertices.Count == trigger.Polygon.Points.Length - 1)
                            break;
                    }
                    List<Vertices> list = FarseerPhysics.Common.Decomposition.BayazitDecomposer.ConvexPartition(vertices);

                    Body body = BodyFactory.CreateCompoundPolygon(
                        GameState.PhysicsWorld,
                        list,
                        0);
                }
                else if (trigger.Bounds.Width == 0)
                {
                    //Console.WriteLine(trigger.Name + ": 2"); //TESTING LINE

                    Body body = BodyFactory.CreateEdge(
                        GameState.PhysicsWorld,
                        new Vector2(ConvertUnits.ToSimUnits(trigger.Bounds.X), ConvertUnits.ToSimUnits(trigger.Bounds.Top)),
                        new Vector2(ConvertUnits.ToSimUnits(trigger.Bounds.X), ConvertUnits.ToSimUnits(trigger.Bounds.Bottom)));
                }
                else if (trigger.Bounds.Height == 0)
                {
                    //Console.WriteLine(trigger.Name + ": 3"); //TESTING LINE

                    Body body = BodyFactory.CreateEdge(
                        GameState.PhysicsWorld,
                        new Vector2(ConvertUnits.ToSimUnits(trigger.Bounds.Left), ConvertUnits.ToSimUnits(trigger.Bounds.Y)),
                        new Vector2(ConvertUnits.ToSimUnits(trigger.Bounds.Right), ConvertUnits.ToSimUnits(trigger.Bounds.Y)));
                }
                else
                {
                    //Console.WriteLine(trigger.Name + ": 4"); //TESTING LINE

                    Body body = BodyFactory.CreateRectangle(
                        GameState.PhysicsWorld,
                        ConvertUnits.ToSimUnits(trigger.Bounds.Width),
                        ConvertUnits.ToSimUnits(trigger.Bounds.Height),
                        0,
                        new Vector2(ConvertUnits.ToSimUnits(trigger.Bounds.Width / 2 + trigger.Bounds.X), ConvertUnits.ToSimUnits(trigger.Bounds.Height / 2 + trigger.Bounds.Y)));
                    body.IsSensor = true;
                    body.CollisionCategories = Category.All;
                    body.CollidesWith = Category.All;

                    GameState.Events.Add(body.BodyId, trigger.Name);

                    //TESTING SECTION START
                    foreach (int test in GameState.Events.Keys)
                    {
                        //Console.WriteLine(test);
                    }
                    foreach (string test in GameState.Events.Values)
                    {
                        //Console.WriteLine(test);
                    }
                    //TESTING SECTION END
                }
            }

            foreach (var wall in GameState.CurrentMap.ObjectLayers[GameState.CollisionLayer].MapObjects)
            {
                if (wall.Polygon != null)
                {
                    Vertices vertices = new Vertices();
                    foreach (Point p in wall.Polygon.Points)
                    {
                        vertices.Add(new Vector2(ConvertUnits.ToSimUnits(p.X), ConvertUnits.ToSimUnits(p.Y)));
                        if (vertices.Count == wall.Polygon.Points.Length - 1)
                            break;
                    }
                    List<Vertices> list = FarseerPhysics.Common.Decomposition.BayazitDecomposer.ConvexPartition(vertices);

                    Body body = BodyFactory.CreateCompoundPolygon(
                        GameState.PhysicsWorld,
                        list,
                        0);
                }
                else if (wall.Bounds.Width == 0)
                {
                    Body body = BodyFactory.CreateEdge(
                        GameState.PhysicsWorld,
                        new Vector2(ConvertUnits.ToSimUnits(wall.Bounds.X), ConvertUnits.ToSimUnits(wall.Bounds.Top)),
                        new Vector2(ConvertUnits.ToSimUnits(wall.Bounds.X), ConvertUnits.ToSimUnits(wall.Bounds.Bottom)));
                }
                else if (wall.Bounds.Height == 0)
                {
                    Body body = BodyFactory.CreateEdge(
                        GameState.PhysicsWorld,
                        new Vector2(ConvertUnits.ToSimUnits(wall.Bounds.Left), ConvertUnits.ToSimUnits(wall.Bounds.Y)),
                        new Vector2(ConvertUnits.ToSimUnits(wall.Bounds.Right), ConvertUnits.ToSimUnits(wall.Bounds.Y)));
                }
                else
                {
                    Body body = BodyFactory.CreateRoundedRectangle(
                        GameState.PhysicsWorld,
                        ConvertUnits.ToSimUnits(wall.Bounds.Width),
                        ConvertUnits.ToSimUnits(wall.Bounds.Height),
                        0.05f,
                        0.05f,
                        8,
                        0,
                        new Vector2(ConvertUnits.ToSimUnits(wall.Bounds.Width / 2 + wall.Bounds.X), ConvertUnits.ToSimUnits(wall.Bounds.Height / 2 + wall.Bounds.Y)));
                    body.IsStatic = true;
                    body.CollisionCategories = Category.All;
                    body.CollidesWith = Category.All;
                }
            }
        }

        internal void Update(GameTime gameTime)
        {
            DamageParticleEngine.Update(gameTime);

            // Adding one pixel to the x and y of the visible area to allow some pre-drawing and avoid black lines appearing.
            GameState.VisibleArea = new Rectangle(0,
                                                  0,
                                                  ResolutionManager.VirtualWidth + (int)GameState.Camera.getCameraVector.X + 1,
                                                  ResolutionManager.VirtualHeight + (int)GameState.Camera.getCameraVector.Y + 1);
            foreach (NPC npc in NPCs)
            {
                npc.Update(gameTime);
            }
            if (GameState.HeroOne.TextDisplayed)
            {
                GameState.HeroOne.TextDisplayed = false;
                //DamageText.Add(GameState.HeroOne._DisplayDamageText.Dequeue());
                foreach (DisplayAbilityText text in GameState.HeroOne._DisplayDamageText)
                {
                    if(text.timer == DisplayAbilityText.TEXTDISPLAYTIME)
                        DamageText.Add(text);
                }
                GameState.HeroOne._DisplayDamageText.Clear();
            }
            foreach (Enemy enemy in Enemies)
            {
                if (enemy.TextDisplayed)
                {
                    enemy.TextDisplayed = false;
                    //DamageText.Add(enemy._DisplayDamageText.Dequeue());
                    foreach (DisplayAbilityText text in enemy._DisplayDamageText)
                    {
                        if (text.timer == DisplayAbilityText.TEXTDISPLAYTIME)
                            DamageText.Add(text);
                    }
                    enemy._DisplayDamageText.Clear();
                }
                if (enemy.MarkedForDeletion)
                {
                    enemy.Selectable = false;
                    enemy.IsActive = false;
                    DestroyedEnemies.Add(enemy);
                }
                else
                {
                    if (Math.Abs(enemy.Position.X - GameState.HeroOne.Position.X) < (ResolutionManager.VirtualWidth / 2) && 
                        Math.Abs(enemy.Position.Y - GameState.HeroOne.Position.Y) < (ResolutionManager.VirtualHeight / 2))
                    {
                        enemy.Selectable = true;
                    }
                    else
                        enemy.Selectable = false;
                    enemy.Update(gameTime);
                }
            }

            //Handles drawing damage text
            foreach (DisplayAbilityText text in DamageText)
            {
                text.Update(gameTime);
                if (text.timer <= 0)
                {
                    StupidList.Add(text);
                }
            }
            foreach (DisplayAbilityText text in StupidList)
            {
                DamageText.Remove(text);
            }
            StupidList.Clear();

            // Handle Destruction
            foreach (Enemy enemy in DestroyedEnemies)
            {
                enemy.Destroy();
                Enemies.Remove(enemy);
            }

            DestroyedEnemies.Clear();
        }

        internal void Draw(SpriteBatch spriteBatch)
        {

            GameState.CurrentMap.DrawLayer(spriteBatch, 0, GameState.VisibleArea, 0);
            GameState.CurrentMap.DrawLayer(spriteBatch, 1, GameState.VisibleArea, 0);
            GameState.CurrentMap.DrawLayer(spriteBatch, 2, GameState.VisibleArea, 0);
            GameState.CurrentMap.DrawLayer(spriteBatch, 3, GameState.VisibleArea, 0);
            GameState.CurrentMap.DrawLayer(spriteBatch, 4, GameState.VisibleArea, 0);

            // Make an ordered list of all entities in the zone.
            List<BaseEntity> entityList = new List<BaseEntity>();
            foreach (NPC npc in NPCs)
            {
                entityList.Add(npc);
            }
            foreach (Enemy enemy in Enemies)
            {
                enemy.Selectable = false;
                entityList.Add(enemy);
            }
            foreach (KeyValuePair<BaseEntity, string> entity in GameState.InteractableEvents)
            {
                if (entity.Key is TreasureChest)
                {
                    entityList.Add(entity.Key);
                }
            }
            entityList.Add(GameState.HeroOne);
            entityList.Sort((a, b) => a.Position.Y.CompareTo(b.Position.Y));

            // Draw entities with lower Y Position values below those with higher.
            foreach (BaseEntity entity in entityList)
            {
                entity.Draw(spriteBatch);
                if (entity is Enemy || entity is Hero)
                {
                    // Will need to add a check for changing colors depending on which hero is targeting what.
                    // Also for showing what the enemy is targeting?
                    if (entity.IsTargeted())
                    {
                        float iconX = entity.SpriteAnimation.Position.X - (targetIcon.Width / 2f);
                        float iconY = entity.SpriteAnimation.Position.Y - (entity.SpriteAnimation.Height / 1.5f) - (healthbar.Height / 10);
                        spriteBatch.Draw(targetIcon, new Vector2(iconX, iconY), Color.White);
                    }
                }
            }
            foreach (DisplayAbilityText text in DamageText)
            {
                text.Draw(spriteBatch);
            }

            GameState.CurrentMap.DrawLayer(spriteBatch, 5, GameState.VisibleArea, 0);

            TimedMessageBoxManager.Draw(spriteBatch);

            if (GameDebug.ShowCollision)
            {
                //GameState.CurrentMap.DrawObjectLayer(spriteBatch, GameState.CollisionLayer, ref GameState.VisibleArea, 0);
                //GameState.CurrentMap.DrawObjectLayer(spriteBatch, GameState.EventLayer, ref GameState.VisibleArea, 0);
            }

            DamageParticleEngine.Draw(spriteBatch);

        }

    }
}
