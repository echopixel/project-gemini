﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectGemini.EnumTypes
{
    public enum EquipmentSlotType : int
    {
        //one hand, two hand, off hand
        Weapon = 0,
        Head = 1,
        Chest = 2,
        Legs = 3 ,
    }
}
