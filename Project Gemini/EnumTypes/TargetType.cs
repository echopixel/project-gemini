﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectGemini.EnumTypes
{
    public enum TargetType
    {
        Ally = 1,
        Enemy = 2,
        None = 3,
    }
}
