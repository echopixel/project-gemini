﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectGemini.EnumTypes
{
    public enum MenuType
    {
        LeftMiddleRight = 0,

        LeftMiddle  = 1,
        LeftRight   = 2,
        MiddleRight = 3,

        Left   = 4,
        Middle = 5,
        Right  = 6,
    }
}
