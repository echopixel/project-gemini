﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectGemini.EnumTypes
{
    public enum ItemEffectType
    {
        FlatHpRestore = 1,
        PercentHpRestore = 2,
        FlatMpRestore = 3,
        PercentMpRestore = 4,
        FlatHpMpRestore = 5,
        AllyRevive = 6,
        StatusRemove = 7,
    }
}
