﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectGemini.EnumTypes
{
    public enum MapTypes
    {
        DemoMap = 1,
        TestHouse = 2,
        TestArenaOne = 3,
        TestArenaTwo = 4,
        DemoArena = 5,
    }
}
