﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectGemini.EnumTypes
{
    public enum EntityType
    {
        BaseEntity = 0,
        DynamicEntity = 1,
        Hero = 2,
        NPC = 3,
        Enemy = 4,
    }
}
