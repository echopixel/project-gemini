﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectGemini.EnumTypes
{
    public enum AbilityElementType
    {
        Physical = 0,
        Fire = 1,
        Frost = 2,
        Nature = 3,
        Light = 4,
        Shadow = 5,
        Earth = 6,
        Lightning = 7,

    }
}
