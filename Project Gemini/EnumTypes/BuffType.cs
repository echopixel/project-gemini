﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectGemini.EnumTypes
{
    public enum BuffType
    {
        TakeDamage = 0,
        TakeHeal = 1,
        MovementSpeed = 2,
        CoreStat = 3,
        HeroStat = 4,
        MPHeal = 5,


    }
}
