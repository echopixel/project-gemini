﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectGemini.EnumTypes
{
    public enum CollisionType
    {
        NULL = 0,
        WallLeft = 1,
        WallRight = 2,
        WallTop = 3,
        WallBottom = 4,
        Enemy = 5,
        Player = 6,
        Door = 7,
    }
}
