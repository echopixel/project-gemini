﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectGemini.EnumTypes
{
    public enum PersonalResourceType
    {
        Light = 0,
        Shadow = 1,
        Earth = 2,
        Elements = 3
    }
}
