﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectGemini.EnumTypes
{
    public enum SaveLoadDeleteType
    {
        Save = 0,
        Load = 1,
        Delete = 2,
    }
}
