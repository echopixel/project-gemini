﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectGemini.EnumTypes
{
    public enum AbilityProjectileType
    {
        Self = 1,
        SingleTarget = 2,
        SelfAoE = 3,
        TargetAoE = 4,
    }
}
