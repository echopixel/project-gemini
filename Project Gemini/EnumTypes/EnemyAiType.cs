﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectGemini.EnumTypes
{
    public enum EnemyAIType
    {
        Melee = 1,
        Caster = 2,
        Boss = 3,
        Coward = 4,
        Godly = 5,
    }
}
