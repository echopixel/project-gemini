﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using ProjectGemini.Static_Classes;
using ProjectGemini.Entities;

namespace ProjectGemini.GUI
{
    public class DisplayAbilityText
    {
        //Need to track alive enemies
        //Needs to display dot/hot damage every second.
        //Need to fix queue.
        public const float TEXTDISPLAYTIME = 750f;
        public float timer;
        public int amount;
        public Vector2 Position;
        public Color color { get; set; }
        public TargetableEntity entity;

        public DisplayAbilityText(TargetableEntity entity, int amount, Color color)
        {
            this.amount = amount;
            this.timer = TEXTDISPLAYTIME;
            this.color = color;
            this.entity = entity;
            this.Position = entity.Position;
            EntityAlive();
        }

        public void Update(GameTime gameTime)
        {
            timer -= (float)gameTime.ElapsedGameTime.TotalMilliseconds;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (entity != null && !entity.MarkedForDeletion)
            {
                EntityAlive();
            }
            spriteBatch.DrawString(
                    GameState.ScreenManager.Font,
                    amount.ToString(),
                    Position,
                    color * (timer / TEXTDISPLAYTIME),
                    0,
                    Vector2.Zero,
                    0.20f,
                    SpriteEffects.None,
                    0);
        }

        private void EntityAlive()
        {
            Position.Y = entity.Position.Y - (entity.Height * 0.7f);
            if (color == Color.White)
                Position.X = entity.Position.X - (entity.Width / 2.2f);
            else
                Position.X = entity.Position.X + (entity.Width / 2.2f);
        }
    }
}
