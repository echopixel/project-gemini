﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using ProjectGemini.Entities;
using ProjectGemini.Static_Classes;
using Microsoft.Xna.Framework.Content;
using ProjectGemini.Utilities;
using ProjectGemini.EnumTypes;

namespace ProjectGemini.GUI
{
    public class HUD
    {
        Hero hero;
        SpriteFont font;
        Texture2D healthBar;
        Texture2D magicBar;
        Texture2D expBar;
        Texture2D personalBar;
        Texture2D heroPortrait;
        Texture2D castBar;
        Texture2D levelArrow;
        double levelArrowYPositionOffset;
        double blinkingFrequency;

        // Pass by reference so we're not copying the hero and can tailor each HUD to the specific Hero.
        public HUD(SpriteFont font, Hero hero, Texture2D heroPortrait, Texture2D healthBar, Texture2D magicBar, Texture2D personalBar, Texture2D expBar, Texture2D castBar, Texture2D levelArrow) 
        {
            this.font = font;
            this.hero = hero;
            this.heroPortrait = heroPortrait;
            this.healthBar = healthBar;
            this.magicBar = magicBar;
            this.personalBar = personalBar;
            this.expBar = expBar;
            this.castBar = castBar;
            this.levelArrow = levelArrow;
        }

        public void Update(GameTime gameTime)
        {
            blinkingFrequency = Math.Sin(gameTime.TotalGameTime.TotalSeconds * 3);
            levelArrowYPositionOffset = Math.Sin(gameTime.TotalGameTime.TotalSeconds * 6);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            #region Health Bar

            // Draw the negative space behind the health bar. (only visible with missing health)
            spriteBatch.Draw(healthBar,
                new Rectangle(
                    (int)(ResolutionManager.VirtualWidth / 2 - healthBar.Width / 16) + (int)GameState.Camera.getCameraVector.X,
                    (int)GameState.Camera.getCameraVector.Y + 4,
                    (int)healthBar.Width / 8, 6),
                    new Rectangle(0, 45, healthBar.Width, 44), Color.Black * 0.30f);

            // Draw the current health level for the health bar.  Based on currentHP / maxHP
            spriteBatch.Draw(healthBar, 
                new Rectangle(
                    (int)(ResolutionManager.VirtualWidth / 2 - healthBar.Width / 16) + (int)GameState.Camera.getCameraVector.X, 
                    (int)GameState.Camera.getCameraVector.Y + 4,
                    (int)(healthBar.Width * ((double)hero.CurrentHP / hero.MaxHP)) / 8, 6), 
                    new Rectangle(0, 45, healthBar.Width, 44), Color.Red);

            // Draw the box around the health bar.
            spriteBatch.Draw(healthBar, 
                new Rectangle(
                (int)(ResolutionManager.VirtualWidth / 2 - healthBar.Width / 16) + (int)GameState.Camera.getCameraVector.X,
                (int)GameState.Camera.getCameraVector.Y + 4, 
                (int)healthBar.Width / 8, 6), 
                new Rectangle(0, 0, healthBar.Width, 44), Color.White);

            // Draw the numerical value of health the player has inside the health bar.
            spriteBatch.DrawString(
                GameState.ScreenManager.Font,
                hero.CurrentHP + " / " + hero.MaxHP,
                new Vector2(
                    (int)((ResolutionManager.VirtualWidth / 2) - 10) + (int)GameState.Camera.getCameraVector.X,
                    (int)GameState.Camera.getCameraVector.Y + 3),
                Color.White,
                0,
                Vector2.Zero,
                0.16f,
                SpriteEffects.None,
                0);

            #endregion

            #region Magic Bar

            // Draw the negative space behind the magic bar. (only visible with missing health)
            spriteBatch.Draw(magicBar,
                new Rectangle(
                    (int)(ResolutionManager.VirtualWidth / 2 - magicBar.Width / 16) + (int)GameState.Camera.getCameraVector.X,
                    (int)GameState.Camera.getCameraVector.Y + 10,
                    (int)magicBar.Width / 8, 6),
                    new Rectangle(0, 45, magicBar.Width, 44), Color.Black * 0.30f);

            // Draw the current magic level for the magic bar.  Based on currentMP / maxMP
            spriteBatch.Draw(magicBar,
                new Rectangle(
                    (int)(ResolutionManager.VirtualWidth / 2 - magicBar.Width / 16) + (int)GameState.Camera.getCameraVector.X,
                    (int)GameState.Camera.getCameraVector.Y + 10,
                    (int)(magicBar.Width * ((double)hero.CurrentMP / hero.MaxMP)) / 8, 6),
                    new Rectangle(0, 45, magicBar.Width, 44), Color.Blue);

            // Draw the box around the magic bar.
            spriteBatch.Draw(magicBar,
                new Rectangle(
                (int)(ResolutionManager.VirtualWidth / 2 - magicBar.Width / 16) + (int)GameState.Camera.getCameraVector.X,
                (int)GameState.Camera.getCameraVector.Y + 10,
                (int)magicBar.Width / 8, 6),
                new Rectangle(0, 0, magicBar.Width, 44), Color.White);

            // Draw the numerical value of magic the player has inside the magic bar.
            spriteBatch.DrawString(
                GameState.ScreenManager.Font,
                hero.CurrentMP + " / " + hero.MaxMP,
                new Vector2(
                    (int)((ResolutionManager.VirtualWidth / 2) - 10) + (int)GameState.Camera.getCameraVector.X,
                    (int)GameState.Camera.getCameraVector.Y + 9),
                Color.White,
                0,
                Vector2.Zero,
                0.16f,
                SpriteEffects.None,
                0);

            #endregion

            #region Personal Bar

            // Draw the negative space behind the health bar. (only visible with missing health)
            

            Color personal = Color.White;

            switch (GameState.HeroOne.ResourceType)
            {
                case PersonalResourceType.Light :
                    personal = Color.DarkGoldenrod;

                    spriteBatch.Draw(personalBar,
                        new Rectangle(
                            (int)(ResolutionManager.VirtualWidth / 2 - personalBar.Width / 16) + (int)GameState.Camera.getCameraVector.X,
                            (int)GameState.Camera.getCameraVector.Y + 16,
                            (int)personalBar.Width / 8, 6),
                            new Rectangle(0, 45, personalBar.Width, 44), Color.Black * 0.30f);
                    break;
                case PersonalResourceType.Shadow:
                    personal = Color.DarkGray;

                    spriteBatch.Draw(personalBar,
                        new Rectangle(
                            (int)(ResolutionManager.VirtualWidth / 2 - personalBar.Width / 16) + (int)GameState.Camera.getCameraVector.X,
                            (int)GameState.Camera.getCameraVector.Y + 16,
                            (int)personalBar.Width / 8, 4),
                            new Rectangle(0, 45, personalBar.Width, 44), Color.LimeGreen);
                    spriteBatch.Draw(personalBar,
                        new Rectangle(
                            (int)(ResolutionManager.VirtualWidth / 2 - personalBar.Width / 16) + (int)GameState.Camera.getCameraVector.X,
                            (int)GameState.Camera.getCameraVector.Y + 19,
                            (int)personalBar.Width / 8, 3),
                            new Rectangle(0, 45, personalBar.Width, 44), Color.Orange);
                    break;
            }

            // Draw the current health level for the health bar.  Based on currentPersonal / maxPersonal
            spriteBatch.Draw(personalBar,
                new Rectangle(
                    (int)(ResolutionManager.VirtualWidth / 2 - personalBar.Width / 16) + (int)GameState.Camera.getCameraVector.X,
                    (int)GameState.Camera.getCameraVector.Y + 16,
                    (int)(personalBar.Width * ((double)hero.CurrentPersonal / hero.MaxPersonal)) / 8, 6),
                    new Rectangle(0, 45, personalBar.Width, 44),personal);

            // Draw the box around the health bar.
            spriteBatch.Draw(personalBar,
                new Rectangle(
                (int)(ResolutionManager.VirtualWidth / 2 - personalBar.Width / 16) + (int)GameState.Camera.getCameraVector.X,
                (int)GameState.Camera.getCameraVector.Y + 16,
                (int)personalBar.Width / 8, 6),
                new Rectangle(0, 0, personalBar.Width, 44), Color.White);

            

            // Draw the numerical value of health the player has inside the health bar.
            spriteBatch.DrawString(
                GameState.ScreenManager.Font,
                hero.CurrentPersonal + " / " + hero.MaxPersonal,
                new Vector2(
                    (int)((ResolutionManager.VirtualWidth / 2) - 10) + (int)GameState.Camera.getCameraVector.X,
                    (int)GameState.Camera.getCameraVector.Y + 15),
                Color.White,
                0,
                Vector2.Zero,
                0.16f,
                SpriteEffects.None,
                0);

            #endregion

            #region Experience Bar

            // Draw the negative space behind the experience bar. (only visible with gained experience)
            spriteBatch.Draw(expBar,
                new Rectangle(
                    (int)(ResolutionManager.VirtualWidth / 2 - expBar.Width / 16) + (int)GameState.Camera.getCameraVector.X,
                    (int)GameState.Camera.getCameraVector.Y + 21,
                    (int)expBar.Width / 8, 4),
                    new Rectangle(0, 45, expBar.Width, 44), Color.Black * 0.30f);

            // Draw the current magic level for the experience bar.  Based on EXP / EXP_TO_LEVEL
            spriteBatch.Draw(expBar,
                new Rectangle(
                    (int)(ResolutionManager.VirtualWidth / 2 - expBar.Width / 16) + (int)GameState.Camera.getCameraVector.X,
                    (int)GameState.Camera.getCameraVector.Y + 21,
                    (int)(expBar.Width * ((double)hero.EXP / Hero.EXP_TO_LEVEL)) / 8, 4),
                    new Rectangle(0, 45, expBar.Width, 44), Color.Green);

            // Draw the box around the experience bar.
            spriteBatch.Draw(expBar,
                new Rectangle(
                (int)(ResolutionManager.VirtualWidth / 2 - expBar.Width / 16) + (int)GameState.Camera.getCameraVector.X,
                (int)GameState.Camera.getCameraVector.Y + 21,
                (int)expBar.Width / 8, 4),
                new Rectangle(0, 0, expBar.Width, 44), Color.White);

            
            // Draw the numerical value of magic the player has inside the experience bar.
            spriteBatch.DrawString(
                GameState.ScreenManager.Font,
                hero.EXP + " / " + Hero.EXP_TO_LEVEL,
                new Vector2(
                    (int)((ResolutionManager.VirtualWidth / 2) - 10) + (int)GameState.Camera.getCameraVector.X,
                    (int)GameState.Camera.getCameraVector.Y + 20),
                Color.White,
                0,
                Vector2.Zero,
                0.13f,
                SpriteEffects.None,
                0);
             

            #endregion

            #region Level

            if (hero.Level >= 100)
            {
                spriteBatch.DrawString(
                        GameState.ScreenManager.Font,
                        "LV: " + hero.Level,
                        new Vector2(
                            (int)(ResolutionManager.VirtualWidth / 2 - healthBar.Width / 16) + (int)GameState.Camera.getCameraVector.X - 16,
                            (int)GameState.Camera.getCameraVector.Y + 16),
                        Color.White,
                        0,
                        Vector2.Zero,
                        0.15f,
                        SpriteEffects.None,
                        0);
            }
            else if (hero.Level >= 10)
            {
                spriteBatch.DrawString(
                        GameState.ScreenManager.Font,
                        "LV: " + hero.Level,
                        new Vector2(
                            (int)(ResolutionManager.VirtualWidth / 2 - healthBar.Width / 16) + (int)GameState.Camera.getCameraVector.X - 14,
                            (int)GameState.Camera.getCameraVector.Y + 16),
                        Color.White,
                        0,
                        Vector2.Zero,
                        0.15f,
                        SpriteEffects.None,
                        0);
            }
            else
            {
                spriteBatch.DrawString(
                        GameState.ScreenManager.Font,
                        "LV: " + hero.Level,
                        new Vector2(
                            (int)(ResolutionManager.VirtualWidth / 2 - healthBar.Width / 16) + (int)GameState.Camera.getCameraVector.X - 13,
                            (int)GameState.Camera.getCameraVector.Y + 16),
                        Color.White,
                        0,
                        Vector2.Zero,
                        0.15f,
                        SpriteEffects.None,
                        0);
            }

            #endregion

            #region Hero Portrait

            // Draw the placeholder hero portrait using a pixel colored black.
            spriteBatch.Draw(heroPortrait,
                new Rectangle(
                (int)(ResolutionManager.VirtualWidth / 2 - healthBar.Width / 16) + (int)GameState.Camera.getCameraVector.X - 14,
                (int)GameState.Camera.getCameraVector.Y + 4,
                12, 12), Color.White);

            #endregion

            #region Level Up Notification

            if (hero.SkillPoints > 0)
            {
                spriteBatch.Draw(levelArrow,
                    new Rectangle(
                        (int)(ResolutionManager.VirtualWidth / 2 - healthBar.Width / 16) + (int)GameState.Camera.getCameraVector.X - 15,
                        (int)GameState.Camera.getCameraVector.Y + 2 + (int)(levelArrowYPositionOffset * 10)/5,
                        (int)(levelArrow.Width / 2),
                        (int)(levelArrow.Height / 2)),
                        Color.Yellow * Math.Abs((float)(blinkingFrequency)));
            }

            #endregion

            #region Casting Bar
            // Only visible when casting.
            if (hero.IsCasting && hero._MaxCastTime != 0)
            {
                // Draw transparent cast bar background.
                spriteBatch.Draw(castBar,
                    new Rectangle(
                        (int)(hero.SpriteAnimation.Position.X - (castBar.Width / 12) / 2),
                        (int)(hero.SpriteAnimation.Position.Y + (hero.Height / 1.5)),
                        (int)castBar.Width / 12, 4),
                        new Rectangle(0, 45, castBar.Width, 44), Color.Black * 0.55f);

                // Draw progresson of the cast bar.  Based on _CastTimeElapsed / _MaxCastTime
                spriteBatch.Draw(castBar,
                    new Rectangle(
                        (int)(hero.SpriteAnimation.Position.X - (castBar.Width / 12) / 2),
                        (int)(hero.SpriteAnimation.Position.Y + (hero.Height / 1.5)),
                        (int)(castBar.Width * hero._CastTimeElapsed / hero._MaxCastTime) / 12, 4),
                        new Rectangle(0, 45, castBar.Width, 44), Color.Purple);

                // Draw the box around the cast bar.
                spriteBatch.Draw(castBar,
                    new Rectangle(
                        (int)(hero.SpriteAnimation.Position.X - (castBar.Width / 12) / 2),
                        (int)(hero.SpriteAnimation.Position.Y + (hero.Height / 1.5)),
                        castBar.Width / 12, 4),
                        new Rectangle(0, 0, castBar.Width, 44), Color.White);

            }
            #endregion

            #region Skills

            Ability checkOne = null;
            Ability checkTwo = null;
            Ability checkThree = null;

            if (GameState.HeroOne.SkillSet == 0)
            {
                checkOne = GameState.HeroOne.Skill1;
                checkTwo = GameState.HeroOne.Skill2;
                checkThree = GameState.HeroOne.Skill3;
            }
            else if (GameState.HeroOne.SkillSet == 1)
            {
                checkOne = GameState.HeroOne.Skill4;
                checkTwo = GameState.HeroOne.Skill5;
                checkThree = GameState.HeroOne.Skill6;
            }
            else if (GameState.HeroOne.SkillSet == 2)
            {
                checkOne = GameState.HeroOne.Skill7;
                checkTwo = GameState.HeroOne.Skill8;
                checkThree = GameState.HeroOne.Skill9;
            }

            String timer = "";
            
            if(checkOne != null)
            {
                // Draw the placeholder hero portrait using a pixel colored black.
                spriteBatch.Draw(checkOne.abilityIconTexture,
                    new Rectangle(
                    (int)(ResolutionManager.VirtualWidth / 2) + (int)GameState.Camera.getCameraVector.X - 13,
                    (int)GameState.Camera.getCameraVector.Y + 27,
                    8, 8), Color.White);
            }

            if (checkOne != null && checkOne.CooldownTimer > 0)
            {
                timer = ((checkOne.CooldownTimer - checkOne.CooldownTimer % 1000f) / 1000f + 1).ToString();

                spriteBatch.DrawString(
                GameState.ScreenManager.Font,
                timer,
                new Vector2(
                    (int)((ResolutionManager.VirtualWidth / 2)) + (int)GameState.Camera.getCameraVector.X - 9,
                    (int)GameState.Camera.getCameraVector.Y + 27),
                Color.White,
                0,
                Vector2.Zero,
                0.2f,
                SpriteEffects.None,
                0);
            }

            if (checkTwo != null)
            {
                // Draw the placeholder hero portrait using a pixel colored black.
                spriteBatch.Draw(checkTwo.abilityIconTexture,
                    new Rectangle(
                    (int)(ResolutionManager.VirtualWidth / 2) + (int)GameState.Camera.getCameraVector.X - 4,
                    (int)GameState.Camera.getCameraVector.Y + 27,
                    8, 8), Color.White);
            }

            if (checkTwo != null && checkTwo.CooldownTimer > 0)
            {
                timer = ((checkTwo.CooldownTimer - checkTwo.CooldownTimer % 1000f) / 1000f + 1).ToString();

                spriteBatch.DrawString(
                GameState.ScreenManager.Font,
                timer,
                new Vector2(
                    (int)((ResolutionManager.VirtualWidth / 2)) + (int)GameState.Camera.getCameraVector.X,
                    (int)GameState.Camera.getCameraVector.Y + 27),
                Color.White,
                0,
                Vector2.Zero,
                0.2f,
                SpriteEffects.None,
                0);
            }

            if (checkThree != null)
            {
                // Draw the placeholder hero portrait using a pixel colored black.
                spriteBatch.Draw(checkThree.abilityIconTexture,
                    new Rectangle(
                    (int)(ResolutionManager.VirtualWidth / 2) + (int)GameState.Camera.getCameraVector.X + 5,
                    (int)GameState.Camera.getCameraVector.Y + 27,
                    8, 8), Color.White);
            }

            if (checkThree != null && checkThree.CooldownTimer > 0)
            {
                timer = ((checkThree.CooldownTimer - checkThree.CooldownTimer % 1000f) / 1000f + 1).ToString();

                spriteBatch.DrawString(
                GameState.ScreenManager.Font,
                timer,
                new Vector2(
                    (int)((ResolutionManager.VirtualWidth / 2)) + (int)GameState.Camera.getCameraVector.X + 9,
                    (int)GameState.Camera.getCameraVector.Y + 27),
                Color.White,
                0,
                Vector2.Zero,
                0.2f,
                SpriteEffects.None,
                0);
            }

            #endregion

            #region Item

            if (GameState.Inventory.items.Count != 0)
            {
                // Draw the placeholder hero portrait using a pixel colored black.
                spriteBatch.Draw(GameState.Inventory.items[GameState.HeroOne.SelectedItem].itemIconTexture,
                    new Rectangle(
                    (int)(ResolutionManager.VirtualWidth / 2) + (int)GameState.Camera.getCameraVector.X - 24,
                    (int)GameState.Camera.getCameraVector.Y + 27,
                    8, 8), Color.White);
            }

            #endregion

        }
    }
}
