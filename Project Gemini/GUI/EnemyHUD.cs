﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using ProjectGemini.Entities;
using ProjectGemini.Static_Classes;

namespace ProjectGemini.GUI
{
    public class EnemyHUD
    {
        Enemy enemy;
        public Texture2D healthBar;
        Texture2D castBar;
        public SpriteFont font;

        // Pass by reference so we're not copying the enemy and can tailor each HUD to the specific enemy. Totally not copypasta
        public EnemyHUD(SpriteFont font, Enemy enemy, Texture2D healthBar, Texture2D castBar) 
        {
            this.font = font;
            this.enemy = enemy;
            this.healthBar = healthBar;
            this.castBar = castBar;
        }

        public void Update(GameTime gameTime)
        {
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            //#region Health Bar
            //// Only visiable when not at full hp.
            //if (enemy.CurrentHP != enemy.MaxHP)
            //{
            //    // Draw the negative space behind the health bar. (only visible with missing health)
            //    spriteBatch.Draw(healthBar,
            //        new Rectangle(
            //            (int)(enemy.SpriteAnimation.Position.X - (healthBar.Width / 20) / 2),
            //            (int)(enemy.SpriteAnimation.Position.Y - (enemy.Height / 1.5)),
            //            (int)healthBar.Width / 20, 2),
            //            new Rectangle(0, 45, healthBar.Width, 44), Color.Black * 0.30f);

            //    // Draw the current health level for the health bar.  Based on currentHP / maxHP
            //    spriteBatch.Draw(healthBar,
            //        new Rectangle(
            //            (int)(enemy.SpriteAnimation.Position.X - (healthBar.Width / 20) / 2),
            //            (int)(enemy.SpriteAnimation.Position.Y - (enemy.Height / 1.5)),
            //            (int)(healthBar.Width * ((double)enemy.CurrentHP / enemy.MaxHP)) / 20, 2),
            //            new Rectangle(0, 45, healthBar.Width, 44), Color.Red);

            //    // Draw the box around the health bar.
            //    spriteBatch.Draw(healthBar,
            //        new Rectangle(
            //            (int)(enemy.SpriteAnimation.Position.X - (healthBar.Width / 20) / 2),
            //            (int)(enemy.SpriteAnimation.Position.Y - (enemy.Height / 1.5)),
            //            (int)healthBar.Width / 20, 2),
            //            new Rectangle(0, 0, healthBar.Width, 44), Color.White);

            //}
            //#endregion
            
            #region Casting Bar
            //only visable when casting. 
            if (enemy.IsCasting)
            {
                // Draw the negative space behind the cast bar.
                spriteBatch.Draw(castBar,
                    new Rectangle(
                        (int)(enemy.SpriteAnimation.Position.X - (castBar.Width / 20) / 2),
                        (int)(enemy.SpriteAnimation.Position.Y + (enemy.Height / 1.5)),
                        (int)castBar.Width / 20, 2),
                        new Rectangle(0, 45, castBar.Width, 44), Color.Black * 0.55f);

                // Draw progresson of the cast bar.  Based on _CastTimeElapsed / _MaxCastTime
                spriteBatch.Draw(castBar,
                    new Rectangle(
                        (int)(enemy.SpriteAnimation.Position.X - (castBar.Width / 20) / 2),
                        (int)(enemy.SpriteAnimation.Position.Y + (enemy.Height / 1.5)),
                        (int)(castBar.Width * ((float)enemy._CastTimeElapsed / enemy._MaxCastTime)) / 20, 2),
                        new Rectangle(0, 45, castBar.Width, 44), Color.Purple);

                // Draw the box around the cast bar.
                spriteBatch.Draw(castBar,
                    new Rectangle(
                        (int)(enemy.SpriteAnimation.Position.X - (castBar.Width / 20) / 2),
                        (int)(enemy.SpriteAnimation.Position.Y + (enemy.Height / 1.5)),
                        (int)castBar.Width / 20, 2),
                        new Rectangle(0, 0, castBar.Width, 44), Color.White);

            }
            #endregion
        }
    }
}
