﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

using ProjectGemini.Entities;
using ProjectGemini.Static_Classes;

namespace ProjectGemini.GUI
{
    class HelperBar
    {
        ContentManager content;

        SpriteFont font;
        Texture2D border;
        Texture2D a;
        Texture2D b;
        Texture2D x;
        Texture2D y;
        Texture2D dPad;

        public string aText;
        public string bText;
        public string xText;
        public string yText;
        public string padText;

        int buttonSize;
        int buttonInc; //button incrememnt
        int textInc; 

        public HelperBar(ContentManager content) 
        {
            this.content = content;

            font = content.Load<SpriteFont>("font");
            border = content.Load<Texture2D>("Menu_Images/HelperBar");
            a = content.Load<Texture2D>("Menu_Images/AButton");
            b = content.Load<Texture2D>("Menu_Images/BButton");
            x = content.Load<Texture2D>("Menu_Images/XButton");
            y = content.Load<Texture2D>("Menu_Images/YButton");
            dPad = content.Load<Texture2D>("Menu_Images/DPad");

            buttonSize = 20;
            buttonInc = 25;
            textInc = 65;

            aText = "";
            bText = "";
            xText = "";
            yText = "";
            padText = "";
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime, float transitionAlpha)
        {
            int buttonPlace = 10; //button placement

            spriteBatch.Draw(border,
                new Rectangle(
                0,
                GameState.ScreenHeight - 30,
                (int)((float)ResolutionManager.VirtualWidth * ((float)GameState.ScreenHeight / (float)ResolutionManager.VirtualHeight)) + 1, //resultwidth=virtualwidth * screenheight / virtualheigh
                30),
                new Rectangle(0, 0, border.Width, 44), Color.White * transitionAlpha);
            if(!padText.Equals(""))
            {
                spriteBatch.Draw(dPad,
                    new Rectangle(
                    buttonPlace,
                    GameState.ScreenHeight - 25,
                    buttonSize,
                    buttonSize),
                    Color.White * transitionAlpha);
                buttonPlace += buttonInc;

                spriteBatch.DrawString(
                    font,
                    padText,
                    new Vector2(
                        buttonPlace,
                        GameState.ScreenHeight - 25),
                    Color.White * transitionAlpha,
                    0,
                    Vector2.Zero,
                    1.25f,
                    SpriteEffects.None,
                    0);
                buttonPlace += textInc;
            }

            if (!aText.Equals(""))
            {
                spriteBatch.Draw(a,
                    new Rectangle(
                    buttonPlace,
                    GameState.ScreenHeight - 25,
                    buttonSize,
                    buttonSize),
                    Color.White * transitionAlpha);
                buttonPlace += buttonInc;

                spriteBatch.DrawString(
                    font,
                    aText,
                    new Vector2(
                        buttonPlace,
                        GameState.ScreenHeight - 25),
                    Color.White * transitionAlpha,
                    0,
                    Vector2.Zero,
                    1.25f,
                    SpriteEffects.None,
                    0);
                buttonPlace += textInc;
            }

            if (!bText.Equals(""))
            {
                spriteBatch.Draw(b,
                    new Rectangle(
                    buttonPlace,
                    GameState.ScreenHeight - 25,
                    buttonSize,
                    buttonSize),
                    Color.White * transitionAlpha);
                buttonPlace += buttonInc;

                spriteBatch.DrawString(
                    font,
                    bText,
                    new Vector2(
                        buttonPlace,
                        GameState.ScreenHeight - 25),
                    Color.White * transitionAlpha,
                    0,
                    Vector2.Zero,
                    1.25f,
                    SpriteEffects.None,
                    0);
                buttonPlace += textInc;
            }

            if (!xText.Equals(""))
            {
                spriteBatch.Draw(x,
                    new Rectangle(
                    buttonPlace,
                    GameState.ScreenHeight - 25,
                    buttonSize,
                    buttonSize),
                    Color.White * transitionAlpha);
                buttonPlace += buttonInc;

                spriteBatch.DrawString(
                    font,
                    xText,
                    new Vector2(
                        buttonPlace,
                        GameState.ScreenHeight - 25),
                    Color.White * transitionAlpha,
                    0,
                    Vector2.Zero,
                    1.25f,
                    SpriteEffects.None,
                    0);
                buttonPlace += textInc;
            }

            if (!yText.Equals(""))
            {
                spriteBatch.Draw(y,
                    new Rectangle(
                    buttonPlace,
                    GameState.ScreenHeight - 25,
                    buttonSize,
                    buttonSize),
                    Color.White * transitionAlpha);
                buttonPlace += buttonInc;

                spriteBatch.DrawString(
                    font,
                    yText,
                    new Vector2(
                        buttonPlace,
                        GameState.ScreenHeight - 25),
                    Color.White * transitionAlpha,
                    0,
                    Vector2.Zero,
                    1.25f,
                    SpriteEffects.None,
                    0);
                buttonPlace += textInc;
            }
             
            /*
            // Draw the numerical value of health the player has inside the health bar.
            spriteBatch.DrawString(
                font,
                hero.CurrentHP + " / " + hero.MaxHP,
                new Vector2(
                    (int)((ResolutionManager.VirtualWidth / 2) - 10) + (int)GameState.Camera.getCameraVector.X,
                    (int)GameState.Camera.getCameraVector.Y + 4),
                Color.White,
                0,
                Vector2.Zero,
                0.35f,
                SpriteEffects.None,
                0);
             * */
        }
    }
}
