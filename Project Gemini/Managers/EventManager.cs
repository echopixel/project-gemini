﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ProjectGemini.Managers
{
    public class EventManager
    {
        private List<IEvent> _events = new List<IEvent>();
        private List<IEvent> _history = new List<IEvent>();
        public bool IsRunning { get; set; }
        public bool Loops { get; set; }

        public void RunEvents()
        {
            if (_events.Count > 0)
            {
                //Console.WriteLine("Events running.");
                IsRunning = true;

                if (_events[0].IsStarted == false)
                {
                    if (_events[0].ShouldLoop == true)
                    {
                        _history.Add(_events[0]);
                    }
                    _events[0].Start();
                }

                if (_events[0].IsFinished == true)
                    _events.RemoveAt(0);
            }
            else
            {
                if (_history.Count > 0)
                {
                    foreach (IEvent e in _history)
                    {
                        e.IsStarted = false;
                        e.IsFinished = false;
                        _events.Add(e);
                    }
                    _history.Clear();
                }
                else
                {
                    //Console.WriteLine("Events finished running.");
                    IsRunning = false;
                }
            }
        }

        public void AddEvent(IEvent e)
        {
            _events.Add(e);
        }

        public void RemoveEvent(IEvent e)
        {
            _events.Remove(e);
        }

        public void SetEvents(List<IEvent> events)
        {
            _events = events;
        }

        public void ClearEvents()
        {
            _events.Clear();
        }
    }
}
