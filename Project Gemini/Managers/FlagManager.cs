﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectGemini.Managers
{
    static public class FlagManager
    {
        static Dictionary<string, bool> Flags = new Dictionary<string,bool>();

        static public void SetFlag(string name, bool state)
        {
            if (Flags.ContainsKey(name))
                Flags[name] = state;
            else
                Flags.Add(name, state);
        }

        static public void SetFlag(FlagEvent flagEvent)
        {
            if (Flags.ContainsKey(flagEvent.FlagName))
                Flags[flagEvent.FlagName] = flagEvent.State;
            else
                Flags.Add(flagEvent.FlagName, flagEvent.State);

            flagEvent.End();
        }

        static public bool CheckFlag(string flagName)
        {
            if (Flags.Keys.Contains(flagName))
                return Flags[flagName];
            else
                return false;
        }

        static public string FlagSaveString()
        {
            string flags = "";

            foreach (KeyValuePair<string, bool> flag in Flags)
            {
                flags += flag.Key + "|" + flag.Value.ToString() + Environment.NewLine;
            }

            return flags;
        }

        static public void ClearFlags()
        {
            Flags.Clear();
        }
    }
}
