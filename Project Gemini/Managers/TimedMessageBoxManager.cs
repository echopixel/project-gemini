﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectGemini.Message_Boxes;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ProjectGemini.Managers
{
    public static class TimedMessageBoxManager
    {
        private static Texture2D messageBorder;

        private static Queue<TimedMessageBox> ItemDropMessages = new Queue<TimedMessageBox>();

        static public void LoadContent(ContentManager content)
        {
            messageBorder = content.Load<Texture2D>("border");
        }

        static public void AddItemDropMessage(int borderSize, string message, float timeToDisplay)
        {
            ItemDropMessages.Enqueue(new TimedMessageBox(messageBorder, borderSize, message, timeToDisplay));
        }

        static public void ClearItemDropMessages()
        {
            ItemDropMessages.Clear();
        }

        static public void Update(GameTime gameTime)
        {
            if (ItemDropMessages.Count > 0)
            {
                if (ItemDropMessages.Peek().fadeTimer > 0)
                    ItemDropMessages.Peek().Update(gameTime);
                else
                    ItemDropMessages.Dequeue();
            }
        }

        static public void Draw(SpriteBatch spriteBatch)
        {
            if (ItemDropMessages.Count > 0)
                ItemDropMessages.Peek().Draw(spriteBatch);
        }
    }
}
