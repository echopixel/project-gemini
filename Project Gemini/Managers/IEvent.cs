﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using ProjectGemini.Entities;
using ProjectGemini.EnumTypes;
using ProjectGemini.Static_Classes;

namespace ProjectGemini.Managers
{
    public interface IEvent
    {
        bool IsStarted { get; set; }
        bool IsFinished { get; set; }
        bool IsCancelable { get; set; }
        bool ShouldLoop { get; set; }

        void Start();
        void End();
        void Cancel();
    }

    #region MoveEvent

    public class MoveEvent : IEvent
    {
        public bool IsStarted { get; set; }
        public bool IsFinished { get; set; }
        public bool IsCanceled { get; set; }
        public bool IsCancelable { get; set; }
        public bool ShouldLoop { get; set; }

        public Vector2 Destination { get; set; }
        public DirectionType Direction { get; set; }

        private DynamicEntity _entity;
        private Vector2 _initialPosition;
        private int _distance;

        public MoveEvent(DynamicEntity entity, DirectionType direction, int distance)
        {
            _entity = entity;
            _distance = distance;
            Direction = direction;
        }

        public void Start()
        {
            IsStarted = true;

            _initialPosition = _entity.Position;

            switch (Direction)
            {
                case DirectionType.Up:
                    Destination = new Vector2(_initialPosition.X, _initialPosition.Y - _distance);
                    break;
                case DirectionType.Down:
                    Destination = new Vector2(_initialPosition.X, _initialPosition.Y + _distance);
                    break;
                case DirectionType.Left:
                    Destination = new Vector2(_initialPosition.X - _distance, _initialPosition.Y);
                    break;
                case DirectionType.Right:
                    Destination = new Vector2(_initialPosition.X + _distance, _initialPosition.Y);
                    break;
            }
            _entity.Move(this);
        }

        public void End()
        {
            IsFinished = true;
        }

        public void Cancel()
        {
            IsFinished = true;
        }
    }
    
    #endregion

    #region ChangeMapEvent

    public class ChangeMapEvent : IEvent
    {
        public bool IsStarted { get; set; }
        public bool IsFinished { get; set; }
        public bool IsCancelable { get; set; }
        public bool ShouldLoop { get; set; }

        public string Position;
        public MapTypes Map { get; set; }

        public ChangeMapEvent(string position, MapTypes map)
        {
            Position = position;
            Map = map;
        }

        public void Start()
        {
            IsStarted = true;

            GameState.ChangeMap(this);
        }

        public void End()
        {
            IsFinished = true;
        }

        public void Cancel()
        {
            IsFinished = true;
        }
    }
    
    #endregion

    #region MessageBoxEvent

    public class MessageBoxEvent : IEvent
    {
        public bool IsStarted { get; set; }
        public bool IsFinished { get; set; }
        public bool IsCancelable { get; set; }
        public bool ShouldLoop { get; set; }

        public string Message { get; set; }

        public MessageBoxEvent(string message)
        {
            Message = message;
        }

        public void Start()
        {
            IsStarted = true;

            GameState.DisplayMessageBox(this);
        }

        public void End()
        {
            IsFinished = true;
        }

        public void Cancel()
        {
            IsFinished = true;
        }
    }

    #endregion

    #region FlagEvent

    public class FlagEvent : IEvent
    {
        public bool IsStarted { get; set; }
        public bool IsFinished { get; set; }
        public bool IsCancelable { get; set; }
        public bool ShouldLoop { get; set; }

        public string FlagName { get; set; }
        public bool State { get; set; }

        public FlagEvent(string flagName, bool state)
        {
            FlagName = flagName;
            State = state;
        }

        public void Start()
        {
            IsStarted = true;

            //Console.WriteLine("Flag");

            FlagManager.SetFlag(this);
        }

        public void End()
        {
            IsFinished = true;
        }

        public void Cancel()
        {
            IsFinished = true;
        }
    }

    #endregion

    #region DialogueEvent

    public class DialogueEvent : IEvent
    {
        public bool IsStarted { get; set; }
        public bool IsFinished { get; set; }
        public bool IsCancelable { get; set; }
        public bool ShouldLoop { get; set; }

        public Tuple<string, string[]> Dialogue { get; set; }

        private NPC _npc;

        public DialogueEvent(NPC npc, Tuple<string, string[]> dialogue)
        {
            _npc = npc;
            Dialogue = dialogue;
        }

        public void Start()
        {
            IsStarted = true;

            //Console.WriteLine("Process");

            _npc.ProcessDialogue(this);
        }

        public void End()
        {
            IsFinished = true;
        }

        public void Cancel()
        {
            IsFinished = true;
        }
    }

    #endregion
}
