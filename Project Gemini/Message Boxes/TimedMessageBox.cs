﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using ProjectGemini.Static_Classes;

namespace ProjectGemini.Message_Boxes
{
    /// <summary>
    /// A class for drawing a solid colored texture with a border where you do not wish the width and height (in pixels) of the border to change.
    /// </summary>
    public class TimedMessageBox
    {
        BorderedTextureBox borderedMessageBox;

        /// <summary>
        /// The message to display when this box is drawn.
        /// </summary>
        string message;

        /// <summary>
        /// Amount of time message should display in milliseconds before beginning to vanish.
        /// </summary>
        float messageDisplayTimer;

        /// <summary>
        /// Amount of time it should take in milliseconds for the message box to completely vanish;
        /// </summary>
        public float fadeTimer { get; private set; }

        /// <summary>
        /// Create a new BorderedTextureBox using the specified parameters.
        /// </summary>
        /// <param name="texture">The texture to use.</param>
        /// <param name="borderSize">The width in pixels of the border around the texture.</param>
        public TimedMessageBox(Texture2D texture, int borderSize, string message, float timeToDisplay)
            : this(texture, borderSize, borderSize, message, timeToDisplay)
        {
        }

        /// <summary>
        /// Create a new BorderedTextureBox using the specified parameters.
        /// </summary>
        /// <param name="texture">The texture to use.</param>
        /// <param name="borderWidth">The width in pixels of the border along the width (i.e. horizontal sides) of the texture.</param>
        /// <param name="borderHeight">The width in pixels of the border along the height (i.e. vertical sides) of the texture.</param>
        public TimedMessageBox(Texture2D texture, int borderWidth, int borderHeight, string message, float timeToDisplay)
        {
            borderedMessageBox = new BorderedTextureBox(texture, borderWidth, borderHeight);
            this.message = message;
            messageDisplayTimer = timeToDisplay;
            fadeTimer = 500;
        }

        public void Update(GameTime gameTime)
        {
            if (messageDisplayTimer > 0)
                messageDisplayTimer -= (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            if (messageDisplayTimer <= 0 && fadeTimer > 0)
                fadeTimer -= (float)gameTime.ElapsedGameTime.TotalMilliseconds;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            SpriteFont font = GameState.ScreenManager.Font;

            // The background includes a border somewhat larger than the text itself.
            const int hPad = 8;
            const int vPad = 4;

            // Center the message text in the viewport.
            Viewport viewport = GameState.ScreenManager.GraphicsDevice.Viewport;
            Vector2 viewportSize = new Vector2(ResolutionManager.VirtualWidth, ResolutionManager.VirtualHeight);
            Vector2 textSize = font.MeasureString(message);
            Vector2 textPosition = new Vector2(
                (int)GameState.Camera.getCameraVector.X + hPad,
                (int)GameState.Camera.getCameraVector.Y + viewportSize.Y - viewportSize.Y / 10 + vPad);

            Rectangle backgroundRectangle = new Rectangle((int)GameState.Camera.getCameraVector.X + (int)hPad / 2,
                                                          (int)GameState.Camera.getCameraVector.Y + (int)(viewportSize.Y - viewportSize.Y / 10) + vPad / 2,
                                                          (int)((ResolutionManager.VirtualWidth - hPad) * (fadeTimer / 500)),
                                                          (int)viewportSize.Y / 10 - vPad);

            Color boxColor = Color.White * (fadeTimer / 500);
            Color textColor = Color.Black * (fadeTimer / 500);

            borderedMessageBox.Draw(spriteBatch, backgroundRectangle, boxColor, 0);
            spriteBatch.DrawString(font, message, textPosition, textColor, 0, Vector2.Zero, 0.30f, SpriteEffects.None, 0);
        }
    }
}
