﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ProjectGemini.Message_Boxes
{
    /// <summary>
    /// A class for drawing a solid colored texture with a border where you do not wish the width and height (in pixels) of the border to change.
    /// </summary>
    public class BorderedTextureBox
    {
        /// <summary>
        /// The texture to be drawn.
        /// </summary>
        Texture2D texture;

        /// <summary>
        /// The width in pixels of the border along the width (i.e. horizontal sides) of the texture.
        /// </summary>
        int borderWidth;

        /// <summary>
        /// The width in pixels of the border along the height (i.e. vertical sides) of the texture.
        /// </summary>
        int borderHeight;

        /// <summary>
        /// The Rectangles used to construct the properly scaled texture. These deal with unscaled corners, scaled verticals along the left and right
        /// borders (sampled from the middle of each, respectively), scaled horizontals along the top and bottom (same sampling method), and a
        /// scaled middle, sampled from the middle of the image and scaled to fill in all areas not covered by the border.
        /// </summary>
        Rectangle upperLeftSource, upperRightSource, upperStretchSource, lowerLeftSource, lowerRightSource, lowerStretchSource, leftStretchSource, rightStretchSource, middleSource;

        /// <summary>
        /// Used to avoid recalculating the other Rectangles every frame if the destination passed is the same as it was last frame.
        /// </summary>
        Rectangle previousDestination;

        /// <summary>
        /// The span, in pixels, between the end of the border on the left and the beginning of the border on the right. Used as a scaling factor.
        /// </summary>
        float horizontalGapWidth;

        /// <summary>
        /// The span, in pixels, between the end of the border on the top and the beginning of the border on the bottom. Used as a scaling factor.
        /// </summary>
        float verticalGapWidth;

        /// <summary>
        /// Create a new BorderedTextureBox using the specified parameters.
        /// </summary>
        /// <param name="texture">The texture to use.</param>
        /// <param name="borderSize">The width in pixels of the border around the texture.</param>
        public BorderedTextureBox(Texture2D texture, int borderSize)
            : this(texture, borderSize, borderSize)
        {
        }

        /// <summary>
        /// Create a new BorderedTextureBox using the specified parameters.
        /// </summary>
        /// <param name="texture">The texture to use.</param>
        /// <param name="borderWidth">The width in pixels of the border along the width (i.e. horizontal sides) of the texture.</param>
        /// <param name="borderHeight">The width in pixels of the border along the height (i.e. vertical sides) of the texture.</param>
        public BorderedTextureBox(Texture2D texture, int borderWidth, int borderHeight)
        {
            this.texture = texture;
            this.borderWidth = borderWidth;
            this.borderHeight = borderHeight;
        }

        /// <summary>
        /// Draws the texture.
        /// </summary>
        /// <param name="spriteBatch">The SpriteBatch with which to draw the texture. The calling method is responsible for calling Begin and End for the SpriteBatch.</param>
        /// <param name="destination">A rectangle specifying the destination. This will be used to appropriately scale the image.</param>
        /// <param name="color">The color to tint the texture. Use Color.White for no tinting. (Same as SpriteBatch.Draw).>
        /// <param name="layerDepth">The layerDepth to be passed to SpriteBatch.Draw. Must be between 0.0f and 1.0f, inclusive.</param>
        public void Draw(SpriteBatch spriteBatch, Rectangle destination, Color color, float layerDepth)
        {
            if (texture == null)
            {
#if DEBUG
                // In debug configuration, we want to know about the problem, so throw an exception.
                throw new InvalidOperationException("Texture is null.");
#else
                // In release configuration, we don't want the game to crash over a missing texture, so return silently.
                return;
#endif
            }
            // If both borderWidth and borderHeight are 0, or if the either of the destination dimensions are less than the value of the combined corners,
            // or if the provided texture's dimensions are less than the borderWidth or borderHeight, 
            // then just draw it auto-scaled to the size requested
            if ((borderWidth == 0 && borderHeight == 0) || destination.Width < borderWidth * 2 || destination.Height < borderHeight * 2 || texture.Width <= borderWidth * 2 || texture.Height <= borderHeight * 2)
            {
                spriteBatch.Draw(texture, destination, color);
                return;
            }

            if (destination.X != previousDestination.X || destination.Y != previousDestination.Y || destination.Width != previousDestination.Width || destination.Height != previousDestination.Height)
            {
                int halfWidth = texture.Width / 2;
                int halfHeight = texture.Height / 2;

                // Upper corners
                upperLeftSource = new Rectangle(0, 0, borderWidth, borderHeight);
                upperRightSource = new Rectangle(texture.Width - borderWidth, 0, borderWidth, borderHeight);

                // Lower corners
                lowerLeftSource = new Rectangle(0, texture.Height - borderWidth, borderWidth, borderHeight);
                lowerRightSource = new Rectangle(texture.Width - borderWidth, texture.Height - borderHeight, borderWidth, borderHeight);

                // Horizontal borders
                upperStretchSource = new Rectangle(halfWidth, 0, 1, borderHeight);
                lowerStretchSource = new Rectangle(halfWidth, texture.Height - borderHeight, 1, borderHeight);

                // Vertical borders
                leftStretchSource = new Rectangle(0, halfHeight, borderWidth, 1);
                rightStretchSource = new Rectangle(texture.Width - borderWidth, halfHeight, borderWidth, 1);

                // Middle (grab from the very middle to avoid sampling issues)
                middleSource = new Rectangle(halfWidth, halfHeight, 1, 1);

                // Determine the span between corners
                horizontalGapWidth = destination.Width - (borderWidth * 2);
                verticalGapWidth = destination.Height - (borderHeight * 2);

                // Assign previousDestination to avoid having to do these calculations whenever this doesn't change.
                previousDestination = destination;
            }

            // First draw the corners since they require no scaling

            // Draw upperLeft
            spriteBatch.Draw(texture,
                new Vector2(destination.X, destination.Y),
                upperLeftSource,
                color,
                0.0f,
                Vector2.Zero,
                Vector2.One,
                SpriteEffects.None,
                layerDepth);

            // Draw upperRight
            spriteBatch.Draw(texture,
                new Vector2(destination.X + destination.Width - borderWidth, destination.Y),
                upperRightSource,
                color,
                0.0f,
                Vector2.Zero,
                Vector2.One,
                SpriteEffects.None,
                layerDepth);

            // Draw lowerLeft
            spriteBatch.Draw(texture,
                new Vector2(destination.X, destination.Y + destination.Height - borderHeight),
                lowerLeftSource,
                color,
                0.0f,
                Vector2.Zero,
                Vector2.One,
                SpriteEffects.None,
                layerDepth);

            // Draw lowerRight
            spriteBatch.Draw(texture,
                new Vector2(destination.X + destination.Width - borderWidth, destination.Y + destination.Height - borderHeight),
                lowerRightSource,
                color,
                0.0f,
                Vector2.Zero,
                Vector2.One,
                SpriteEffects.None,
                layerDepth);

            // Draw the horizontals

            // Draw upperStretch
            spriteBatch.Draw(texture,
                new Vector2(destination.X + borderWidth, destination.Y),
                upperStretchSource,
                color,
                0.0f,
                Vector2.Zero,
                new Vector2(horizontalGapWidth, 1.0f),
                SpriteEffects.None,
                layerDepth);

            // Draw lowerStretch
            spriteBatch.Draw(texture,
                new Vector2(destination.X + borderWidth, destination.Y + destination.Height - borderHeight),
                lowerStretchSource,
                color,
                0.0f,
                Vector2.Zero,
                new Vector2(horizontalGapWidth, 1.0f),
                SpriteEffects.None,
                layerDepth);

            // Draw the verticals
            // Draw leftStretch
            spriteBatch.Draw(texture,
                new Vector2(destination.X, destination.Y + borderHeight),
                leftStretchSource,
                color,
                0.0f,
                Vector2.Zero,
                new Vector2(1.0f, verticalGapWidth),
                SpriteEffects.None,
                layerDepth);

            // Draw rightStretch
            spriteBatch.Draw(texture,
                new Vector2(destination.X + destination.Width - borderWidth, destination.Y + borderHeight),
                rightStretchSource,
                color,
                0.0f,
                Vector2.Zero,
                new Vector2(1.0f, verticalGapWidth),
                SpriteEffects.None,
                layerDepth);

            // Draw middle
            spriteBatch.Draw(texture,
                new Vector2(destination.X + borderWidth, destination.Y + borderHeight),
                middleSource,
                color,
                0.0f,
                Vector2.Zero,
                new Vector2(horizontalGapWidth, verticalGapWidth),
                SpriteEffects.None,
                layerDepth);
        }
    }
}
