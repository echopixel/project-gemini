#region File Description
//-----------------------------------------------------------------------------
// Game.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ProjectGemini.Components;
using FuncWorks.XNA.XTiled;
using ProjectGemini.Static_Classes;
using FarseerPhysics.Dynamics;
using ProjectGemini.ContentReader;
using ProjectGemini.Utilities;
using EasyStorage;
using System;
using ProjectGemini.Audio;
using ProjectGemini.Managers;
#endregion

namespace ProjectGemini
{
    /// <summary>
    /// 2D Action RPG inspired by SNES games.
    /// All the interesting stuff happens in the ScreenManager component.
    /// </summary>
    public class GeminiGame : Microsoft.Xna.Framework.Game
    {
        #region Fields

        GraphicsDeviceManager graphics;
        public ScreenManager screenManager;
        private AudioManager audioManager;

        // By preloading any assets used by UI rendering, we avoid framerate glitches
        // when they suddenly need to be loaded in the middle of a menu transition.
        static readonly string[] preloadAssets =
        {
            "Backgrounds/gradient",
        };

        
        #endregion

        #region Initialization


        /// <summary>
        /// The main game constructor.
        /// </summary>
        public GeminiGame()
        {
            //Console.WriteLine(DateTime.Now.Month + "-" + DateTime.Now.Day + "-" + DateTime.Now.Year + " " +
            //    DateTime.Now.Hour + "-" + DateTime.Now.Minute + "-" + DateTime.Now.Second + ".sav");
            GameState.Game = this;
            Content.RootDirectory = "Content";

            graphics = new GraphicsDeviceManager(this);
            ResolutionManager.Init(ref graphics);
            // Set Screen Size
            GameState.ScreenWidth = 1280;
            GameState.ScreenHeight = 720;
            // Change Virtual Resolution
            ResolutionManager.SetVirtualResolution(256, 224);
            ResolutionManager.SetResolution(GameState.ScreenWidth, GameState.ScreenHeight, true);

            GameState.PhysicsWorld = new World(new Vector2(0, 0));
            GameState.PhysicsWorld.ContactManager.BeginContact += GamePhysics.BeginContact;
            GameState.PhysicsWorld.ContactManager.EndContact += GamePhysics.EndContact;
            GameState.PhysicsWorld.ContactManager.PreSolve += GamePhysics.PreSolve;
            GameState.PhysicsWorld.ContactManager.PostSolve += GamePhysics.PostSolve;
            // 1 meter = 64 pixels
            ConvertUnits.SetDisplayUnitToSimUnitRatio(64f);

            // Create the screen manager component.
            screenManager = new ScreenManager(this);
            GameState.ScreenManager = screenManager;
            audioManager = new AudioManager(this);
            GameState.AudioManager = audioManager;

            GameState.WorldEventManager = new EventManager();
            GameState.Inventory = new Inventory();

            Components.Add(screenManager);
            Components.Add(audioManager);
            Components.Add(new FrameRateComponent(this));

            // Activate the first screen.
            screenManager.AddScreen(new BackgroundScreen(), null);

            GameState.SaveDevicePrompt();


            WeaponReader wr = new WeaponReader("..\\..\\..\\..\\Content\\Game Data\\weapons.xml");
            wr.ReadXml();
            ArmorReader arr = new ArmorReader("..\\..\\..\\..\\Content\\Game Data\\armors.xml");
            arr.ReadXml();
            AbilityReader ar = new AbilityReader("..\\..\\..\\..\\Content\\Game Data\\abilities.xml");
            ar.ReadXml();
            ItemReader ir = new ItemReader("..\\..\\..\\..\\Content\\Game Data\\items.xml");
            ir.ReadXml();
            EnemyReader er = new EnemyReader("..\\..\\..\\..\\Content\\Game Data\\enemies.xml");
            er.ReadXml();

            //Leave, have to use this to publish for now
            /*
            WeaponReader wr = new WeaponReader("Content\\Game Data\\weapons.xml");
            wr.ReadXml();
            ArmorReader arr = new ArmorReader("Content\\Game Data\\armors.xml");
            arr.ReadXml();
            AbilityReader ar = new AbilityReader("Content\\Game Data\\abilities.xml");
            ar.ReadXml();
            ItemReader ir = new ItemReader("Content\\Game Data\\items.xml");
            ir.ReadXml();
            EnemyReader er = new EnemyReader("Content\\Game Data\\enemies.xml");
            er.ReadXml();
             */

        }

        /// <summary>
        /// Loads graphics content.
        /// </summary>
        protected override void LoadContent()
        {
            foreach (string asset in preloadAssets)
            {
                Content.Load<object>(asset);
            }
        }

        #endregion

        #region Draw


        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        protected override void Draw(GameTime gameTime)
        {
            ResolutionManager.BeginDraw();

            // The real drawing happens inside the screen manager component.
            base.Draw(gameTime);
        }


        #endregion

    }

    #region Entry Point

    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    static class Program
    {
        static void Main()
        {
            using (GeminiGame game = new GeminiGame())
            {
                game.Run();
            }
        }
    }

    #endregion
}
