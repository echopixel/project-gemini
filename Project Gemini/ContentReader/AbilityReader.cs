﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Diagnostics;
using ProjectGemini.Entities;
using ProjectGemini.Static_Classes;
using ProjectGemini.Utilities;
using ProjectGemini.EnumTypes;
using ProjectGemini.Utilities.Buffs;

namespace ProjectGemini.ContentReader
{
    class AbilityReader : DataReader
    {
        XDocument xmlfile;

        /// <summary>
        /// Reads the enemy xml file.
        /// </summary>
        /// <param name="filename"></param>
        public AbilityReader(string filename)
        {
            xmlfile = LoadXml(filename);
        }

        /// <summary>
        /// Reads the abilities.xml file and creates every ability in the file and stores it in a static list.
        /// </summary>
        public override void ReadXml()
        {
            foreach (XElement ability in xmlfile.Root.Elements("Ability"))
            {
                int abilityId;
                string abilityName;
                string abilityIcon;
                string abilityDescription;
                double abilityDamageFormula;
                AbilityElementType abilityElement;
                AbilityProjectileType abilityProjectileType;
                float abilityAoERadius;
                TargetType abilityTargetType;
                string abilityAnimation;
                float abilityCastTime;
                int abilityResourceCost;
                int abilityPersonalCost;
                string user;
                //placeholder, will need to be buff/debuff class
                List<string> abilityPassives = new List<string>();
                List<BaseBuff> Debuffs = new List<BaseBuff>();
                List<BaseBuff> Buffs = new List<BaseBuff>();
                double cooldown;
                
                abilityId = Convert.ToInt32(ability.Attribute("id").Value);
                abilityName = ability.Attribute("Name").Value;
                abilityIcon = ability.Attribute("Icon").Value;
                abilityDescription = ability.Attribute("Description").Value;
                abilityDamageFormula = Convert.ToDouble(ability.Attribute("DamageFormula").Value);
                abilityElement = (AbilityElementType) Enum.Parse(typeof(AbilityElementType), ability.Attribute("DamageType").Value, true);
                abilityProjectileType = (AbilityProjectileType) Enum.Parse(typeof(AbilityProjectileType), ability.Attribute("ProjectileType").Value, true);
                if (abilityProjectileType == AbilityProjectileType.TargetAoE)
                    abilityAoERadius = Convert.ToInt32(ability.Attribute("AoERadius").Value);
                else
                    abilityAoERadius = -1;
                abilityTargetType = (TargetType)Enum.Parse(typeof(TargetType), ability.Attribute("TargetType").Value, true);
                abilityAnimation = ability.Attribute("Animation").Value;
                abilityCastTime = float.Parse(ability.Attribute("CastTime").Value);
                abilityResourceCost = Convert.ToInt32(ability.Attribute("ResourceCost").Value);
                abilityPersonalCost = Convert.ToInt32(ability.Attribute("PersonalCost").Value);
                user = ability.Attribute("User").Value;
                foreach (XElement passives in ability.Elements("Passives"))
                {
                    foreach (XElement passive in passives.Elements("Passive"))
                    {
                        abilityPassives.Add(passive.Attribute("Name").Value);
                    }
                }
                foreach (XElement debuffs in ability.Elements("DeBuffs"))
                {
                    foreach (XElement debuff in debuffs.Elements("DeBuff"))
                    {
                        MakeBuffs(Debuffs, debuff, false);
                    }
                }

                foreach (XElement buffs in ability.Elements("Buffs"))
                {
                    foreach (XElement buff in buffs.Elements("Buff"))
                    {
                        MakeBuffs(Buffs, buff, false);
                    }
                }

                cooldown = Convert.ToDouble(ability.Attribute("Cooldown").Value) * 1000f;


                Ability newAbility = new Ability(abilityId,
                    abilityName,
                    abilityIcon,
                    abilityDescription,
                    abilityDamageFormula,
                    abilityElement,
                    abilityProjectileType,
                    abilityAoERadius,
                    abilityTargetType,
                    abilityPassives,
                    abilityAnimation,
                    abilityCastTime,
                    abilityResourceCost,
                    abilityPersonalCost,
                    user,
                    Debuffs,
                    Buffs,
                    cooldown);

                GameState.Abilities.Add(newAbility);
            }
            foreach (Ability a in GameState.Abilities)
            {
                //Console.WriteLine(a.ToString());
            }
        }
    }
}
