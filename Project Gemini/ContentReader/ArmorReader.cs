﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using ProjectGemini.Static_Classes;
using ProjectGemini.Utilities;
using ProjectGemini.EnumTypes;

namespace ProjectGemini.ContentReader
{
    class ArmorReader : DataReader
    {
        XDocument xmlfile;

        public ArmorReader(string filename)
        {
            xmlfile = LoadXml(filename);
        }

        public override void ReadXml()
        {
            foreach (XElement armor in xmlfile.Root.Elements("Armor"))
            {
                int armorId;
                string armorName;
                string armorDescription;
                string armorIcon;
                EquipmentSlotType armorSlot;
                int armorHpmod;
                int armorMpmod;
                int armorDamageMod;
                int armorMagicMod;
                int armorHealMod;
                int armorDefenseMod;
                int armorResistanceMod;
                int armorStrmod;
                int armorIntmod;
                int armorDexmod;
                int armorAlamod;
                int armorStamod;
                int armorWismod;
                //placeholder, needs to be buff/debuff class.
                List<BaseBuff> armorPassives = new List<BaseBuff>();

                armorId = Convert.ToInt32(armor.Attribute("id").Value);
                armorName = armor.Attribute("Name").Value;
                armorDescription = armor.Attribute("Description").Value;
                armorIcon = armor.Attribute("Icon").Value;
                armorSlot = (EquipmentSlotType) Enum.Parse(typeof(EquipmentSlotType), armor.Attribute("Slot").Value, true);
                armorHpmod = Convert.ToInt32(armor.Attribute("Hpmod").Value);
                armorMpmod = Convert.ToInt32(armor.Attribute("Mpmod").Value);
                armorDamageMod = Convert.ToInt32(armor.Attribute("DamageMod").Value);
                armorMagicMod = Convert.ToInt32(armor.Attribute("MagicMod").Value);
                armorHealMod = Convert.ToInt32(armor.Attribute("HealMod").Value);
                armorDefenseMod = Convert.ToInt32(armor.Attribute("DefenseMod").Value);
                armorResistanceMod = Convert.ToInt32(armor.Attribute("ResistanceMod").Value);
                armorStrmod = Convert.ToInt32(armor.Attribute("Strmod").Value);
                armorIntmod = Convert.ToInt32(armor.Attribute("Intmod").Value);
                armorDexmod = Convert.ToInt32(armor.Attribute("Dexmod").Value);
                armorAlamod = Convert.ToInt32(armor.Attribute("Alamod").Value);
                armorStamod = Convert.ToInt32(armor.Attribute("Stamod").Value);
                armorWismod = Convert.ToInt32(armor.Attribute("Wismod").Value);

                foreach (XElement buffs in armor.Elements("Passives"))
                {
                    foreach (XElement buff in buffs.Elements("Passive"))
                    {
                        MakeBuffs(armorPassives, buff, true);
                    }
                }

                Armor newArmor = new Armor(armorId,
                    armorName,
                    armorDescription,
                    armorIcon,
                    armorSlot,
                    armorHpmod,
                    armorMpmod,
                    armorDamageMod,
                    armorMagicMod,
                    armorHealMod,
                    armorDefenseMod,
                    armorResistanceMod,
                    armorStrmod,
                    armorIntmod,
                    armorDexmod,
                    armorAlamod,
                    armorStamod,
                    armorWismod,
                    armorPassives);

                GameState.Armors.Add(newArmor);
            }
            foreach (Armor w in GameState.Armors)
            {
                //Console.WriteLine(w.ToString());            
            }
        }
    }
}
