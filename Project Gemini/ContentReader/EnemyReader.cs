﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Diagnostics;
using ProjectGemini.Entities;
using ProjectGemini.Static_Classes;
using ProjectGemini.Utilities;
using ProjectGemini.EnumTypes;

namespace ProjectGemini.ContentReader
{
    class EnemyReader : DataReader
    {
        XDocument xmlfile;

        /// <summary>
        /// Reads the enemy xml file.
        /// </summary>
        /// <param name="filename"></param>
        public EnemyReader(string filename)
        {
            xmlfile = LoadXml(filename);
        }


        /// <summary>
        /// Reads the enemies.xml file and creates every enemy in the file and stores it in a static list.
        /// </summary>
        public override void ReadXml()
        {
            foreach (XElement enemy in xmlfile.Root.Elements("Enemy"))
            {
                int enemyId;
                string enemyName;
                string spriteLocation;
                int enemyLevel;
                int enemyExp;
                int enemyHP;
                int enemyDefense;
                int enemyResist;
                int enemySpeed;
                int enemyDamage;
                int enemyMagic;
                EnemyAIType enemyAI;
                string enemyAnimation;
                Int64 enemyCurrency;
                List<Ability> enemyAbilities = new List<Ability>();
                Dictionary<BaseItem, float> enemyDropList = new Dictionary<BaseItem, float>();
                List<string> enemyPassives = new List<string>();

                enemyId = Convert.ToInt32(enemy.Attribute("id").Value);
                enemyName = enemy.Attribute("Enemy_Name").Value;
                spriteLocation = enemy.Attribute("Sprite_Filename").Value;
                enemyLevel = Convert.ToInt32(enemy.Attribute("Level").Value);
                enemyExp = Convert.ToInt32(enemy.Attribute("Exp").Value);
                enemyHP = Convert.ToInt32(enemy.Attribute("HP").Value);
                enemyDefense = Convert.ToInt32(enemy.Attribute("Defense").Value);
                enemyResist = Convert.ToInt32(enemy.Attribute("Resistance").Value);
                enemySpeed = Convert.ToInt32(enemy.Attribute("Speed").Value);
                enemyDamage = Convert.ToInt32(enemy.Attribute("Damage").Value);
                enemyMagic = Convert.ToInt32(enemy.Attribute("Magic").Value);
                enemyAI = (EnemyAIType) Enum.Parse(typeof(EnemyAIType), enemy.Attribute("AI").Value, true);
                enemyAnimation = enemy.Attribute("Animation").Value;
                enemyCurrency = Convert.ToInt64(enemy.Attribute("Currency").Value);
                //Console.WriteLine(enemy.Element("Abilities"));
                foreach (XElement abilities in enemy.Elements("Abilities"))
                {
                    //Console.WriteLine(abilities.Element("Enemy").Value);
                    foreach (XElement ability in abilities.Elements("Ability"))
                    {
                        //Console.WriteLine(ability.Attribute("id").Value);
                        enemyAbilities.Add(GameState.Abilities[Convert.ToInt32(ability.Attribute("id").Value)]);
                    }
                }
                foreach (XElement drop in enemy.Elements("Drop_List"))
                {
                    foreach (XElement item in drop.Elements("Item"))
                    {
                        enemyDropList.Add(GameState.Items[Convert.ToInt32(item.Attribute("id").Value)], (float) Convert.ToDouble(item.Attribute("DropChance").Value));
                    }
                    foreach (XElement weapon in drop.Elements("Weapon"))
                    {
                        enemyDropList.Add(GameState.Weapons[Convert.ToInt32(weapon.Attribute("id").Value)], (float)Convert.ToDouble(weapon.Attribute("DropChance").Value));
                    }
                    foreach (XElement armor in drop.Elements("Armor"))
                    {
                        enemyDropList.Add(GameState.Armors[Convert.ToInt32(armor.Attribute("id").Value)], (float)Convert.ToDouble(armor.Attribute("DropChance").Value));
                    }
                }
                foreach (XElement passives in enemy.Elements("Passives"))
                {
                    foreach (XElement passive in passives.Elements("Passive"))
                    {
                        enemyPassives.Add(passive.Attribute("Name").Value);
                    }
                }

                Enemy newEnemy = new Enemy(enemyId,
                    enemyName,
                    spriteLocation,
                    enemyLevel,
                    enemyExp,
                    enemyHP,
                    enemyDefense,
                    enemyResist,
                    enemySpeed,
                    enemyDamage,
                    enemyMagic,
                    enemyAI,
                    enemyAnimation,
                    enemyCurrency,
                    enemyAbilities,
                    enemyDropList,
                    enemyPassives);

                GameState.Enemies.Add(newEnemy);
            }
            foreach (Enemy e in GameState.Enemies)
            {
                //Console.WriteLine(e.ToString());
            }
        }
    }
}
