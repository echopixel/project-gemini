﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Diagnostics;
using ProjectGemini.Entities;
using ProjectGemini.Static_Classes;
using ProjectGemini.Utilities;
using ProjectGemini.EnumTypes;

namespace ProjectGemini.ContentReader
{
    class ItemReader : DataReader
    {
        XDocument xmlfile;

        /// <summary>
        /// Reads the enemy xml file.
        /// </summary>
        /// <param name="filename"></param>
        public ItemReader(string filename)
        {
            xmlfile = LoadXml(filename);
        }

        /// <summary>
        /// Reads the items.xml file and creates every item in the file and stores it in a static list.
        /// </summary>
        public override void ReadXml()
        {
            foreach (XElement item in xmlfile.Root.Elements("Item"))
            {
                int itemId;
                string itemName;
                string itemDescription;
                string itemIcon;
                Dictionary<ItemEffectType, int> itemEffect = new Dictionary<ItemEffectType, int>();
                TargetType itemTarget;
                int itemStackSize;
                string itemIsKey;


                itemId = Convert.ToInt32(item.Attribute("id").Value);
                itemName = item.Attribute("Name").Value;
                itemDescription = item.Attribute("Description").Value;
                itemIcon = item.Attribute("Icon").Value;
                foreach (XElement effects in item.Elements("Effects"))
                {
                    foreach (XElement effect in effects.Elements("Effect"))
                    {
                        ItemEffectType ie = (ItemEffectType)Enum.Parse(typeof(ItemEffectType), effect.Attribute("EffectType").Value, true);
                        int mag = Convert.ToInt32(effect.Attribute("Magnitude").Value);
                        itemEffect.Add(ie, mag);
                    }
                }
                itemTarget = (TargetType) Enum.Parse(typeof(TargetType), item.Attribute("Target").Value, true);
                itemStackSize = Convert.ToInt32(item.Attribute("StackSize").Value);
                itemIsKey = item.Attribute("KeyItem").Value;

                Item newItem = new Item(itemId,
                    itemName,
                    itemDescription,
                    itemIcon,
                    itemEffect,
                    itemTarget,
                    itemStackSize,
                    itemIsKey);

                GameState.Items.Add(newItem);
            }
            foreach (Item i in GameState.Items)
            {
                //Console.WriteLine(i.ToString());
            }
        }
    }
}
