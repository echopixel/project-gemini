﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using ProjectGemini.EnumTypes;
using ProjectGemini.Utilities;
using ProjectGemini.Utilities.Buffs;

namespace ProjectGemini.ContentReader
{
    public class DataReader
    {
        
        /// <summary>
        /// Load the xml file.
        /// </summary>
        /// <param name="filename">Location of xml file.</param>
        public XDocument LoadXml(string filename)
        {
            return XDocument.Load(filename);

        }


        public virtual void ReadXml()
        {
             
        }

        protected void MakeBuffs(List<BaseBuff> list, XElement buff, bool passive)
        {
            Double magnitude = Convert.ToDouble(buff.Attribute("magnitude").Value);
            BuffType type = (BuffType) Enum.Parse(typeof(BuffType), buff.Attribute("buffType").Value, true);
            AbilityElementType element = (AbilityElementType)Enum.Parse(typeof(AbilityElementType), buff.Attribute("element").Value, true);

            Double duration = -1;
            string unique = "";

            if (!passive)
            {
                duration = Convert.ToDouble(buff.Attribute("duration").Value);
                unique = buff.Attribute("unique").Value;
            }
            //Console.WriteLine(duration);

            if (type == BuffType.TakeDamage)
            {
                

                list.Add(new TakeDamageBuff(unique, duration, magnitude, null, null, element, type));
            }
            else if (type == BuffType.TakeHeal)
            {
                bool counthealstat = Convert.ToBoolean(buff.Attribute("counthealstat").Value);
                //Console.WriteLine(counthealstat);

                list.Add(new TakeHealBuff(unique, duration, magnitude, null, null, counthealstat, type, element));
            }
            else if (type == BuffType.MovementSpeed)
            {
                list.Add(new MovementSpeedBuff(unique, duration, magnitude, null, null, type, element));
            }
            else if (type == BuffType.CoreStat)
            {
                string stat = buff.Attribute("stat").Value;
                list.Add(new CoreStatBuff(unique, duration, magnitude, null, null, stat, type, element));
            }
            else if (type == BuffType.HeroStat)
            {
                string stat = buff.Attribute("stat").Value;
                list.Add(new HeroStatBuff(unique, duration, magnitude, null, null, stat, type, element));
            }
}

    }

    
}
