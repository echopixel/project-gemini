﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using ProjectGemini.Static_Classes;
using ProjectGemini.Utilities;
using ProjectGemini.EnumTypes;

namespace ProjectGemini.ContentReader
{
    class WeaponReader : DataReader
    {
        XDocument xmlfile;

        public WeaponReader(string filename)
        {
            xmlfile = LoadXml(filename);
        }

        public override void ReadXml()
        {
            foreach (XElement weapon in xmlfile.Root.Elements("Weapon"))
            {
                int weaponId;
                string weaponName;
                string weaponDescription;
                string weaponIcon;
                EquipmentSlotType weaponSlot;
                int weaponTier;
                int weaponHpmod;
                int weaponMpmod;
                int weaponDamageMod;
                int weaponMagicMod;
                int weaponHealMod;
                int weaponDefenseMod;
                int weaponResistanceMod;
                int weaponStrmod;
                int weaponIntmod;
                int weaponDexmod;
                int weaponAlamod;
                int weaponStamod;
                int weaponWismod;
                //placeholder, needs to be buff/debuff class.
                List<BaseBuff> weaponPassives = new List<BaseBuff>();
                //not sure what this should be.
                List<string> weaponUpgrades = new List<string>();

                weaponId = Convert.ToInt32(weapon.Attribute("id").Value);
                weaponName = weapon.Attribute("Name").Value;
                weaponDescription = weapon.Attribute("Description").Value;
                weaponIcon = weapon.Attribute("Icon").Value;
                weaponSlot = (EquipmentSlotType) Enum.Parse(typeof(EquipmentSlotType), weapon.Attribute("Slot").Value, true);
                weaponTier = Convert.ToInt32(weapon.Attribute("Tier").Value);
                weaponHpmod = Convert.ToInt32(weapon.Attribute("Hpmod").Value);
                weaponMpmod = Convert.ToInt32(weapon.Attribute("Mpmod").Value);
                weaponDamageMod = Convert.ToInt32(weapon.Attribute("DamageMod").Value);
                weaponMagicMod = Convert.ToInt32(weapon.Attribute("MagicMod").Value);
                weaponHealMod = Convert.ToInt32(weapon.Attribute("HealMod").Value);
                weaponDefenseMod = Convert.ToInt32(weapon.Attribute("DefenseMod").Value);
                weaponResistanceMod = Convert.ToInt32(weapon.Attribute("ResistanceMod").Value);
                weaponStrmod = Convert.ToInt32(weapon.Attribute("Strmod").Value);
                weaponIntmod = Convert.ToInt32(weapon.Attribute("Intmod").Value);
                weaponDexmod = Convert.ToInt32(weapon.Attribute("Dexmod").Value);
                weaponAlamod = Convert.ToInt32(weapon.Attribute("Alamod").Value);
                weaponStamod = Convert.ToInt32(weapon.Attribute("Stamod").Value);
                weaponWismod = Convert.ToInt32(weapon.Attribute("Wismod").Value);

                foreach (XElement buffs in weapon.Elements("Passives"))
                {
                    foreach (XElement buff in buffs.Elements("Passive"))
                    {
                        MakeBuffs(weaponPassives, buff, true);
                    }
                }
                foreach (XElement upgrades in weapon.Elements("Upgrades"))
                {
                    foreach (XElement upgrade in upgrades.Elements("Upgrade"))
                    {
                        weaponUpgrades.Add(upgrade.Attribute("Name").Value);
                    }
                }

                Weapon newWeapon = new Weapon(weaponId,
                    weaponName,
                    weaponDescription,
                    weaponIcon,
                    weaponSlot,
                    weaponTier,
                    weaponHpmod,
                    weaponMpmod,
                    weaponDamageMod,
                    weaponMagicMod,
                    weaponHealMod,
                    weaponDefenseMod,
                    weaponResistanceMod,
                    weaponStrmod,
                    weaponIntmod,
                    weaponDexmod,
                    weaponAlamod,
                    weaponStamod,
                    weaponWismod,
                    weaponPassives,
                    weaponUpgrades);

                GameState.Weapons.Add(newWeapon);
            }
            foreach (Weapon w in GameState.Weapons)
            {
                //Console.WriteLine(w.ToString());
            }
        }
    }
}
