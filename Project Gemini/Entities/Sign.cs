﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using ProjectGemini.Static_Classes;
using FarseerPhysics.Factories;

namespace ProjectGemini.Entities
{
    public class Sign : BaseEntity
    {
        public string SignText { get; set; }

        public Body Body { get; set; }
        public void SetPosition(Vector2 position)
        {
            Body.Position = ConvertUnits.ToSimUnits(position);
        }

        public Sign(Vector2 position, int width, int height, string signText)
        {
            Bounds.Width = width;
            Bounds.Height = height;

            Body = BodyFactory.CreateRectangle(
                GameState.PhysicsWorld,
                ConvertUnits.ToSimUnits(width),
                ConvertUnits.ToSimUnits(height),
                0,
                this);
            Body.BodyType = BodyType.Static;
            Body.SleepingAllowed = false;

            SetPosition(position);
            SignText = signText;

            Body.UserData = this;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override bool IsTouchingHero(ref Body eventBody)
        {
            eventBody = Body;
            return true;
        }

        public override bool InteractWith(ScreenManager screenManager, PlayerIndex? controllingPlayer)
        {
            MessageBoxScreen signMessageBox = new MessageBoxScreen(SignText,
                            HelperMethods.TestRange(GameState.HeroOne.Position.Y,
                                                GameState.CurrentMap.Bounds.Height - ResolutionManager.VirtualHeight / 4,
                                                GameState.CurrentMap.Bounds.Height));
            screenManager.AddScreen(signMessageBox, controllingPlayer);

            return true;
        }
    }
}
