﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectGemini.Graphics_Utilities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ProjectGemini.Controllers;
using Microsoft.Xna.Framework.Input;
using FuncWorks.XNA.XTiled;
using ProjectGemini.Static_Classes;
using ProjectGemini.EnumTypes;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using FarseerPhysics.Common;
using FarseerPhysics.Collision;
using FarseerPhysics;
using FarseerPhysics.Dynamics.Contacts;
using ProjectGemini.Message_Boxes;
using FarseerPhysics.Dynamics.Joints;
using ProjectGemini.Utilities;
using Microsoft.Xna.Framework.Content;
using ProjectGemini.GUI;
using ProjectGemini.Utilities.Buffs;
using ProjectGemini.Managers;
using ParticleEngine2D;
namespace ProjectGemini.Entities
{
    public class Hero : TargetableEntity
    {
        private bool AutoAttackMelee;

        

        private SpriteFont _font;

        //int RegenTimer = 0;
        

        public const int EXP_TO_LEVEL = 1000;

        ContentManager content;
        public SpriteAnimation AttackAnimation { get; set; }
        public Vector2 AttackPosition { get { return AttackAnimation.Position; } set { AttackAnimation.Position = value; } }
        public SpriteAnimation CastingAnimation { get; set; }
        public Vector2 CastingPosition { get { return CastingAnimation.Position; } set { CastingAnimation.Position = value; } }

        #region Player Stats

        public int Level { get; set; }
        public int SkillPoints;
        public int EXP { get; set; }
        public Armor ArmorHead { get; private set; }
        public Armor ArmorChest { get; private set; }
        public Armor ArmorLegs { get; private set; }
        public Weapon Weapon { get; private set; }
        public List<Ability> Skills { get; private set; }
        public List<Ability> AvailableSkills;
        public PersonalResourceType ResourceType;
        public int SelectedItem;
        //public List<Ability> onCooldown { get; private set; }

        private double HPRegenLeft;
        private double MPRegenLeft;

        public int SkillSet { get; set; }

        public Ability Skill1 { get; set; }
        public Ability Skill2 { get; set; }
        public Ability Skill3 { get; set; }

        public Ability Skill4 { get; set; }
        public Ability Skill5 { get; set; }
        public Ability Skill6 { get; set; }

        public Ability Skill7 { get; set; }
        public Ability Skill8 { get; set; }
        public Ability Skill9 { get; set; }

        #endregion

        #region Base Stats

        public int BaseHP { get; set; }
        public int BaseMP { get; set; }

        public int BaseStr { get; set; }
        public int BaseInt { get; set; }
        public int BaseDex { get; set; }
        public int BaseAla { get; set; }
        public int BaseSta { get; set; }
        public int BaseWis { get; set; }

        #endregion

        #region Stat Modifiers

        public float HPMod { get; set; }
        public float MPMod { get; set; }

        public float StrMod { get; set; }
        public float IntMod { get; set; }
        public float DexMod { get; set; }
        public float AlaMod { get; set; }
        public float StaMod { get; set; }
        public float WisMod { get; set; }

        public float HPRegenMod { get; set; }
        public float MPRegenMod { get; set; }

        #endregion

        #region Total Gear Stats

        public int TotalStr { get; set; }
        public int TotalInt { get; set; }
        public int TotalDex { get; set; }
        public int TotalAla { get; set; }
        public int TotalSta { get; set; }
        public int TotalWis { get; set; }

        #endregion

        #region Core Stats

        public double HPRegen { get; set; }
        public double MPRegen { get;  set; }

        public double DamageCritChance { get;  set; }
        public int DamageCritDamage { get;  set; }
        public double MagicCritChance { get;  set; }
        public int MagicCritDamage { get;  set; }
        public double Haste { get; set; }

        public int Defense { get; set; }
        public int Resistance { get; set; }

        #endregion

        #region Formulas

        public float[] HPForm { get; protected set; }
        public float[] MPForm { get; protected set; }

        public float[] DamageForm { get; protected set; }
        public float[] MagicForm { get; protected set; }
        public float[] HealForm { get; protected set; }
        public float[] DefenseForm { get; protected set; }
        public float[] ResistanceForm { get; protected set; }

        public float[] DamageCritChanceForm { get; protected set; }
        public float[] MagicCritChanceForm { get; protected set; }
        public float[] HasteForm { get; protected set; }

        public float[] HPRegenForm { get; protected set; }
        public float[] MPRegenForm { get; protected set; }

        public int[] LevelUpStats;

        #endregion

        #region Personal Resource

        public int MaxPersonal { get; set; }
        public int CurrentPersonal { get; set; }
        public float PersonalRegen { get; set; }
        public float PersonalRegenLeft { get; set; }

        #endregion


        #region Player Status

        public bool HasUnusedLevel { get; set; }
        public bool IsDead { get; set; }

        #endregion

        #region Misc Stats

        public HUD HeroHUD { get; set; }

        private Vector2 _movementVector;
        private bool _isMovingDiagonally = false;
        private Body _eventBody;
        //private Body _weaponCenter;
        //private RevoluteJoint _weaponJoint;
        private int _bodyXRadius;
        private int _bodyYRadius;
        private float _invincibleTime = 500f;
        private float _invincibilityTimer;
        private int _flashingValue = 1;
        private float _weaponSwingTime = 125f;
        private float _weaponSwingTimer;
        private float _castTimer;
        private int _targetIndex;
        //private float _spellEffectTimer;
        public float _CastTimeElapsed;
        public float _MaxCastTime;
        private bool _recentlyLeveled;
        private float _levelTextTimer;
        private float _manaMessageTimer;
        private float _lightMessageTimerTimer;
        //private Dictionary<Keys, Ability> _bindings = new Dictionary<Keys, Ability>();
       //private Dictionary<Buttons, Ability> _bindings2 = new Dictionary<Buttons, Ability>();
        //private Keys _abilityKey;

        //public Body WeaponBody { get; set; }
        public Fixture LeftWeaponFixture { get; set; }
        public Fixture RightWeaponFixture { get; set; }
        public Fixture TopWeaponFixture { get; set; }
        public Fixture BottomWeaponFixture { get; set; }
        public int WeaponWidth { get; set; }
        public int WeaponLength { get; set; }

        #endregion

        //For starting fresh
        public Hero(Texture2D texture, int frames, int animations, 
            Texture2D attackTexture, int attackFrames, int attackAnimations,
            Texture2D castingTexture, int castingFrames, int castingAnimations,
            Map map, PersonalResourceType personal)
        {
            SetupViaPersonal(personal);
            SetupStartingStats(personal);
            SetupHero(texture, frames, animations, 
                attackTexture, attackFrames, attackAnimations,
                castingTexture, castingFrames, castingAnimations,
                map);

            UpdateStats();

            CurrentHP = MaxHP;
            CurrentMP = MaxMP;
        }

        //For loading
        public Hero(Texture2D texture, int frames, int animations, 
            Texture2D attackTexture, int attackFrames, int attackAnimations,
            Texture2D castingTexture, int castingFrames, int castingAnimations,
            Map map)
        {
            SetupHero(texture, frames, animations, 
                attackTexture, attackFrames, attackAnimations, 
                castingTexture, castingFrames, castingAnimations,
                map);
        }

        public void SetupHero(Texture2D texture, int frames, int animations, 
            Texture2D attackTexture, int attackFrames, int attackAnimations, 
            Texture2D castingTexture, int castingFrames, int castingAnimations,
            Map map)
        {
            if (content == null)
                content = new ContentManager(GameState.ScreenManager.Game.Services, "Content");

            _font = content.Load<SpriteFont>("font");

            HeroHUD = new HUD(
                _font,
                this,
                content.Load<Texture2D>("Sprites/duskarportrait"),
                content.Load<Texture2D>("GUI/HealthBar"),
                content.Load<Texture2D>("GUI/HealthBar"),
                content.Load<Texture2D>("GUI/HealthBar"),
                content.Load<Texture2D>("GUI/HealthBar"),
                content.Load<Texture2D>("GUI/HealthBar"),
                content.Load<Texture2D>("GUI/levelarrow"));

            Level = 1;
            SkillPoints = Level;
            SelectedItem = 0;

            HPRegenLeft = 0;
            MPRegenLeft = 0;
            PersonalRegenLeft = 0;

            #region Starting Mods

            HPMod = 1;
            MPMod = 1;
            StrMod = 1;
            IntMod = 1;
            DexMod = 1;
            AlaMod = 1;
            StaMod = 1;
            WisMod = 1;

            DamageCritDamage = 50;
            MagicCritDamage = 50;

            HPRegenMod = 1;
            MPRegenMod = 1;

            #endregion

            MovementSpeed = 2;
            BaseSpeed = MovementSpeed;
            _bodyXRadius = 6;
            _bodyYRadius = 4;
            WeaponWidth = 12;
            WeaponLength = 20;
            _weaponSwingTimer = _weaponSwingTime;
            _invincibilityTimer = _invincibleTime;

            ArmorHead = GameState.Armors[0];
            ArmorChest = GameState.Armors[1];
            ArmorLegs = GameState.Armors[2];
            Weapon = GameState.Weapons[0];
            Skills = new List<Ability>();
            //onCooldown = new List<Ability>();

            SpriteAnimation = new SpriteAnimation(texture, frames, animations);
            AttackAnimation = new SpriteAnimation(attackTexture, attackFrames, attackAnimations);
            CastingAnimation = new SpriteAnimation(castingTexture, castingFrames, castingAnimations);
            SetUpAnimations();
            Bounds.Width = SpriteAnimation.Width;
            Bounds.Height = SpriteAnimation.Height;
            CreateHeroBody();
            //_bindings.Add(KEYBIND_1, GameState.Abilities[0]);
            //_bindings.Add(KEYBIND_2, GameState.Abilities[1]);
            //_bindings.Add(KEYBIND_3, GameState.Abilities[2]);
            //_bindings.Add(KEYBIND_4, null);

            //_bindings2.Add(KEYBIND_5, GameState.Abilities[0]);
            //_bindings2.Add(KEYBIND_6, GameState.Abilities[1]);
            //_bindings2.Add(KEYBIND_7, GameState.Abilities[2]);

            SkillSet = 0;

            Skill1 = null;
            Skill2 = null;
            Skill3 = null;

            Skill4 = null;
            Skill5 = null;
            Skill6 = null;

            Skill7 = null;
            Skill8 = null;
            Skill9 = null;            
        }

        public void SetupStartingStats(PersonalResourceType personal)
        {
            if (personal == PersonalResourceType.Light)
            {
                BaseHP = 90;
                BaseMP = 40;
                BaseStr = 20;
                BaseInt = 20;
                BaseDex = 22;
                BaseAla = 21;
                BaseSta = 20;
                BaseWis = 20;
            }
            else if (personal == PersonalResourceType.Shadow)
            {
                BaseHP = 90;
                BaseMP = 40;
                BaseStr = 21;
                BaseInt = 22;
                BaseDex = 20;
                BaseAla = 20;
                BaseSta = 20;
                BaseWis = 20;
            }
        }

        public void SetupViaPersonal(PersonalResourceType personal)
        {
            if (personal == PersonalResourceType.Light)
            {
                //HP, MP, STR, INT, DEX, ALA, STA, WIS
                HPForm = new float[8] { 1f, 0, 0, 0, 0, 0, .75f, 0 };
                MPForm = new float[8] { 0, 1f, 0, .10f, 0, .10f, 0, .55f };

                DamageForm = new float[8] { 0, 0, 0, 0, 1f, 0, 0, 0 };
                MagicForm = new float[8] { 0, 0, 0, 1f, 0, 0, 0, 0 };
                HealForm = new float[8] { 0, 0, 0, 0, 0, 0, 0, 1f };
                DefenseForm = new float[8] { 0, 0, .3f, 0, 0, 0, .2f, 0 };
                ResistanceForm = new float[8] { 0, 0, 0, 0, .1f, .1f, 0, .2f };

                DamageCritChanceForm = new float[8] { 0, 0, 0, 0, .3f, 0, 0, 0 };
                MagicCritChanceForm = new float[8] { 0, 0, 0, 0, 0, .3f, 0, 0 };
                HasteForm = new float[8] { 0, 0, 0, 0, 0, .2f, 0, .1f };

                HPRegenForm = new float[8] { 0, 0, 0, 0, 0, 0, .2f, 0 };
                MPRegenForm = new float[8] { 0, 0, 0, 0, 0, 0, 0, .05f };

                LevelUpStats = new int[8] { 10, 5, 2, 2, 4, 3, 2, 2 };

                ResourceType = PersonalResourceType.Light;
                MaxPersonal = 6;
                CurrentPersonal = MaxPersonal;
                PersonalRegen = .5f;

                AvailableSkills = new List<Ability>()
                { 
                    GameState.Abilities[0], GameState.Abilities[2], GameState.Abilities[4],
                    GameState.Abilities[1], GameState.Abilities[3], GameState.Abilities[5],
                    GameState.Abilities[7], GameState.Abilities[8], GameState.Abilities[9]
                };

                AutoAttackMelee = false;
            }
            else if (personal == PersonalResourceType.Shadow)
            {
                //HP, MP, STR, INT, DEX, ALA, STA, WIS
                HPForm = new float[8] { 1f, 0, 0, 0, 0, 0, .75f, 0 };
                MPForm = new float[8] { 0, 1f, 0, .10f, 0, .10f, 0, .55f };

                DamageForm = new float[8] { 0, 0, 1f, 0, 0, 0, 0, 0 };
                MagicForm = new float[8] { 0, 0, 0, 1f, 0, 0, 0, 0 };
                HealForm = new float[8] { 0, 0, 0, 0, 0, 0, 0, 1f };
                DefenseForm = new float[8] { 0, 0, .3f, 0, 0, 0, .2f, 0 };
                ResistanceForm = new float[8] { 0, 0, 0, 0, .1f, .1f, 0, .2f };

                DamageCritChanceForm = new float[8] { 0, 0, 0, 0, .3f, 0, 0, 0 };
                MagicCritChanceForm = new float[8] { 0, 0, 0, 0, 0, .3f, 0, 0 };
                HasteForm = new float[8] { 0, 0, 0, 0, 0, .2f, 0, .1f };

                HPRegenForm = new float[8] { 0, 0, 0, 0, 0, 0, .2f, 0 };
                MPRegenForm = new float[8] { 0, 0, 0, 0, 0, 0, 0, .05f };

                LevelUpStats = new int[8] { 10, 5, 3, 4, 2, 2, 2, 2 };

                ResourceType = PersonalResourceType.Shadow;
                MaxPersonal = 100;
                CurrentPersonal = 50;
                PersonalRegen = 1f;

                AvailableSkills = new List<Ability>()
                { 
                    GameState.Abilities[11], GameState.Abilities[12], GameState.Abilities[13],
                    GameState.Abilities[14], GameState.Abilities[15], GameState.Abilities[16],
                    GameState.Abilities[17], GameState.Abilities[18], GameState.Abilities[19]
                };

                AutoAttackMelee = true;
            }
        }

        private bool Hero_OnCollision(Fixture fixtureA, Fixture fixtureB, Contact contact)
        {
            //Console.WriteLine(fixtureA.Body.BodyId);
            //Console.WriteLine(fixtureB.Body.BodyId);

            foreach (var trigger in GameState.PhysicsWorld.BodyList)
            {
                //Console.WriteLine(trigger.BodyId);//TileID.ToString());
            }

            // Checking if the Hero must make a map transition.
            CheckEventContact(fixtureA, fixtureB, contact);
            // Checking if the Hero is in a position to interact with an object.
            CheckInteractableEventContact(fixtureA, fixtureB, contact);

            return true;
        }

        private void Hero_OnSeparation(Fixture fixtureA, Fixture fixtureB)
        {
            //Console.WriteLine("YOU STOPPED TOUCHING THINGS");
            if (fixtureB.Body.UserData is Sign || fixtureB.Body.UserData is TreasureChest || fixtureB.Body.UserData is NPC)
                _eventBody = null;
        }

        private bool Weapon_OnCollision(Fixture fixtureA, Fixture fixtureB, Contact contact)
        {
            //Console.WriteLine("Weapon collision happened");
            if (fixtureB.UserData != null)
            {
                Double check = GameState.RandomNumGenerator.NextDouble();
                float mod = 1;

                if (DamageCritChance / 100 > check)
                {
                    mod += DamageCritDamage / 100f;
                }

                //Console.WriteLine(TotalStr + weapon.equipStrmod + weapon.weaponBaseDamage);
                return ((BaseEntity)fixtureB.UserData).TakeDamage((int)(Damage * mod * damageVariance()), AbilityElementType.Physical, true, true);
			}
            return false;
        }

        /// <summary>
        /// Checks to make sure a player is making contact with an entity in the appropriate direction for interaction.
        /// </summary>
        /// <param name="fixtureA"></param>
        /// <param name="fixtureB"></param>
        /// <param name="contact"></param>
        private bool CheckInteractableEventContact(Fixture fixtureA, Fixture fixtureB, FarseerPhysics.Dynamics.Contacts.Contact contact)
        {
            if (fixtureB.Body.UserData != null)
            {
                return ((BaseEntity)fixtureB.Body.UserData).IsTouchingHero(ref _eventBody);
            }

            return false;
        }

        private bool IsHeroFacingInteractableEvent()
        {
            if (HelperMethods.TestRange(this.Body.Position.X,
                _eventBody.Position.X - ConvertUnits.ToSimUnits(((BaseEntity)_eventBody.UserData).Bounds.Width / 2),
                _eventBody.Position.X + ConvertUnits.ToSimUnits(((BaseEntity)_eventBody.UserData).Bounds.Width / 2))
                &&
                this.Body.Position.Y > _eventBody.Position.Y
                &&
                (this.Animation == "Up" || this.Animation == "UpIdle"))
            {
                return true;
            }

            if (HelperMethods.TestRange(this.Body.Position.X,
                _eventBody.Position.X - ConvertUnits.ToSimUnits(((BaseEntity)_eventBody.UserData).Bounds.Width / 2),
                _eventBody.Position.X + ConvertUnits.ToSimUnits(((BaseEntity)_eventBody.UserData).Bounds.Width / 2))
                &&
                this.Body.Position.Y < _eventBody.Position.Y
                &&
                (this.Animation == "Down" || this.Animation == "DownIdle"))
            {
                return true;
            }

            if (HelperMethods.TestRange(this.Body.Position.Y,
                _eventBody.Position.Y - ConvertUnits.ToSimUnits(((BaseEntity)_eventBody.UserData).Bounds.Height / 2),
                _eventBody.Position.Y + ConvertUnits.ToSimUnits(((BaseEntity)_eventBody.UserData).Bounds.Height / 2))
                &&
                this.Body.Position.X > _eventBody.Position.X
                &&
                (this.Animation == "Left" || this.Animation == "LeftIdle"))
            {
                return true;
            }

            if (HelperMethods.TestRange(this.Body.Position.Y,
                _eventBody.Position.Y - ConvertUnits.ToSimUnits(((BaseEntity)_eventBody.UserData).Bounds.Height / 2),
                _eventBody.Position.Y + ConvertUnits.ToSimUnits(((BaseEntity)_eventBody.UserData).Bounds.Height / 2))
                &&
                this.Body.Position.X < _eventBody.Position.X
                &&
                (this.Animation == "Right" || this.Animation == "RightIdle"))
            {
                return true;
            }

            return false;
        }
        
        /// <summary>
        /// A method that allows us to check if the Hero should transition to a new map.
        /// </summary>
        /// <param name="fixtureA"></param>
        /// <param name="fixtureB"></param>
        /// <param name="contact"></param>
        private void CheckEventContact(Fixture fixtureA, Fixture fixtureB, FarseerPhysics.Dynamics.Contacts.Contact contact)
        {
            if (GameState.Events.ContainsKey(fixtureB.Body.BodyId) && GameState.Events[fixtureB.Body.BodyId].Equals("DemoMap"))
            {
                GameState.ChangeMap("HeroOneStart", MapTypes.DemoMap);
            }
            else if (GameState.Events.ContainsKey(fixtureB.Body.BodyId) && GameState.Events[fixtureB.Body.BodyId].Equals("TestHouse"))
            {
                GameState.ChangeMap("HeroOneStart", MapTypes.TestHouse);
            }
            else if (GameState.Events.ContainsKey(fixtureB.Body.BodyId) && GameState.Events[fixtureB.Body.BodyId].Equals("TestArenaTwo"))
            {
                GameState.ChangeMap("TestArenaTwoA", MapTypes.TestArenaTwo);
            }
            else if (GameState.Events.ContainsKey(fixtureB.Body.BodyId) && GameState.Events[fixtureB.Body.BodyId].Equals("TestArenaOne"))
            {
                GameState.ChangeMap("HeroOneStart", MapTypes.TestArenaOne);
            }
            else if (GameState.Events.ContainsKey(fixtureB.Body.BodyId) && GameState.Events[fixtureB.Body.BodyId].Equals("DemoArena"))
            {
                GameState.ChangeMap("DemoArenaA", MapTypes.DemoArena);
            }
        }

        public override bool TakeDamage(int amount, AbilityElementType elementType, bool invulnerableCheck, bool show)
        {
            if (!IsInvincible || !invulnerableCheck)
            {
                if (elementType == AbilityElementType.Physical && invulnerableCheck)
                    amount = (int)(amount * (1f - DefenseRating / 100f));
                else if (invulnerableCheck)
                    amount = (int)(amount * (1f - ResistanceRating / 100f));


                CurrentHP -= amount;
                if(amount != 0)
                    base.TakeDamage(amount, elementType, invulnerableCheck, show);
                if (invulnerableCheck)
                {
                    IsInvincible = true;
                }
                //Console.WriteLine("Locke HP: " + CurrentHP + "/" + MaxHP);
                if (CurrentHP <= 0)
                {
                    IsDead = true;
                    IsInvincible = false;
                    _invincibilityTimer = _invincibleTime;
                }
                return true;
            }

            return false;
        }

        public override bool TakeHeal(int amount, bool show)
        {
            if (CurrentHP == MaxHP || amount == 0)
                return true;
            if (CurrentHP + amount >= MaxHP)
            {
                base.TakeHeal((MaxHP - CurrentHP), show);
                CurrentHP = MaxHP;
            }
            else
            {
                base.TakeHeal(amount, show);
                CurrentHP += amount;
            }
            return true;
        }

        public void CreateHeroBody()
        {
            Settings.MaxPolygonVertices = 10;
            Body = BodyFactory.CreateEllipse(
                GameState.PhysicsWorld,
                ConvertUnits.ToSimUnits(_bodyXRadius),
                ConvertUnits.ToSimUnits(_bodyYRadius),
                10,
                0,
                this);
            Body.BodyType = BodyType.Dynamic;
            Body.SleepingAllowed = false;
            Body.UserData = this;

            Body.OnCollision += new OnCollisionEventHandler(Hero_OnCollision);
            Body.OnSeparation += new OnSeparationEventHandler(Hero_OnSeparation);

            LeftWeaponFixture = FixtureFactory.AttachRectangle(
                ConvertUnits.ToSimUnits(WeaponLength), 
                ConvertUnits.ToSimUnits(WeaponWidth), 
                0, 
                ConvertUnits.ToSimUnits(-WeaponLength / 2, -SpriteAnimation.Height / 4), 
                Body);
            RightWeaponFixture = FixtureFactory.AttachRectangle(
                ConvertUnits.ToSimUnits(WeaponLength),
                ConvertUnits.ToSimUnits(WeaponWidth),
                0,
                ConvertUnits.ToSimUnits(WeaponLength / 2, -SpriteAnimation.Height / 4),
                Body);
            TopWeaponFixture = FixtureFactory.AttachRectangle(
                ConvertUnits.ToSimUnits(WeaponWidth),
                ConvertUnits.ToSimUnits(WeaponLength),
                0,
                ConvertUnits.ToSimUnits(0, -WeaponLength / 2),
                Body);
            BottomWeaponFixture = FixtureFactory.AttachRectangle(
                ConvertUnits.ToSimUnits(WeaponWidth),
                ConvertUnits.ToSimUnits(WeaponLength),
                0,
                ConvertUnits.ToSimUnits(0, WeaponLength / 2),
                Body);



            LeftWeaponFixture.IsSensor = true;
            RightWeaponFixture.IsSensor = true;
            TopWeaponFixture.IsSensor = true;
            BottomWeaponFixture.IsSensor = true;
            LeftWeaponFixture.OnCollision += new OnCollisionEventHandler(Weapon_OnCollision);
            RightWeaponFixture.OnCollision += new OnCollisionEventHandler(Weapon_OnCollision);
            TopWeaponFixture.OnCollision += new OnCollisionEventHandler(Weapon_OnCollision);
            BottomWeaponFixture.OnCollision += new OnCollisionEventHandler(Weapon_OnCollision);

            LeftWeaponFixture.CollidesWith = Category.None;
            RightWeaponFixture.CollidesWith = Category.None;
            TopWeaponFixture.CollidesWith = Category.None;
            BottomWeaponFixture.CollidesWith = Category.None;

            #region Comments

            /*
            WeaponBody = BodyFactory.CreateRectangle(
                GameState.PhysicsWorld,
                ConvertUnits.ToSimUnits(WeaponWidth),
                ConvertUnits.ToSimUnits(WeaponLength),
                0,
                this);
            WeaponBody.BodyType = BodyType.Dynamic;
            WeaponBody.SleepingAllowed = false;
            WeaponBody.IsSensor = true;
            WeaponBody.OnCollision += new OnCollisionEventHandler(Weapon_OnCollision);
            WeaponBody.CollidesWith = Category.None;

            _weaponCenter = BodyFactory.CreateCircle(
                GameState.PhysicsWorld,
                ConvertUnits.ToSimUnits(4f),
                0,
                Body.Position);
            _weaponCenter.CollidesWith = Category.None;

            _weaponJoint = JointFactory.CreateRevoluteJoint(
                GameState.PhysicsWorld,
                WeaponBody,
                _weaponCenter,
                new Vector2(0.1f,0.1f));
            */

            #endregion
        }

        public override void Update(GameTime gameTime)
        {
            #region Cooldown lower

            foreach (Ability skill in Skills)
            {
                if (skill.CooldownTimer > 0)
                {
                    skill.CooldownTimer -= (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                }
                else if (skill.CooldownTimer != 0)
                {
                    skill.CooldownTimer = 0;
                }
            }

            #endregion

            
            

            if (SelectedItem >= GameState.Inventory.items.Count)
            {
                SelectedItem = 0;
            }
            else if (SelectedItem < 0)
            {
                SelectedItem = GameState.Inventory.items.Count - 1;
            }
            if (SelectedItem == -1)
            {
                SelectedItem = 0;
            }

            //Console.WriteLine(SelectedItem);

            //Console.WriteLine(Passives.Count);

            #region HP / MP / Personal regen

            if (!GameState.PlayersInCombat)
            {
                HPRegenLeft += (HPRegen / 100) * gameTime.ElapsedGameTime.TotalMilliseconds / 10;
                MPRegenLeft += (MPRegen / 33) * (float)gameTime.ElapsedGameTime.TotalMilliseconds / 10;
            }
            else
            {
                HPRegenLeft += (HPRegen / 2000) * (float)gameTime.ElapsedGameTime.TotalMilliseconds / 10;
                MPRegenLeft += (MPRegen / 100) * (float)gameTime.ElapsedGameTime.TotalMilliseconds / 10;
            }

            

            if (HPRegenLeft > 1)
            {
                CurrentHP++;
                HPRegenLeft--;
            }
            if (MPRegenLeft > 1)
            {
                CurrentMP++;
                MPRegenLeft--;
            }

            if (ResourceType == PersonalResourceType.Light)
            {
                PersonalRegenLeft += (PersonalRegen / 100) * (float)gameTime.ElapsedGameTime.TotalMilliseconds / 10;

                if (PersonalRegenLeft > 1)
                {
                    CurrentPersonal++;
                    PersonalRegenLeft--;
                }
                
            }
            else if (ResourceType == PersonalResourceType.Shadow)
            {
                int inCombat = 1;

                if (!GameState.PlayersInCombat)
                {
                    inCombat = 5;
                }

                if (CurrentPersonal < 50)
                {
                    PersonalRegenLeft += (PersonalRegen * inCombat / 100) * (float)gameTime.ElapsedGameTime.TotalMilliseconds / 10;

                    if (PersonalRegenLeft > 1)
                    {
                        CurrentPersonal++;
                        PersonalRegenLeft--;
                    }
                }
                else if (CurrentPersonal > 50)
                {
                    PersonalRegenLeft -= (PersonalRegen * inCombat / 100) * (float)gameTime.ElapsedGameTime.TotalMilliseconds / 10;

                    if (PersonalRegenLeft < -1)
                    {
                        CurrentPersonal--;
                        PersonalRegenLeft++;
                    }
                }
            }

            #endregion         

            //WeaponBody.Rotation = WeaponBody.Rotation + 0.01f;
            //_weaponCenter.Rotation = _weaponCenter.Rotation + 0.01f;
            if ((int)_invincibilityTimer % 4 == 0)
                _flashingValue = 1;
            else
                _flashingValue = 0;
            if (IsInvincible)
            {
                _invincibilityTimer -= (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                //Console.WriteLine(_invincibilityTimer);
            }
            if (_invincibilityTimer <= 0)
            {
                IsInvincible = false;
                _invincibilityTimer = _invincibleTime;
            }
            if (IsCasting)
            {
                CastingAnimation.Update(gameTime);
                _castTimer -= (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                //this might be the source 
                if (_CastTimeElapsed < _MaxCastTime)
                    _CastTimeElapsed += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                if (_CastTimeElapsed > _MaxCastTime)
                    _CastTimeElapsed = _MaxCastTime;
                if (_castTimer <= 0 && !IsTargeting && !IsFinishingCast)
                {
                    // spell animation_CastedAbility
                    this.CurrentMP -= _CastedAbility.abilityResourceCost;
                    this.CurrentPersonal -= _CastedAbility.abilityPersonalCost;
                    IsFinishingCast = true;
                }

                //Console.WriteLine(CastingAnimation.AnimationFinished);
                if (IsFinishingCast)
                {
                    if (CastingAnimation.AnimationFinished)
                    {
                        EndCast();
                    }
                }
            }
            if (IsAttacking)
            {
                AttackAnimation.Update(gameTime);
                _weaponSwingTimer -= (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                //Console.WriteLine(_weaponSwingTimer);
            }
            if (_weaponSwingTimer <= 0)
            {
                _weaponSwingTimer = _weaponSwingTime;
                IsAttacking = false;
                //WeaponBody.CollidesWith = Category.None;
                
                LeftWeaponFixture.CollidesWith = Category.None;
                RightWeaponFixture.CollidesWith = Category.None;
                TopWeaponFixture.CollidesWith = Category.None;
                BottomWeaponFixture.CollidesWith = Category.None;
                
            }

            base.Update(gameTime);
            //Console.WriteLine("Body: " + Body.Position);
            //Console.WriteLine("Hero: " + ConvertUnits.ToSimUnits(Position));

            #region HP / MP / Personal Check

            if (CurrentMP > MaxMP)
            {
                CurrentMP = MaxMP;
            }
            else if (CurrentMP < 0)
            {
                CurrentMP = 0;
            }

            if (CurrentHP > MaxHP)
            {
                CurrentHP = MaxHP;
            }
            else if (CurrentHP < 0)
            {
                CurrentHP = 0;
            }

            if (CurrentPersonal > MaxPersonal)
            {
                CurrentPersonal = MaxPersonal;
            }
            else if (CurrentPersonal < 0)
            {
                CurrentPersonal = 0;
            }

            #endregion
        }

        public override void Draw(SpriteBatch spriteBatch)
        {


            if (IsAttacking)
                AttackAnimation.Draw(spriteBatch, new Color(255 * _flashingValue, 255 * _flashingValue, 255 * _flashingValue, 255 * _flashingValue));
            else if (IsCasting)
                CastingAnimation.Draw(spriteBatch, new Color(255 * _flashingValue, 255 * _flashingValue, 255 * _flashingValue, 255 * _flashingValue));
            else
                base.Draw(spriteBatch, new Color(255 * _flashingValue, 255 * _flashingValue, 255 * _flashingValue, 255 * _flashingValue));

            SpriteFont font = GameState.ScreenManager.Font;

            if (_recentlyLeveled)
            {
                _levelTextTimer = 1500f;
                _recentlyLeveled = false;
            }

            if (_levelTextTimer > 0)
            {
                spriteBatch.DrawString(font, "LEVEL UP!", new Vector2(Position.X - Width / 1.25f, 
                    Position.Y - SpriteAnimation.Height + (_levelTextTimer / 200)), 
                    Color.White * (_levelTextTimer / 1500f), 
                    0, 
                    Vector2.Zero, 
                    0.20f, 
                    SpriteEffects.None, 
                    0);
                _levelTextTimer -= (float)GameState.GameTime.ElapsedGameTime.TotalMilliseconds;
            }

            if (_manaMessageTimer > 0)
            {
                spriteBatch.DrawString(font, "NOT ENOUGH MP!", 
                    new Vector2(Position.X - Width * 1.35f, 
                        Position.Y + (SpriteAnimation.Height / 1.3f) - (_manaMessageTimer / 200)), 
                        Color.Blue * (_manaMessageTimer / 1500f), 
                        0, 
                        Vector2.Zero, 
                        0.20f, 
                        SpriteEffects.None, 
                        0);
                _manaMessageTimer -= (float)GameState.GameTime.ElapsedGameTime.TotalMilliseconds;
            }

            if (_lightMessageTimerTimer > 0)
            {
                spriteBatch.DrawString(font, "NOT ENOUGH LIGHT!",
                    new Vector2(Position.X - Width * 1.35f,
                        Position.Y + (SpriteAnimation.Height / 1.3f) - (_lightMessageTimerTimer / 200)),
                        Color.DarkGoldenrod * (_lightMessageTimerTimer / 1500f),
                        0,
                        Vector2.Zero,
                        0.20f,
                        SpriteEffects.None,
                        0);
                _lightMessageTimerTimer -= (float)GameState.GameTime.ElapsedGameTime.TotalMilliseconds;
            }

            if (IsCasting && _CastedAbility.abilityProjectileType == AbilityProjectileType.TargetAoE)
            {
                //TODO: Draw a targeting circle or something showing radius.
            }

            foreach (BaseBuff buff in Buffs)
            {
                buff.DrawBuff(spriteBatch);
            }
        }

        public void SetUpAnimations()
        {
            Animated = true;

            AnimationClass ani = new AnimationClass();
            ani.FramesPerSecond = 5;

            SpriteAnimation.AddAnimation("Right", 4, 4, ani.Copy());
            SpriteAnimation.AddAnimation("Left", 3, 4, ani.Copy());
            SpriteAnimation.AddAnimation("Up", 1, 4, ani.Copy());
            SpriteAnimation.AddAnimation("Down", 2, 4, ani.Copy());

            SpriteAnimation.AddAnimation("RightIdle", 4, 1, ani.Copy());
            SpriteAnimation.AddAnimation("LeftIdle", 3, 1, ani.Copy());
            SpriteAnimation.AddAnimation("UpIdle", 1, 1, ani.Copy());
            SpriteAnimation.AddAnimation("DownIdle", 2, 1, ani.Copy());

            SpriteAnimation.AddAnimation("RightCombat", 8, 4, ani.Copy());
            SpriteAnimation.AddAnimation("LeftCombat", 7, 4, ani.Copy());
            SpriteAnimation.AddAnimation("UpCombat", 5, 4, ani.Copy());
            SpriteAnimation.AddAnimation("DownCombat", 6, 4, ani.Copy());

            SpriteAnimation.AddAnimation("RightCombatIdle", 8, 1, ani.Copy());
            SpriteAnimation.AddAnimation("LeftCombatIdle", 7, 1, ani.Copy());
            SpriteAnimation.AddAnimation("UpCombatIdle", 5, 1, ani.Copy());
            SpriteAnimation.AddAnimation("DownCombatIdle", 6, 1, ani.Copy());

            SpriteAnimation.Animation = "DownIdle";

            AnimationClass attackAni = new AnimationClass();
            attackAni.FramesPerSecond = 16;
            attackAni.IsLooping = false;

            AttackAnimation.AddAnimation("AttackUp", 1, 2, attackAni.Copy());
            AttackAnimation.AddAnimation("AttackDown", 2, 2, attackAni.Copy());
            AttackAnimation.AddAnimation("AttackLeft", 3, 2, attackAni.Copy());
            AttackAnimation.AddAnimation("AttackRight", 4, 2, attackAni.Copy());

            AttackAnimation.Animation = "AttackDown";

            AnimationClass castingAni = new AnimationClass();
            castingAni.FramesPerSecond = 10;

            castingAni.IsLooping = false;
            CastingAnimation.AddAnimation("BeginCastUp", 1, 4, castingAni.Copy());
            CastingAnimation.AddAnimation("BeginCastDown", 2, 4, castingAni.Copy());
            CastingAnimation.AddAnimation("BeginCastLeft", 3, 4, castingAni.Copy());
            CastingAnimation.AddAnimation("BeginCastRight", 4, 4, castingAni.Copy());

            castingAni.IsLooping = true;
            CastingAnimation.AddAnimation("CastingUp", 1, 4, 4, castingAni.Copy());
            CastingAnimation.AddAnimation("CastingDown", 2, 4, 4, castingAni.Copy());
            CastingAnimation.AddAnimation("CastingLeft", 3, 4, 4, castingAni.Copy());
            CastingAnimation.AddAnimation("CastingRight", 4, 4, 4, castingAni.Copy());

            castingAni.FramesPerSecond = 15;
            castingAni.IsLooping = false;
            CastingAnimation.AddAnimation("FinishCastUp", 1, 8, 4, castingAni.Copy());
            CastingAnimation.AddAnimation("FinishCastDown", 2, 8, 4, castingAni.Copy());
            CastingAnimation.AddAnimation("FinishCastLeft", 3, 8, 4, castingAni.Copy());
            CastingAnimation.AddAnimation("FinishCastRight", 4, 8, 4, castingAni.Copy());

            CastingAnimation.Animation = "BeginCastUp";
        }

        public void HandleInput(InputState input, ScreenManager screenManager, PlayerIndex? ControllingPlayer, Camera camera)
        {
            // Look up inputs for the active player profile.
            int playerIndex = (int)ControllingPlayer.Value;

            KeyboardState keyboardState = input.CurrentKeyboardStates[playerIndex];
            GamePadState gamePadState = input.CurrentGamePadStates[playerIndex];

            // The game pauses either if the user presses the pause button, or if
            // they unplug the active gamepad. This requires us to keep track of
            // whether a gamepad was ever plugged in, because we don't want to pause
            // on PC if they are playing with a keyboard and have no gamepad at all!
            bool gamePadDisconnected = !gamePadState.IsConnected &&
                                       input.GamePadWasConnected[playerIndex];

            if (input.IsPauseGame(ControllingPlayer) || gamePadDisconnected)
            {
                //Body.LinearVelocity = Vector2.Zero; //Stop the player's physics object when pausing.
                screenManager.AddScreen(new PauseMenuScreen(), ControllingPlayer);
            }
            else
            {
                CheckDebugInput(input, screenManager, ControllingPlayer);

                if (!EventManager.IsRunning)
                {
                    // Zero out the movement vector
                    _movementVector = Vector2.Zero;

                    // Actions
                    CheckActionInput(input, ControllingPlayer, screenManager);
                    CheckAbilityInput(input, ControllingPlayer);

                    // Movement
                    if (!IsAttacking && !IsCasting)
                        CheckMovementInput(input, ControllingPlayer);

                    if (_movementVector != Vector2.Zero && _isMovingDiagonally == false)
                        _movementVector.Normalize();

                    Body.LinearVelocity = _movementVector * MovementSpeed;

                    //_weaponCenter.Position = Body.Position;

                }

                // Clamps the player so they can't walk off the map.
                Vector2 min = new Vector2(SpriteAnimation.Width / 2, SpriteAnimation.Height / 2);
                Vector2 max = new Vector2(GameState.CurrentMap.Bounds.Width - _bodyXRadius, GameState.CurrentMap.Bounds.Height - _bodyYRadius);

                Body.Position = Vector2.Clamp(Body.Position,
                    ConvertUnits.ToSimUnits(min),
                    ConvertUnits.ToSimUnits(max)); // Map width - player height

                // Attach sprite to body
                Vector2 v;
                if (IsAttacking)
                {
                    v = ConvertUnits.ToDisplayUnits(
                        new Vector2(Body.Position.X,
                                    Body.Position.Y - ConvertUnits.ToSimUnits(11)));
                    AttackPosition = new Vector2((int)v.X, (int)v.Y);
                }
                else
                {
                    v = ConvertUnits.ToDisplayUnits(
                        new Vector2(Body.Position.X,
                                    Body.Position.Y - ConvertUnits.ToSimUnits(8)));
                    Position = new Vector2((int)v.X, (int)v.Y);
                }

                // Set the new bounds for the player's bounding rectangle
                //Bounds.X = (int)(Position.X - SpriteAnimation.Width / 2);
                //Bounds.Y = (int)(Position.Y - SpriteAnimation.Height / 2);

                // Update the camera if it is following the player
                if (GameState.Camera.IsFollowingHero)
                    GameState.Camera.Update();

                _isMovingDiagonally = false;
            }
        }

        private void CheckActionInput(InputState input, PlayerIndex? controllingPlayer, ScreenManager screenManager)
        {
            PlayerIndex buttonPusher;

            if (input.IsConfirmAction(controllingPlayer, out buttonPusher))
            {
                if (GameState.PlayersInCombat && IsCasting == false && IsAttacking == false)
                {
                    if (AutoAttackMelee)
                    {
                        //Console.WriteLine("SWING WEAPON");
                        GameState.AudioManager.PlaySound("Slash", 1.0f, 0f, 0f);
                        IsAttacking = true;
                        //WeaponBody.CollidesWith = Category.All;

                        if (this.Animation == "LeftCombat" || this.Animation == "LeftCombatIdle")
                        {
                            //WeaponBody.Position = Body.Position - new Vector2(ConvertUnits.ToSimUnits(_bodyXRadius), 0);
                            AttackAnimation.Animation = "AttackLeft";
                            LeftWeaponFixture.CollidesWith = Category.All;
                        }
                        else if (this.Animation == "RightCombat" || this.Animation == "RightCombatIdle")
                        {
                            AttackAnimation.Animation = "AttackRight";
                            RightWeaponFixture.CollidesWith = Category.All;
                            //WeaponBody.Position = Body.Position;
                        }
                        else if (this.Animation == "UpCombat" || this.Animation == "UpCombatIdle")
                        {
                            AttackAnimation.Animation = "AttackUp";
                            TopWeaponFixture.CollidesWith = Category.All;
                            //WeaponBody.Position = Body.Position;
                        }
                        else if (this.Animation == "DownCombat" || this.Animation == "DownCombatIdle")
                        {
                            AttackAnimation.Animation = "AttackDown";
                            BottomWeaponFixture.CollidesWith = Category.All;
                            //WeaponBody.Position = Body.Position;
                        }
                    }
                    else
                    {
                        _CastedAbility = GameState.Abilities[10];
                        FindTargets();
                        StartCast();
                    }
                }
                    /*
                else if (!autoAttackMelee && input.IsConfirmActionDown(controllingPlayer))
                {
                    FindTargets();
                    ChooseTarget(this, targets, input, controllingPlayer);
                    if (targets.Count != 0)
                    {
                        _Target = targets[_targetIndex];
                    }
                }
                else if (!autoAttackMelee && IsCasting && input.IsConfirmActionUp(controllingPlayer))
                {
                     if (_castTimer <= 0 && !_Target.IsInvincible)
                    {
                        IsTargeting = false;
                        FinishCast();
                        //_abilityKey = Keys.None;
                    }
                    else if (IsMoving(input, controllingPlayer))
                    {
                        IsTargeting = false;
                        IsCasting = false;
                        _Target._IsSelected = false;
                        targets.Clear();
                        _Target = null;
                        //_abilityKey = Keys.None;
                    }
                }
                     */
                else if (_eventBody != null && IsHeroFacingInteractableEvent() && !GameState.PlayersInCombat && !IsCasting)
                {
                    ((BaseEntity)_eventBody.UserData).InteractWith(screenManager, controllingPlayer);
                }
             }
            else if (input.IsItemUse(controllingPlayer) && GameState.Inventory.items.Count > 0)
            {
                bool itemWasUsed = false;

                foreach (KeyValuePair<ItemEffectType, int> effect in GameState.Inventory.items[SelectedItem].itemEffect)
                {
                    if (GameState.HeroOne.effectedBy(effect.Key, effect.Value) && itemWasUsed == false)
                    {
                        //Console.WriteLine("item used");
                        GameState.Inventory.AddRemoveItem(GameState.Inventory.items[SelectedItem], -1);
                        itemWasUsed = true;

                        if (SelectedItem >= GameState.Inventory.items.Count)
                        {
                            SelectedItem = 0;
                        }
                        else if (SelectedItem < 0)
                        {
                            SelectedItem = GameState.Inventory.items.Count - 1;
                        }
                        if (SelectedItem == -1)
                        {
                            SelectedItem = 0;
                        }
                    }
                }
            }
        }

        private void CheckMovementInput(InputState input, PlayerIndex? controllingPlayer)
        {
            if (input.IsMovementLeft(controllingPlayer) && input.IsMovementDown(controllingPlayer))
            {
                _movementVector = new Vector2(-1, 1);
                _isMovingDiagonally = true;

                if (this.Animation != "Down" && this.Animation != "Left" && !GameState.PlayersInCombat)
                    this.Animation = "Left";
                else if (this.Animation != "DownCombat" && this.Animation != "LeftCombat" && GameState.PlayersInCombat)
                    this.Animation = "LeftCombat";
            }
            else if (input.IsMovementLeft(controllingPlayer) && input.IsMovementUp(controllingPlayer))
            {
                _movementVector = new Vector2(-1, -1);
                _isMovingDiagonally = true;

                if (this.Animation != "Up" && this.Animation != "Left" && !GameState.PlayersInCombat)
                    this.Animation = "Left";
                else if (this.Animation != "UpCombat" && this.Animation != "LeftCombat" && GameState.PlayersInCombat)
                    this.Animation = "LeftCombat";
            }
            else if (input.IsMovementRight(controllingPlayer) && input.IsMovementDown(controllingPlayer))
            {
                _movementVector = new Vector2(1, 1);
                _isMovingDiagonally = true;

                if (this.Animation != "Right" && this.Animation != "Down" && !GameState.PlayersInCombat)
                    this.Animation = "Right";
                else if (this.Animation != "RightCombat" && this.Animation != "DownCombat" && GameState.PlayersInCombat)
                    this.Animation = "RightCombat";
            }
            else if (input.IsMovementRight(controllingPlayer) && input.IsMovementUp(controllingPlayer))
            {
                _movementVector = new Vector2(1, -1);
                _isMovingDiagonally = true;

                if (this.Animation != "Right" && this.Animation != "Up" && !GameState.PlayersInCombat)
                    this.Animation = "Right";
                else if (this.Animation != "RightCombat" && this.Animation != "UpCombat" && GameState.PlayersInCombat)
                    this.Animation = "RightCombat";
            }
            else if (input.IsMovementLeft(controllingPlayer))
            {
                _movementVector.X--;

                if (this.Animation != "Left" && !GameState.PlayersInCombat)
                    this.Animation = "Left";
                else if (this.Animation != "LeftCombat" && GameState.PlayersInCombat)
                    this.Animation = "LeftCombat";
            }
            else if (input.IsMovementRight(controllingPlayer))
            {
                _movementVector.X++;

                if (this.Animation != "Right" && !GameState.PlayersInCombat)
                    this.Animation = "Right";
                else if (this.Animation != "RightCombat" && GameState.PlayersInCombat)
                    this.Animation = "RightCombat";
            }
            else if (input.IsMovementUp(controllingPlayer))
            {
                _movementVector.Y--;

                if (this.Animation != "Up" && !GameState.PlayersInCombat)
                    this.Animation = "Up";
                else if (this.Animation != "UpCombat" && GameState.PlayersInCombat)
                    this.Animation = "UpCombat";
            }
            else if (input.IsMovementDown(controllingPlayer))
            {
                _movementVector.Y++;

                if (this.Animation != "Down" && !GameState.PlayersInCombat)
                    this.Animation = "Down";
                else if (this.Animation != "DownCombat" && GameState.PlayersInCombat)
                    this.Animation = "DownCombat";
            }
            else
            {
                if (!this.Animation.Contains("Idle"))
                    this.Animation += "Idle";
            }

            if(input.IsBumperRight(controllingPlayer))
            {
                SkillSet++;

                if(SkillSet > 2)
                    SkillSet = 0;
            }
            else if (input.IsBumperLeft(controllingPlayer))
            {
                SkillSet--;

                    if(SkillSet < 0)
                    SkillSet = 2;
            }

            if (input.IsTriggerRight(controllingPlayer))
            {
                SelectedItem++;

                if (SelectedItem >= GameState.Inventory.items.Count)
                {
                    SelectedItem = 0;
                }
                else if (SelectedItem < 0)
                {
                    SelectedItem = GameState.Inventory.items.Count - 1;
                }
                if (SelectedItem == -1)
                {
                    SelectedItem = 0;
                }
            }
            else if (input.IsTriggerLeft(controllingPlayer))
            {
                SelectedItem--;

                if (SelectedItem >= GameState.Inventory.items.Count)
                {
                    SelectedItem = 0;
                }
                else if (SelectedItem < 0)
                {
                    SelectedItem = GameState.Inventory.items.Count - 1;
                }
                if (SelectedItem == -1)
                {
                    SelectedItem = 0;
                }
            }
        }

        private bool IsMoving(InputState input, PlayerIndex? controllingPlayer)
        {
            if (input.IsMovementUp(controllingPlayer) ||
                    input.IsMovementRight(controllingPlayer) ||
                    input.IsMovementLeft(controllingPlayer) ||
                    input.IsMovementDown(controllingPlayer))
            {
                return true;
            }
            else
                return false;
        }

        private void CheckAbilityInput(InputState input, PlayerIndex? controllingPlayer)
        {
            //Keys key = PressedBindedKey(input, controllingPlayer);

            Ability checkOne = null;
            Ability checkTwo = null;
            Ability checkThree = null;

            if (SkillSet == 0)
            {
                checkOne = Skill1;
                checkTwo = Skill2;
                checkThree = Skill3;
            }
            else if (SkillSet == 1)
            {
                checkOne = Skill4;
                checkTwo = Skill5;
                checkThree = Skill6;
            }
            else if (SkillSet == 2)
            {
                checkOne = Skill7;
                checkTwo = Skill8;
                checkThree = Skill9;
            }

            if (((input.IsAbilityOneDown(controllingPlayer) && checkOne != null && checkOne.CooldownTimer == 0) || (input.IsAbilityTwoDown(controllingPlayer) && checkTwo != null && checkTwo.CooldownTimer == 0) || (input.IsAbilityThreeDown(controllingPlayer) && checkThree != null && checkThree.CooldownTimer == 0L)) && !IsCasting && !IsMoving(input, controllingPlayer))
            {
                if (input.IsAbilityOne(controllingPlayer))
                {
                    _CastedAbility = checkOne;
                    FindTargets();
                    StartCast();
                }
                else if (input.IsAbilityTwo(controllingPlayer))
                {
                    _CastedAbility = checkTwo;
                    FindTargets();
                    StartCast();
                }
                else if (input.IsAbilityThree(controllingPlayer))
                {
                    _CastedAbility = checkThree;
                    FindTargets();
                    StartCast();
                }
            }
            else if (((input.IsAbilityOneDown(controllingPlayer) && checkOne != null) || (input.IsAbilityTwoDown(controllingPlayer) && checkTwo != null) || (input.IsAbilityThreeDown(controllingPlayer) && checkThree != null) || (!AutoAttackMelee && input.IsConfirmActionDown(controllingPlayer))) && IsCasting)
            {
                if (CastingAnimation.AnimationFinished == true &&
                    !CastingAnimation.Animation.Contains("Casting"))
                {
                    if (SpriteAnimation.Animation.Contains("Left"))
                        CastingAnimation.Animation = "CastingLeft";
                    else if (SpriteAnimation.Animation.Contains("Right"))
                        CastingAnimation.Animation = "CastingRight";
                    else if (SpriteAnimation.Animation.Contains("Up"))
                        CastingAnimation.Animation = "CastingUp";
                    else
                        CastingAnimation.Animation = "CastingDown";
                }
                CastingAnimation.Position = new Vector2(SpriteAnimation.Position.X, SpriteAnimation.Position.Y - 2);

                FindTargets();
                ChooseTarget(this, targets, input, controllingPlayer);
                if (targets.Count != 0)
                {
                    _Target = targets[_targetIndex];
                }
            }
            else if (((input.IsAbilityOneUp(controllingPlayer) && checkOne != null) || (input.IsAbilityTwoUp(controllingPlayer) && checkTwo != null) || (input.IsAbilityThreeUp(controllingPlayer) && checkThree != null) || (!AutoAttackMelee && input.IsConfirmActionUp(controllingPlayer))) && IsCasting)
            {
                if (_castTimer <= 0 && (!_Target.IsInvincible || _CastedAbility.abilityTargetType == TargetType.Ally))
                {
                    IsTargeting = false;
                    if (IsFinishingCast == false)
                        FinishCast();
                    //_abilityKey = Keys.None;
                }
                else if (IsMoving(input, controllingPlayer))
                {
                    IsFinishingCast = false;
                    IsTargeting = false;
                    IsCasting = false;
                    _Target._IsSelected = false;
                    targets.Clear();
                    _Target = null;
                    //_abilityKey = Keys.None;
                }
            }
        }

        private void CheckDebugInput(InputState input, ScreenManager screenManager, PlayerIndex? ControllingPlayer)
        {
            PlayerIndex playerIndex;

            if (input.IsNewKeyPress(Keys.R, null, out playerIndex))
            {
                //IEvent u = new MoveEvent(this, DirectionType.Up, 40);
                //IEvent r = new MoveEvent(this, DirectionType.Right, 40);
                //IEvent d = new MoveEvent(this, DirectionType.Down, 40);
                //IEvent l = new MoveEvent(this, DirectionType.Left, 40);
                //l.IsCancelable = true;
                //u.IsCancelable = true;
                //d.IsCancelable = true;
                //r.IsCancelable = true;
                //EventManager.AddEvent(u);
                //EventManager.AddEvent(r);
                //EventManager.AddEvent(d);
                //EventManager.AddEvent(l);

                //IEvent change = new ChangeMapEvent(40, 40, MapTypes.TestHouse);
                //EventManager.AddEvent(change);

                IEvent mb1 = new MessageBoxEvent("Hello, hero!  Are you ready to begin a journey?");
                IEvent mb2 = new MessageBoxEvent("This will be a daunting task, but I have faith in you.");
                IEvent mb3 = new MessageBoxEvent("Good luck on your quest!");
                EventManager.AddEvent(mb1);
                EventManager.AddEvent(mb2);
                EventManager.AddEvent(mb3);

                EventManager.RunEvents();
            }

            //if (input.CurrentKeyboardStates[0].IsKeyDown(Keys.U))
            //{
            //    GainExperience(Level, 1000);
            //}
            if (input.IsNewKeyPress(Keys.U, ControllingPlayer, out playerIndex))
            {
                GainExperience(Level, 1000);
            }


            /*
            if (input.IsNewKeyPress(Keys.U, null, out playerIndex))
            {
                ScreenManager.AddScreen(new OptionsMenuScreen(), ControllingPlayer);
            }
            */

            /*
            if (input.IsNewKeyPress(Keys.K, null, out playerIndex))
            {
                GameState.SaveGame(DateTime.Now.ToString());
            }

            if (input.IsNewKeyPress(Keys.L, null, out playerIndex))
            {
                GameState.LoadGame(content, GameState.LastPlayedSave);
            }

            if (input.IsNewKeyPress(Keys.Z, null, out playerIndex))
            {
                Skill1 = GameState.Abilities[0];
                Skill2 = GameState.Abilities[2];
                Skill3 = GameState.Abilities[4];
            }
             */

            if (input.IsNewKeyPress(Keys.P, null, out playerIndex))
            {
                foreach(Weapon weapon in GameState.Weapons)
                {
                    if(weapon.itemId >= 1)
                        GameState.Inventory.AddRemoveItem(weapon, 1);
                }
                foreach (Armor armor in GameState.Armors)
                {
                    if(armor.itemId >= 3)
                        GameState.Inventory.AddRemoveItem(armor, 1);
                }

                //Console.WriteLine(GameState.Inventory.SaveOutput());
                
            }
            if (input.IsNewKeyPress(Keys.C, null, out playerIndex))
                GameDebug.ShowCollision = !GameDebug.ShowCollision;

            if (input.IsNewKeyPress(Keys.V, null, out playerIndex))
                CollisionEnabled = !CollisionEnabled;

            if (input.IsNewKeyPress(Keys.Subtract, null, out playerIndex))
            {
                GameState.HeroOne.CurrentHP -= 5;
                GameState.HeroOne.CurrentMP--;
                //GameState.HeroOne.CurrentPersonal--;
                GameState.HeroOne.CurrentHP = (int)MathHelper.Clamp(GameState.HeroOne.CurrentHP, 0, GameState.HeroOne.MaxHP);
                GameState.HeroOne.CurrentMP = (int)MathHelper.Clamp(GameState.HeroOne.CurrentMP, 0, GameState.HeroOne.MaxMP);
            }
            if (input.CurrentKeyboardStates[0].IsKeyDown(Keys.Subtract))
            {
                GameState.HeroOne.CurrentHP -= 10;
                GameState.HeroOne.CurrentMP--;
                //GameState.HeroOne.CurrentPersonal--;
                GameState.HeroOne.CurrentHP = (int)MathHelper.Clamp(GameState.HeroOne.CurrentHP, 0, GameState.HeroOne.MaxHP);
                GameState.HeroOne.CurrentMP = (int)MathHelper.Clamp(GameState.HeroOne.CurrentMP, 0, GameState.HeroOne.MaxMP);
            }

            if (input.IsNewKeyPress(Keys.Add, null, out playerIndex))
            {
                GameState.HeroOne.CurrentHP += 5;
                GameState.HeroOne.CurrentMP++;
                GameState.HeroOne.CurrentHP = (int)MathHelper.Clamp(GameState.HeroOne.CurrentHP, 0, GameState.HeroOne.MaxHP);
                GameState.HeroOne.CurrentMP = (int)MathHelper.Clamp(GameState.HeroOne.CurrentMP, 0, GameState.HeroOne.MaxMP);
            }

            if (input.IsNewKeyPress(Keys.T, null, out playerIndex))
            {
                //screenManager.TraceScreens(); //TESTING LINE

                GameState.ChangeMap("HeroOneStart", MapTypes.DemoMap);
            }

            if (input.IsNewKeyPress(Keys.G, null, out playerIndex))
            {
                //screenManager.TraceScreens(); //TESTING LINE

                GameState.ChangeMap("HeroOneStart", MapTypes.TestHouse);
            }

            if (input.IsNewKeyPress(Keys.X, null, out playerIndex))
            {
                Buffs.Add(new TakeDamageBuff("", -1, .5f, this, this, AbilityElementType.Physical, BuffType.TakeDamage));
                //Buffs.Add(new CoreStatBuff("", 2, .5f, this, this, "Damage"));
                //Buffs.Add(new TakeHealBuff("", 1, 1f, this, this));
            }

            if (input.IsNewKeyPress(Keys.B, null, out playerIndex))
            {
                if (AutoAttackMelee)
                    AutoAttackMelee = false;
                else
                    AutoAttackMelee = true;
            }
        }

        public void GainExperience(int enemyLevel, int enemyExp)
        {
            int levelDifference = enemyLevel - Level;
            if (levelDifference > 10)
                levelDifference = 10;
            else if (levelDifference < -10)
                levelDifference = -10;
            EXP = EXP + (enemyExp + (enemyExp * levelDifference / 10));

            //Console.WriteLine((enemyExp + (enemyExp * levelDifference / 10)));
            if (EXP >= EXP_TO_LEVEL)
            {
                _recentlyLeveled = true;
                Level += 1;
                SkillPoints += 1;
                BaseHP += LevelUpStats[0];
                BaseMP += LevelUpStats[1];
                BaseStr += LevelUpStats[2];
                BaseInt += LevelUpStats[3];
                BaseDex += LevelUpStats[4];
                BaseAla += LevelUpStats[5];
                BaseSta += LevelUpStats[6];
                BaseWis += LevelUpStats[7];
                EXP -= EXP_TO_LEVEL;

                UpdateStats();

                CurrentHP = MaxHP;
                CurrentMP = MaxMP;
            }
        }

        public bool Equip(BaseEquipment equipment)
        {
            BaseEquipment temp = GameState.Armors[0];

            switch (equipment.equipSlot)
            {
                case EquipmentSlotType.Head :
                    temp = ArmorHead;
                    ArmorHead = (Armor)equipment;
                    GameState.Inventory.AddRemoveItem((Armor)equipment, -1);
                    break;
                case EquipmentSlotType.Chest :
                    temp = ArmorChest;
                    ArmorChest = (Armor)equipment;
                    GameState.Inventory.AddRemoveItem((Armor)equipment, -1);
                    break;
                case EquipmentSlotType.Legs :
                    temp = ArmorLegs;
                    ArmorLegs = (Armor)equipment;
                    GameState.Inventory.AddRemoveItem((Armor)equipment, -1);
                    break;
                case EquipmentSlotType.Weapon:
                    temp = Weapon;
                    Weapon = (Weapon)equipment;
                    GameState.Inventory.AddRemoveItem((Weapon)equipment, -1);
                    break;
            }

            foreach (BaseBuff passive in equipment.equipPassives)
            {
                //Console.WriteLine("added");
                BaseBuff add = ApplyBuff(Passives, this, this, passive, 1);
                add.StartBuff(GameState.GameTime);
                equipment.onHero.Add(add);
                UpdateStats();

                
            }

            foreach (BaseBuff passive in temp.onHero)
            {
                //Console.WriteLine("removed");
                //passive.Duration = -1;
                passive.StopBuff(GameState.GameTime);
                Passives.Remove(passive);
            }

            temp.onHero.Clear();

            //Console.WriteLine(Passives.Count());

            if (equipment.equipSlot == EquipmentSlotType.Weapon && temp.itemId > 0)
            {
                GameState.Inventory.AddRemoveItem((Weapon)temp, 1);
            }
            else if (temp.itemId > 2)
            {
                GameState.Inventory.AddRemoveItem((Armor)temp, 1);
            }

            return true;
        }

        /*
        /// <summary>
        /// Checks if a key in _bindings was pressed. Returns the key in context to situation. 
        /// Ex: If you try to start a cast, it'll return the key you pressed. If you are casting, it will only respond to when you press the key for the ability.
        /// </summary>
        /// <param name="input">I have had plenty of input for this bloody thing.</param>
        /// <param name="controllingPlayer">Control Freak.</param>
        /// <returns>Returns the key that was pressed depending on context.</returns>
        private Keys PressedBindedKey(InputState input, PlayerIndex? controllingPlayer)
        {
            PlayerIndex buttonPusher;
            foreach (Keys key in _bindings.Keys)
            {
                if (input.IsNewKeyPress(key, controllingPlayer, out buttonPusher) && !IsCasting)
                {
                    return key;
                }
                else if (input.CurrentKeyboardStates[0].IsKeyDown(key))
                {
                    return key;
                }
                else if (input.CurrentKeyboardStates[0].IsKeyUp(key) && IsCasting)
                {
                    return key;
                }
            }
            return Keys.None;
            
        }
         */ 
        
        /// <summary>
        /// Used to check if hero can cast the ability.
        /// </summary>
        protected override void StartCast()
        {
            if (this.CurrentMP >= _CastedAbility.abilityResourceCost && this.CurrentPersonal >= _CastedAbility.abilityPersonalCost)
            {
                if (targets.Count != 0)
                {
                    if (SpriteAnimation.Animation.Contains("Left"))
                        CastingAnimation.Animation = "BeginCastLeft";
                    else if (SpriteAnimation.Animation.Contains("Right"))
                        CastingAnimation.Animation = "BeginCastRight";
                    else if (SpriteAnimation.Animation.Contains("Up"))
                        CastingAnimation.Animation = "BeginCastUp";
                    else
                        CastingAnimation.Animation = "BeginCastDown";
                    CastingAnimation.Position = new Vector2(SpriteAnimation.Position.X, SpriteAnimation.Position.Y - 2);
                    base.StartCast();
                    _targetIndex = 0;
                    _castTimer = (float)_CastedAbility.abilityCastTime * (1 - (float)Haste / 100);
                    //For display purposes
                    _CastTimeElapsed = 0f;
                    _MaxCastTime = (float)_CastedAbility.abilityCastTime * (1 - (float)Haste / 100);
                }
            }
            else
            {
                EndCast();
                if(this.CurrentMP < _CastedAbility.abilityResourceCost)
                    UnableToCast("Mana");
                if (this.CurrentPersonal < _CastedAbility.abilityPersonalCost)
                    UnableToCast("Light");
            }
        }

        /// <summary>
        /// Makes a list of targets that the selected hero ability can target.
        /// TODO: Needs to take multiple cases.
        /// </summary>
        private void FindTargets()
        {
            targets.Clear();
            switch(_CastedAbility.abilityTargetType)
            {
                case TargetType.Enemy:
                    {
                        foreach (Enemy enemy in GameState.CurrentZone.Enemies)
                        {
                            //Needs to check an alternative like InRange.
                            if (enemy.Selectable)
                            {
                                targets.Add(enemy);
                            }
                        }
                        break;
                    }
                case TargetType.Ally:
                    {
                        targets.Add(GameState.HeroOne);
                        break;
                    }
                case TargetType.None:
                    {
                        targets.Add(this);
                        break;
                    }
            }
        }

        /// <summary>
        /// This is how you move the icon from one entity to another.
        /// </summary>
        /// <param name="caster">The Hero casting the ability.</param>
        /// <param name="targets">The list of possible targets on the screen. Should be both Heros and Enemies.</param>
        /// <param name="input">The button input</param>
        /// <param name="ControllingPlayer">The player controlling this hero.</param>
        protected void ChooseTarget(TargetableEntity caster, List<TargetableEntity> targets, InputState input, PlayerIndex? ControllingPlayer)
        {
                switch (_CastedAbility.abilityProjectileType)
                {
                    case AbilityProjectileType.Self:
                        {
                            if (targets[_targetIndex] != caster)
                                TargetCycle(targets.Count, true);
                            else
                                targets[_targetIndex]._IsSelected = true;
                            break;
                        }
                    case AbilityProjectileType.SelfAoE:
                        {
                            break;
                        }
                    case AbilityProjectileType.SingleTarget:
                        {
                            if (targets.Count == 0)
                            {
                                _targetIndex = 0;
                                break;
                            }
                            else if (targets.Count < _targetIndex)
                                _targetIndex = 0;
                            targets[_targetIndex]._IsSelected = true;
                            if (targets.Count >= 1)
                            {
                                if (input.IsMenuUp(ControllingPlayer) || input.IsMenuRight(ControllingPlayer))
                                {
                                    //Console.WriteLine("up/right");
                                    targets[_targetIndex]._IsSelected = false;
                                    _targetIndex = TargetCycle(targets.Count, true);
                                    targets[_targetIndex]._IsSelected = true;
                                    break;
                                }
                                else if (input.IsMenuDown(ControllingPlayer) || input.IsMenuLeft(ControllingPlayer))
                                {
                                    //Console.WriteLine("down/left");
                                    targets[_targetIndex]._IsSelected = false;
                                    _targetIndex = TargetCycle(targets.Count, false);
                                    targets[_targetIndex]._IsSelected = true;
                                    break;
                                }
                                else
                                    break;
                            }
                            else
                                break;
                        }
                    case AbilityProjectileType.TargetAoE:
                        {
                            if (targets.Count == 0)
                            {
                                _targetIndex = 0;
                                break;
                            }
                            else if (targets.Count < _targetIndex)
                                _targetIndex = 0;
                            targets[_targetIndex]._IsSelected = true;
                            //Need to check nearby targets from selected target. If in ability.abilityRange, it also displays target icon, maybe on and off or something.
                            if (targets.Count >= 1)
                            {
                                if (input.IsMenuUp(ControllingPlayer) || input.IsMenuRight(ControllingPlayer))
                                {
                                    //Console.WriteLine("up/right");
                                    targets[_targetIndex]._IsSelected = false;
                                    _targetIndex = TargetCycle(targets.Count, true);
                                    targets[_targetIndex]._IsSelected = true;
                                    break;
                                }
                                else if (input.IsMenuDown(ControllingPlayer) || input.IsMenuLeft(ControllingPlayer))
                                {
                                    //Console.WriteLine("down/left");
                                    targets[_targetIndex]._IsSelected = false;
                                    _targetIndex = TargetCycle(targets.Count, false);
                                    targets[_targetIndex]._IsSelected = true;
                                    break;
                                }
                                else
                                    break;
                            }
                            else
                                break;
                        }
                }
        }

        /// <summary>
        /// This is called by Choosetarget(). It is to allow target selecting to cycle.
        /// </summary>
        /// <param name="numEnemies">The number of targetable entities of possible targets.</param>
        /// <param name="upistrue">To control which way you are cycling though targets. Up/Right should be true, Left/Down should be false.</param>
        /// <returns>A new index that is used for toggling which target is selected.</returns>
        private int TargetCycle(int numEnemies, bool upistrue)
        {
            int maxIndex = numEnemies - 1;
            if (numEnemies == 1)
                return _targetIndex;
            else if(upistrue)
            {
                if (maxIndex < _targetIndex + 1)
                    return 0;
                else
                    return _targetIndex + 1;
            }
            else if (!upistrue)
            {
                if (_targetIndex - 1 < 0)
                    return maxIndex;
                else
                    return _targetIndex - 1;
            }
            else
            {
                //Console.WriteLine("you are dumb, le cry.");
                return -1;
            }
        }

        /// <summary>
        /// Gets called when hero doesn't have sufficient MP to cast the ability.
        /// </summary>
        protected void UnableToCast(string type)
        {
            //_abilityKey = Keys.None;
            if (type.Equals("Mana"))
            {
                _manaMessageTimer = 1500f;
            }
            if (type.Equals("Light"))
            {
                _lightMessageTimerTimer = 1500f;
            }
            _CastTimeElapsed = 0f;
        }
        /// <summary>
        /// This is to check if an ability is done casting and to trigger the ability effects and remove the targeting icon.
        /// </summary>
        private void FinishCast()
        {
            if (!CastingAnimation.Animation.Contains("Finish"))
            {
                if (SpriteAnimation.Animation.Contains("Left"))
                    CastingAnimation.Animation = "FinishCastLeft";
                else if (SpriteAnimation.Animation.Contains("Right"))
                    CastingAnimation.Animation = "FinishCastRight";
                else if (SpriteAnimation.Animation.Contains("Up"))
                    CastingAnimation.Animation = "FinishCastUp";
                else
                    CastingAnimation.Animation = "FinishCastDown";
            }
            CastingAnimation.Position = new Vector2(SpriteAnimation.Position.X, SpriteAnimation.Position.Y - 2);

            GameState.CurrentZone.DamageParticleEngine.Live = 100;
            GameState.CurrentZone.DamageParticleEngine.Color = HelperMethods.GetColor(_CastedAbility.abilityElement);
            GameState.CurrentZone.DamageParticleEngine.Size = 1;
            GameState.CurrentZone.DamageParticleEngine.EmitterLocation = new Vector2(_Target.Position.X, _Target.Position.Y);

            if (_CastedAbility.Cooldown > 0)
            {
                _CastedAbility.CooldownTimer = _CastedAbility.Cooldown;
                //onCooldown.Add(_CastedAbility);
                //Console.WriteLine("cooldown added test");
            }

            Double check = GameState.RandomNumGenerator.NextDouble();
            float mod = 1;

            if ((MagicCritChance / 100 > check) && _CastedAbility.abilityElement != AbilityElementType.Physical)
            {
                mod += MagicCritDamage / 100f;
                //Console.WriteLine("magic crit");
            }
            else if (DamageCritChance / 100 > check)
            {
                mod += DamageCritDamage / 100f;
                //Console.WriteLine("physical crit");
            }

            if (ResourceType == PersonalResourceType.Shadow)
            {
                if (_CastedAbility.abilityElement == AbilityElementType.Shadow)
                {
                    mod = mod * (1f + (((float)CurrentPersonal - 50f) / 100f));

                    CurrentPersonal -= 10;

                    if (CurrentPersonal < 0)
                    {
                        CurrentPersonal = 0;
                    }
                }
                else if (_CastedAbility.abilityElement == AbilityElementType.Nature || _CastedAbility.abilityElement == AbilityElementType.Fire)
                {
                    mod = mod * (1f + ((-(float)CurrentPersonal + 50f) / 100f));

                    CurrentPersonal += 10;

                    if (CurrentPersonal >100)
                    {
                        CurrentPersonal = 100;
                    }
                }
            }
            
            if (IsCasting && !IsTargeting)
            {
                if (targets.Count == 0)
                {
                    EndCast();
                }
                if (_CastedAbility.abilityTargetType == TargetType.Enemy)
                {
                    if (_CastedAbility.abilityElement != AbilityElementType.Physical)
                    {
                        _Target.TakeDamage((int)(_CastedAbility.abilityDamageFormula * Magic * mod * damageVariance()), _CastedAbility.abilityElement, true, true);
                        //Console.WriteLine("magic");
                        if (_CastedAbility.abilityProjectileType == AbilityProjectileType.TargetAoE && _Target != null)
                        {
                            GameState.CurrentZone.DamageParticleEngine.Size = _CastedAbility.abilityAoERadius / 10;

                            foreach (TargetableEntity target in targets)
                            {
                                if (ApplyAoEDamage(target) && target != _Target)
                                {

                                    target.TakeDamage((int)(_CastedAbility.abilityDamageFormula * Magic * mod * damageVariance()), _CastedAbility.abilityElement, true, true);
                                }
                            }
                        }
                    }
                    else
                    {
                        _Target.TakeDamage((int)(_CastedAbility.abilityDamageFormula * Damage * mod * damageVariance()), _CastedAbility.abilityElement, true, true);
                        //Console.WriteLine("physical");
                        if (_CastedAbility.abilityProjectileType == AbilityProjectileType.TargetAoE && _Target != null)
                        {
                            GameState.CurrentZone.DamageParticleEngine.Size = _CastedAbility.abilityAoERadius / 10;

                            foreach (TargetableEntity target in targets)
                            {
                                if (ApplyAoEDamage(target))
                                {

                                    target.TakeDamage((int)(_CastedAbility.abilityDamageFormula * Damage * mod * damageVariance()), _CastedAbility.abilityElement, true, true);
                                }
                            }
                        }
                    }
                    foreach (BaseBuff debuff in _CastedAbility.Debuffs)
                    {
                        ApplyBuff(_Target.Buffs, _Target, this, debuff, mod);

                        if (_CastedAbility.abilityProjectileType == AbilityProjectileType.TargetAoE && _Target != null)
                        {
                            foreach (TargetableEntity target in targets)
                            {
                                if (ApplyAoEDamage(target))
                                {
                                    //Console.WriteLine("Test");
                                    ApplyBuff(target.Buffs, target, this, debuff, mod);
                                }
                            }
                        }
                    }
                }
                else
                {

                    _Target.TakeHeal((int)(_CastedAbility.abilityDamageFormula * Heal * mod * damageVariance()), true);

                    foreach (BaseBuff buff in _CastedAbility.Buffs)
                    {
                        ApplyBuff(_Target.Buffs, _Target, this, buff, mod);
                    }
                }
            }
        }

        private bool ApplyAoEDamage(TargetableEntity targetableentity)
        {

            if (Math.Abs(targetableentity.Position.X - _Target.Position.X) < _CastedAbility.abilityAoERadius &&
                Math.Abs(targetableentity.Position.Y - _Target.Position.Y) < _CastedAbility.abilityAoERadius)
            {
                return true;
            }
            else 
                return false;
            
        }

        public double[] UpdateStats(BaseEquipment equipment)
        {
            double[] ret = new double[18];


            int TempHP = BaseHP;
            int TempMP = BaseMP;

            int TempDam = BaseDamage;
            int TempMag = BaseMagic;
            int TempHea = BaseHeal;
            int TempDef = BaseDefense;
            int TempRes = BaseResistance;

            int TempStr = BaseStr;
            int TempInt = BaseInt;
            int TempDex = BaseDex;
            int TempAla = BaseAla;
            int TempSta = BaseSta;
            int TempWis = BaseWis;

            switch (equipment.equipSlot)
            {
                case EquipmentSlotType.Weapon:
                    TempHP += equipment.equipHpmod + ArmorHead.equipHpmod + ArmorChest.equipHpmod + ArmorLegs.equipHpmod;
                    TempMP += equipment.equipMpmod + ArmorHead.equipMpmod + ArmorChest.equipMpmod + ArmorLegs.equipMpmod;

                    TempDam += equipment.equipDamageMod + ArmorHead.equipDamageMod + ArmorChest.equipDamageMod + ArmorLegs.equipDamageMod;
                    TempMag += equipment.equipMagicMod + ArmorHead.equipMagicMod + ArmorChest.equipMagicMod + ArmorLegs.equipMagicMod;
                    TempHea += equipment.equipHealMod + ArmorHead.equipHealMod + ArmorChest.equipHealMod + ArmorLegs.equipHealMod;
                    TempDef += equipment.equipDefenseMod + ArmorHead.equipDefenseMod + ArmorChest.equipDefenseMod + ArmorLegs.equipDefenseMod;
                    TempRes += equipment.equipResistanceMod + ArmorHead.equipResistanceMod + ArmorChest.equipResistanceMod + ArmorLegs.equipResistanceMod;

                    TempStr += equipment.equipStrmod + ArmorHead.equipStrmod + ArmorChest.equipStrmod + ArmorLegs.equipStrmod;
                    TempInt += equipment.equipIntmod + ArmorHead.equipIntmod + ArmorChest.equipIntmod + ArmorLegs.equipIntmod;
                    TempDex += equipment.equipDexmod + ArmorHead.equipDexmod + ArmorChest.equipDexmod + ArmorLegs.equipDexmod;
                    TempAla += equipment.equipAlamod + ArmorHead.equipAlamod + ArmorChest.equipAlamod + ArmorLegs.equipAlamod;
                    TempSta += equipment.equipStamod + ArmorHead.equipStamod + ArmorChest.equipStamod + ArmorLegs.equipStamod;
                    TempWis += equipment.equipWismod + ArmorHead.equipWismod + ArmorChest.equipWismod + ArmorLegs.equipWismod;
                    break;

                case EquipmentSlotType.Head:
                    TempHP += Weapon.equipHpmod + equipment.equipHpmod + ArmorChest.equipHpmod + ArmorLegs.equipHpmod;
                    TempMP += Weapon.equipMpmod + equipment.equipMpmod + ArmorChest.equipMpmod + ArmorLegs.equipMpmod;

                    TempDam += Weapon.equipDamageMod + equipment.equipDamageMod + ArmorChest.equipDamageMod + ArmorLegs.equipDamageMod;
                    TempMag += Weapon.equipMagicMod + equipment.equipMagicMod + ArmorChest.equipMagicMod + ArmorLegs.equipMagicMod;
                    TempHea += Weapon.equipHealMod + equipment.equipHealMod + ArmorChest.equipHealMod + ArmorLegs.equipHealMod;
                    TempDef += Weapon.equipDefenseMod + equipment.equipDefenseMod + ArmorChest.equipDefenseMod + ArmorLegs.equipDefenseMod;
                    TempRes += Weapon.equipResistanceMod + equipment.equipResistanceMod + ArmorChest.equipResistanceMod + ArmorLegs.equipResistanceMod;

                    TempStr += Weapon.equipStrmod + equipment.equipStrmod + ArmorChest.equipStrmod + ArmorLegs.equipStrmod;
                    TempInt += Weapon.equipIntmod + equipment.equipIntmod + ArmorChest.equipIntmod + ArmorLegs.equipIntmod;
                    TempDex += Weapon.equipDexmod + equipment.equipDexmod + ArmorChest.equipDexmod + ArmorLegs.equipDexmod;
                    TempAla += Weapon.equipAlamod + equipment.equipAlamod + ArmorChest.equipAlamod + ArmorLegs.equipAlamod;
                    TempSta += Weapon.equipStamod + equipment.equipStamod + ArmorChest.equipStamod + ArmorLegs.equipStamod;
                    TempWis += Weapon.equipWismod + equipment.equipWismod + ArmorChest.equipWismod + ArmorLegs.equipWismod;
                    break;

                case EquipmentSlotType.Chest:
                    TempHP += Weapon.equipHpmod + ArmorHead.equipHpmod + equipment.equipHpmod + ArmorLegs.equipHpmod;
                    TempMP += Weapon.equipMpmod + ArmorHead.equipMpmod + equipment.equipMpmod + ArmorLegs.equipMpmod;

                    TempDam += Weapon.equipDamageMod + ArmorHead.equipDamageMod + equipment.equipDamageMod + ArmorLegs.equipDamageMod;
                    TempMag += Weapon.equipMagicMod + ArmorHead.equipMagicMod + equipment.equipMagicMod + ArmorLegs.equipMagicMod;
                    TempHea += Weapon.equipHealMod + ArmorHead.equipHealMod + equipment.equipHealMod + ArmorLegs.equipHealMod;
                    TempDef += Weapon.equipDefenseMod + ArmorHead.equipDefenseMod + equipment.equipDefenseMod + ArmorLegs.equipDefenseMod;
                    TempRes += Weapon.equipResistanceMod + ArmorHead.equipResistanceMod + equipment.equipResistanceMod + ArmorLegs.equipResistanceMod;

                    TempStr += Weapon.equipStrmod + ArmorHead.equipStrmod + equipment.equipStrmod + ArmorLegs.equipStrmod;
                    TempInt += Weapon.equipIntmod + ArmorHead.equipIntmod + equipment.equipIntmod + ArmorLegs.equipIntmod;
                    TempDex += Weapon.equipDexmod + ArmorHead.equipDexmod + equipment.equipDexmod + ArmorLegs.equipDexmod;
                    TempAla += Weapon.equipAlamod + ArmorHead.equipAlamod + equipment.equipAlamod + ArmorLegs.equipAlamod;
                    TempSta += Weapon.equipStamod + ArmorHead.equipStamod + equipment.equipStamod + ArmorLegs.equipStamod;
                    TempWis += Weapon.equipWismod + ArmorHead.equipWismod + equipment.equipWismod + ArmorLegs.equipWismod;
                    break;

                case EquipmentSlotType.Legs:
                    TempHP += Weapon.equipHpmod + ArmorHead.equipHpmod + ArmorChest.equipHpmod + equipment.equipHpmod;
                    TempMP += Weapon.equipMpmod + ArmorHead.equipMpmod + ArmorChest.equipMpmod + equipment.equipMpmod;

                    TempDam += Weapon.equipDamageMod + ArmorHead.equipDamageMod + ArmorChest.equipDamageMod + equipment.equipDamageMod;
                    TempMag += Weapon.equipMagicMod + ArmorHead.equipMagicMod + ArmorChest.equipMagicMod + equipment.equipMagicMod;
                    TempHea += Weapon.equipHealMod + ArmorHead.equipHealMod + ArmorChest.equipHealMod + equipment.equipHealMod;
                    TempDef += Weapon.equipDefenseMod + ArmorHead.equipDefenseMod + ArmorChest.equipDefenseMod + equipment.equipDefenseMod;
                    TempRes += Weapon.equipResistanceMod + ArmorHead.equipResistanceMod + ArmorChest.equipResistanceMod + equipment.equipResistanceMod;

                    TempStr += Weapon.equipStrmod + ArmorHead.equipStrmod + ArmorChest.equipStrmod + equipment.equipStrmod;
                    TempInt += Weapon.equipIntmod + ArmorHead.equipIntmod + ArmorChest.equipIntmod + equipment.equipIntmod;
                    TempDex += Weapon.equipDexmod + ArmorHead.equipDexmod + ArmorChest.equipDexmod + equipment.equipDexmod;
                    TempAla += Weapon.equipAlamod + ArmorHead.equipAlamod + ArmorChest.equipAlamod + equipment.equipAlamod;
                    TempSta += Weapon.equipStamod + ArmorHead.equipStamod + ArmorChest.equipStamod + equipment.equipStamod;
                    TempWis += Weapon.equipWismod + ArmorHead.equipWismod + ArmorChest.equipWismod + equipment.equipWismod;
                    break;

            }

            TempStr = (int)(TempStr * StrMod);
            TempInt = (int)(TempInt * IntMod);
            TempDex = (int)(TempDex * DexMod);
            TempAla = (int)(TempAla * AlaMod);
            TempSta = (int)(TempSta * StaMod);
            TempWis = (int)(TempWis * WisMod);

            /*
            if (TempHP < 0)
                TempHP = 0;

            if (TempMP < 0)
                TempMP = 0;

            if (TempDam < 0)
                TempDam = 0;

            if (TempMag < 0)
                TempMag = 0;

            if (TempHea < 0)
                TempHea = 0;

            if (TempDef < 0)
                TempMP = 0;

            if (TempRes < 0)
                TempRes = 0;
             */

            if (TempStr < 0)
                TempStr = 0;
            ret[6] = TempStr;

            if (TempInt < 0)
                TempInt = 0;
            ret[15] = TempInt;

            if (TempDex < 0)
                TempDex = 0;
            ret[7] = TempDex;

            if (TempAla < 0)
                TempAla = 0;
            ret[16] = TempAla;

            if (TempSta < 0)
                TempSta = 0;
            ret[8] = TempSta;

            if (TempWis < 0)
                TempWis = 0;
            ret[17] = TempWis;

            ret[0] =
                (int)
                (
                    (
                        TempHP * HPForm[0] +
                        TempMP * HPForm[1] +
                        TempStr * HPForm[2] +
                        TempInt * HPForm[3] +
                        TempDex * HPForm[4] +
                        TempAla * HPForm[5] +
                        TempSta * HPForm[6] +
                        TempWis * HPForm[7]
                    ) * HPMod
                );

            ret[9] =
                (int)
                (
                    (
                        TempHP * MPForm[0] +
                        TempMP * MPForm[1] +
                        TempStr * MPForm[2] +
                        TempInt * MPForm[3] +
                        TempDex * MPForm[4] +
                        TempAla * MPForm[5] +
                        TempSta * MPForm[6] +
                        TempWis * MPForm[7]
                    ) * MPMod
                );

            ret[2] =
                (int)
                (
                    (
                        TempHP * DamageForm[0] +
                        TempMP * DamageForm[1] +
                        TempStr * DamageForm[2] +
                        TempInt * DamageForm[3] +
                        TempDex * DamageForm[4] +
                        TempAla * DamageForm[5] +
                        TempSta * DamageForm[6] +
                        TempWis * DamageForm[7] +
                        TempDam
                    ) * ModDam
                );

            ret[11] =
                (int)
                (
                    (
                        TempHP * MagicForm[0] +
                        TempMP * MagicForm[1] +
                        TempStr * MagicForm[2] +
                        TempInt * MagicForm[3] +
                        TempDex * MagicForm[4] +
                        TempAla * MagicForm[5] +
                        TempSta * MagicForm[6] +
                        TempWis * MagicForm[7] +
                        TempMag
                    ) * ModMag
                );

            ret[5] =
                (int)
                (
                    (
                        TempHP * HealForm[0] +
                        TempMP * HealForm[1] +
                        TempStr * HealForm[2] +
                        TempInt * HealForm[3] +
                        TempDex * HealForm[4] +
                        TempAla * HealForm[5] +
                        TempSta * HealForm[6] +
                        TempWis * HealForm[7] +
                        TempHea
                    ) * ModHea
                );

            ret[4] =
                (int)
                (
                    (
                        TempHP * DefenseForm[0] +
                        TempMP * DefenseForm[1] +
                        TempStr * DefenseForm[2] +
                        TempInt * DefenseForm[3] +
                        TempDex * DefenseForm[4] +
                        TempAla * DefenseForm[5] +
                        TempSta * DefenseForm[6] +
                        TempWis * DefenseForm[7] +
                        TempDef
                    ) * ModDef
                );

            ret[13] =
                (int)
                (
                    (
                        TempHP * ResistanceForm[0] +
                        TempMP * ResistanceForm[1] +
                        TempStr * ResistanceForm[2] +
                        TempInt * ResistanceForm[3] +
                        TempDex * ResistanceForm[4] +
                        TempAla * ResistanceForm[5] +
                        TempSta * ResistanceForm[6] +
                        TempWis * ResistanceForm[7] +
                        TempRes
                    ) * ModRes
                );

            ret[3] =
                (
                    TempHP * DamageCritChanceForm[0] +
                    TempMP * DamageCritChanceForm[1] +
                    TempStr * DamageCritChanceForm[2] +
                    TempInt * DamageCritChanceForm[3] +
                    TempDex * DamageCritChanceForm[4] +
                    TempAla * DamageCritChanceForm[5] +
                    TempSta * DamageCritChanceForm[6] +
                    TempWis * DamageCritChanceForm[7]
                ) / (1f + (Level / 10f));
            ret[3] = ret[3] - (ret[3] % .1f);

            ret[12] =
                (
                    TempHP * MagicCritChanceForm[0] +
                    TempMP * MagicCritChanceForm[1] +
                    TempStr * MagicCritChanceForm[2] +
                    TempInt * MagicCritChanceForm[3] +
                    TempDex * MagicCritChanceForm[4] +
                    TempAla * MagicCritChanceForm[5] +
                    TempSta * MagicCritChanceForm[6] +
                    TempWis * MagicCritChanceForm[7]
                ) / (1f + (Level / 10f));
            ret[12] = ret[12] - (ret[12] % .1f);

            ret[14] =
                (
                    TempHP * HasteForm[0] +
                    TempMP * HasteForm[1] +
                    TempStr * HasteForm[2] +
                    TempInt * HasteForm[3] +
                    TempDex * HasteForm[4] +
                    TempAla * HasteForm[5] +
                    TempSta * HasteForm[6] +
                    TempWis * HasteForm[7]
                ) / (1f + (Level / 10f));
            ret[14] = ret[14] - (ret[14] % .1f);

            ret[1] =
                (
                    (
                        TempHP * HPRegenForm[0] +
                        TempMP * HPRegenForm[1] +
                        TempStr * HPRegenForm[2] +
                        TempInt * HPRegenForm[3] +
                        TempDex * HPRegenForm[4] +
                        TempAla * HPRegenForm[5] +
                        TempSta * HPRegenForm[6] +
                        TempWis * HPRegenForm[7]
                    ) * HPRegenMod
                );
            ret[1] = ret[1] - (ret[1] % .1f);

            ret[10] =
                (
                    (
                        TempHP * MPRegenForm[0] +
                        TempMP * MPRegenForm[1] +
                        TempStr * MPRegenForm[2] +
                        TempInt * MPRegenForm[3] +
                        TempDex * MPRegenForm[4] +
                        TempAla * MPRegenForm[5] +
                        TempSta * MPRegenForm[6] +
                        TempWis * MPRegenForm[7]
                    ) * MPRegenMod
                );
            ret[10] = ret[10] - (ret[10] % .1f);

            return ret;
        }

        public override void UpdateStats()
        {
            int TempHP  = BaseHP;
            int TempMP  = BaseMP;

            int TempDam = BaseDamage;
            int TempMag = BaseMagic;
            int TempHea = BaseHeal;
            int TempDef = BaseDefense;
            int TempRes = BaseResistance;

            int TempStr = BaseStr;
            int TempInt = BaseInt;
            int TempDex = BaseDex;
            int TempAla = BaseAla;
            int TempSta = BaseSta;
            int TempWis = BaseWis;


            TempHP += Weapon.equipHpmod + ArmorHead.equipHpmod + ArmorChest.equipHpmod + ArmorLegs.equipHpmod;
            TempMP += Weapon.equipMpmod + ArmorHead.equipMpmod + ArmorChest.equipMpmod + ArmorLegs.equipMpmod;

            TempDam += Weapon.equipDamageMod + ArmorHead.equipDamageMod + ArmorChest.equipDamageMod + ArmorLegs.equipDamageMod;
            TempMag += Weapon.equipMagicMod + ArmorHead.equipMagicMod + ArmorChest.equipMagicMod + ArmorLegs.equipMagicMod;
            TempHea += Weapon.equipHealMod + ArmorHead.equipHealMod + ArmorChest.equipHealMod + ArmorLegs.equipHealMod;
            TempDef += Weapon.equipDefenseMod + ArmorHead.equipDefenseMod + ArmorChest.equipDefenseMod + ArmorLegs.equipDefenseMod;
            TempRes += Weapon.equipResistanceMod + ArmorHead.equipResistanceMod + ArmorChest.equipResistanceMod + ArmorLegs.equipResistanceMod;

            TempStr += Weapon.equipStrmod + ArmorHead.equipStrmod + ArmorChest.equipStrmod + ArmorLegs.equipStrmod;
            TempInt += Weapon.equipIntmod + ArmorHead.equipIntmod + ArmorChest.equipIntmod + ArmorLegs.equipIntmod;
            TempDex += Weapon.equipDexmod + ArmorHead.equipDexmod + ArmorChest.equipDexmod + ArmorLegs.equipDexmod;
            TempAla += Weapon.equipAlamod + ArmorHead.equipAlamod + ArmorChest.equipAlamod + ArmorLegs.equipAlamod;
            TempSta += Weapon.equipStamod + ArmorHead.equipStamod + ArmorChest.equipStamod + ArmorLegs.equipStamod;
            TempWis += Weapon.equipWismod + ArmorHead.equipWismod + ArmorChest.equipWismod + ArmorLegs.equipWismod;

            TempStr = (int)(TempStr * StrMod);
            TempInt = (int)(TempInt * IntMod);
            TempDex = (int)(TempDex * DexMod);
            TempAla = (int)(TempAla * AlaMod);
            TempSta = (int)(TempSta * StaMod);
            TempWis = (int)(TempWis * WisMod);

            
            /*
            if (TempHP < 0)
                TempHP = 0;

            if (TempMP < 0)
                TempMP = 0;

            if (TempDam < 0)
                TempDam = 0;

            if (TempMag < 0)
                TempMag = 0;

            if (TempHea < 0)
                TempHea = 0;

            if (TempDef < 0)
                TempMP = 0;

            if (TempRes < 0)
                TempRes = 0;
             */
            
            if (TempStr < 0)
                TempStr = 0;
            TotalStr = TempStr;

            if (TempInt < 0)
                TempInt = 0;
            TotalInt = TempInt;

            if (TempDex < 0)
                TempDex = 0;
            TotalDex = TempDex;

            if (TempAla < 0)
                TempAla = 0;
            TotalAla = TempAla;

            if (TempSta < 0)
                TempSta = 0;
            TotalSta = TempSta;

            if (TempWis < 0)
                TempWis = 0;
            TotalWis = TempWis;

            MaxHP =
                (int)
                (
                    (
                        TempHP  * HPForm[0] +
                        TempMP  * HPForm[1] +
                        TempStr * HPForm[2] +
                        TempInt * HPForm[3] +
                        TempDex * HPForm[4] +
                        TempAla * HPForm[5] +
                        TempSta * HPForm[6] +
                        TempWis * HPForm[7]
                    ) * HPMod
                );

            MaxMP =
                (int)
                (
                    (
                        TempHP  * MPForm[0] +
                        TempMP  * MPForm[1] +
                        TempStr * MPForm[2] +
                        TempInt * MPForm[3] +
                        TempDex * MPForm[4] +
                        TempAla * MPForm[5] +
                        TempSta * MPForm[6] +
                        TempWis * MPForm[7]
                    ) * MPMod
                );

            Damage =
                (int)
                (
                    (
                        TempHP * DamageForm[0] +
                        TempMP * DamageForm[1] +
                        TempStr * DamageForm[2] +
                        TempInt * DamageForm[3] +
                        TempDex * DamageForm[4] +
                        TempAla * DamageForm[5] +
                        TempSta * DamageForm[6] +
                        TempWis * DamageForm[7] +
                        TempDam
                    ) * ModDam
                );

            Magic =
                (int)
                (
                    (
                        TempHP * MagicForm[0] +
                        TempMP * MagicForm[1] +
                        TempStr * MagicForm[2] +
                        TempInt * MagicForm[3] +
                        TempDex * MagicForm[4] +
                        TempAla * MagicForm[5] +
                        TempSta * MagicForm[6] +
                        TempWis * MagicForm[7] +
                        TempMag
                    ) * ModMag
                );

            Heal =
                (int)
                (
                    (
                        TempHP * HealForm[0] +
                        TempMP * HealForm[1] +
                        TempStr * HealForm[2] +
                        TempInt * HealForm[3] +
                        TempDex * HealForm[4] +
                        TempAla * HealForm[5] +
                        TempSta * HealForm[6] +
                        TempWis * HealForm[7] +
                        TempHea
                    ) * ModHea
                );

            Defense =
                (int)
                (
                    (
                        TempHP * DefenseForm[0] +
                        TempMP * DefenseForm[1] +
                        TempStr * DefenseForm[2] +
                        TempInt * DefenseForm[3] +
                        TempDex * DefenseForm[4] +
                        TempAla * DefenseForm[5] +
                        TempSta * DefenseForm[6] +
                        TempWis * DefenseForm[7] +
                        TempDef
                    ) * ModDef
                );

            Resistance =
                (int)
                (
                    (
                        TempHP * ResistanceForm[0] +
                        TempMP * ResistanceForm[1] +
                        TempStr * ResistanceForm[2] +
                        TempInt * ResistanceForm[3] +
                        TempDex * ResistanceForm[4] +
                        TempAla * ResistanceForm[5] +
                        TempSta * ResistanceForm[6] +
                        TempWis * ResistanceForm[7] +
                        TempRes
                    ) * ModRes
                );

            DamageCritChance =
                (
                    TempHP * DamageCritChanceForm[0] +
                    TempMP * DamageCritChanceForm[1] +
                    TempStr * DamageCritChanceForm[2] +
                    TempInt * DamageCritChanceForm[3] +
                    TempDex * DamageCritChanceForm[4] +
                    TempAla * DamageCritChanceForm[5] +
                    TempSta * DamageCritChanceForm[6] +
                    TempWis * DamageCritChanceForm[7]
                ) / (1f + (Level / 10f));
            DamageCritChance = DamageCritChance - (DamageCritChance % .1f);

            MagicCritChance =
                (
                    TempHP * MagicCritChanceForm[0] +
                    TempMP * MagicCritChanceForm[1] +
                    TempStr * MagicCritChanceForm[2] +
                    TempInt * MagicCritChanceForm[3] +
                    TempDex * MagicCritChanceForm[4] +
                    TempAla * MagicCritChanceForm[5] +
                    TempSta * MagicCritChanceForm[6] +
                    TempWis * MagicCritChanceForm[7]
                ) / (1f + (Level / 10f));
            MagicCritChance = MagicCritChance - (MagicCritChance % .1f);

            Haste =
                (
                    TempHP * HasteForm[0] +
                    TempMP * HasteForm[1] +
                    TempStr * HasteForm[2] +
                    TempInt * HasteForm[3] +
                    TempDex * HasteForm[4] +
                    TempAla * HasteForm[5] +
                    TempSta * HasteForm[6] +
                    TempWis * HasteForm[7]
                ) / (1f + (Level / 10f));
            Haste = Haste - (Haste % .1f);

            HPRegen =
                (
                    (
                        TempHP * HPRegenForm[0] +
                        TempMP * HPRegenForm[1] +
                        TempStr * HPRegenForm[2] +
                        TempInt * HPRegenForm[3] +
                        TempDex * HPRegenForm[4] +
                        TempAla * HPRegenForm[5] +
                        TempSta * HPRegenForm[6] +
                        TempWis * HPRegenForm[7]
                    ) * HPRegenMod
                );
            HPRegen = HPRegen - (HPRegen % .1f);

            MPRegen =
                (
                    (
                        TempHP * MPRegenForm[0] +
                        TempMP * MPRegenForm[1] +
                        TempStr * MPRegenForm[2] +
                        TempInt * MPRegenForm[3] +
                        TempDex * MPRegenForm[4] +
                        TempAla * MPRegenForm[5] +
                        TempSta * MPRegenForm[6] +
                        TempWis * MPRegenForm[7]
                    ) * MPRegenMod
                );
            MPRegen = MPRegen - (MPRegen % .1f);

            DefenseRating = (int)(Defense / (1f + (Level / 10f)));
            ResistanceRating = (int)(Resistance / (1f + (Level / 10f)));
            
            if (CurrentHP > MaxHP)
                CurrentHP = MaxHP;
            if (CurrentMP > MaxMP)
                CurrentMP = MaxMP;
        }

    }
}
