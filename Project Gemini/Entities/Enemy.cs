﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectGemini.Graphics_Utilities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using FarseerPhysics.Dynamics;
using ProjectGemini.Static_Classes;
using FarseerPhysics.Factories;
using ProjectGemini.Utilities;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Dynamics.Contacts;
using ProjectGemini.EnumTypes;
using ProjectGemini.AI_Routines;
using ProjectGemini.GUI;
using ProjectGemini.Message_Boxes;
using ProjectGemini.Managers;
using ParticleEngine2D;

namespace ProjectGemini.Entities
{
    public class Enemy : TargetableEntity
    {
        private float _invincibleTime = 500f;
        private float _invincibilityTimer;
        //public bool IsAttacking = false;
        public bool HeroWithinRange = false;
        private float _attackTime = 250f;
        private float _attackTimer = 250f;

        private float _castTimer;
        public float _CastTimeElapsed;
        public float _MaxCastTime;
        //private int _speed;
        public EnemyHUD enemyHUD;

        public bool IsActive { get; set; }
        public AI AIRoutine { get; set; }

        public int enemyId { get; private set; }
        public string enemyName { get; private set; }
        public string enemySpriteLocation { get; private set; }
        public int enemyLevel { get; private set; }
        public int enemyExp { get; private set; }
        //public float enemySpeed { get; private set; }
        public EnemyAIType enemyAI { get; private set; }
        public string enemyAnimation { get; private set; }
        public Int64 enemyCurrency { get; private set; }
        public List<Ability> enemyAbilities { get; private set; }
        public Dictionary<BaseItem, float> enemyDropList { get; private set; }
        public List<string> enemyPassives { get; private set; }
        public Texture2D texture;

        public override string ToString()
        {
            return enemyId + " " +
                enemyName + " " +
                enemyLevel + " " +
                enemyExp + " " +
                MaxHP + " " +
                DefenseRating + " " +
                ResistanceRating + " " +
                BaseSpeed + " " +
                Damage + " " +
                Magic + " " +
                enemyCurrency + " " +
                enemyAI;
        }

        public Fixture MeleeRangeSensor { get; set; }
        public Fixture LeftDamageFixture { get; set; }
        public Fixture RightDamageFixture { get; set; }
        public Fixture TopDamageFixture { get; set; }
        public Fixture BottomDamageFixture { get; set; }

        /// <summary>
        /// Creates an enemy with stats.
        /// </summary>
        /// <param name="id">The unique id for the enemy.</param>
        /// <param name="name">The name of the enemy.</param>
        /// <param name="spriteLocation">The location of the enemy sprite.</param>
        /// <param name="level">The level of the enemy.</param>
        /// <param name="exp">The amount of xp the enemy gives when defeated.</param>
        /// <param name="hp">The max amount of hp the enemy can have.</param>
        /// <param name="defense">The amount of defense the enemy has.</param>
        /// <param name="resistance">The amount of resistance the enemy has.</param>
        /// <param name="speed">The movement speed of the enemy.</param>
        /// <param name="damage">The base damage the enemy can do.</param>
        /// <param name="magic">The base magic damage the enemy can do.</param>
        /// <param name="ai">How smart the enemy acts.</param>
        /// <param name="animation">The location of the file which holds the enemy's animation.</param>
        /// <param name="abilities">The list of abilities the enemy knows.</param>
        /// <param name="dropList">The lits of items the enemy holds.</param>
        /// <param name="passives">The list of buffs/debuffs the enemy has.</param>
        public Enemy(int id, 
            string name, 
            string spriteLocation, 
            int level, 
            int exp, 
            int hp, 
            int defense, 
            int resistance, 
            int speed, 
            int damage, 
            int magic,
            EnemyAIType ai,
            string animation,
            Int64 currency,
            List<Ability> abilities, 
            Dictionary<BaseItem, float> dropList, 
            List<string> passives)
        {
            enemyId = id;
            enemyName = name;
            enemySpriteLocation = spriteLocation;
            enemyLevel = level;
            enemyExp = exp;
            CurrentHP = hp;
            MaxHP = hp;
            DefenseRating = defense;
            ResistanceRating = resistance;
            BaseSpeed = speed;
            Damage = damage;
            Magic = magic;
            enemyAI = ai;
            enemyAnimation = animation;
            enemyCurrency = currency;
            enemyAbilities = abilities;
            enemyDropList = dropList;
            enemyPassives = passives;

            BaseDamage = damage;
            BaseMagic = magic;
            //BaseHeal = Heal;
            BaseDefense = defense;
            BaseResistance = resistance;
        }

        public Enemy(Enemy enemy, Texture2D texture, Texture2D healthbar, SpriteFont font)
        {
            enemyId = enemy.enemyId;
            enemyName = enemy.enemyName;
            enemySpriteLocation = enemy.enemySpriteLocation;
            enemyLevel = enemy.enemyLevel;
            enemyExp = enemy.enemyExp;
            CurrentHP = enemy.CurrentHP;
            MaxHP = enemy.MaxHP;
            DefenseRating = enemy.DefenseRating;
            ResistanceRating = enemy.ResistanceRating;
            BaseSpeed = enemy.BaseSpeed;
            Damage = enemy.Damage;
            Magic = enemy.Magic;
            enemyAI = enemy.enemyAI;
            enemyAnimation = enemy.enemyAnimation;
            enemyCurrency = enemy.enemyCurrency;
            enemyAbilities = enemy.enemyAbilities;
            enemyDropList = enemy.enemyDropList;
            enemyPassives = enemy.enemyPassives;
            IsAttacking = false;

            AIRoutine = AI.GetAIInstance(enemyAI);
            _invincibilityTimer = _invincibleTime;
            MovementSpeed = 1;
            //_speed = MovementSpeed;
            enemyHUD = new EnemyHUD(font, this, healthbar, healthbar);

            if (enemyName == "Slime" || enemyName == "Slime+" || enemyName == "Slime++" || enemyName == "KingSlime" || enemyName == "TargetSlime" || enemyName == "CasterSlime")
            {
                SpriteAnimation = new SpriteAnimation(texture, 8, 3);
                SetUpAnimations();
                //Console.WriteLine(texture.Width);
                //Console.WriteLine(texture.Height);
            }
            else
                SpriteAnimation = new SpriteAnimation(texture);

            this.texture = texture;

            Bounds.Width = SpriteAnimation.Width;
            Bounds.Height = SpriteAnimation.Height;

            BaseDamage = enemy.BaseDamage;
            BaseMagic = enemy.BaseMagic;
            //BaseHeal = Heal;
            BaseDefense = enemy.BaseDefense;
            BaseResistance = enemy.BaseResistance;

            CreateEnemyBody();
        }

        public Enemy(Texture2D texture, int frames, int animations)
        {
            MovementSpeed = 1;

            SpriteAnimation = new SpriteAnimation(texture);
            SetUpAnimations();
            Bounds.Width = SpriteAnimation.Width;
            Bounds.Height = SpriteAnimation.Height;

            CreateEnemyBody();
        }

        public override void Update(GameTime gameTime)
        {
            if (AIRoutine is BasicMeleeAI || AIRoutine is BasicCasterAI)
            {
                    AIRoutine.Update(this, GameState.HeroOne);
            }

            if (Animation != "static")
            {
                if (Body.LinearVelocity.X > 0 && Math.Abs(Body.LinearVelocity.X) > Math.Abs(Body.LinearVelocity.Y) && Animation != "Right")
                    SpriteAnimation.AnimationWithRetainedFrameIndex = "Right";
                else if (Body.LinearVelocity.X < 0 && Math.Abs(Body.LinearVelocity.X) > Math.Abs(Body.LinearVelocity.Y) && Animation != "Left")
                    SpriteAnimation.AnimationWithRetainedFrameIndex = "Left";
                else if (Body.LinearVelocity.Y > 0 && Math.Abs(Body.LinearVelocity.Y) > Math.Abs(Body.LinearVelocity.X) && Animation != "Down")
                    SpriteAnimation.AnimationWithRetainedFrameIndex = "Down";
                else if (Body.LinearVelocity.Y < 0 && Math.Abs(Body.LinearVelocity.Y) > Math.Abs(Body.LinearVelocity.X) && Animation != "Up")
                    SpriteAnimation.AnimationWithRetainedFrameIndex = "Up";
                else if (Body.LinearVelocity == Vector2.Zero)
                {
                    if (!Animation.Contains("Idle"))
                        Animation += "Idle";
                }
            }

            if (HeroWithinRange)
            {
                IsAttacking = true;
                LeftDamageFixture.CollidesWith = Category.All;
                RightDamageFixture.CollidesWith = Category.All;
                TopDamageFixture.CollidesWith = Category.All;
                BottomDamageFixture.CollidesWith = Category.All;
            }
            if (IsInvincible)
            {
                _invincibilityTimer -= (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                if (_DisplayDamageText != null)
                {
                    foreach (DisplayAbilityText text in _DisplayDamageText)
                    {
                        text.Update(gameTime);
                    }
                }
                //Console.WriteLine(_invincibilityTimer);
            }
            if (_invincibilityTimer <= 0)
            {
                IsInvincible = false;
                _invincibilityTimer = _invincibleTime;
            }
            if (IsCasting)
            {
                _castTimer -= (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                if (_CastTimeElapsed < _MaxCastTime)
                    _CastTimeElapsed += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                if (_castTimer <= 0)
                {
                    // spell animation_CastedAbility
                    this.CurrentMP -= _CastedAbility.abilityResourceCost;
                    FinishCast();
                }
            }
            if (IsAttacking)
            {
                _attackTimer -= (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                //Console.WriteLine(_weaponSwingTimer);
            }
            if (_attackTimer <= 0)
            {
                _attackTimer = _attackTime;
                IsAttacking = false;
                //WeaponBody.CollidesWith = Category.None;

                LeftDamageFixture.CollidesWith = Category.None;
                RightDamageFixture.CollidesWith = Category.None;
                TopDamageFixture.CollidesWith = Category.None;
                BottomDamageFixture.CollidesWith = Category.None;
            }

            Vector2 v = ConvertUnits.ToDisplayUnits(
                       new Vector2(this.Body.Position.X,
                       this.Body.Position.Y - ConvertUnits.ToSimUnits(SpriteAnimation.Height / 4)));
            this.Position = new Vector2((int)v.X, (int)v.Y);


            base.Update(gameTime);
        }

        public void StartCast(Hero hero)
        {
            if (CurrentMP >= enemyAbilities[0].abilityResourceCost)
            {
                _CastedAbility = enemyAbilities[0];
                _Target = hero;
                _castTimer = (float)_CastedAbility.abilityCastTime;
                _CastTimeElapsed = 0f;
                _MaxCastTime = (float)_CastedAbility.abilityCastTime;
                base.StartCast();
            }
        }

        private void FinishCast()
        {
            if (IsCasting)
            {
                if (_CastedAbility.abilityTargetType == TargetType.Enemy)
                {
                    _Target.TakeDamage((int)(_CastedAbility.abilityDamageFormula * Magic * damageVariance()), _CastedAbility.abilityElement, true, true);

                    foreach (BaseBuff debuff in _CastedAbility.Debuffs)
                    {
                        ApplyBuff(_Target.Buffs, _Target, this, debuff, 1);
                    }
                }
                else
                {
                    _Target.TakeHeal((int)(_CastedAbility.abilityDamageFormula * Heal * damageVariance()), true);

                    foreach (BaseBuff debuff in _CastedAbility.Debuffs)
                    {
                        ApplyBuff(_Target.Buffs, _Target, this, debuff, 1);
                    }
                }
                //_Target._IsSelected = false;
                //_Target = null;
                EndCast();
            }
        }
        
        public override void Draw(SpriteBatch spriteBatch)
        {
            if ((int)_invincibilityTimer % 4f == 0) // Causes a flashing animation to indicate invincibility.
            {
                if (enemyName.Equals("Slime+"))
                {
                    base.Draw(spriteBatch, Color.LightBlue);
                }
                else if (enemyName.Equals("Slime++"))
                {
                    base.Draw(spriteBatch, Color.Yellow);
                }
                else if (enemyName.Equals("KingSlime"))
                {
                    base.Draw(spriteBatch, Color.Gray, 2.7f);
                }
                else if (enemyName.Equals("TargetSlime"))
                {
                    base.Draw(spriteBatch, Color.Brown);
                }
                else if (enemyName.Equals("CasterSlime"))
                {
                    base.Draw(spriteBatch, Color.LightGreen);
                }
                else
                {
                    base.Draw(spriteBatch);
                }
            }
            enemyHUD.Draw(spriteBatch);

            /*
            if (ParticleEngine == null)
            {
                ParticleEngine = new DamageParticleEngine(GameState.CurrentZone.Textures, new Vector2(400, 240), Color.White, 1);
            }

            ParticleEngine.Draw(spriteBatch);
             */

            foreach (BaseBuff buff in Buffs)
            {
                buff.DrawBuff(spriteBatch);
            }
        }

        public void CreateEnemyBody()
        {
            Body = BodyFactory.CreateCircle(
                GameState.PhysicsWorld,
                ConvertUnits.ToSimUnits(Bounds.Width / 2.5),
                0,
                this);
            Body.UserData = this;
            Body.BodyType = BodyType.Dynamic;
            Body.SleepingAllowed = false;
            Body.LinearVelocity = new Vector2(0, 0);

            //Body.FixtureList[0].AfterCollision += new AfterCollisionEventHandler(Enemy_AfterCollision);
            MeleeRangeSensor = FixtureFactory.AttachCircle(
                ConvertUnits.ToSimUnits(Bounds.Width / 1.5),
                0,
                Body);
            MeleeRangeSensor.IsSensor = true;
            MeleeRangeSensor.OnCollision += AIRoutine.GetMeleeRangeSensorOnCollisionListener();
            MeleeRangeSensor.OnSeparation += AIRoutine.GetMeleeRangeSensorOnSeparationListener();
            
            LeftDamageFixture = FixtureFactory.AttachRectangle(
                ((CircleShape)Body.FixtureList[0].Shape).Radius / 2,
                ((CircleShape)Body.FixtureList[0].Shape).Radius * 2,
                0,
                new Vector2(0 - ((CircleShape)Body.FixtureList[0].Shape).Radius - ((CircleShape)Body.FixtureList[0].Shape).Radius / 4, 0),
                Body);
            RightDamageFixture = FixtureFactory.AttachRectangle(
                ((CircleShape)Body.FixtureList[0].Shape).Radius / 2,
                ((CircleShape)Body.FixtureList[0].Shape).Radius * 2,
                0,
                new Vector2(0 + ((CircleShape)Body.FixtureList[0].Shape).Radius + ((CircleShape)Body.FixtureList[0].Shape).Radius / 4, 0),
                Body);
            TopDamageFixture = FixtureFactory.AttachRectangle(
                ((CircleShape)Body.FixtureList[0].Shape).Radius * 2,
                ((CircleShape)Body.FixtureList[0].Shape).Radius / 2,
                0,
                new Vector2(0, 0 - ((CircleShape)Body.FixtureList[0].Shape).Radius - ((CircleShape)Body.FixtureList[0].Shape).Radius / 4),
                Body);
            BottomDamageFixture = FixtureFactory.AttachRectangle(
                ((CircleShape)Body.FixtureList[0].Shape).Radius * 2,
                ((CircleShape)Body.FixtureList[0].Shape).Radius / 2,
                0,
                new Vector2(0, 0 + ((CircleShape)Body.FixtureList[0].Shape).Radius + ((CircleShape)Body.FixtureList[0].Shape).Radius / 4),
                Body);

            LeftDamageFixture.IsSensor = true;
            RightDamageFixture.IsSensor = true;
            TopDamageFixture.IsSensor = true;
            BottomDamageFixture.IsSensor = true;

            LeftDamageFixture.OnCollision += AIRoutine.GetMeleeAttackOnCollisionListener();
            RightDamageFixture.OnCollision += AIRoutine.GetMeleeAttackOnCollisionListener();
            TopDamageFixture.OnCollision += AIRoutine.GetMeleeAttackOnCollisionListener();
            BottomDamageFixture.OnCollision += AIRoutine.GetMeleeAttackOnCollisionListener();
            LeftDamageFixture.CollidesWith = Category.None;
            RightDamageFixture.CollidesWith = Category.None;
            TopDamageFixture.CollidesWith = Category.None;
            BottomDamageFixture.CollidesWith = Category.None;
        }

        public override bool TakeDamage(int amount, AbilityElementType elementType, bool invulnerableCheck, bool show)
        {
            if (!IsInvincible || !invulnerableCheck)
            {
                if (elementType == AbilityElementType.Physical && invulnerableCheck)
                    amount = (int)(amount * (1f - (DefenseRating / 100f)));
                else if (invulnerableCheck)
                    amount = (int)(amount * (1f - (ResistanceRating / 100f)));

                CurrentHP -= amount;
                if (amount != 0)
                {
                    base.TakeDamage(amount, elementType, invulnerableCheck, show);
                }
                if (invulnerableCheck)
                {
                    IsInvincible = true;

                    Double check = GameState.RandomNumGenerator.NextDouble();

                    switch (enemyName)
                    {
                        case "Slime":
                            BaseSpeed += .10f;

                            if (check > .97)
                            {
                                Enemy e = new Enemy(GameState.Enemies.Find(x => x.enemyName.Contains("Slime")),
                                                texture,
                                                enemyHUD.healthBar,
                                                enemyHUD.font);
                                e.SetPosition(new Vector2(Position.X, Position.Y));
                                GameState.CurrentZone.Enemies.Add(e);

                                //Console.WriteLine("Slime Spawn");
                            }
                            break;

                        case "Slime+":
                            BaseSpeed += .10f;

                            if (check > .97)
                            {
                                Enemy e = new Enemy(GameState.Enemies.Find(x => x.enemyName.Contains("Slime+")),
                                                texture,
                                                enemyHUD.healthBar,
                                                enemyHUD.font);
                                e.SetPosition(new Vector2(Position.X, Position.Y));
                                GameState.CurrentZone.Enemies.Add(e);

                                //Console.WriteLine("Slime+ Spawn");
                            }
                            break;

                        case "Slime++":
                            BaseSpeed += .10f;

                            if (check > .97)
                            {
                                Enemy e = new Enemy(GameState.Enemies.Find(x => x.enemyName.Contains("Slime++")),
                                                texture,
                                                enemyHUD.healthBar,
                                                enemyHUD.font);
                                e.SetPosition(new Vector2(Position.X, Position.Y));
                                GameState.CurrentZone.Enemies.Add(e);

                                //Console.WriteLine("Slime++ Spawn");
                            }
                            break;
                        case "KingSlime":
                            BaseSpeed += .02f;

                            if (check > .97)
                            {
                                Enemy e = new Enemy(GameState.Enemies.Find(x => x.enemyName.Contains("Slime++")),
                                                texture,
                                                enemyHUD.healthBar,
                                                enemyHUD.font);
                                e.SetPosition(new Vector2(Position.X, Position.Y));
                                GameState.CurrentZone.Enemies.Add(e);

                                //Console.WriteLine("Slime++ Spawn");
                            }
                            else if (check > .93)
                            {
                                Enemy e = new Enemy(GameState.Enemies.Find(x => x.enemyName.Contains("Slime+")),
                                                texture,
                                                enemyHUD.healthBar,
                                                enemyHUD.font);
                                e.SetPosition(new Vector2(Position.X, Position.Y));
                                GameState.CurrentZone.Enemies.Add(e);

                                //Console.WriteLine("Slime++ Spawn");
                            }
                            else if (check > .88)
                            {
                                Enemy e = new Enemy(GameState.Enemies.Find(x => x.enemyName.Contains("Slime")),
                                                texture,
                                                enemyHUD.healthBar,
                                                enemyHUD.font);
                                e.SetPosition(new Vector2(Position.X, Position.Y));
                                GameState.CurrentZone.Enemies.Add(e);

                                //Console.WriteLine("Slime++ Spawn");
                            }
                            else if (check > .83)
                            {
                                Enemy e = new Enemy(GameState.Enemies.Find(x => x.enemyName.Contains("CasterSlime")),
                                                texture,
                                                enemyHUD.healthBar,
                                                enemyHUD.font);
                                e.SetPosition(new Vector2(Position.X, Position.Y));
                                GameState.CurrentZone.Enemies.Add(e);

                                //Console.WriteLine("Slime++ Spawn");
                            }
                            break;
                        case "CasterSlime":
                            BaseSpeed += .10f;

                            if (check > .97)
                            {
                                Enemy e = new Enemy(GameState.Enemies.Find(x => x.enemyName.Contains("CasterSlime")),
                                                texture,
                                                enemyHUD.healthBar,
                                                enemyHUD.font);
                                e.SetPosition(new Vector2(Position.X, Position.Y));
                                GameState.CurrentZone.Enemies.Add(e);

                                //Console.WriteLine("Slime++ Spawn");
                            }
                            break;
                    }
                }
                //Console.WriteLine(enemyName + ": " + CurrentHP);
                if (CurrentHP <= 0)
                {
                    MarkForDeletion();
                    GameState.HeroOne.GainExperience(enemyLevel, enemyExp);
                    GameState.Currency += enemyCurrency;
                    RollForDrops();
                    
                    //Console.WriteLine("Locke XP = " + GameState.HeroOne.EXP);
                }

                

                

                return true;
            }

            return false;
        }

        private void RollForDrops()
        {
            bool itemObtained = false;
            // OrderBy call to make lowest drop chance items get rolled for first.
            foreach (KeyValuePair<BaseItem, float> itemEntry in enemyDropList.OrderBy(i => i.Value))
            {
                //Console.WriteLine(itemEntry.Key.itemName + " has a drop chance of " + itemEntry.Value);
                Double check = GameState.RandomNumGenerator.NextDouble();
                //Console.WriteLine("Roll value for item acquisition: " + check);
                if (check <= itemEntry.Value / 100 && itemObtained == false)
                {
                    TimedMessageBoxManager.AddItemDropMessage(1, enemyName + " dropped " + itemEntry.Key.itemName + "!", 2000);
                    //Console.WriteLine("You got a(n) " + itemEntry.Key.itemName);
                    GameState.Inventory.AddRemoveItem(itemEntry.Key, 1);
                    itemObtained = true;
                }
            }
        }

        public override void Destroy()
        {
            GameState.PhysicsWorld.RemoveBody(Body);
        }

        private void SetUpAnimations()
        {
            Animated = true;

            AnimationClass ani = new AnimationClass();
            ani.FramesPerSecond = 8;
            ani.SpriteEffect = SpriteEffects.FlipHorizontally;
            SpriteAnimation.AddAnimation("Right", 3, 8, ani.Copy());
            ani.SpriteEffect = SpriteEffects.None;

            SpriteAnimation.AddAnimation("Left", 3, 8, ani.Copy());

            SpriteAnimation.AddAnimation("Up", 1, 8, ani.Copy());

            SpriteAnimation.AddAnimation("Down", 2, 8, ani.Copy());

            ani.SpriteEffect = SpriteEffects.FlipHorizontally;
            SpriteAnimation.AddAnimation("RightIdle", 3, 1, ani.Copy());
            ani.SpriteEffect = SpriteEffects.None;

            SpriteAnimation.AddAnimation("LeftIdle", 3, 1, ani.Copy());

            SpriteAnimation.AddAnimation("UpIdle", 1, 1, ani.Copy());

            SpriteAnimation.AddAnimation("DownIdle", 2, 1, ani.Copy());

            SpriteAnimation.Animation = "DownIdle";
        }

        public override void UpdateStats()
        {
            Damage           = (int)(BaseDamage     * ModDam);
            Magic            = (int)(BaseMagic      * ModMag);
            Heal             = (int)(BaseHeal       * ModHea);
            DefenseRating    = (int)(BaseDefense    * ModDef);
            ResistanceRating = (int)(BaseResistance * ModRes);
        }
    }
}
