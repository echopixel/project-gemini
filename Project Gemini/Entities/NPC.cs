﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectGemini.Graphics_Utilities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using FarseerPhysics.Dynamics;
using ProjectGemini.Static_Classes;
using FarseerPhysics.Factories;
using ProjectGemini.Managers;

namespace ProjectGemini.Entities
{
    public class NPC : DynamicEntity
    {
        public List<Tuple<string, string[]>> Dialogue { get; set; }

        private int _dialogueIndex = 0;

        public NPC(Texture2D texture)
        {
            Dialogue = new List<Tuple<string, string[]>>();

            MovementSpeed = 2;

            SpriteAnimation = new SpriteAnimation(texture);
            //SetUpAnimations();
            Bounds.Width = SpriteAnimation.Width;
            Bounds.Height = SpriteAnimation.Height;

            Body = BodyFactory.CreateCircle(
                GameState.PhysicsWorld,
                ConvertUnits.ToSimUnits(Bounds.Width / 2.5),
                0,
                this);
            Body.UserData = this;
            Body.BodyType = BodyType.Dynamic;
            Body.SleepingAllowed = false;
            Body.LinearVelocity = new Vector2(0, 0);
        }

        public NPC(Texture2D texture, int frames, int animations)
        {
            MovementSpeed = 2;

            SpriteAnimation = new SpriteAnimation(texture);
            SetUpAnimations();
            Bounds.Width = SpriteAnimation.Width;
            Bounds.Height = SpriteAnimation.Height;

            Body = BodyFactory.CreateCircle(
                GameState.PhysicsWorld,
                ConvertUnits.ToSimUnits(Bounds.Width / 2.5),
                0);
            Body.UserData = this;
            Body.BodyType = BodyType.Dynamic;
            Body.SleepingAllowed = false;
        }

        public override void Update(GameTime gameTime)
        {
            Vector2 v = ConvertUnits.ToDisplayUnits(
                    new Vector2(this.Body.Position.X,
                                this.Body.Position.Y - ConvertUnits.ToSimUnits(SpriteAnimation.Height / 4)));
            this.Position = new Vector2((int)v.X, (int)v.Y);

            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }

        public override bool IsTouchingHero(ref Body eventBody)
        {
            eventBody = Body;
            return true;
        }

        public override bool InteractWith(ScreenManager screenManager, PlayerIndex? controllingPlayer)
        {
            ProcessDialogue(new DialogueEvent(this, Dialogue[0]));

            return true;
        }

        public void ProcessDialogue(DialogueEvent dialogueEvent)
        {
            if (dialogueEvent.Dialogue.Item1 != "")
                EventManager.AddEvent(new MessageBoxEvent(dialogueEvent.Dialogue.Item1));
            else
            {
                //Console.WriteLine("Empty");
            }

            Int32 nextIndex;

            // Check tags.   
            // Current tags 
            // /#
            // /end
            // /set_t
            // /set_f
            // /check
            for (int i = 0; i < dialogueEvent.Dialogue.Item2.Length; i++)
            {
                //Console.WriteLine(Dialogue[_dialogueIndex].Item2[i]);

                if (dialogueEvent.Dialogue.Item2[i].Contains("set_t"))
                {
                    char openParen = '(';
                    char closeParen = ')';
                    int openParenIndex = dialogueEvent.Dialogue.Item2[i].IndexOf(openParen);
                    int closeParenIndex = dialogueEvent.Dialogue.Item2[i].IndexOf(closeParen);
                    string flagName = dialogueEvent.Dialogue.Item2[i].Substring(openParenIndex + 1, closeParenIndex - openParenIndex - 1);
                    EventManager.AddEvent(new FlagEvent(flagName, true));
                    //Console.WriteLine(Dialogue[_dialogueIndex].Item2[i]);
                }
                else if (dialogueEvent.Dialogue.Item2[i].Contains("set_f"))
                {
                    char openParen = '(';
                    char closeParen = ')';
                    int openParenIndex = dialogueEvent.Dialogue.Item2[i].IndexOf(openParen);
                    int closeParenIndex = dialogueEvent.Dialogue.Item2[i].IndexOf(closeParen);
                    string flagName = dialogueEvent.Dialogue.Item2[i].Substring(openParenIndex + 1, closeParenIndex - openParenIndex - 1);
                    EventManager.AddEvent(new FlagEvent(flagName, false));
                    //Console.WriteLine(Dialogue[_dialogueIndex].Item2[i]);
                }
                else if (dialogueEvent.Dialogue.Item2[i].Contains("check"))
                {
                    char openParen = '(';
                    char closeParen = ')';
                    int openParenIndex = dialogueEvent.Dialogue.Item2[i].IndexOf(openParen);
                    int closeParenIndex = dialogueEvent.Dialogue.Item2[i].IndexOf(closeParen);
                    string flagName = dialogueEvent.Dialogue.Item2[i].Substring(openParenIndex + 1, closeParenIndex - openParenIndex - 1);

                    string[] delimiters = new string[] { "@" };
                    string[] flagStrings  = dialogueEvent.Dialogue.Item2[i].Split(delimiters, StringSplitOptions.None);

                    if (FlagManager.CheckFlag(flagName))
                    {
                        _dialogueIndex = int.Parse(flagStrings[1]);
                        EventManager.AddEvent(new DialogueEvent(this, Dialogue[_dialogueIndex]));
                    }
                    else
                    {
                        _dialogueIndex = int.Parse(flagStrings[2]);
                        EventManager.AddEvent(new DialogueEvent(this, Dialogue[_dialogueIndex]));
                    }
                    //Console.WriteLine(Dialogue[_dialogueIndex].Item2[i]);
                }
                else if (Int32.TryParse(dialogueEvent.Dialogue.Item2[i], out nextIndex))
                {
                    //Console.WriteLine(nextIndex);
                    //InteractWith(screenManager, controllingPlayer);
                    _dialogueIndex = nextIndex;
                    EventManager.AddEvent(new DialogueEvent(this, Dialogue[_dialogueIndex]));
                    EventManager.RunEvents();
                }
                else if (dialogueEvent.Dialogue.Item2[i].Contains("end"))
                {
                    _dialogueIndex = 0;
                    EventManager.RunEvents();
                }
            }

            dialogueEvent.End();
        }

        private void SetUpAnimations()
        {
            Animated = true;

            AnimationClass ani = new AnimationClass();
            ani.FramesPerSecond = 5;
            ani.SpriteEffect = SpriteEffects.FlipHorizontally;
            SpriteAnimation.AddAnimation("Right", 3, 4, ani.Copy());
            ani.SpriteEffect = SpriteEffects.None;

            SpriteAnimation.AddAnimation("Left", 3, 4, ani.Copy());

            SpriteAnimation.AddAnimation("Up", 1, 4, ani.Copy());

            SpriteAnimation.AddAnimation("Down", 2, 4, ani.Copy());

            ani.SpriteEffect = SpriteEffects.FlipHorizontally;
            SpriteAnimation.AddAnimation("RightIdle", 3, 1, ani.Copy());
            ani.SpriteEffect = SpriteEffects.None;

            SpriteAnimation.AddAnimation("LeftIdle", 3, 1, ani.Copy());

            SpriteAnimation.AddAnimation("UpIdle", 1, 1, ani.Copy());

            SpriteAnimation.AddAnimation("DownIdle", 2, 1, ani.Copy());

            SpriteAnimation.Animation = "RightIdle";
        }
    }
}
