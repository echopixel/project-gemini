﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using ProjectGemini.Static_Classes;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework.Graphics;
using ProjectGemini.Utilities;
using ProjectGemini.Graphics_Utilities;

namespace ProjectGemini.Entities
{
    public class TreasureChest : BaseEntity
    {
        public int ChestID { get; set; }
        public string ChestText { get; set; }
        public Dictionary<BaseItem, int> Content { get; set; }

        public Body Body { get; set; }
        public void SetPosition(Vector2 position)
        {
            Body.Position = ConvertUnits.ToSimUnits(position);
            SpriteAnimation.Position = position;
        }

        public TreasureChest(Texture2D texture, Vector2 position, int width, int height, int chestID, Dictionary<BaseItem, int> content, bool beenOpened)
        {
            SpriteAnimation = new SpriteAnimation(texture, 1, 2);
            AnimationClass ani = new AnimationClass();
            ani.FramesPerSecond = 1;
            SpriteAnimation.AddAnimation("Closed", 1, 1, ani.Copy());
            SpriteAnimation.AddAnimation("Opened", 2, 1, ani.Copy());
            if (beenOpened)
            {
                SpriteAnimation.Animation = "Opened";
            }
            else
            {
                SpriteAnimation.Animation = "Closed";
            }

            Bounds.Width = width;
            Bounds.Height = height;

            Body = BodyFactory.CreateRectangle(
                GameState.PhysicsWorld,
                ConvertUnits.ToSimUnits(width),
                ConvertUnits.ToSimUnits(height),
                0,
                this);
            Body.BodyType = BodyType.Static;
            Body.SleepingAllowed = false;

            ChestText = "";
            ChestID = chestID;
            Content = content;
            foreach (KeyValuePair<BaseItem, int> item in content)
            {
                if (item.Value == 1)
                {
                    ChestText += "You found a " + item.Key.itemName + ".\n";
                }
                else
                {
                    ChestText += "You found " + item.Key.itemName + " x " + item.Value + ".\n";
                }
            }
            
            SetPosition(position);

            Body.UserData = this;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }

        public override bool IsTouchingHero(ref Body eventBody)
        {
            eventBody = Body;
            return true;
        }

        public override bool InteractWith(ScreenManager screenManager, PlayerIndex? controllingPlayer)
        {
            if (SpriteAnimation.Animation == "Closed")
            {
                SpriteAnimation.Animation = "Opened";
                MessageBoxScreen treasureMessageBox = new MessageBoxScreen(ChestText,
                    HelperMethods.TestRange(GameState.HeroOne.Position.Y,
                                    GameState.CurrentMap.Bounds.Height - ResolutionManager.VirtualHeight / 4,
                                    GameState.CurrentMap.Bounds.Height),
                                    true);
                screenManager.AddScreen(treasureMessageBox, controllingPlayer);
                foreach (KeyValuePair<BaseItem, int> item in Content)
                {
                    GameState.Inventory.AddRemoveItem(item.Key, item.Value);
                }
                GameState.OpenedChests.Add(Tuple.Create(GameState.CurrentMap.Properties["fileName"].Value, ChestID));
                return true;
            }

            return false;
        }
    }
}
