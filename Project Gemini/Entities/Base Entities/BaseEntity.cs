﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using ProjectGemini.EnumTypes;
using ProjectGemini.Graphics_Utilities;
using Microsoft.Xna.Framework.Graphics;
using ProjectGemini.Static_Classes;
using FuncWorks.XNA.XTiled;
using FarseerPhysics.Dynamics;
using ProjectGemini.Managers;

namespace ProjectGemini.Entities
{
    public class BaseEntity
    {
        public bool CollisionEnabled { get; set; }
        public bool MarkedForDeletion { get; set; }
        public bool TextDisplayed { get; set; }

        public SpriteAnimation SpriteAnimation { get; set; }
        public string Animation { get { return SpriteAnimation.Animation; } set { SpriteAnimation.Animation = value; } }
        public virtual Vector2 Position { get { return SpriteAnimation.Position; } set { SpriteAnimation.Position = value;} }
        public bool Animated { get; set; }
        public Rectangle Bounds;
        public EntityType EntityType { get; set; }
        public EventManager EventManager { get; set; }

        public BaseEntity()
        {
            this.EventManager = new EventManager();
            EntityType = EntityType.BaseEntity;
            CollisionEnabled = true;
        }

        public virtual void Update(GameTime gameTime)
        {
            if (this.EventManager.IsRunning)
            {
                this.EventManager.RunEvents();
            }

            if (Animated)
                SpriteAnimation.Update(gameTime);
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            SpriteAnimation.Draw(spriteBatch);

            //if (GameDebug.ShowCollision)
            //    Map.DrawRectangle(spriteBatch, Bounds, GameState.VisibleArea, 0, Color.AliceBlue, Color.Transparent);
        }

        public virtual void Draw(SpriteBatch spriteBatch, Color color)
        {
            SpriteAnimation.Draw(spriteBatch, color);
        }
        
        public virtual void Draw(SpriteBatch spriteBatch, Color color, float scale)
        {
            SpriteAnimation.Draw(spriteBatch, color, scale);
        }

        public virtual bool IsTouchingHero(ref Body eventBody)
        {
            return false;
        }

        public virtual bool InteractWith()
        {
            return false;
        }

        public virtual bool InteractWith(ScreenManager screenManager, PlayerIndex? controllingPlayer)
        {
            return false;
        }

        public virtual bool TakeDamage(int amount, AbilityElementType elementType, bool invulnerable, bool show)
        {
            return false;
        }

        public virtual bool TakeHeal(int amount, bool show)
        {
            return false;
        }

        public virtual void ItemUsed(Action<int> function, int count)
        {

        }

        public virtual bool IsTargeted()
        {
            return false;
        }

        public virtual void Destroy()
        {
            MarkForDeletion();
        }

        public virtual void MarkForDeletion()
        {
            MarkedForDeletion = true;
        }
    }
}
