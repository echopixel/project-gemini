﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using FarseerPhysics.Dynamics;
using ProjectGemini.Static_Classes;
using ProjectGemini.Managers;
using ProjectGemini.EnumTypes;

namespace ProjectGemini.Entities
{
    public class DynamicEntity : BaseEntity
    {
        public bool CanControlMovement { get; set; }
        private Vector2 _movementVector { get; set; }
        private Vector2 _previousPosition { get; set; }
        private Vector2 _eventDestination { get; set; }
        private IEvent _eventSource { get; set; }

        public float MovementSpeed { get; set; }
        public bool IsInvincible { get; set; }

        public int Height { get { return SpriteAnimation.Height; } }
        public int Width { get { return SpriteAnimation.Width; } }
        public Body Body { get; set; }
        public void SetPosition(Vector2 position)
        {
            SpriteAnimation.Position = position;
            Body.Position = ConvertUnits.ToSimUnits(position);
        }

        public DynamicEntity()
        {
            CanControlMovement = true;
            IsInvincible = false;
            MovementSpeed = 0f;
        }

        public void Move(MoveEvent source)
        {
            _eventSource = source;
            CanControlMovement = false;
            _eventDestination = source.Destination;

            //Vector2 movementVector = Vector2.Zero;

            switch (source.Direction)
            {
                case DirectionType.Up:
                    _movementVector = new Vector2(0, -1);
                    Animation = "Up";
                    break;
                case DirectionType.Down:
                    _movementVector = new Vector2(0, 1);
                    Animation = "Down";
                    break;
                case DirectionType.Left:
                    _movementVector = new Vector2(-1, 0);
                    Animation = "Left";
                    break;
                case DirectionType.Right:
                    _movementVector = new Vector2(1, 0);
                    Animation = "Right";
                    break;
            }
            
            Body.LinearVelocity = _movementVector * MovementSpeed;
        }

        private bool HasReachedDestination(Vector2 destination)
        {
            if (HelperMethods.TestRange(Position.Y, destination.Y - 2, destination.Y + 2) &&
                HelperMethods.TestRange(Position.X, destination.X - 2, destination.X + 2))
            {
                Body.LinearVelocity = Vector2.Zero;
                CanControlMovement = true;
                _eventSource.End();
                return true;
            }
                
            return false;
        }

        private bool IsMovementBlocked()
        {
            if (_previousPosition == Position)
                return true;

            return false;
        }

        public override void Update(GameTime gameTime)
        {
            if (CanControlMovement == false)
            {
                Body.LinearVelocity = _movementVector * MovementSpeed;
                if (IsMovementBlocked() && _eventSource.IsCancelable)
                {
                    Body.LinearVelocity = Vector2.Zero;
                    _previousPosition = Vector2.Zero;
                    CanControlMovement = true;
                    _eventSource.Cancel();
                }
                else
                {
                    _previousPosition = Position;
                }
                HasReachedDestination(_eventDestination);

            }
            base.Update(gameTime);
        }

    }
}
