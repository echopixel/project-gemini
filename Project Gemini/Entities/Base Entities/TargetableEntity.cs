﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using FarseerPhysics.Dynamics;
using ProjectGemini.EnumTypes;
using ProjectGemini.Utilities;
using Microsoft.Xna.Framework.Graphics;
using ProjectGemini.Static_Classes;
using ProjectGemini.GUI;using ProjectGemini.Utilities.Buffs;
using ParticleEngine2D;
namespace ProjectGemini.Entities
{
    public class TargetableEntity : DynamicEntity
    {
        //public DamageParticleEngine ParticleEngine;

        #region Properties

        public bool IsCasting { get; set; }
        public bool IsFinishingCast { get; set; }
        protected float FinishingCastTimer = 0;
        public bool IsTargeting { get; set; }
        protected TargetableEntity _Target;
        protected List<TargetableEntity> targets = new List<TargetableEntity>();
        public bool IsAttacking { get; set; }
        public bool _IsSelected { get; set; }
        public bool Selectable { get; set; }
        protected int HoT;
        protected int DoT;
        protected float BuffTimer = 0;
        protected bool HoTTick = false;
        protected bool DoTTick = false;
        public Queue<DisplayAbilityText> _DisplayDamageText;
        protected Ability _CastedAbility { get; set; }

        #endregion

        #region Base Stats

        public int BaseDamage { get;  set; }
        public int BaseMagic { get;  set; }
        public int BaseHeal { get; set; }
        public int BaseDefense { get;  set; }
        public int BaseResistance { get;  set; }

        #endregion

        #region Stat Modifiers

        public float ModDam { get; set; }
        public float ModMag { get; set; }
        public float ModHea { get; set; }
        public float ModDef { get; set; }
        public float ModRes { get; set; }

        #endregion

        #region Core Stats

        public int MaxHP { get;  set; }
        public int MaxMP { get;  set; }
        public int Damage { get;  set; }
        public int Magic { get;  set; }
        public int Heal { get; set; }
        public int DefenseRating { get; set; }
        public int ResistanceRating { get; set; }

        public int CurrentHP { get;  set; }
        public int CurrentMP { get;  set; }

        #endregion

        #region Buff Stats

        public List<BaseBuff> Buffs { get; set; }
        public List<BaseBuff> Passives { get; set; }
        public List<BaseBuff> RemoveBuff { get; set; }

        public float BaseSpeed = 0;

        #endregion

        public TargetableEntity()
        {
            Selectable = true;
            IsCasting = false;
            IsFinishingCast = false;

            BaseDamage = 0;
            BaseMagic = 0;
            BaseHeal = 0;
            BaseDefense = 0;
            BaseResistance = 0;

            ModDam = 1;
            ModMag = 1;
            ModHea = 1;
            ModDef = 1;
            ModRes = 1;

            Buffs = new List<BaseBuff>();
            Passives = new List<BaseBuff>();
            RemoveBuff = new List<BaseBuff>();
            _DisplayDamageText = new Queue<DisplayAbilityText>();
            BaseSpeed = 0;
        }

        public TargetableEntity(int HP, int MP, int Damage, int Magic, int Heal, int Defense, int Resistance)
        {
            Selectable = true;
            IsCasting = false;
            IsFinishingCast = false;

            MaxHP = HP;
            MaxMP = MP;
            BaseDamage = Damage;
            BaseMagic = Magic;
            BaseHeal = Heal;
            BaseDefense = Defense;
            BaseResistance = Resistance;

            ModDam = 1;
            ModMag = 1;
            ModHea = 1;
            ModDef = 1;
            ModRes = 1;

            CurrentHP = HP;
            CurrentMP = MP;

            Buffs = new List<BaseBuff>();
            Passives = new List<BaseBuff>();
            RemoveBuff = new List<BaseBuff>();
            BaseSpeed = 0;
        }

        public override void Update(GameTime gameTime)
        {    
            MovementSpeed = BaseSpeed;

            List<string> check = new List<string>();

            foreach (BaseBuff buff in Buffs)
            {
                var match = check
                    .FirstOrDefault(stringToCheck => stringToCheck.Contains(buff.Unique));

                if (buff.Unique == "" || match == null)
                {
                    buff.StartBuff(gameTime);

                    if(buff.Unique != "")
                    {
                        check.Add(buff.Unique);
                    }

                    if (!buff.RunBuff(gameTime))
                    {
                        RemoveBuff.Add(buff);
                    }
                }
                //Console.WriteLine(buff.Duration);
            }

            foreach (BaseBuff buff in Passives)
            {
                //Console.WriteLine(buff.Duration);
                buff.StartBuff(gameTime);
                buff.RunBuff(gameTime);
            }
            if (BuffTimer >= 1000)
            {
                //Console.WriteLine(BuffTimer);
                HoTTick = true;
                DoTTick = true;
                BuffTimer -= 1000f;
            }
            //if (Buffs.Count > 0)
            //{
            BuffTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                //Console.WriteLine(BuffTimer);
            //}

            foreach(BaseBuff remove in RemoveBuff)
            {
                remove.StopBuff(gameTime);
                Buffs.Remove(remove);
            }

            RemoveBuff.Clear();

            base.Update(gameTime);
        }

        public double damageVariance()
        {
            Double check = GameState.RandomNumGenerator.NextDouble();
            check = 1 + ((check - .5f) * 3f / 10f);
            //Console.WriteLine(check);
            //Console.WriteLine(check);
            return check;
        }

        public bool effectedBy(ItemEffectType type, int mag)
        {
            switch (type)
            {
                case ItemEffectType.FlatHpRestore :
                    return HealHPFlat(mag);
                    
                case ItemEffectType.PercentHpRestore :
                    return HealHPPercent(mag);

                case ItemEffectType.FlatMpRestore :
                    return HealMPFlat(mag);

                case ItemEffectType.PercentMpRestore :
                    return HealMPPercent(mag);
            }

            return false;
        }

        public bool HealHPFlat(int amount)
        {
            //CurrentHP = CurrentHP + amount;

            if (CurrentHP == MaxHP)
                return false;

            int check = CurrentHP + amount;

            if (check >= MaxHP)
                CurrentHP = MaxHP;
            else if (check <= 0)
                CurrentHP = 0;
            else
                CurrentHP = check;

            return true;
        }

        public bool HealHPPercent(int ammount)
        {
            if (CurrentHP == MaxHP)
                return false;

            int check = CurrentHP + (MaxHP * ammount / 100);

            if (check >= MaxHP)
                CurrentHP = MaxHP;
            else if (check <= 0)
                CurrentHP = 0;
            else
                CurrentHP = check;

            return true;
        }

        public bool HealMPFlat(int amount)
        {
            //CurrentHP = CurrentHP + amount;

            if (CurrentMP == MaxMP)
                return false;

            int check = CurrentMP + amount;

            if (check >= MaxMP)
                CurrentMP = MaxMP;
            else if (check <= 0)
                CurrentMP = 0;
            else
                CurrentMP = check;

            return true;
        }

        public bool HealMPPercent(int amount)
        {
            if (CurrentMP == MaxMP)
                return false;

            int check = CurrentMP + (MaxMP * amount / 100);

            if (check >= MaxMP)
                CurrentMP = MaxMP;
            else if (check <= 0)
                CurrentMP = 0;
            else
                CurrentMP = check;

            return true;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            /*
            if (ParticleEngine == null)
            {
                ParticleEngine = new DamageParticleEngine(GameState.CurrentZone.Textures, new Vector2(400, 240), Color.White, 1);
            }
             */

            base.Draw(spriteBatch);
        }

        public override void Draw(SpriteBatch spriteBatch, Color color)
        {
            /*
            if (ParticleEngine == null)
            {
                ParticleEngine = new DamageParticleEngine(GameState.CurrentZone.Textures, new Vector2(400, 240), Color.White, 1);
            }
             */

            base.Draw(spriteBatch, color);
        }

        public override void Draw(SpriteBatch spriteBatch, Color color, float scale)
        {
            /*
            if (ParticleEngine == null)
            {
                ParticleEngine = new DamageParticleEngine(GameState.CurrentZone.Textures, new Vector2(400, 240), Color.White, 1);
            }
             */

            base.Draw(spriteBatch, color, scale);
        }

        public override bool IsTargeted()
        {
            return _IsSelected;
        }

        public bool TakeBuffHeal(int amount)
        {
            HoT += amount;
            this.TakeHeal(amount, false);

            if (HoTTick)
            {
                HoTTick = false;
                _DisplayDamageText.Enqueue(new DisplayAbilityText(this, HoT, Color.Chartreuse));
                HoT = 0;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool TakeBuffDamage(int amount, AbilityElementType elementType, bool invulnerable)
        {
            DoT += amount;
            this.TakeDamage(amount, elementType, invulnerable, false);
            if (DoTTick)
            {
                DoTTick = false;
                _DisplayDamageText.Enqueue(new DisplayAbilityText(this, DoT, Color.White));
                DoT = 0;
                return true;
            }
            else
            {
                return false;
            }
        }
        public override bool TakeHeal(int amount, bool show)
        {
            if (show)
            {
                _DisplayDamageText.Enqueue(new DisplayAbilityText(this, amount, Color.GreenYellow));
            }
            TextDisplayed = true;
            return base.TakeHeal(amount, show);
        }
        public override bool TakeDamage(int amount, AbilityElementType elementType, bool invulnerable, bool show)
        {
            if (show)
            {
                _DisplayDamageText.Enqueue(new DisplayAbilityText(this, amount, Color.White));
            }
            TextDisplayed = true;
            return base.TakeDamage(amount, elementType, invulnerable, show);
        }


        protected virtual TargetableEntity ChooseTarget(Ability ability, TargetableEntity caster, List<TargetableEntity> targets)
        {
            return null;
        }

        /// <summary>
        /// Sets flags on for when an entity starts to cast.
        /// </summary>
        protected virtual void StartCast()
        {
            IsCasting = true;
            IsTargeting = true;
        }

        /// <summary>
        /// Sets flags off for when an entity ends a cast.
        /// </summary>
        protected void EndCast()
        {
            IsCasting = false;
            IsFinishingCast = false;
            if (_Target != null)
                _Target._IsSelected = false;
            targets.Clear();
            _Target = null;
        }

        protected BaseBuff ApplyBuff(List<BaseBuff> list, TargetableEntity host, TargetableEntity owner, BaseBuff buff, float mod)
        {
            BaseBuff add = null;

            switch (buff.Type)
            {
                case BuffType.TakeDamage:
                    add = new TakeDamageBuff((TakeDamageBuff)buff, host, owner, mod);
                    break;
                case BuffType.TakeHeal:
                    add = new TakeHealBuff((TakeHealBuff)buff, host, owner, mod);
                    break;
                case BuffType.CoreStat:
                    add = new CoreStatBuff((CoreStatBuff)buff, host, owner);
                    break;
                case BuffType.HeroStat:
                    add = new HeroStatBuff((HeroStatBuff)buff, host, owner);
                    break;
                case BuffType.MovementSpeed:
                    add = new MovementSpeedBuff((MovementSpeedBuff)buff, host, owner);
                    break;
            }

            list.Add(add);

            return add;
        }

        public virtual void UpdateStats()
        {
            
        }
    }
}
