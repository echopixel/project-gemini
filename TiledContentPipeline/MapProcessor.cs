using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Graphics;
using TiledLib;

namespace TiledContentPipeline
{
    //// Each object has ...
    //[ContentSerializerRuntimeType("ProjectGemini.Object, ProjectGemini")]
    //public class GameMapObjectContent
    //{
    //    public MapObjectType ObjectType;
    //    public string Name;
    //    public string Type;
    //    public Rectangle Bounds;
    //    public List<Vector2> Points;
    //    public int GID; // if there's a GID, it's a tile object
    //    public Dictionary<string, string> Properties;
    //}

    // Each object layer has
    //[ContentSerializerRuntimeType("ProjectGemini.Object, ProjectGemini")]
    //public class GameMapObjectLayerContent
    //{
    // Stuff goes here
    //}

    // Each tile has a texture, source rect, and sprite effects.
    [ContentSerializerRuntimeType("ProjectGemini.Tile, ProjectGemini")]
    public class GameMapTileContent
    {
        public ExternalReference<Texture2DContent> Texture;
        public Rectangle SourceRectangle;
        public SpriteEffects SpriteEffects;
        public Dictionary<string, string> Properties;
        public uint TileID;
    }

    // For each layer, we store the size of the layer and the tiles.
    [ContentSerializerRuntimeType("ProjectGemini.Layer, ProjectGemini")]
    public class GameMapLayerContent
    {
        public int Width;
        public int Height;
        public bool Visible;
        public float Opacity;
        public Dictionary<string, string> Properties;
        public GameMapTileContent[] Tiles;
        //public GameMapObjectContent[] Objects;
    }

    // For the map itself, we just store the size, tile size, and a list of layers.
    [ContentSerializerRuntimeType("ProjectGemini.Map, ProjectGemini")]
    public class GameMapContent
    {
        public int TileWidth;
        public int TileHeight;
        public int Width;
        public int Height;
        public Dictionary<string, string> Properties;
        public List<GameMapLayerContent> Layers = new List<GameMapLayerContent>();
    }

    [ContentProcessor(DisplayName = "TMX Processor - ProjectGemini")]
    public class MapProcessor : ContentProcessor<MapContent, GameMapContent>
    {
        public override GameMapContent Process(MapContent input, ContentProcessorContext context)
        {
            // build the textures
            TiledHelpers.BuildTileSetTextures(input, context);

            // generate source rectangles
            TiledHelpers.GenerateTileSourceRectangles(input);

            // Store the map properties
            Dictionary<string, string> mapProperties = new Dictionary<string, string>();
            foreach (var p in input.Properties)
            {
                mapProperties.Add(p.Key, p.Value);
            }
            // now build the output, first by just copying over some data
            GameMapContent output = new GameMapContent
            {
                TileWidth = input.TileWidth,
                TileHeight = input.TileHeight,
                Width = input.Width,
                Height = input.Height,
                Properties = mapProperties
            };

            // iterate all the layers of the input
            foreach (LayerContent layer in input.Layers)
            {
                // Beginning of implementing object layers for collision
                //MapObjectLayerContent olc = layer as MapObjectLayerContent;
                //if (olc != null)
                //{
                //    Dictionary<string, string> layerProperties
                //        olc
                //}

                // we only care about tile layers in our demo
                TileLayerContent tlc = layer as TileLayerContent;
                if (tlc != null)
                {
                    // Store layer properties
                    Dictionary<string, string> layerProperties = new Dictionary<string, string>();
                    foreach (var p in tlc.Properties)
                    {
                        layerProperties.Add(p.Key, p.Value);
                    }

                    // create the new layer
                    GameMapLayerContent outLayer = new GameMapLayerContent
                    {
                        Width = tlc.Width,
                        Height = tlc.Height,
                        Visible = tlc.Visible,
                        Opacity = tlc.Opacity,
                        Properties = layerProperties
                    };

                    // we need to build up our tile list now
                    outLayer.Tiles = new GameMapTileContent[tlc.Data.Length];
                    for (int i = 0; i < tlc.Data.Length; i++)
                    {
                        // get the ID of the tile
                        uint tileID = tlc.Data[i];
                        // If tile was deleted in Tiled it will have to be set to null.
                        if (tileID == 0)
                        {
                            outLayer.Tiles[i] = null;
                            continue;
                        } 
                        // use that to get the actual index as well as the SpriteEffects
                        int tileIndex;
                        SpriteEffects spriteEffects;
                        TiledHelpers.DecodeTileID(tileID, out tileIndex, out spriteEffects);

                        // figure out which tileset has this tile index in it and grab
                        // the texture reference and source rectangle.
                        ExternalReference<Texture2DContent> textureContent = null;
                        Rectangle sourceRect = new Rectangle();
                        Dictionary<string, string> tileProperties = new Dictionary<string,string>();
                        uint tileid = 0;

                        // iterate all the tilesets
                        foreach (var tileSet in input.TileSets)
                        {
                            // if our tile index is in this set
                            if (tileIndex - tileSet.FirstId < tileSet.Tiles.Count)
                            {
                                // store the texture content, source rectangle and tile properties
                                textureContent = tileSet.Texture;
                                sourceRect = tileSet.Tiles[(int)(tileIndex - tileSet.FirstId)].Source;
                                tileid = (uint)(tileIndex - tileSet.FirstId);
                                foreach (var p in tileSet.Tiles[(int)(tileIndex - tileSet.FirstId)].Properties)
                                {
                                    tileProperties.Add(p.Key, p.Value);
                                }
                                // and break out of the foreach loop
                                break;
                            }
                        }

                        // now insert the tile into our output
                        outLayer.Tiles[i] = new GameMapTileContent
                        {
                            Texture = textureContent,
                            SourceRectangle = sourceRect,
                            SpriteEffects = spriteEffects,
                            Properties = tileProperties,
                            TileID = tileid
                        };
                    }

                    // add the layer to our output
                    output.Layers.Add(outLayer);
                }

                //MapObjectLayerContent olc = layer as MapObjectLayerContent;
                //if (olc != null)
                //{
                //    // Store object layer properties
                //    Dictionary<string, string> objectLayerProperties = new Dictionary<string, string>();
                //    foreach (var p in olc.Properties)
                //    {
                //        objectLayerProperties.Add(p.Key, p.Value);
                //    }

                //    // create the new layer
                //    GameMapLayerContent outLayer = new GameMapLayerContent
                //    {
                //        Width = olc.Width,
                //        Height = olc.Height,
                //        Visible = olc.Visible,
                //        Opacity = olc.Opacity,
                //        Properties = objectLayerProperties,
                //    };

                //    // we need to build up our tile list now
                //    outLayer.Objects = new GameMapObjectContent[olc.Objects.Count];
                //    for (int i = 0; i < olc.Objects.Count; i++)
                //    {
                //        outLayer.Objects[i] = new GameMapObjectContent
                //            {

                //            };
                //        // get the ID of the tile
                //        uint tileID = tlc.Data[i];
                //        // If tile was deleted in Tiled it will have to be set to null.
                //        if (tileID == 0)
                //        {
                //            outLayer.Tiles[i] = null;
                //            continue;
                //        }
                //        // use that to get the actual index as well as the SpriteEffects
                //        int tileIndex;
                //        SpriteEffects spriteEffects;
                //        TiledHelpers.DecodeTileID(tileID, out tileIndex, out spriteEffects);

                //        // figure out which tileset has this tile index in it and grab
                //        // the texture reference and source rectangle.
                //        ExternalReference<Texture2DContent> textureContent = null;
                //        Rectangle sourceRect = new Rectangle();
                //        Dictionary<string, string> tileProperties = new Dictionary<string, string>();
                //        uint tileid = 0;

                //        // iterate all the tilesets
                //        foreach (var tileSet in input.TileSets)
                //        {
                //            // if our tile index is in this set
                //            if (tileIndex - tileSet.FirstId < tileSet.Tiles.Count)
                //            {
                //                // store the texture content, source rectangle and tile properties
                //                textureContent = tileSet.Texture;
                //                sourceRect = tileSet.Tiles[(int)(tileIndex - tileSet.FirstId)].Source;
                //                tileid = (uint)(tileIndex - tileSet.FirstId);
                //                foreach (var p in tileSet.Tiles[(int)(tileIndex - tileSet.FirstId)].Properties)
                //                {
                //                    tileProperties.Add(p.Key, p.Value);
                //                }
                //                // and break out of the foreach loop
                //                break;
                //            }
                //        }

                //        // now insert the tile into our output
                //        outLayer.Tiles[i] = new GameMapTileContent
                //        {
                //            Texture = textureContent,
                //            SourceRectangle = sourceRect,
                //            SpriteEffects = spriteEffects,
                //            Properties = tileProperties,
                //            TileID = tileid
                //        };
                //    }

                //    // add the layer to our output
                //    output.Layers.Add(outLayer);
                //}
            }

            // return the output object.  because we have ContentSerializerRuntimeType attributes on our
            // objects, we don't need a ContentTypeWriter and can just use the automatic serialization.
            return output;
        }
    }
}